<?php
	class export_controller extends register_controller {
		private function show_form($dates) {
			$first_date = $this->model->first_risk_date();

			$this->view->add_css("webui/jquery-ui.css");
			$this->view->add_javascript("webui/jquery-ui.js");
			$this->view->add_javascript("banshee/datepicker.js");

			$this->view->open_tag("form", array("first" => $first_date));
			$this->view->add_tag("first", $dates["first"]);
			$this->view->add_tag("last", $dates["last"]);
			$this->view->close_tag();
		}

		public function execute() {
			$this->view->add_javascript("export.js");

			if ($_SERVER["REQUEST_METHOD"] == "POST") {
				if ($this->model->dates_okay($_POST)) {
					if ($_POST["submit_button"] == $this->language->module_text("export_risks")) {
						if ($this->model->export_risks($_POST) == false) {
							$this->view->add_message($this->language->module_text("error_export_risks"));
						}
					} else if ($_POST["submit_button"] == $this->language->module_text("export_tags")) {
						if ($this->model->export_tags($_POST) == false) {
							$this->view->add_message($this->language->module_text("error_export_tags"));
						}
					} else if ($_POST["submit_button"] == $this->language->module_text("export_measures")) {
						if ($this->model->export_measures($_POST) == false) {
							$this->view->add_message($this->language->module_text("error_export_measures"));
						}
					} else if ($_POST["submit_button"] == $this->language->module_text("export_issues")) {
						if ($this->model->export_issues($_POST) == false) {
							$this->view->add_message($this->language->module_text("error_export_issues"));
						}
					} else if ($_POST["submit_button"] == $this->language->module_text("export_logs")) {
						if ($this->model->export_logs($_POST) == false) {
							$this->view->add_message($this->language->module_text("error_export_logs"));
						}
					}
				}

				$this->show_form($_POST);
			} else {
				$dates = array(
					"first" => date("Y-m-d", strtotime("-1 month")),
					"last"  => date("Y-m-d"));
				$this->show_form($dates);
			}
		}
	}
?>
