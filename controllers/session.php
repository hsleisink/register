<?php
	/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
	 * This file is part of the Banshee PHP framework
	 * https://www.banshee-php.org/
	 *
	 * Licensed under The MIT License
	 */

	class session_controller extends Banshee\controller {
		private function show_sessions() {
			if (($sessions = $this->model->get_sessions()) === false) {
				$this->view->add_tag("result", $this->language->module_text("error_fetching_information"));
				return;
			}

			$this->view->open_tag("sessions", array("logout" => LOGOUT_MODULE));
			foreach ($sessions as $session) {
				$owner = show_boolean($session["session_id"] == $this->user->session->id);
				$session["expire"] = date_string("d M Y, H:i:s", $session["expire"]);
				$session["bind_to_ip"] = $this->language->global_text(show_boolean($session["bind_to_ip"]));
				$this->view->record($session, "session", array("owner" => $owner));
			}
			$this->view->close_tag();
		}

		private function show_session_form($session) {
			$this->view->open_tag("edit");
			$this->view->record($session, "session");
			$this->view->close_tag();
		}

		public function execute() {
			$this->view->title = "Session manager";

			if ($this->user->logged_in == false) {
				$this->view->add_tag("result", $this->language->module_text("error_not_authenticated"));
				return;
			}

			if ($_SERVER["REQUEST_METHOD"] == "POST") {
				if ($_POST["submit_button"] == $this->language->module_text("btn_update_session")) {
					/* Update session
				 	 */
					if ($this->model->session_okay($_POST) == false) {
						$this->show_session_form($_POST);
					} else if ($this->model->update_session($_POST) == false) {
						$this->view->add_tag("result", $this->language->module_text("error_session_update"));
					} else {
						$this->show_sessions();
					}
				} else if ($_POST["submit_button"] == $this->language->module_text("btn_delete_session")) {
					/* Delete session
					 */
					if ($this->model->delete_session($_POST["id"]) == false) {
						$this->view->add_tag("result", $this->language->module_text("error_session_delete"));
					} else {
						$this->show_sessions();
					}
				} else {
					$this->show_sessions();
				}
			} else if ($this->page->parameter_numeric(0)) {
				/* Edit session
				 */
				if (($session = $this->model->get_session($this->page->parameters[0])) == false) {
					$this->view->add_tag("result", $this->language->module_text("error_session_not_found"));
				} else {
					$this->show_session_form($session);
				}
			} else {
				/* Show overview
				 */
				$this->show_sessions();
			}
		}
	}
?>
