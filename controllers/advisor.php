<?php
	class advisor_controller extends Banshee\controller {
		protected $prevent_repost = false;

		private function overview() {
			if (($roles = $this->model->get_roles()) === false) {
				$this->view->add_tag("result", $this->language->global_text("error_database"));
				return;
			}

			$this->view->add_help_button();

			$this->view->open_tag("overview");
			foreach ($roles as $role) {
				if ($role["organisation_id"] == ($_SESSION["advisor_organisation_id"] ?? null)) {
					$ready = "active";
				} else {
					$ready = show_boolean($role["crypto_key"] != null);
				}

				$attr = array(
					"id"    => $role["id"],
					"ready" => $ready);
				$this->view->add_tag("role", $role["name"], $attr);
			}
			$this->view->close_tag();
		}

		public function execute() {
			if ($_SERVER["REQUEST_METHOD"] == "POST") {
				if ($_POST["submit_button"] == $this->language->module_text("activate")) {
					if ($this->model->activate_role($_POST["id"])) {
						$this->view->add_message($this->language->module_text("role_activated"));
					}
				} else if ($_POST["submit_button"] == $this->language->module_text("deactivate")) {
					$this->model->deactivate_role();
					$this->view->add_message($this->language->module_text("role_deactivated"));
				} else if ($_POST["submit_button"] == $this->language->module_text("remove")) {
					if (($_SESSION["advisor_organisation_id"] ?? null) == $_POST["id"]) {
						$this->model->deactivate_role();
						$this->view->add_system_message($this->language->module_text("role_deactivated"));
						$this->user->log_action("advisor role deactivated");
					}
					if ($this->model->delete_role($_POST["id"])) {
						$this->view->add_system_message("Role removed.");
					}
				} else if ($_POST["submit_button"] == $this->language->module_text("request")) {
					if ($this->model->request_adviser_role($_POST["user"])) {
						$this->view->add_system_message($this->language->module_text("request_sent"));
					}
				}
			}

			$this->overview();
		}
	}
?>
