<?php
	class logs_controller extends register_controller {
		private function show_logs($logs, $pagination = null) {
			$this->view->open_tag("overview");

			$this->view->open_tag("logs");
			foreach ($logs as $log) {
				$log["timestamp"] = date_string("j M Y, H:i:s", $log["timestamp"]);
				$this->view->record($log, "log");
			}
			$this->view->close_tag();

			if ($pagination != null) {
				$pagination->show_browse_links();
			}

			$this->view->close_tag();
		}

		private function show_overview() {
			if (($log_count = $this->model->count_logs()) === false) {
				$this->view->add_tag("result", $this->language->global_text("error_database"));
				return;
			}

			$pagination = new \Banshee\pagination($this->view, "risk_log", $this->settings->admin_page_size, $log_count);

			if ($log_count == 0) {
				$logs = array();
			} else if (($logs = $this->model->get_logs($pagination->offset, $pagination->size)) === false) {
				$this->view->add_tag("result", $this->language->global_text("error_database"));
				return;
			}

			$this->show_logs($logs, $pagination);
		}

		private function show_risk_logs($identifier) {
			if (($logs = $this->model->get_risk_logs($identifier)) === false) {
				$this->view->add_tag("result", $this->language->global_text("error_database"));
				return;
			}

			if (count($logs) == 0) {
				$this->view->add_message($this->language->module_text("error_no_risks_found"));
				$this->show_overview();
			} else {
				$this->show_logs($logs);
			}
		}

		public function execute() {
			if ($_SERVER["REQUEST_METHOD"] == "POST") {
				if (trim ($_POST["identifier"]) == "") {
					$this->show_overview();
				} else {
					$this->show_risk_logs($_POST["identifier"]);
				}
			} else if ($this->page->parameter_numeric(0)) {
				$this->show_risk_logs((int)$this->page->parameters[0]);
			} else {
				$this->show_overview();
			}
		}
	}
?>
