<?php
	/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
	 * This file is part of the Banshee PHP framework
	 * https://gitlab.com/hsleisink/banshee/
	 *
	 * Licensed under The MIT License
	 */

	class issue_calendar_controller extends register_controller {
		private function show_month($month, $year) {
			if (($tasks = $this->model->get_task_deadlines_for_month($month, $year)) === false) {
				$this->view->add_tag("result", $this->language->global_text("error_database"));
				return;
			}

			$day = $this->model->monday_before($month, $year);
			$last_day = $this->model->sunday_after($month, $year);
			$today = strtotime("today 00:00:00");

			$this->view->open_tag("month", array("title" => MONTHS_OF_YEAR[$month - 1]." ".$year));

			/* Links
			 */
			$y = $year;
			if (($m = $month - 1) == 0) {
				$m = 12;
				$y--;
			}
			$this->view->add_tag("prev", $y."/".$m);

			$y = $year;
			if (($m = $month + 1) == 13) {
				$m = 1;
				$y++;
			}
			$this->view->add_tag("next", $y."/".$m);

			/* Days of week
			 */
			$this->view->open_tag("days_of_week");
			foreach (DAYS_OF_WEEK as $dow) {
				if ($this->view->mobile) {
					$dow = substr($dow, 0, 3);
				}
				$this->view->add_tag("day", $dow);
			}
			$this->view->close_tag();

			/* Weeks
			 */
			while ($day < $last_day) {
				$this->view->open_tag("week");
				for ($dow = 1; $dow <= 7; $dow++) {
					$params = array("nr" => date_string("j", $day), "dow" => $dow);
					if ($day == $today) {
						$params["today"] = " today";
					}
					$this->view->open_tag("day", $params);

					foreach ($tasks as $task) {
						if (($task["deadline"] >= $day) && ($task["deadline"] < $day + DAY)) {
							$attr = array(
								"id"   => $task["id"],
								"type" => $task["type"]);
							$this->view->add_tag("task", $task["title"], $attr);
						}
					}
					$this->view->close_tag();

					$day = strtotime(date_string("d-m-Y", $day)." +1 day");
				}
				$this->view->close_tag();
			}
			$this->view->close_tag();
		}

		public function execute() {
			if (isset($_SESSION["calendar_month"]) == false) {
				$_SESSION["calendar_month"] = (int)date_string("m");
				$_SESSION["calendar_year"]  = (int)date("Y");
			}

			if ($this->page->parameter_value(0, "current")) {
				/* Select current month
				 */
				$_SESSION["calendar_month"] = (int)date_string("m");
				$_SESSION["calendar_year"]  = (int)date("Y");
			} else if ($this->page->parameter_numeric(0) && $this->page->parameter_numeric(1)) {
				/* Select specific month
				 */
				$m = (int)$this->page->parameters[1];
				$y = (int)$this->page->parameters[0];

				if (($m >= 1) && ($m <= 12) && ($y > 1902) && ($y <= 2037)) {
					$_SESSION["calendar_month"] = $m;
					$_SESSION["calendar_year"]  = $y;
				}
			}

			$this->show_month($_SESSION["calendar_month"], $_SESSION["calendar_year"]);
		}
	}
?>
