<?php
	class issue_controller extends register_controller {
		/* Issues
		 */
		private function issues_to_view($issues, $pagination = null) {
			$issue_statuses = array_keys(ISSUE_STATUSES);

			$this->view->open_tag("overview", array(
				"calendar" => show_boolean($this->user->access_allowed("measure/calendar")),
				"subpage"  => show_boolean($pagination == null)));

			$this->view->open_tag("issues");
			foreach ($issues as $issue) {
				$issue["created"] = date_string("j M Y", $issue["created"]);
				$issue["status"] = $issue_statuses[$issue["status"]];
				$this->view->record($issue, "issue");
			}
			$this->view->close_tag();

			if ($pagination != null) {
				$pagination->show_browse_links();
			}

			$this->view->close_tag();

			$this->language->replace_module_text("help_overview", "closed", $issue_statuses[ISSUE_STATUS_CLOSED]);
		}

		private function show_overview() {
			if (($issue_count = $this->model->count_issues()) === false) {
				$this->view->add_tag("result", $this->language->global_text("error_database"));
				return;
			}

			$pagination = new \Banshee\pagination($this->view, "issues", $this->settings->risk_page_size, $issue_count);

			if ($issue_count == 0) {
				$issues = array();
			} else if (($issues = $this->model->get_issues($pagination->offset, $pagination->size)) === false) {
				$this->view->add_tag("result", $this->language->global_text("error_database"));
				return;
			}

			$this->issues_to_view($issues, $pagination);
		}

		private function show_issues_attention() {
			if (($issues = $this->model->get_issues_attention()) === false) {
				$this->view->add_tag("result", $this->language->global_text("error_database"));
				return;
			}

			$this->issues_to_view($issues);
		}

		private function show_issue($issue) {
			if (($measure = $this->model->get_measure($issue["measure_id"], $issue["type"])) == false) {
				return false;
			}

			if (($tasks = $this->model->get_tasks($issue["id"], $issue["type"])) === false) {
				return false;
			}

			$issue["created"] = date_string("j F Y, H:i:s", $issue["created"]);

			$this->view->add_javascript("issue.js");

			$edit = $this->model->may_edit_issue($issue["id"], $issue["type"]);

			$this->view->open_tag("view", array("edit" => show_boolean($edit)));

			$this->view->open_tag("statuses");
			foreach (ISSUE_STATUSES as $status => $text) {
				$this->view->add_tag("status", $status);
			}
			$this->view->close_tag();

			$this->view->record($issue, "issue");
			$this->view->record($measure, "measure");

			$today = strtotime(date("Y-m-d 00:00:00"));

			$this->view->open_tag("tasks");
			foreach ($tasks as $task) {
				$task["deadline_passed"] = show_boolean(is_false($task["done"]) && ($task["deadline"] < $today));
				$task["deadline"] = date_string("j F Y", $task["deadline"]);
				$task["done"] = show_boolean($task["done"] ?? false);

				$this->view->record($task, "task");
			}
			$this->view->close_tag();

			$this->view->close_tag();
		}

		private function show_issue_form($issue) {
			if (isset($issue["measure_id"])) {
				$result = $this->model->may_add_issue_to_measure($issue["measure_id"], $issue["type"]);

				if ($result) {
					if (($measure = $this->model->get_measure($issue["measure_id"], $issue["type"])) == false) {
						return false;
					}
				}
			} else if (isset($issue["id"])) {
				$result = $this->model->may_edit_issue($issue["id"], $issue["type"]);
			} else {
				$result = false;
			}

			if ($result == false) {
				$this->view->add_tag("result", $this->language->module_text("error_add_or_edit_not_allowed"));
				return;
			}

			$this->view->open_tag("edit_issue");

			$this->view->record($issue, "issue");

			if (isset($measure)) {
				$this->view->add_tag("measure", $measure["title"]);
			}

			if (isset($issue["id"])) {
				$this->view->open_tag("statuses");
				foreach (ISSUE_STATUSES as $status => $text) {
					$this->view->add_tag("status", $status, array("help" => $text));
				}
				$this->view->close_tag();
			}

			$this->view->close_tag();
		}

		/* Tasks
		 */
		private function show_task_form($task) {
			$this->view->add_css("webui/jquery-ui.css");
			$this->view->add_javascript("webui/jquery-ui.js");
			$this->view->add_javascript("banshee/datepicker.js");

			$this->view->open_tag("edit_task");

			if (($issue = $this->model->get_issue($task["issue_id"], $task["type"])) !== false) {
				$this->view->add_tag("issue", $issue["title"]);
			}

			$task["done"] = show_boolean($task["done"] ?? false);
			$this->view->record($task, "task");

			$this->view->close_tag();
		}

		/* Log issue changes
		 */
		private function log_issue_changes($old, $new) {
			if (($risk_id = $this->model->issue_to_risk($new["id"], $new["type"])) == false) {
				return;
			}

			$event = "issue '%s' updated (id:%d/%s)";
			$args = array($new["title"], $new["id"], $new["type"]);

			$issue_statuses = array_keys(ISSUE_STATUSES);

			if ($old["status"] != $new["status"]) {
				$event .= ", status %s -> %s";
				array_push($args, $issue_statuses[$old["status"]]);
				array_push($args, $issue_statuses[$new["status"]]);
			}

			$this->model->log_risk_event($risk_id, $event, $args);
		}

		/* Log task changes
		 */
		private function log_task_changes($old, $new) {
			if (($risk_id = $this->model->task_to_risk($new["id"], $new["type"])) == false) {
				return;
			}

			$event = "task '%s' updated (id:%d/%s)";
			$args = array($new["title"], $new["id"], $new["type"]);

			if ($old["executor"] != $new["executor"]) {
				$event .= ", executor %s -> %s";
				array_push($args, $old["executor"]);
				array_push($args, $new["executor"]);
			}

			if ($old["deadline"] != $new["deadline"]) {
				$event .= ", deadline %s -> %s";
				array_push($args, $old["deadline"]);
				array_push($args, $new["deadline"]);
			}

			if (is_true($old["done"]) != is_true($new["done"] ?? false)) {
				$event .= ", done %s -> %s";
				array_push($args, show_boolean($old["done"]));
				array_push($args, show_boolean($new["done"] ?? false));
			}

			$this->model->log_risk_event($risk_id, $event, $args);
		}

		/* Main
		 */
		public function execute() {
			if ($_SERVER["REQUEST_METHOD"] == "POST") {
				if ($_POST["submit_button"] == $this->language->module_text("btn_issue_save")) {
					/* Save issue
					 */
					if ($this->model->issue_okay($_POST) == false) {
						$this->show_issue_form($_POST);
					} else if (isset($_POST["id"]) === false) {
						/* Create issue
						 */
						if (($issue_id = $this->model->create_issue($_POST)) === false) {
							$this->view->add_message($this->language->module_text("error_issue_create"));
							$this->show_issue_form($_POST);
						} else {
							$this->user->log_action("issue %d created", $issue_id);

							if (($risk_id = $this->model->issue_to_risk($issue_id, $_POST["type"])) != false) {
								$this->model->log_risk_event($risk_id, "issue '%s' created (id:%d)", $_POST["title"], $issue_id);
							}

							if (($issue = $this->model->get_issue($issue_id, $_POST["type"])) == false) {
								$this->show_overview();
							} else {
								$this->show_issue($issue);
							}
						}
					} else {
						/* Update issue
						 */
						if (($old = $this->model->get_issue($_POST["id"], $_POST["type"])) == false) {
							$this->view->add_tag("result", $this->language->module_text("error_issue_edit_not_allowed"));
							$this->user->log_action("unauthorized issue (%d) update attempt issue for %s measure", $_POST["id"], $_POST["type"]);
						} else if ($this->model->update_issue($_POST) === false) {
							$this->view->add_message($this->language->module_text("error_issue_update"));
							$this->show_issue_form($_POST);
						} else {
							$this->log_issue_changes($old, $_POST);
							$this->user->log_action("issue %d updated", $_POST["id"]);

							if (($issue = $this->model->get_issue($_POST["id"], $_POST["type"])) == false) {
								$this->show_overview();
							} else {
								$this->show_issue($issue);
							}
						}
					}
				} else if ($_POST["submit_button"] == $this->language->module_text("btn_task_save")) {
					/* Save task
					 */
					if ($this->model->task_okay($_POST) == false) {
						$this->show_task_form($_POST);
					} else if (isset($_POST["id"]) === false) {
						/* Create task
						 */
						if (($task_id = $this->model->create_task($_POST)) === false) {
							$this->view->add_message($this->language->module_text("error_task_create"));
							$this->show_task_form($_POST);
						} else {
							$this->user->log_action("task %d created", $task_id);

							if (($risk_id = $this->model->task_to_risk($task_id, $_POST["type"])) != false) {
								$this->model->log_risk_event($risk_id, "task '%s' created (id:%d)", $_POST["title"], $task_id);
							}

							if (($issue = $this->model->get_issue($_POST["issue_id"], $_POST["type"])) == false) {
								$this->show_overview();
							} else {
								$this->show_issue($issue);
							}
						}
					} else {
						/* Update task
						 */
						if (($old = $this->model->get_task($_POST["id"], $_POST["type"])) == false) {
							$this->view->add_tag("result", $this->language->module_text("error_task_edit_not_allowed"));
							$this->user->log_action("unauthorized task (id:%d) update attempt for %s measure", $_POST["id"], $_POST["type"]);
						} else if ($this->model->update_task($_POST) === false) {
							$this->view->add_message($this->language->module_text("error_task_update"));
							$this->show_task_form($_POST);
						} else {
							$this->log_task_changes($old, $_POST);
							$this->user->log_action("task %d updated", $_POST["id"]);

							if (($issue = $this->model->get_issue($_POST["issue_id"], $_POST["type"])) == false) {
								$this->show_overview();
							} else {
								$this->show_issue($issue);
							}
						}
					}
				} else if ($_POST["submit_button"] == $this->language->module_text("btn_issue_delete")) {
					/* Delete issue
					 */
					$risk_id = $this->model->issue_to_risk($_POST["id"], $_POST["type"]);

					if ($this->model->delete_issue($_POST["id"], $_POST["type"]) === false) {
						$this->view->add_message($this->language->module_text("error_issue_delete"));
						$this->show_issue_form($_POST);
					} else {
						$this->user->log_action("issue %d deleted", $_POST["id"]);

						if ($risk_id != false) {
							$this->model->log_risk_event($risk_id, "issue deleted (id:%d)", $_POST["id"]);
						}

						$this->show_overview();
					}
				} else if ($_POST["submit_button"] == $this->language->module_text("btn_task_delete")) {
					/* Delete task
					 */
					$risk_id = $this->model->task_to_risk($_POST["id"], $_POST["type"]);

					if ($this->model->delete_task($_POST["id"], $_POST["type"]) === false) {
						$this->view->add_message($this->language->module_text("error_task_delete"));
						$this->show_task_form($_POST);
					} else {
						$this->user->log_action("task %d deleted", $_POST["id"]);

						if ($risk_id != false) {
							$this->model->log_risk_event($risk_id, "task deleted (id:%d)", $_POST["id"]);
						}

						if (($issue = $this->model->get_issue($_POST["issue_id"], $_POST["type"])) == false) {
							$this->show_overview();
						} else {
							$this->show_issue($issue);
						}
					}
				} else {
					$this->show_overview();
				}
			} else if ($this->page->parameter_value(0, "task")) {
				/* Tasks
				 */
				if ($this->page->parameter_numeric(1) && in_array($this->page->parameters[2] ?? null, $this->model->measure_types)) {
					/* New task
					 */
					$task = array(
						"issue_id" => $this->page->parameters[1],
						"type"     => $this->page->parameters[2],
						"deadline" => date("Y-m-d", strtotime("+1 month")));
					$this->show_task_form($task);
				} else if (in_array($this->page->parameters[1] ?? null, $this->model->measure_types) && $this->page->parameter_numeric(2)) {
					/* Edit task
					 */
					if (($task = $this->model->get_task($this->page->parameters[2], $this->page->parameters[1])) == false) {
 						$this->view->add_tag("result", $this->language->module_text("error_task_not_found"));
						$this->user->log_action("unauthorized %s issue edit attempt (id:%d)", $this->page->parameters[2], $this->page->parameters[1]);
					} else {
						$this->show_task_form($task);
					}
				}
			} else if ($this->page->parameter_numeric(0) && in_array($this->page->parameters[1] ?? null, $this->model->measure_types)) {
				/* New issue
				 */
				$issue = array(
					"measure_id" => $this->page->parameters[0],
					"type"       => $this->page->parameters[1]);
				$this->show_issue_form($issue);
			} else if (in_array($this->page->parameters[0] ?? null, $this->model->measure_types) && $this->page->parameter_numeric(1)) {
				/* View or edit issue
				 */
				if (($issue = $this->model->get_issue($this->page->parameters[1], $this->page->parameters[0])) == false) {
					$this->view->add_tag("result", $this->language->module_text("error_issue_not_found"));
					$this->user->log_action("unauthorized %s issue view/edit attempt (id:%d)", $this->page->parameters[0], $this->page->parameters[1]);
				} else if ($this->page->parameter_value(2, "edit")) {
					$this->show_issue_form($issue);
				} else {
					$this->show_issue($issue);
				}
			} else if ($this->page->parameter_value(0, "attention")) {
				/* Risks requiring attention
				 */
				$this->show_issues_attention();
			} else {
				/* Show overview
				 */
				$this->show_overview();
			}
		}
	}
?>
