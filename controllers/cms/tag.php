<?php
	class cms_tag_controller extends Banshee\controller {
		private function show_overview() {
			if (isset($_SESSION["tag_category"])) {
				if (($tags = $this->model->get_tags($_SESSION["tag_category"])) === false) {
					$this->view->add_tag("result", "Database error.");
					return;
				}
			} else {
				$tags = array();
			}

			$this->view->add_help_button();

			$this->view->open_tag("overview");

			if (($categories = $this->model->get_categories()) != false) {
				$this->view->open_tag("categories", array("current" => $_SESSION["tag_category"]));
				foreach ($categories as $category) {
					$this->view->record($category, "category");
				}
				$this->view->close_tag();
			}

			$this->view->open_tag("tags");
			foreach ($tags as $tag) {
				$this->view->record($tag, "tag");
			}
			$this->view->close_tag();

			$this->view->close_tag();
		}

		private function show_tag_form($tag) {
			if ($this->model->get_categories() == false) {
				$this->view->add_tag("result", "You will be redirected to the Tag category administration page.", array("url" => "cms/tag/category/new"));
				return;
			}

			$this->view->add_help_button();

			$this->view->open_tag("edit");
			$this->view->record($tag, "tag");
			$this->view->close_tag();
		}

		public function execute() {
			if (($categories = $this->model->get_categories()) === false) {
				$this->view->add_tag("result", "Database error.", array("url" => ""));
				return;
			}

			if (isset($_SESSION["tag_category"]) == false) {
				if (count($categories) == 0) {
					$this->view->add_system_message("Add a tag category first.");
					$_SERVER["REQUEST_METHOD"] = "GET";
				} else {
					$_SESSION["tag_category"] = $categories[0]["id"];
				}
			}

			if ($_SERVER["REQUEST_METHOD"] == "POST") {
				if ($_POST["submit_button"] == "Save tag") {
					/* Save tag
					 */
					if ($this->model->save_oke($_POST) == false) {
						$this->show_tag_form($_POST);
					} else if (isset($_POST["id"]) === false) {
						/* Create tag
						 */
						if ($this->model->create_tag($_POST) === false) {
							$this->view->add_message("Error creating tag.");
							$this->show_tag_form($_POST);
						} else {
							$this->user->log_action("tag %d created", $this->db->last_insert_id);
							$this->show_overview();
						}
					} else {
						/* Update tag
						 */
						if ($this->model->update_tag($_POST) === false) {
							$this->view->add_message("Error updating tag.");
							$this->show_tag_form($_POST);
						} else {
							$this->user->log_action("tag %d updated", $_POST["id"]);
							$this->show_overview();
						}
					}
				} else if ($_POST["submit_button"] == "Delete tag") {
					/* Delete tag
					 */
					if ($this->model->delete_oke($_POST) == false) {
						$this->show_tag_form($_POST);
					} else if ($this->model->delete_tag($_POST["id"]) === false) {
						$this->view->add_message("Error deleting tag.");
						$this->show_tag_form($_POST);
					} else {
						$this->user->log_action("tag %d deleted", $_POST["id"]);
						$this->show_overview();
					}
				} else if ($_POST["submit_button"] == "category") {
					/* Set category
					 */
					foreach ($categories as $category) {
						if ($_POST["category"] == $category["id"]) {
							$_SESSION["tag_category"] = $_POST["category"];
							break;
						}
					}
					$this->show_overview();
				} else {
					$this->show_overview();
				}
			} else if (($this->page->parameters[0] ?? null) == "new") {
				/* New tag
				 */
				$tag = array();
				$this->show_tag_form($tag);
			} else if (valid_input($this->page->parameters[0] ?? null, VALIDATE_NUMBERS, VALIDATE_NONEMPTY)) {
				/* Edit tag
				 */
				if (($tag = $this->model->get_tag($this->page->parameters[0])) === false) {
					$this->view->add_tag("result", "Tag not found.");
				} else {
					$this->show_tag_form($tag);
				}
			} else {
				/* Show overview
				 */
				$this->show_overview();
			}
		}
	}
?>
