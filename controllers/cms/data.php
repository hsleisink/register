<?php
	/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
	 * Licensed under the RAFIS license.
	 */

	class cms_data_controller extends Banshee\controller {
		protected $prevent_repost = false;

		private function show_overview() {	
			$this->view->add_help_button();
			$this->view->add_tag("overview");
		}

		private function export_data() {
			if (($export = $this->model->get_export()) == false) {
				$this->view->add_tag("result", "Export error.");
				return false;
			}

			$export["signature"] = $this->model->signature($export);

			if (($organisation = $this->model->get_organisation($this->user->organisation_id)) == false) {
				$organisation = "Backup";
			}

			$this->view->disable();

			$filename = $this->model->generate_filename($organisation)." ".date("Y-m-d").".rrd";
			header("Content-Type: application/x-binary");
			header("Content-Disposition: attachment; filename=\"".$filename."\"");
			print gzencode(json_encode($export));

			return true;
		}

		private function test_import($data) {
			if (substr($data, 0, 2) != "\x1F\x8B") {
				$this->view->add_system_warning("The file does not contain risk register data.");
				return false;
			}

			if (($data = @gzdecode($data)) === false) {
				$this->view->add_system_warning("Error while decompresssing risk register data.");
				return false;
			}

			if (($data = @json_decode($data, true)) === null) {
				$this->view->add_system_warning("Error decoding risk register data.");
				return false;
			}

			$signature = $data["signature"];
			unset($data["signature"]);

			if ($this->settings->validate_import_signature) {
				if ($this->model->signature($data) != $signature) {
					$this->view->add_system_warning("The ILD data signature is invalid.");
					return false;
				}
			}

			return $this->model->test_import($data);
		}

		private function import_data($data) {
			if (substr($data, 0, 2) != "\x1F\x8B") {
				$this->view->add_system_warning("The file does not contain risk register data.");
				return false;
			}

			if (($data = @gzdecode($data)) === false) {
				$this->view->add_system_warning("Error while decompresssing risk register data.");
				return false;
			}

			if (($data = @json_decode($data, true)) === null) {
				$this->view->add_system_warning("Error decoding risk register data.");
				return false;
			}

			$signature = $data["signature"];
			unset($data["signature"]);

			if ($this->settings->validate_import_signature) {
				if ($this->model->signature($data) != $signature) {
					$this->view->add_system_warning("The ILD data signature is invalid.");
					return false;
				}
			}

			return $this->model->import_data($data);
		}

		public function execute() {
			if ($_SERVER["REQUEST_METHOD"] == "POST") {
				if ($_POST["submit_button"] == "Export") {
					$this->export_data();
				} else if ($_POST["submit_button"] == "Test") {
					if ($_FILES["file"]["error"] == 0) {
						$data = file_get_contents($_FILES["file"]["tmp_name"]);
						if ($this->test_import($data) == false) {
							$this->view->add_system_warning("Error while loading file.");
						} else {
							$this->view->add_system_message("Import test done.");
						}
					} else {
						$this->view->add_system_warning("Error while uploading file.");
					}
				} else if ($_POST["submit_button"] == "Import") {
					if ($_FILES["file"]["error"] == 0) {
						$data = file_get_contents($_FILES["file"]["tmp_name"]);
						if ($this->import_data($data) == false) {
							$this->view->add_system_warning("Error while importing file.");
						} else {
							$this->view->add_system_message("File has been imported.");
						}
					} else {
						$this->view->add_system_warning("Error while uploading file.");
					}
				} else if ($_POST["submit_button"] == "Delete") {
					if ($this->model->delete_oke($_POST) == false) {
						$this->view->add_system_warning("Confirm the deleting correctly.");
					} else if ($this->model->reset_data() == false) {
						$this->view->add_system_warning("Error while deleting data.");
					} else {
						$this->view->add_system_warning("All data has been deleted.");
					}
				}

				$this->model->reset_pagination_and_filters();
			}

			$this->show_overview();
		}
	}
?>
