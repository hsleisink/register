<?php
	class cms_control_controller extends Banshee\controller {
		private function show_overview() {
			if (isset($_SESSION["control_standard"])) {
				if (($controls = $this->model->get_controls($_SESSION["control_standard"])) === false) {
					$this->view->add_tag("result", "Database error.");
					return;
				}
			} else {
				$controls = array();
			}

			$this->view->add_help_button();

			$this->view->open_tag("overview");

			if (($standards = $this->model->get_standards()) != false) {
				$this->view->open_tag("standards", array("current" => $_SESSION["control_standard"]));
				foreach ($standards as $standard) {
					$this->view->record($standard, "standard");
				}
				$this->view->close_tag();
			}

			$this->view->open_tag("controls");
			foreach ($controls as $control) {
				$this->view->record($control, "control");
			}
			$this->view->close_tag();

			$this->view->close_tag();
		}

		private function show_control_form($control) {
			if ($this->model->get_standards() == false) {
				$this->view->add_tag("result", "You will be redirected to the Control standard administration page.", array("url" => "cms/control/standard/new"));
				return;
			}

			$this->view->open_tag("edit");
			$this->view->record($control, "control");
			$this->view->close_tag();
		}

		public function execute() {
			if (($standards = $this->model->get_standards()) === false) {
				$this->view->add_tag("result", "Database error.", array("url" => ""));
			}


			if (isset($_SESSION["control_standard"]) == false) {
				if (count($standards) == 0) {
					$this->view->add_system_message("Add a control standard first.");
					$_SERVER["REQUEST_METHOD"] = "GET";
				} else {
					$_SESSION["control_standard"] = $standards[0]["id"];
				}
			}
			
			if ($_SERVER["REQUEST_METHOD"] == "POST") {
				if (($_POST["submit_button"] == "Save control") || ($_POST["submit_button"] == "Save control and new")) {
					/* Save control
					 */
					if ($this->model->save_oke($_POST) == false) {
						$this->show_control_form($_POST);
					} else if (isset($_POST["id"]) === false) {
						/* Create control
						 */
						if ($this->model->create_control($_POST) === false) {
							$this->view->add_message("Error creating control.");
							$this->show_control_form($_POST);
						} else {
							$this->user->log_action("control %d created", $this->db->last_insert_id);
							if ($_POST["submit_button"] == "Save control") {
								$this->show_overview();
							} else {
								$this->show_control_form(array());
							}
						}
					} else {
						/* Update control
						 */
						if ($this->model->update_control($_POST) === false) {
							$this->view->add_message("Error updating control.");
							$this->show_control_form($_POST);
						} else {
							$this->user->log_action("control %d updated", $_POST["id"]);
							if ($_POST["submit_button"] == "Save control") {
								$this->show_overview();
							} else {
								$this->show_control_form(array());
							}
						}
					}
				} else if ($_POST["submit_button"] == "Delete control") {
					/* Delete control
					 */
					if ($this->model->delete_oke($_POST) == false) {
						$this->show_control_form($_POST);
					} else if ($this->model->delete_control($_POST["id"]) === false) {
						$this->view->add_message("Error deleting control.");
						$this->show_control_form($_POST);
					} else {
						$this->user->log_action("control %d deleted", $_POST["id"]);
						$this->show_overview();
					}
				} else if ($_POST["submit_button"] == "standard") {
					/* Set standard
					 */
					foreach ($standards as $standard) {
						if ($_POST["standard"] == $standard["id"]) {
							$_SESSION["control_standard"] = $_POST["standard"];
							break;
						}
					}
					$this->show_overview();
				} else {
					$this->show_overview();
				}
			} else if (($this->page->parameters[0] ?? null) == "new") {
				/* New control
				 */
				$control = array();
				$this->show_control_form($control);
			} else if (valid_input($this->page->parameters[0] ?? null, VALIDATE_NUMBERS, VALIDATE_NONEMPTY)) {
				/* Edit control
				 */
				if (($control = $this->model->get_control($this->page->parameters[0])) === false) {
					$this->view->add_tag("result", "Control not found.");
				} else {
					$this->show_control_form($control);
				}
			} else {
				/* Show overview
				 */
				$this->show_overview();
			}
		}
	}
?>
