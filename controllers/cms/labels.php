<?php
	class cms_labels_controller extends Banshee\controller {
		private function show_form($data) {
			$this->view->add_css("banshee/colorpicker.css");
			$this->view->add_javascript("banshee/jquery.colorpicker.js");
			$this->view->add_javascript("cms/labels.js");
			$this->view->add_help_button();

			$this->view->open_tag("form");

			$this->view->add_tag("impact_axis", $this->model->impact_axis);

			$this->view->open_tag("horizontal", array("label" => $this->model->matrix_horizontal));
			foreach ($data["horizontal"] as $i => $label) {
				$this->view->add_tag("label".($i + 1), $label);
			}
			$this->view->close_tag();

			$this->view->open_tag("vertical", array("label" => $this->model->matrix_vertical));
			foreach ($data["vertical"] as $i => $label) {
				$this->view->add_tag("label".($i + 1), $label);
			}
			$this->view->close_tag();

			$this->view->open_tag("level");
			foreach ($data["level"]["label"] as $i => $label) {
				$this->view->add_tag("label".($i  + 1) , $label);
			}
			foreach ($data["level"]["color"] as $i => $label) {
				$this->view->add_tag("color".($i  + 1) , $label);
			}
			$this->view->close_tag();

			$this->view->close_tag();
		}

		public function execute() {
			if ($_SERVER["REQUEST_METHOD"] == "POST") {
				if ($this->model->labels_okay($_POST) == false) {
					$this->show_form($_POST);
				} else if ($this->model->save_labels($_POST) == false) {
					$this->view->add_message("Error while saving labels.");
					$this->show_form($_POST);
				} else {
					$this->view->add_tag("result", "Labels have been saved.");
				}
			} else {
				if (($labels_horizontal = $this->model->get_horizontal_labels()) == false) {
					$this->view->add_tag("result", "Database error.");
					return false;
				}

				if (($labels_vertical = $this->model->get_vertical_labels()) == false) {
					$this->view->add_tag("result", "Database error.2");
					return false;
				}

				if (($level_labels = $this->model->get_level_labels()) == false) {
					$this->view->add_tag("result", "Database error.3");
					return false;
				}

				$data = array(
					"horizontal" => $labels_horizontal,
					"vertical"   => $labels_vertical,
					"level"      => $level_labels);

				$this->show_form($data);
			}
		}
	}
?>
