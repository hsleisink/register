<?php
	class cms_matrix_controller extends register_controller {
		private function show_form($matrix) {
			if (($labels_horizontal = $this->model->get_horizontal_labels()) == false) {
				$this->view->add_tag("result", "Database error.", array("url" => "cms"));
				return false;
			}

			if (($labels_vertical = $this->model->get_vertical_labels()) == false) {
				$this->view->add_tag("result", "Database error.", array("url" => "cms"));
				return false;
			}
			$labels_vertical = array_reverse($labels_vertical);

			if (($level_labels = $this->model->get_level_labels()) == false) {
				$this->view->add_tag("result", "Database error.", array("url" => "cms"));
				return false;
			}

			$this->view->add_javascript("includes/risk_level.js");
			$this->view->add_javascript("cms/matrix.js");

			$this->view->open_tag("form");

			$this->view->open_tag("labels");

			$this->show_matrix_labels("horizontal", $this->model->matrix_horizontal, $labels_horizontal);
			$this->show_matrix_labels("vertical", $this->model->matrix_vertical, $labels_vertical);
			$this->show_level_labels($level_labels);

			$this->view->close_tag();

			$this->view->open_tag("matrix", array("size" => $this->model->matrix_size));
			foreach ($matrix["matrix"] as $line) {
				$this->view->open_tag("row");
				foreach ($line as $value) {
					$this->view->add_tag("cell", $value);
				}
				$this->view->close_tag();
			}
			$this->view->close_tag();

			$this->view->close_tag();
		}

		public function execute() {
			if ($_SERVER["REQUEST_METHOD"] == "POST") {
				if ($this->model->matrix_okay($_POST) == false) {
					$this->show_form($_POST);
				} else if ($this->model->save_matrix($_POST) == false) {
					$this->view->add_message("Error while saving matrix.");
					$this->show_form($_POST);
				} else {
					$this->view->add_tag("result", "Matrix have been saved.");
				}
			} else {
				if (($matrix = $this->model->get_matrix()) == false) {
					$this->view->add_tag("result", "Database error.", array("url" => "cms"));
					return false;
				}

				$data = array("matrix" => $matrix);

				$this->show_form($data);
			}
		}
	}
?>
