<?php
	class cms_control_standard_controller extends Banshee\controller {
		private function show_overview() {
			if (($standards = $this->model->get_standards()) === false) {
				$this->view->add_tag("result", "Database error.");
				return;
			}

			$this->view->add_help_button();

			$this->view->open_tag("overview");

			$this->view->open_tag("standards");
			foreach ($standards as $standard) {
				$this->view->record($standard, "standard");
			}
			$this->view->close_tag();

			$this->view->close_tag();
		}

		private function show_standard_form($standard) {
			$this->view->open_tag("edit");
			$this->view->record($standard, "standard");
			$this->view->close_tag();
		}

		public function execute() {
			if ($_SERVER["REQUEST_METHOD"] == "POST") {
				if ($_POST["submit_button"] == "Save standard") {
					/* Save standard
					 */
					if ($this->model->save_oke($_POST) == false) {
						$this->show_standard_form($_POST);
					} else if (isset($_POST["id"]) === false) {
						/* Create standard
						 */
						if ($this->model->create_standard($_POST) === false) {
							$this->view->add_message("Error creating standard.");
							$this->show_standard_form($_POST);
						} else {
							$_SESSION["control_standard"] = $this->db->last_insert_id;
							$this->user->log_action("standard %d created", $this->db->last_insert_id);
							$this->show_overview();
						}
					} else {
						/* Update standard
						 */
						if ($this->model->update_standard($_POST) === false) {
							$this->view->add_message("Error updating standard.");
							$this->show_standard_form($_POST);
						} else {
							$this->user->log_action("standard %d updated", $_POST["id"]);
							$this->show_overview();
						}
					}
				} else if ($_POST["submit_button"] == "Delete standard") {
					/* Delete standard
					 */
					if ($this->model->delete_oke($_POST) == false) {
						$this->show_standard_form($_POST);
					} else if ($this->model->delete_standard($_POST["id"]) === false) {
						$this->view->add_message("Error deleting standard.");
						$this->show_standard_form($_POST);
					} else {
						unset($_SESSION["control_standard"]);
						$this->user->log_action("standard %d deleted", $_POST["id"]);
						$this->show_overview();
					}
				} else {
					$this->show_overview();
				}
			} else if (($this->page->parameters[0] ?? null) == "new") {
				/* New standard
				 */
				$standard = array();
				$this->show_standard_form($standard);
			} else if (valid_input($this->page->parameters[0] ?? null, VALIDATE_NUMBERS, VALIDATE_NONEMPTY)) {
				/* Edit standard
				 */
				if (($standard = $this->model->get_standard($this->page->parameters[0])) === false) {
					$this->view->add_tag("result", "Category not found.");
				} else {
					$this->show_standard_form($standard);
				}
			} else {
				/* Show overview
				 */
				$this->show_overview();
			}
		}
	}
?>
