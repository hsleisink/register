<?php
	class cms_cause_controller extends Banshee\controller {
		private function show_overview() {
			if (($causes = $this->model->get_causes()) === false) {
				$this->view->add_tag("result", "Database error.");
				return;
			}

			$this->view->open_tag("overview");

			$this->view->open_tag("causes");
			foreach ($causes as $cause) {
				$this->view->record($cause, "cause");
			}
			$this->view->close_tag();

			$this->view->close_tag();
		}

		private function show_cause_form($cause) {
			$this->view->open_tag("edit", array("maxlength" => MAXLENGTH_BOWTIE_TITLE));
			$this->view->record($cause, "cause");
			$this->view->close_tag();
		}

		public function execute() {
			$this->view->add_help_button();

			if ($_SERVER["REQUEST_METHOD"] == "POST") {
				if ($_POST["submit_button"] == "Save risk cause") {
					/* Save cause 
					 */
					if ($this->model->save_okay($_POST) == false) {
						$this->show_cause_form($_POST);
					} else if (isset($_POST["id"]) === false) {
						/* Create cause 
						 */
						if ($this->model->create_cause($_POST) === false) {
							$this->view->add_message("Error creating risk cause");
							$this->show_cause_form($_POST);
						} else {
							$this->user->log_action("risk cause %d created", $this->db->last_insert_id);
							$this->show_overview();
						}
					} else {
						/* Update cause
						 */
						if ($this->model->update_cause($_POST) === false) {
							$this->view->add_message("Error updating risk cause.");
							$this->show_cause_form($_POST);
						} else {
							$this->user->log_action("risk cause %d updated", $_POST["id"]);
							$this->show_overview();
						}
					}
				} else if ($_POST["submit_button"] == "Delete risk cause") {
					/* Delete cause
					 */
					if ($this->model->delete_okay($_POST) == false) {
						$this->show_cause_form($_POST);
					} else if ($this->model->delete_cause($_POST["id"]) === false) {
						$this->view->add_message("Error deleting risk cause.");
						$this->show_cause_form($_POST);
					} else {
						$this->user->log_action("risk cause %d deleted", $_POST["id"]);
						$this->show_overview();
					}
				} else {
					$this->show_overview();
				}
			} else if ($this->page->parameter_value(0, "new")) {
				/* New cause
				 */
				$cause = array();
				$this->show_cause_form($cause);
			} else if ($this->page->parameter_numeric(0)) {
				/* Edit cause
				 */
				if (($cause = $this->model->get_cause($this->page->parameters[0])) == false) {
					$this->view->add_tag("result", "Risk cause not found.");
				} else {
					$this->show_cause_form($cause);
				}
			} else {
				/* Show overview
				 */
				$this->show_overview();
			}
		}
	}
?>
