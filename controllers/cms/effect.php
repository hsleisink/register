<?php
	class cms_effect_controller extends Banshee\controller {
		protected $effect = "Risk effect";

		private function show_overview() {
			if (($effects = $this->model->get_effects()) === false) {
				$this->view->add_tag("result", "Database error.");
				return;
			}

			$this->view->open_tag("overview");

			$this->view->open_tag("effects");
			foreach ($effects as $effect) {
				$this->view->record($effect, "effect");
			}
			$this->view->close_tag();

			$this->view->close_tag();
		}

		private function show_effect_form($effect) {
			$impact_labels = $this->model->get_impact_labels();

			$this->view->open_tag("edit", array("maxlength" => MAXLENGTH_BOWTIE_TITLE));

			$this->view->open_tag("impact");
			foreach ($impact_labels as $label) {
				$this->view->add_tag("level", $label);
			}
			$this->view->close_tag();

			$this->view->record($effect, "effect");

			$this->view->close_tag();
		}

		public function execute() {
			$this->view->add_help_button();

			if ($_SERVER["REQUEST_METHOD"] == "POST") {
				if ($_POST["submit_button"] == "Save risk effect") {
					/* Save effect
					 */
					if ($this->model->save_okay($_POST) == false) {
						$this->show_effect_form($_POST);
					} else if (isset($_POST["id"]) === false) {
						/* Create effect
						 */
						if ($this->model->create_effect($_POST) === false) {
							$this->view->add_message("Error creating risk effect.");
							$this->show_effect_form($_POST);
						} else {
							$this->user->log_action("risk effect %d created", $this->db->last_insert_id);
							$this->show_overview();
						}
					} else {
						/* Update effect
						 */
						if ($this->model->update_effect($_POST) === false) {
							$this->view->add_message("Error updating risk effect.");
							$this->show_effect_form($_POST);
						} else {
							$this->user->log_action("risk effect %d updated", $_POST["id"]);
							$this->show_overview();
						}
					}
				} else if ($_POST["submit_button"] == "Delete risk effect") {
					/* Delete effect
					 */
					if ($this->model->delete_okay($_POST) == false) {
						$this->show_effect_form($_POST);
					} else if ($this->model->delete_effect($_POST["id"]) === false) {
						$this->view->add_message("Error deleting risk effect.");
						$this->show_effect_form($_POST);
					} else {
						$this->user->log_action("risk effect %d deleted", $_POST["id"]);
						$this->show_overview();
					}
				} else {
					$this->show_overview();
				}
			} else if ($this->page->parameter_value(0, "new")) {
				/* New effect
				 */
				$effect = array();
				for ($i = 1; $i <= $this->model->matrix_size; $i++) {
					$effect["impact".$i] = "";
				}
				$this->show_effect_form($effect);
			} else if ($this->page->parameter_numeric(0)) {
				/* Edit effect
				 */
				if (($effect = $this->model->get_effect($this->page->parameters[0])) == false) {
					$this->view->add_tag("result", "Risk effect not found.");
				} else {
					$this->show_effect_form($effect);
				}
			} else {
				/* Show overview
				 */
				$this->show_overview();
			}
		}
	}
?>
