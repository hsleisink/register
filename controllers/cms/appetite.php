<?php
	class cms_appetite_controller extends register_controller {
		private function show_form($data) {
			if (($level_labels = $this->model->get_level_labels()) == false) {
				$this->view->add_tag("result", "Database error.");
				return false;
			}

			$this->view->add_javascript("includes/risk_level.js");
			$this->view->add_javascript("cms/appetite.js");

			$this->view->open_tag("form");

			$this->show_level_labels($level_labels);

			$this->view->add_tag("appetite", $data["risk_appetite"]);

			$this->view->close_tag();
		}

		public function execute() {
			if ($_SERVER["REQUEST_METHOD"] == "POST") {
				if ($this->model->appetite_okay($_POST) == false) {
					$this->show_form($_POST);
				} else if ($this->model->save_appetite($_POST) == false) {
					$this->show_form($_POST);
				} else {
					$this->view->add_tag("result", "Risk appetite saved.");
				}
			} else {
				$appetite = array("risk_appetite" => $this->model->risk_appetite);
				$this->show_form($appetite);
			}
		}
	}
?>
