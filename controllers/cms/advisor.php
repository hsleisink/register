<?php
	class cms_advisor_controller extends Banshee\controller {
		protected $prevent_repost = false;

		private function overview() {
			if (($advisors = $this->model->get_advisors()) === false) {
				$this->view->add_tag("result", "Database error.");
				return false;
			}

			$this->view->add_help_button();

			$this->view->open_tag("overview");
			foreach ($advisors as $advisor) {
				$attr = array(
					"id"	=> $advisor["id"],
					"ready" => show_boolean($advisor["crypto_key"] != null));
				unset($advisor["crypto_key"]);
				$this->view->record($advisor, "advisor", $attr);
			}
			$this->view->close_tag();
		}

		public function execute() {
			if ($_SERVER["REQUEST_METHOD"] == "POST") {
				if ($_POST["submit_button"] == "Add advisor") {
					if ($this->model->add_advisor($_POST["user"])) {
						$this->view->add_system_message("Advisor added.");
					}
				} else if ($_POST["submit_button"] == "Accept request") {
					if ($this->model->activate_advisor($_POST["id"])) {
						$this->view->add_system_message("Advisor activated.");
					}
				} else if ($_POST["submit_button"] == "Delete") {
					if ($this->model->delete_advisor($_POST["id"])) {
						$this->view->add_system_message("Advisor deleted.");
					}
				}
			}

			$this->overview();
		}
	}
?>
