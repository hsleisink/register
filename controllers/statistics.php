<?php
	class statistics_controller extends register_controller {
		protected $prevent_repost = false;

		public function show_statistics($first_date, $last_date) {
			$first = $first_date." 00:00:00";
			$last = $last_date." 23:59:59";

			if (($risk_count = $this->model->count_risks($first, $last)) === false) {
				$this->view->add_tag("result", $this->language->global_text("error_database"));
				return false;
			}

			if (($measures = $this->model->measure_count($first, $last)) === false) {
				$this->view->add_tag("result", $this->language->global_text("error_database"));
				return false;
			}

			if (($causes = $this->model->cause_count($first, $last)) === false) {
				$this->view->add_tag("result", $this->language->global_text("error_database"));
				return false;
			}

			if (($effects = $this->model->effect_count($first, $last)) === false) {
				$this->view->add_tag("result", $this->language->global_text("error_database"));
				return false;
			}

			if (($categories = $this->model->tag_count($first, $last)) === false) {
				$this->view->add_tag("result", $this->language->global_text("error_database"));
				return false;
			}

			$this->view->add_css("webui/jquery-ui.css");
			$this->view->add_javascript("webui/jquery-ui.js");
			$this->view->add_javascript("banshee/datepicker.js");

			$this->view->add_javascript("statistics.js");

			$this->view->open_tag("statistics", array(
				"first" => $first_date,
				"last"  => $last_date));

			$this->view->add_tag("risks", $risk_count);

			$this->view->open_tag("measures");
			foreach ($measures as $type => $count) {
				$this->view->add_tag("measure", $count, array("type" => $type));
			}
            $this->view->close_tag();

			/* Causes
			 */
			$this->view->open_tag("causes");
			foreach ($causes as $cause) {
				$this->view->record($cause, "cause");
			}
			$this->view->close_tag();

			/* Effects
			 */
			$this->view->open_tag("effects");
			foreach ($effects as $effect) {
				$this->view->record($effect, "effect");
			}
			$this->view->close_tag();

			/* Tags
			 */
			$this->view->open_tag("categories");
			foreach ($categories as $category => $tags) {
				if (count($tags) == 0) {
					continue;
				}

				$this->view->open_tag("category", array("title" => $category));
				foreach ($tags as $id => $tag) {
					$tag["percentage"] = ($risk_count > 0) ? round(100 * $tag["count"] / $risk_count) : 0;
					$this->view->record($tag, "tag");
				}
				$this->view->close_tag();
			}
			$this->view->close_tag();

			$this->view->close_tag();
		}

		private function show_causes($cause_id, $first, $last) {
			if (($cause = $this->model->get_cause($cause_id)) == false) {
				$this->view->add_tag("result", $this->language->module_text("error_cause_not_found"));
				return false;
			}

			if (($causes = $this->model->get_causes($cause_id, $first, $last)) === false) {
				return false;
			}

			$this->view->open_tag("list", array("type" => "Cause", "title" => $cause));
			foreach ($causes as $cause) {
				$this->view->record($cause, "item");
			}
			$this->view->close_tag();
		}

		private function show_effects($effect_id, $first, $last) {
			if (($effect = $this->model->get_effect($effect_id)) == false) {
				$this->view->add_tag("result", $this->language->module_text("error_effect_not_found"));
				return false;
			}

			if (($effects = $this->model->get_effects($effect_id, $first, $last)) === false) {
				return false;
			}

			$this->view->open_tag("list", array("type" => "Effect", "title" => $effect));
			foreach ($effects as $effect) {
				$this->view->record($effect, "item");
			}
			$this->view->close_tag();
		}

		public function execute() {
			if (isset($_SESSION["statistics_filter"]) == false) {
				$_SESSION["statistics_filter"] = array(
					"first" => date("Y-m-d", strtotime("-1 year")),
					"last"  => date("Y-m-d"));
			}

			if ($_SERVER["REQUEST_METHOD"] == "POST") {
				if ($this->model->dates_okay($_POST)) {
					$_SESSION["statistics_filter"] = array(
						"first" => $_POST["first"],
						"last"  => $_POST["last"]);
				}
			}

			list($first, $last) = array_values($_SESSION["statistics_filter"]);

			if ($this->page->parameter_value(0, "cause") && $this->page->parameter_numeric(1)) {
				$this->show_causes($this->page->parameters[1], $first, $last);
			} else if ($this->page->parameter_value(0, "effect") && $this->page->parameter_numeric(1)) {
				$this->show_effects($this->page->parameters[1], $first, $last);
			} else {
				$this->show_statistics($first, $last);
			}
		}
	}
?>
