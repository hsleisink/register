<?php
	class control_controller extends register_controller {
		private function show_standards() {
			if (($standards = $this->model->get_standards()) == false) {
				return false;
			}

			if (isset($_SESSION["control_standard"]) == false) {
				if (($standard_id = $this->model->get_most_used_standard()) == false) {
					$standard_id = $standards[0]["id"];
				}

				$_SESSION["control_standard"] = $standard_id;
			}

			$this->view->open_tag("standards", array("selected" => $_SESSION["control_standard"]));

			foreach ($standards as $standard) {
				$this->view->add_tag("standard", $standard["name"], array("id" => $standard["id"]));
			}

			$this->view->close_tag();

			return true;
		}

		private function show_controls($standard_id) {
			if (($controls = $this->model->get_controls($standard_id)) == false) {
				return false;
			}

			if (($measures = $this->model->get_control_measures()) === false) {
				return false;
			}

			$measure_statuses = array_keys(MEASURE_STATUSES);

			$this->view->add_javascript("control.js");

			$this->view->open_tag("controls", array("effective" => $measure_statuses[MEASURE_STATUS_EFFECTIVE]));

			foreach ($controls as $control) {
				$this->view->open_tag("control");

				$this->view->record($control);

				if (isset($measures[$control["id"]])) {
					$this->view->open_tag("measures");
					foreach ($measures[$control["id"]] as $measure) {
						$measure["effective"] = show_boolean($measure["status"] == MEASURE_STATUS_EFFECTIVE);
						$measure["status"] = $measure_statuses[$measure["status"]];
						$measure["measure_type"] = $this->language->global_text($measure["type"]);
						$this->view->record($measure, "measure");
					}
					$this->view->close_tag();
				}

				$this->view->close_tag();
			}

			$this->view->close_tag();
		}

		public function execute() {
			if ($_SERVER["REQUEST_METHOD"] == "POST") {
				if (isset($_POST["standard_id"])) {
					$_SESSION["control_standard"] = $_POST["standard_id"];
				}
			}

			if ($this->show_standards() == false) {
				return;
			}

			$this->show_controls($_SESSION["control_standard"]);

			$measure_statuses = array_keys(MEASURE_STATUSES);
			$this->language->replace_module_text("help_text", "effective", $measure_statuses[MEASURE_STATUS_EFFECTIVE]);
		}
	}
?>
