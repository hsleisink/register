<?php
	/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
	 * This file is part of the Banshee PHP framework
	 * https://www.banshee-php.org/
	 *
	 * Licensed under The MIT License
	 */

	class register_controller extends Banshee\splitform_controller {
		protected $button_submit = "Register";
		protected $back_page = "/";
		protected $ask_organisation = null;

		protected function prepare_matrix($data) {
			$this->view->open_tag("sizes");
			for ($size = MATRIX_MIN_SIZE; $size <= MATRIX_MAX_SIZE; $size++) {
				$this->view->add_tag("size", $size);
			}
			$this->view->close_tag();
		}

		protected function prepare_code($data) {
			if (($_SESSION["register_email"] ?? null) == $data["email"]) {
				return;
			}

			$_SESSION["register_code"] = random_string(20);

			$email = new register_email($this->language->module_text("verifcation_code_for")." ".$this->settings->head_title, $this->settings->webmaster_email);
			$email->set_message_fields(array(
				"CODE"    => $_SESSION["register_code"],
				"WEBSITE" => $this->settings->head_title));
			$email->message(file_get_contents("../extra/register_".$this->view->language.".txt"));
			$email->send($data["email"]);

			$_SESSION["register_email"] = $data["email"];
			$this->model->set_value("code", "");
		}

		protected function prepare_account($data) {
			if (is_true(ENCRYPT_DATA)) {
				$this->view->run_javascript("add_delay_warning()");
			}

			if ($this->ask_organisation) {
				$this->ask_organisation = (($data["invitation"] ?? null) == "");
			}

			if (empty($data["username"])) {
				$this->model->set_value("username", strtolower($data["email"]));
			}

			$this->view->add_tag("ask_organisation", show_boolean($this->ask_organisation));
		}

		protected function prepare_authenticator($data) {
			$this->view->add_help_button();

			if (is_true(ENCRYPT_DATA)) {
				$this->view->run_javascript("add_delay_warning()");
			}
		}

		private function show_totp() {
			if ($this->page->parameter_value(1) == false) {
				return;
			}

			$username = $_SESSION["splitform"][$this->page->module]["values"]["username"];

			$totp = new \Banshee\TOTP($this->view, $this->settings);
			$totp->add_to_view($username, $this->page->parameters[1]);
		}

		public function execute() {
			if ($this->user->logged_in) {
				$this->view->add_tag("result", $this->language->module_text("error_already_have_account"), array("url" => ""));
				return;
			}

			if ($this->page->parameter_value(0, "totp")) {
				$this->show_totp();
				return;
			} 

			if ($this->page->ajax_request) {
				$authenticator = new \Banshee\authenticator;
				$this->view->add_tag("secret", $authenticator->create_secret());
				return;
			}

			$this->button_submit = $this->language->module_text("btn_submit");

			$this->view->add_javascript("register.js");

			if ($_SERVER["REQUEST_METHOD"] == "GET") {
				$this->model->reset_form_progress();
			} else if (($_POST["splitform_current"] ?? null) == 2) {
				$_POST["username"] = strtolower($_POST["username"] ?? "");
			}

			$this->ask_organisation = (DEFAULT_ORGANISATION_ID == 0);

			parent::execute();
		}
	}
?>
