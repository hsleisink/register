<?php
	class risk_controller extends register_controller {
		/* Risks to view
		 */
		private function risks_to_view($risks, $pagination = null) {
			if (($matrix = $this->model->get_matrix()) == false) {
				$this->view->add_tag("result", $this->language->global_text("error_database"));
				return false;
			}

			if (($level_labels = $this->model->get_level_labels()) == false) {
				$this->view->add_tag("result", $this->language->global_text("error_database"));
				return false;
			}

			$approach_labels = array_keys(RISK_APPROACHES);

			if (count($_SESSION["risk_filter"]) != 0) {
				$this->view->add_system_message($this->language->module_text("filter_active"));
			}

			$employee = in_array($this->settings->role_id_employee, $this->user->role_ids);
			$measure_statuses = array_keys(MEASURE_STATUSES);

			$this->view->open_tag("overview", array(
				"matrix"           => show_boolean($this->user->access_allowed("risk/matrix")),
				"subpage"          => show_boolean($pagination == null),
				"employee"         => show_boolean($employee)));

			$this->show_level_labels($level_labels);

			$this->view->open_tag("risks");
			foreach ($risks as $risk) {
				$created_today = (date("Y-m-d") == date("Y-m-d", $risk["created"]));
				$risk["owner_warning"] = show_boolean(($risk["owner"] == "") && ($created_today == false));
				$risk["level"] = $this->model->level_from_matrix($matrix, $risk["horizontal"], $risk["vertical"]);
				$risk["mitigated"] = max($risk["level"] - $risk["levels_mitigated"], 1);
				$risk["approach_text"] = $approach_labels[$risk["approach"]];
				$risk["created"] = date_string("j M Y", $risk["created"]);

				$this->view->record($risk, "risk");
			}
			$this->view->close_tag();

			if ($pagination != null) {
				$pagination->show_browse_links();
			}

			$this->view->close_tag();

			$risk_approaches = array_keys(RISK_APPROACHES);
			$this->language->replace_module_text("help_overview_criteria", "decide_later", $risk_approaches[RISK_APPROACH_DECIDE_LATER]);

			$measures_statuses = array_keys(MEASURE_STATUSES);
			$this->language->replace_module_text("help_overview_criteria", "effective", $measure_statuses[MEASURE_STATUS_EFFECTIVE]);
		}

		/* Show overview
		 */
		private function show_overview() {
			if (($risk_count = $this->model->count_risks()) === false) {
				$this->view->add_tag("result", $this->language->global_text("error_database"));
				return;
			}

			$pagination = new \Banshee\pagination($this->view, "risks", $this->settings->risk_page_size, $risk_count);
			if (($_POST["submit_button"] ?? null) == $this->language->module_text("filter_set_filter")) {
				$pagination->reset();
			}

			if ($risk_count == 0) {
				$risks = array();
			} else if (($risks = $this->model->get_risks($pagination->offset, $pagination->size)) === false) {
				$this->view->add_tag("result", $this->language->global_text("error_database"));
				return;
			}

			foreach ($risks as $i => $risk) {
				if ($risk["acceptor_id"] == $this->user->id) {
					$not_set = ($risk["accept_end_date"] == DATE_NOT_SET);
					$passed = $not_set ? false : (strtotime($risk["accept_end_date"]) <= time());

					$risks[$i]["attention"] = show_boolean($not_set || $passed);
				}
			}

			$key = is_true(ENCRYPT_DATA) ? "identifier" : "search_query";
			$this->view->add_tag("search_placeholder", $this->language->module_text($key));

			$this->risks_to_view($risks, $pagination);
		}

		/* Show risks requiring attention
		 */
		private function show_risks_attention() {
			if (($risks = $this->model->get_risks_attention()) === false) {
				$this->view->add_tag("result", $this->language->global_text("error_database"));
				return;
			}

			$this->risks_to_view($risks);
		}

		/* Show risk
		 */
		private function show_risk($risk_id) {
			if (($risk = $this->model->get_risk($risk_id)) == false) {
				$this->view->add_tag("result", $this->language->module_text("error_risk_not_found"));
				$this->user->log_action("unauthorized risk view attempt %d", $risk_id);
				return false;
			}

			$viewers = array($risk["creator_id"], $risk["owner_id"], $risk["acceptor_id"]);
			$viewable = in_array($this->user->id, $viewers);
			$editable = $this->model->may_edit_risk($risk["id"]);
			$risk_manager = in_array($this->settings->role_id_risk_manager, $this->user->role_ids);
			$risk_viewer = in_array($this->settings->role_id_risk_viewer, $this->user->role_ids);

			if (($viewable == false) && ($risk_manager == false) && ($risk_viewer == false)) {
				$this->view->add_tag("result", $this->language->module_text("error_view_not_allowed"));
				$this->user->log_action("unauthorized risk view attempt %d", $risk["id"]);
				return false;
			}

			if (($horizontal_labels = $this->model->get_horizontal_labels()) == false) {
				$this->view->add_tag("result", $this->language->global_text("error_database"));
				return false;
			}

			if (($vertical_labels = $this->model->get_vertical_labels()) == false) {
				$this->view->add_tag("result", $this->language->global_text("error_database"));
				return false;
			}

			if (($level_labels = $this->model->get_level_labels()) == false) {
				$this->view->add_tag("result", $this->language->global_text("error_database"));
				return false;
			}

			if (($matrix = $this->model->get_matrix()) == false) {
				$this->view->add_tag("result", $this->language->global_text("error_database"));
				return false;
			}

			if (($causes = $this->model->get_risk_causes($risk_id)) === false) {
				$this->view->add_tag("result", $this->language->global_text("error_database"));
				return false;
			}

			if (($effects = $this->model->get_risk_effects($risk_id)) === false) {
				$this->view->add_tag("result", $this->language->global_text("error_database"));
				return false;
			}

			if (($tag_categories = $this->model->get_tags()) === false) {
				$this->view->add_tag("result", $this->language->global_text("error_database"));
				return false;
			}

			if (($risk_tags = $this->model->get_risk_tags($risk_id)) === false) {
				$this->view->add_tag("result", $this->language->global_text("error_database"));
			}

			$this->view->title = $risk["identifier"];

			$this->view->add_javascript("includes/risk_level.js");
			$this->view->add_javascript("risk_view.js");

			$risk["created"] = date_string("j F Y, H:i:s", $risk["created"]);
			$risk["level"] = $this->model->level_from_matrix($matrix, $risk["horizontal"], $risk["vertical"]);
			$risk["mitigated"] = max($risk["level"] - $risk["levels_mitigated"], 1);

			if ($risk["owner"] == "") {
				$risk["owner"] = "-";
			}
			$approach_labels = array_keys(RISK_APPROACHES);
			$risk["approach_text"] = $approach_labels[$risk["approach"]];

			$risk["accept_end_date_passed"] = show_boolean($this->model->accept_end_date_passed($risk["accept_end_date"]));

			if ($risk["acceptor_id"] != null) {
				if ($risk["acceptor_id"] == $this->user->id) {
					$risk["accept_procedure"] = $risk["accept_end_date_passed"];
				}

				if ($risk["accept_end_date"] == DATE_NOT_SET) {
					$risk["accept_end_date"] = "not accepted yet";
				} else {
					$risk["accept_end_date"] = date_string("j F Y", strtotime($risk["accept_end_date"]));
				}
			}

			$access_to_measures = $this->user->access_allowed("risk/measure");

			$this->view->open_tag("view", array(
				"measures"   => show_boolean($access_to_measures),
				"log_access" => show_boolean($this->user->access_allowed("logs"))));

			$this->show_matrix_labels("horizontal", $this->model->matrix_horizontal, $horizontal_labels);
			$this->show_matrix_labels("vertical", $this->model->matrix_vertical, $vertical_labels);
			$this->show_level_labels($level_labels);

			$this->view->open_tag("matrix", array("size" => $this->model->matrix_size));
			foreach ($matrix as $line) {
				$this->view->open_tag("row");
				foreach ($line as $value) {
					$this->view->add_tag("cell", $value);
				}
				$this->view->close_tag();
			}
			$this->view->close_tag();

			$this->view->record($risk, "risk", array("editable" => show_boolean($editable)));

			$this->view->open_tag("tags");
			foreach ($tag_categories as $category_title => $category_tags) {
				foreach ($category_tags as $tag_id => $tag_name) {
					if (in_array($tag_id, $risk_tags["tags"]) == false) {
						unset($category_tags[$tag_id]);
					}
				}

				if (count($category_tags) == 0) {
					continue;
				}

				$this->view->open_tag("category", array("title" => $category_title));
				foreach ($category_tags as $tag) {
					$this->view->add_tag("tag", $tag);
				}
				$this->view->close_tag();
			}
			$this->view->close_tag();

			$this->view->open_tag("causes");
			foreach ($causes as $cause) {
				$this->view->record($cause, "cause");
			}
			$this->view->close_tag();

			$this->view->open_tag("effects");
			foreach ($effects as $effect) {
				$this->view->record($effect, "effect");
			}
			$this->view->close_tag();

			$this->view->close_tag();
		}

		/* Show risk form
		 */
		private function show_risk_form($risk) {
			if (($labels_horizontal = $this->model->get_horizontal_labels()) == false) {
				$this->view->add_tag("result", $this->language->global_text("error_database"));
				return false;
			}

			if (($labels_vertical = $this->model->get_vertical_labels()) == false) {
				$this->view->add_tag("result", $this->language->global_text("error_database"));
				return false;
			}

			if (($identifiers = $this->model->get_all_identifiers()) === false) {
				return false;
			}

			$impact_labels = $this->model->get_impact_labels();

			if (($references = $this->model->get_reference_table()) === false) {
				$this->view->add_tag("result", $this->language->global_text("error_database"));
			}

			if (isset($risk["id"])) {
				if (($level_labels = $this->model->get_level_labels()) == false) {
					$this->view->add_tag("result", $this->language->global_text("error_database"));
					return false;
				}

				if (($matrix = $this->model->get_matrix()) == false) {
					$this->view->add_tag("result", $this->language->global_text("error_database"));
					return false;
				}

				$risk_level = $this->model->level_from_matrix($matrix, $risk["horizontal"], $risk["vertical"]);
			}

			if (isset($risk["id"])) {
				if (($accept_status = $this->model->get_risk_accept_status($risk["id"])) == false) {
					$this->view->add_tag("result", $this->language->global_text("error_database"));
					return false;
				}

				$risk["accept_end_date"] = $accept_status["accept_end_date"];
				$risk["acceptor_id"] = $accept_status["acceptor_id"];

				if ($risk["accept_end_date"] != DATE_NOT_SET) {
					if (($acceptor = $this->model->get_user_fullname($risk["acceptor_id"])) == false) {
						$this->view->add_tag("result", $this->language->global_text("error_database"));
						return false;
					}

					$this->view->add_system_warning($this->language->module_text("changing_accepted_risk"), $acceptor);
				}
			}

			$this->view->add_javascript("includes/risk_level.js");
			$this->view->add_javascript("risk_edit.js");

			$measure_statuses = array_keys(MEASURE_STATUSES);

			$args = array(
				"deletable"            => show_boolean(RISKS_DELETABLE),
				"status_effective"     => $measure_statuses[MEASURE_STATUS_EFFECTIVE],
				"maxlength_identifier" => MAXLENGTH_RISK_IDENTIFIER,
				"maxlength_title"      => MAXLENGTH_RISK_TITLE);
			$this->view->open_tag("edit_risk", $args);

			$this->show_matrix_labels("horizontal", $this->model->matrix_horizontal, $labels_horizontal);
			$this->show_matrix_labels("vertical", $this->model->matrix_vertical, $labels_vertical);

			$this->view->open_tag("approaches");
			foreach (RISK_APPROACHES as $approach => $description) {
				$this->view->add_tag("approach", $approach, array("description" => $description));
			}
			$this->view->close_tag();

			$this->view->record($risk, "risk");

			if (isset($risk["id"]) && ($risk_level > 1)) {
				$risk_level -= 1;

				$this->view->open_tag("mitigated");
				$this->view->add_tag("level", "-", array(
					"level" => 0,
					"color" => "#ffffff"));
				for ($i = $risk_level - 1; $i >= 0; $i--) {
					$this->view->add_tag("level", $level_labels["label"][$i], array(
						"level" => $risk_level - $i, 
						"color" => $level_labels["color"][$i]));
				}
				$this->view->close_tag();
			}

			$this->view->open_tag("identifiers");
			foreach ($identifiers as $identifier) {
				$this->view->add_tag("identifier", $identifier);
			}
            $this->view->close_tag();

            $this->view->open_tag("impact", array("text" => $this->model->label_impact));
            foreach ($impact_labels as $label) {
                $this->view->add_tag("level", $label);
            }
            $this->view->close_tag();

            $this->view->open_tag("references",);
            foreach ($references as $reference) {
				$this->view->open_tag("reference");
				$this->view->add_tag("effect", $reference["effect"]);
				for ($i = 1; $i <= $this->model->matrix_size; $i++) {
					$this->view->add_tag("impact", $reference["impact".$i]);
				}
				$this->view->close_tag();
            }
            $this->view->close_tag();

			$this->view->close_tag();

			$this->language->replace_module_text("help_edit_2", "horizontal_axis", $this->model->matrix_horizontal);
			$this->language->replace_module_text("help_edit_2", "vertical_axis", $this->model->matrix_vertical);
		}

		/* Show acceptor form
		 */
		private function show_acceptor_form($data) {
			if (($level = $this->model->get_risk_level($data["risk_id"])) === false) {
				$this->view->add_tag("result", $this->language->global_text("error_database"));
				return false;
			}

			if (($owners = $this->model->get_risk_acceptors($level)) == false) {
				$this->view->add_tag("result", $this->language->module_text("error_no_risk_acceptance_approvers_defined"), array("url" => "risk/".$data["risk_id"]));
				return false;
			}

			$this->view->add_message($this->language->module_text("error_level_above_appetite"));

			$approach_labels = array_keys(RISK_APPROACHES);
			$this->view->add_message($this->language->module_text("approach_set_to_decide_later"), $approach_labels[RISK_APPROACH_DECIDE_LATER], $approach_labels[RISK_APPROACH_ACCEPT]);

			$this->view->open_tag("set_acceptor");

			$this->view->open_tag("acceptors");
			foreach ($owners as $owner) {
				$this->view->add_tag("acceptor", $owner["fullname"], array("id" => $owner["id"]));
			}
			$this->view->close_tag();

			$this->view->add_tag("risk_id", $data["risk_id"]);
			$this->view->add_tag("acceptor_id", $data["acceptor_id"] ?? 0);

			$this->view->close_tag();
		}

		/* Show filter form
		 */
		private function show_filter_form() {
			if (($level_labels = $this->model->get_level_labels()) == false) {
				$this->view->add_tag("result", $this->language->global_text("error_database"));
				return false;
			}

			if (($tag_categories = $this->model->get_tags()) === false) {
				$this->view->add_tag("result", $this->language->global_text("error_database"));
				return false;
			}

			if (in_array($this->settings->role_id_risk_manager, $this->user->role_ids) == false) {
				$owners = array();
			} else if (($owners = $this->model->get_risk_owners()) === false) {
				$this->view->add_tag("result", $this->language->global_text("error_database"));
				return false;
			}

			if ((count($tag_categories) == 0) && (count($owners) == 0)) {
				$this->view->add_tag("result", $this->language->module_text("error_no_filter_options"), array("url" => "risk"));
				return true;
			}

			$this->view->add_javascript("risk_filter.js");

			$this->view->open_tag("filter");

			/* Risk levels
			 */
			$this->show_level_labels($level_labels);

			if (isset($_SESSION["risk_filter"]["level"])) {
				$this->view->add_tag("level", $_SESSION["risk_filter"]["level"]);
			}

			/* Tags
			 */
			if (count($tag_categories) > 0) {
				$this->view->open_tag("tags");

				foreach ($tag_categories as $category => $tags) {
					if (count($tags) == 0) {
						continue;
					}

					$this->view->open_tag("category", array("title" => $category));
					foreach ($tags as $id => $tag) {
						$this->view->add_tag("tag", $tag, array("id" => $id));
					}
					$this->view->close_tag();
				}
					
				if (isset($_SESSION["risk_filter"]["tags"])) {
					foreach ($_SESSION["risk_filter"]["tags"] as $tag) {
						$this->view->add_tag("set", $tag);
					}
				}

				$this->view->close_tag();
			}

			/* Owners
			 */
			if (count($owners) > 0) {
				$this->view->open_tag("owners");

				foreach ($owners as $owner) {
					$this->view->add_tag("owner", $owner["fullname"], array("id" => $owner["id"]));
				}

				$this->view->close_tag();

				if (isset($_SESSION["risk_filter"]["owner"])) {
					$this->view->add_tag("owner", $_SESSION["risk_filter"]["owner"]);
				}
			}

			$this->view->close_tag();
		}

		/* Show tags form
		 */
		private function show_tags_form($risk_tags) {
			if (($categories = $this->model->get_tags()) === false) {
				$this->view->add_tag("result", $this->language->global_text("error_database"));
				return false;
			}

			$this->view->add_javascript("risk_tags.js");

			$this->view->open_tag("assign_tags");

			$this->view->open_tag("categories");
			foreach ($categories as $category => $tags) {
				if (count($tags) == 0) {
					continue;
				}

				$this->view->open_tag("category", array("title" => $category));
				foreach ($tags as $id => $tag) {
					$this->view->add_tag("tag", $tag, array("id" => $id));
				}
				$this->view->close_tag();
			}
			$this->view->close_tag();

			$this->view->add_tag("risk_id", $risk_tags["risk_id"]);

			$this->view->open_tag("tags");
			foreach ($risk_tags["tags"] as $tag) {
				$this->view->add_tag("tag", $tag);
			}
			$this->view->close_tag();
			
			$this->view->close_tag();
		}

		/* Show owner form
		 */
		private function show_owner_form($data) {
			if (($owners = $this->model->get_risk_owners($data["risk_id"])) == false) {
				$this->view->add_tag("result", $this->language->module_text("error_no_risk_owners_available"), array("url" => "risk/".$data["risk_id"]));
				return false;
			}

			if ($data["owner_id"] != $this->user->id) {
				$action = $this->language->module_text("change_ownership_assign");
			} else {
				$action = $this->language->module_text("change_ownership_transfer");
				$action = "you transfer the risk ownership to somebody else";
			}
			$this->view->add_system_warning($this->language->module_text("change_ownership"), $action);

			$this->view->open_tag("set_owner");

			$this->view->open_tag("owners");
			foreach ($owners as $owner) {
				$this->view->add_tag("owner", $owner["fullname"], array("id" => $owner["id"]));
			}
			$this->view->close_tag();

			$this->view->add_tag("risk_id", $data["risk_id"]);
			$this->view->add_tag("owner_id", $data["owner_id"] ?? 0);

			$this->view->close_tag();
		}

		/* Show cause form
		 */
		private function show_cause_form($risk_cause) {
			if (($causes = $this->model->get_causes()) === false) {
				$this->view->add_tag("result", $this->language->global_text("error_database"));
				return false;
			}

			if (count($causes) == 0) {
				$this->view->add_tag("result", $this->language->module_text("error_cause_categories_not_available"), array("url" => "risk/".$risk_cause["risk_id"]));
				return false;
			}

			$this->view->open_tag("edit_cause", array("maxlength_title" => MAXLENGTH_CAUSE_EFFECT_TITLE));

			$this->view->open_tag("causes");
			foreach ($causes as $cause) {
				$this->view->add_tag("cause", $cause["cause"], array("id" => $cause["id"]));
			}
			$this->view->close_tag();

			$this->view->record($risk_cause, "cause");

			$this->view->close_tag();
		}

		/* Show effect form
		 */
		private function show_effect_form($risk_effect) {
			if (($effects = $this->model->get_effects()) === false) {
				$this->view->add_tag("result", $this->language->global_text("error_database"));
				return false;
			}

			if (count($effects) == 0) {
				$this->view->add_tag("result", $this->language->module_text("error_effect_categories_not_available"), array("url" => "risk/".$risk_effect["risk_id"]));
				return false;
			}

			$this->view->open_tag("edit_effect", array("maxlength_title" => MAXLENGTH_CAUSE_EFFECT_TITLE));

			$this->view->open_tag("effects");
			foreach ($effects as $effect) {
				$this->view->add_tag("effect", $effect["effect"], array("id" => $effect["id"]));
			}
			$this->view->close_tag();

			$this->view->record($risk_effect, "effect");

			$this->view->close_tag();
		}

		/* Risk accept proceudre
		 */
		private function risk_accept_procedure(&$risk) {
			if ($_POST["approach"] != RISK_APPROACH_ACCEPT) {
				return false;
			}

			if (($matrix = $this->model->get_matrix()) == false) {
				return null;
			}

			if (isset($risk["id"])) {
				if (($accept_status = $this->model->get_risk_accept_status($risk["id"])) === false) {
					return null;
				}

				$risk_level = $this->model->level_from_matrix($matrix, $_POST["horizontal"], $_POST["vertical"]);
				$accept_level = $this->model->level_from_matrix($matrix, $accept_status["horizontal"], $accept_status["vertical"]);

				if (($accept_status["approach"] == RISK_APPROACH_ACCEPT) && ($accept_status["acceptor_id"] != null) && ($risk_level == $accept_level)) {
					return false;
				}
			}

			$level = $this->model->level_from_matrix($matrix, $_POST["horizontal"], $_POST["vertical"]);
			if ($level <= $this->model->risk_appetite) {
				return false;
			}

			$risk["approach"] = RISK_APPROACH_DECIDE_LATER;

			return true;
		}

		/* Log risk changes
		 */
		private function log_risk_changes($old, $new) {
			if ($old["owner_id"] == null) {
				return;
			}

			$event = "risk updated";
			$args = array();

			if (isset($new["levels_mitigated"])) {
				if ($new["levels_mitigated"] != $old["levels_mitigated"]) {
					if (($matrix = $this->model->get_matrix()) != false) {
						$level_old = $this->model->level_from_matrix($matrix, $old["horizontal"], $old["vertical"]);
						$level_old = max($level_old - $old["levels_mitigated"], 1);

						$level_new = $this->model->level_from_matrix($matrix, $new["horizontal"], $new["vertical"]);
						$level_new = max($level_new - $new["levels_mitigated"], 1);

						if (($labels = $this->model->get_level_labels()) != false) {
							$level_old = $labels["label"][$level_old - 1];
							$level_new = $labels["label"][$level_new - 1];
						}

						$event .= ", level %s -> %s";
						array_push($args, $level_old, $level_new);
					}
				}
			}

			if (($labels = $this->model->get_vertical_labels()) != false) {
				$old["vertical"] = $labels[$old["vertical"] - 1];
				$new["vertical"] = $labels[$new["vertical"] - 1];
			}

			if (($labels = $this->model->get_horizontal_labels()) != false) {
				$old["horizontal"] = $labels[$old["horizontal"] - 1];
				$new["horizontal"] = $labels[$new["horizontal"] - 1];
			}

			if ($new["identifier"] != $old["identifier"]) {
				$event .= ", identifier %s -> %s";
				array_push($args, $old["identifier"], $new["identifier"]);
			}

			if ($new["vertical"] != $old["vertical"]) {
				$event .= ", %s %s -> %s";
				array_push($args, strtolower($this->model->matrix_vertical), $old["vertical"], $new["vertical"]);
			}

			if ($new["horizontal"] != $old["horizontal"]) {
				$event .= ", %s %s -> %s";
				array_push($args, strtolower($this->model->matrix_horizontal), $old["horizontal"], $new["horizontal"]);
			}

			$approach_labels = array_keys(RISK_APPROACHES);
			if ($new["approach"] != $old["approach"]) {
				$event .= ", approach %s -> %s";
				array_push($args, $approach_labels[$old["approach"]], $approach_labels[$new["approach"]]);
			}

			$this->model->log_risk_event($new["id"], $event, $args);
		}

		/* Main
		 */
		public function execute() {
			if (isset($_SESSION["risk_filter"]) == false) {
				$_SESSION["risk_filter"] = array();
			}

			if ($_SERVER["REQUEST_METHOD"] == "POST") {
				if ($_POST["submit_button"] == $this->language->module_text("filter_set_filter")) {
					/* Set filter
					 */
					$this->model->set_filter($_POST);
					$this->show_overview();
				} else if ($_POST["submit_button"] == $this->language->module_text("btn_save_risk")) {
					/* Save risk
					 */
					if ($this->model->risk_okay($_POST) == false) {
						$this->show_risk_form($_POST);
					} else if (($risk_accept_procedure = $this->risk_accept_procedure($_POST)) === null) {
						$this->view->add_tag("result", $this->language->global_text("error_database"));
					} else if (isset($_POST["id"]) === false) {
						/* Create risk
						 */
						if (($risk_id = $this->model->create_risk($_POST)) === false) {
							$this->view->add_message($this->language->module_text("error_creating_risk"));
							$this->show_risk_form($_POST);
						} else {
							$this->view->add_message($this->language->module_text("complete_risk_first"));
							$this->user->log_action("risk %d created", $risk_id);
							$this->model->log_risk_event($risk_id, "risk created");

							if ($risk_accept_procedure) {
								$data = array(
									"risk_id"     => $risk_id,
									"acceptor_id" => 0);
								$this->show_acceptor_form($data);
							} else {
								$this->show_risk($risk_id);
							}
						}
					} else {
						/* Update risk
						 */
						if (($old = $this->model->get_risk($_POST["id"])) == false) {
							$this->view->add_message($this->language->module_text("error_risk_not_found"));
							$this->user->log_action("unauthorized risk update attempt %d", $_POST["id"]);
							$this->show_risk_form($_POST);
						} else if ($this->model->update_risk($_POST) === false) {
							$this->view->add_message($this->language->module_text("error_risk_update"));
							$this->show_risk_form($_POST);
						} else {
							$this->log_risk_changes($old, $_POST);
							$this->user->log_action("risk updated", $_POST["id"]);

							if ($risk_accept_procedure) {
								$data = array(
									"risk_id"     => $_POST["id"],
									"acceptor_id" => 0);
								$this->show_acceptor_form($data);
							} else {
								$this->show_risk($_POST["id"]);
							}
						}
					}
				} else if ($_POST["submit_button"] == $this->language->module_text("btn_delete_risk")) {
					/* Delete risk
					 */
					if (is_false(RISKS_DELETABLE)) {
						$this->show_risk($_POST["id"]);
					} else if ($this->model->delete_risk($_POST["id"]) === false) {
						$this->view->add_message($this->language->module_text("error_risk_delete"));
						$this->show_risk_form($_POST);
					} else {
						$this->user->log_action("risk %d deleted", $_POST["id"]);
						$this->show_overview();
					}
				} else if ($_POST["submit_button"] == $this->language->module_text("btn_assign_tags")) {
					/* Assign tags
					 */
					if ($this->model->assign_tags($_POST) == false) {
						$this->show_tags_form($_POST);
					} else {
						$this->show_risk($_POST["risk_id"]);
					}
				} else if ($_POST["submit_button"] == $this->language->module_text("btn_set_acceptance_approver")) {
					/* Risk acceptor
					 */
					if ($this->model->set_risk_acceptor($_POST) == false) {
						$this->show_acceptor_form($_POST);
					} else {
						if (($acceptor = $this->model->get_user_fullname($_POST["acceptor_id"])) == false) {
							$acceptor = $_POST["acceptor_id"];
						}

						$this->model->send_accept_notification($_POST["risk_id"], $_POST["acceptor_id"]);
						$this->model->log_risk_event($_POST["risk_id"], "risk acceptance approver assigned to %s (id:%d)", $acceptor, $_POST["acceptor_id"]);
						$this->view->add_system_message("Risk acceptance request has been send to %s.", $acceptor);

						$this->show_risk($_POST["risk_id"]);
					}
				} else if ($_POST["submit_button"] == $this->language->module_text("btn_approve_acceptance")) {
					/* Accept risk
					 */
					if ($this->model->approve_risk_acceptance($_POST["risk_id"], $_POST["period"]) == false) {
						$this->view->add_message($this->language->module_text("error_approving_risk_acceptance"));
					} else {
						$this->model->log_risk_event($_POST["risk_id"], "risk acceptance approved for %s months", $_POST["period"]);

						$this->view->add_message($this->language->module_text("risk_acceptance_approved"));
					}
					$this->show_risk($_POST["risk_id"]);
				} else if ($_POST["submit_button"] == $this->language->module_text("btn_deny_acceptance")) {
					/* Deny acceptance
					 */
					if ($this->model->deny_risk_acceptance($_POST["risk_id"]) == false) {
						$this->view->add_message($this->language->module_text("error_denying_acceptance"));
					} else {
						$this->model->log_risk_event($_POST["risk_id"], "risk acceptance denied");

						$this->view->add_message($this->language->module_text("risk_acceptance_denied"));
					}
					$this->show_overview();
				} else if ($_POST["submit_button"] == $this->language->module_text("btn_set_owner")) {
					/* Risk owner
					 */
					if (($current_owner_id = $this->model->get_risk_owner($_POST["risk_id"])) === false) {
						$this->view->add_message($this->language->global_text("error_database"));
						$this->show_risk($_POST["risk_id"]);
					} else if ($_POST["owner_id"] == $current_owner_id) {
						$this->show_risk($_POST["risk_id"]);
					} else if ($this->model->set_risk_owner($_POST) == false) {
						$this->show_owner_form($_POST);
					} else {
						if (($owner = $this->model->get_user_fullname($_POST["owner_id"])) == false) {
							$owner = $_POST["owner_id"];
						}

						$this->model->send_owner_notification($_POST["risk_id"], $_POST["owner_id"]);
						$this->model->log_risk_event($_POST["risk_id"], "risk ownership assigned to %s (id:%d)", $owner, $_POST["owner_id"]);
						$this->show_overview();
					}
				} else if ($_POST["submit_button"] == $this->language->module_text("btn_save_cause")) {
					/* Save cause
					 */
					if ($this->model->cause_okay($_POST) == false) {
						$this->show_cause_form($_POST);
					} else if (isset($_POST["id"]) === false) {
						/* Create cause
						 */
						if (($cause_id = $this->model->create_cause($_POST)) === false) {
							$this->view->add_message($this->language->module_text("error_cause_create"));
							$this->show_cause_form($_POST);
						} else {
							$this->user->log_action("risk cause %d created", $cause_id);
							$this->model->log_risk_event($_POST["risk_id"], "cause '%s' created (id:%d)", $_POST["title"], $cause_id);
							$this->show_risk($_POST["risk_id"]);
						}
					} else {
						/* Update cause
						 */
						if ($this->model->update_cause($_POST) === false) {
							$this->view->add_message($this->language->module_text("error_cause_update"));
							$this->show_cause_form($_POST);
						} else {
							$this->user->log_action("risk cause %d updated", $_POST["id"]);
							$this->model->log_risk_event($_POST["risk_id"], "cause '%s' updated (id:%d)", $_POST["title"], $_POST["id"]);
							$this->show_risk($_POST["risk_id"]);
						}
					}
				} else if ($_POST["submit_button"] == $this->language->module_text("btn_delete_cause")) {
					/* Delete cause
					 */
					if ($this->model->delete_cause_okay($_POST["id"]) === false) {
						$this->show_cause_form($_POST);
					} else if ($this->model->delete_cause($_POST["id"]) === false) {
						$this->view->add_message($this->language->module_text("error_cause_delete"));
						$this->show_cause_form($_POST);
					} else {
						$this->user->log_action("risk cause %d deleted", $_POST["id"]);
						$this->model->log_risk_event($_POST["risk_id"], "cause '%s' deleted (id:%d)", $_POST["title"], $_POST["id"]);
						$this->show_risk($_POST["risk_id"]);
					}
				} else if ($_POST["submit_button"] == $this->language->module_text("btn_save_effect")) {
					/* Save effect
					 */
					if ($this->model->effect_okay($_POST) == false) {
						$this->show_effect_form($_POST);
					} else if (isset($_POST["id"]) === false) {
						/* Create effect
						 */
						if (($effect_id = $this->model->create_effect($_POST)) === false) {
							$this->view->add_message($this->language->module_text("error_effect_create"));
							$this->show_effect_form($_POST);
						} else {
							$this->user->log_action("risk effect %d created", $this->db->last_insert_id);
							$this->model->log_risk_event($_POST["risk_id"], "effect '%s' created (id:%d)", $_POST["title"], $effect_id);
							$this->show_risk($_POST["risk_id"]);
						}
					} else {
						/* Update effect
						 */
						if ($this->model->update_effect($_POST) === false) {
							$this->view->add_message($this->language->module_text("error_effect_update"));
							$this->show_effect_form($_POST);
						} else {
							$this->user->log_action("risk effect %d updated", $_POST["id"]);
							$this->model->log_risk_event($_POST["risk_id"], "effect '%s' updated (id:%d)", $_POST["title"], $_POST["id"]);
							$this->show_risk($_POST["risk_id"]);
						}
					}
				} else if ($_POST["submit_button"] == $this->language->module_text("btn_delete_effect")) {
					/* Delete effect
					 */
					if ($this->model->delete_effect_okay($_POST["id"]) === false) {
						$this->show_effect_form($_POST);
					} else if ($this->model->delete_effect($_POST["id"]) === false) {
						$this->view->add_message($this->language->module_text("error_effect_delete"));
						$this->show_effect_form($_POST);
					} else {
						$this->user->log_action("risk effect %d deleted", $_POST["id"]);
						$this->model->log_risk_event($_POST["risk_id"], "effect '%s' deleted (id:%d)", $_POST["title"], $_POST["id"]);
						$this->show_risk($_POST["risk_id"]);
					}
				} else if ($_POST["submit_button"] == $this->language->module_text("btn_search")) {
					/* Search risks
					 */
					if (($risks = $this->model->search_risks($_POST["query"])) === false) {
						$this->view->add_system_message($this->language->module_text("error_risk_not_found"));
						$this->show_overview();
					} else if (count($risks) == 1) {
						$this->show_risk($risks[0]["id"]);
					} else if (count($risks) == 0) {
						$this->view->add_tag("result", $this->language->module_text("error_no_search_results"));
					} else {
						$this->risks_to_view($risks);
					}
				} else {
					$this->show_overview();
				}
			} else if ($this->page->parameter_value(0, "new")) {
				/* New risk
				 */
				if (in_array($this->settings->role_id_employee, $this->user->role_ids) == false) {
					$this->view->add_system_warning($this->language->module_text("error_risk_create_not_allowed"));
					$this->show_overview();
				} else {
					$risk = array();
					$this->show_risk_form($risk);
				}
			} else if ($this->page->parameter_value(0, "filter")) {
				/* Filter form
				 */
				$this->show_filter_form();
			} else if ($this->page->parameter_numeric(0)) {
				if ($this->page->parameter_value(1, "edit")) {
					/* Edit risk
					 */
					if ($this->model->may_edit_risk($this->page->parameters[0]) == false) {
						$this->view->add_tag("result", $this->language->module_text("error_risk_edit_not_allowed"));
						$this->user->log_action("unauthorized risk edit attempt %d", $this->page->parameters[0]);
					} else if (($risk = $this->model->get_risk($this->page->parameters[0])) == false) {
						$this->view->add_tag("result", $this->language->module_text("error_risk_not_found"));
					} else {
						$this->show_risk_form($risk);
					}
				} else if ($this->page->parameter_value(1, "tags")) {
					/* Assign tags
					 */
					if ($this->model->may_edit_risk($this->page->parameters[0]) == false) {
						$this->view->add_tag("result", $this->language->module_text("error_risk_edit_not_allowed"));
						$this->user->log_action("unauthorized risk tag edit attempt %d", $this->page->parameters[0]);
					} else if (($tags = $this->model->get_risk_tags($this->page->parameters[0])) === false) {
						$this->view->add_tag("result", $this->language->module_text("error_risk_not_found"));
					} else {
						$this->show_tags_form($tags);
					}
				} else if ($this->page->parameter_value(1, "owner")) {
					/* Risk owner
					 */
					if (($owner_id = $this->model->get_risk_owner($this->page->parameters[0])) === false) {
						$this->view->add_tag("result", $this->language->module_text("error_risk_not_found"));
					} else {
						$data = array(
							"risk_id"  => $this->page->parameters[0],
							"owner_id" => $owner_id);
						$this->show_owner_form($data);
					}
				} else if ($this->page->parameter_value(1, "cause")) {
					/* Cause form
					 */
					$cause = array("risk_id" => $this->page->parameters[0]);
					$this->show_cause_form($cause);
				} else if ($this->page->parameter_value(1, "effect")) {
					/* Effect form
					 */
					$effect = array("risk_id" => $this->page->parameters[0]);
					$this->show_effect_form($effect);
				} else {
					/* View risk
					 */
					$this->show_risk($this->page->parameters[0]);
				}
			} else if ($this->page->parameter_value(0, "cause") && $this->page->parameter_numeric(1)) {
				/* Edit cause
				 */
				if (($cause = $this->model->get_risk_cause($this->page->parameters[1])) == false) {
					$this->view->add_tag("result", $this->language->module_text("error_risk_not_found"));
				} else if ($this->model->may_edit_risk($cause["risk_id"]) == false) {
					$this->view->add_tag("result", $this->language->module_text("error_cause_edit_not_allowed"));
					$this->user->log_action("unauthorized risk cause edit attempt %d", $cause["id"]);
				} else {
					$this->show_cause_form($cause);
				}
			} else if ($this->page->parameter_value(0, "effect") && $this->page->parameter_numeric(1)) {
				/* Edit effect
				 */
				if (($effect = $this->model->get_risk_effect($this->page->parameters[1])) == false) {
					$this->view->add_tag("result", $this->language->module_text("error_risk_not_found"));
				} else if ($this->model->may_edit_risk($effect["risk_id"]) == false) {
					$this->view->add_tag("result", $this->language->module_text("error_effect_edit_not_allowed"));
					$this->user->log_action("unauthorized risk effect edit attempt %d", $effect["id"]);
				} else {
					$this->show_effect_form($effect);
				}
			} else if ($this->page->parameter_value(0, "attention")) {
				/* Risks requiring attention
				 */
				$this->show_risks_attention();
			} else {
				/* Show overview
				 */
				$this->show_overview();
			}
		}
	}
?>
