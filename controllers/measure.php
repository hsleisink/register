<?php
	class measure_controller extends register_controller {
		private function risk_viewable($measure) {
			$risk_manager = in_array($this->settings->role_id_risk_manager, $this->user->role_ids);
			$risk_owner = ($measure["risk_owner_id"] == $this->user->id);
			$risk_creator = ($measure["risk_creator_id"] == $this->user->id);
			$risk_acceptor = ($measure["risk_acceptor_id"] == $this->user->id);
			$risk_viewer = $this->user->has_role($this->settings->role_id_risk_viewer);

			return $risk_manager || $risk_owner || $risk_creator || $risk_acceptor || $risk_viewer;
		}

		/* Measures to view
		 */
		private function measures_to_view($measures, $pagination = null) {
			if (($matrix = $this->model->get_matrix()) == false) {
				$this->view->add_tag("result", $this->language->global_text("error_database"));
				return false;
			}

			if (($level_labels = $this->model->get_level_labels()) == false) {
				$this->view->add_tag("result", $this->language->global_text("error_database"));
				return false;
			}

			$this->view->add_javascript("measure_list.js");

			if (count($_SESSION["measure_filter"]) != 0) {
				$this->view->add_system_message($this->language->module_text("filter_active"));
			}

			$measure_statuses = array_keys(MEASURE_STATUSES);

			$this->view->open_tag("overview", array(
				"calendar"  => show_boolean($this->user->access_allowed("measure/calendar")),
				"subpage"   => show_boolean($pagination == null)));

			$this->show_level_labels($level_labels);

			$this->view->open_tag("measures");
			foreach ($measures as $measure) {
				$measure["measure_type"] = $this->language->global_text($measure["type"]);
				$measure["deadline"] = date_string("j M Y", $measure["deadline"]);
				$measure["status"] = $measure_statuses[$measure["status"]];
				$measure["urgency"] = $this->model->level_from_matrix($matrix, $measure["horizontal"], $measure["vertical"]);

				$measure["risk_viewable"] = show_boolean($this->risk_viewable($measure));

				$this->view->record($measure, "measure");
			}
			$this->view->close_tag();

			if ($pagination != null) {
				$pagination->show_browse_links();
			}

			$this->show_measure_statuses();

			$this->view->close_tag();

			$measure_statuses = array_keys(MEASURE_STATUSES);
			$this->language->replace_global_text("help_measure_status", "design", $measure_statuses[0]);
			$this->language->replace_global_text("help_measure_status", "implemented", $measure_statuses[1]);
			$this->language->replace_global_text("help_measure_status", "effective", $measure_statuses[2]);
			$this->language->replace_global_text("help_measure_status", "ineffective", $measure_statuses[3]);

			$this->language->replace_module_text("help_2", "effective", $measure_statuses[MEASURE_STATUS_EFFECTIVE]);
		}

		/* Show overview
		 */
		private function show_overview() {
			if (($measure_count = $this->model->count_measures()) === false) {
				$this->view->add_tag("result", $this->language->global_text("error_database"));
				return;
			}

			$pagination = new \Banshee\pagination($this->view, "measures", $this->settings->risk_page_size, $measure_count);
			if (($_POST["submit_button"] ?? null) == "Set filter") {
				$pagination->reset();
			}

			if ($measure_count == 0) {
				$measures = array();
			} else if (($measures = $this->model->get_measures($pagination->offset, $pagination->size)) === false) {
				$this->view->add_tag("result", $this->language->global_text("error_database"));
				return;
			}

			$this->view->add_tag("search", show_boolean(is_false(ENCRYPT_DATA)));

			$this->measures_to_view($measures, $pagination);
		}

		/* Show measures requiring attention
		 */
		private function show_measures_attention() {
			if (($measures = $this->model->get_measures_attention()) === false) {
				$this->view->add_tag("result", $this->language->global_text("error_database"));
				return;
			}

			$this->measures_to_view($measures);
		}

		/* Show measure
		 */
		private function show_measure($type, $measure_id) {
			switch ($type) {
				case "preventive":
					$measure = $this->model->get_preventive_measure($measure_id);
					break;
				case "detective":
					$measure = $this->model->get_detective_measure($measure_id);
					break;
				case "repressive":
					$measure = $this->model->get_repressive_measure($measure_id);
					break;
				default:
					$this->view->add_tag("result", $this->language->module_text("error_measure_type_invalid"));
					return false;
			}

			if ($measure == false) {
				$this->view->add_tag("result", $this->language->module_text("error_measure_not_found"));
				$this->user->log_action("unauthorized %s measure view attempt (id:%d)", $type, $measure_id);
				return false;
			}

			if (($standards = $this->model->get_measure_controls($type, $measure_id)) === false) {
				$this->view->add_tag("result", $this->language->global_text("error_database"));
				return false;
			}

			if (($issues = $this->model->get_measure_issues($type, $measure_id)) === false) {
				$this->view->add_tag("result", $this->language->global_text("error_database"));
				return false;
			}

			switch ($measure["status"]) {
				case 0:
					$measure["reason"] = $this->language->module_text("implementation");
					break;
				case 1:
					$measure["reason"] = $this->language->module_text("effectiveness_check");
					break;
				case 2:
				case 3:
					$measure["reason"] = $this->language->module_text("effectiveness_recheck");
					break;
			}

			$deadline = strtotime($measure["deadline"]);
			$issue = ($measure["owner_id"] == $this->user->id) || $this->model->may_edit_risk($measure["risk_id"]);

			$measure_statuses = array_keys(MEASURE_STATUSES);

			$measure["status"] = $measure_statuses[$measure["status"]];
			$measure["deadline_passed"] = show_boolean(time() >= $deadline);
			$measure["deadline"] = date_string("j F Y", $deadline);
			$measure["risk_viewable"] = show_boolean($this->risk_viewable($measure));
			$measure["issue"] = show_boolean($issue);
			$measure["measure_type"] = $this->language->global_text($measure["type"]);
			$measure["editable"] = show_boolean($this->model->may_edit_measure($measure_id, $type));

			$this->view->add_javascript("measure_view.js");

			$this->show_measure_statuses();

			$this->view->record($measure, "measure");

			$this->view->open_tag("controls");
			foreach ($standards as $standard => $controls) {
				$this->view->open_tag("standard", array("name" => $standard));

				foreach ($controls as $control) {
					$this->view->record($control, "control", array("id" => $control["id"]));
				}

				$this->view->close_tag();
			}
			$this->view->close_tag();

			if (count($issues) > 0) {
				$this->view->open_tag("issues");

				$issue_statuses = array_keys(ISSUE_STATUSES);

				$this->view->open_tag("statuses");
				foreach ($issue_statuses as $status) {
					$this->view->add_tag("status", $status);
				}
				$this->view->close_tag();

				foreach ($issues as $issue) {
					$issue["created"] = date("d-m-Y", $issue["created"]);
					$this->view->record($issue, "issue");
				}

				$this->view->close_tag();
			}
		}

		/* Show filter form
		 */
		private function show_filter_form() {
			if (in_array($this->settings->role_id_risk_manager, $this->user->role_ids) == false) {
				$owners = array();
			} else if (($owners = $this->model->get_measure_owners()) === false) {
				$this->view->add_tag("result", $this->language->global_text("error_database"));
				return false;
			}

			$this->view->add_javascript("measure_filter.js");
	
			$this->view->open_tag("filter");

			/* Type
			 */
			$this->view->open_tag("types");
			foreach (array("preventive", "detective", "repressive") as $type) {
				$checked = in_array($type, $_SESSION["measure_filter"]["type"] ?? array());
				$this->view->add_tag("type", $type, array(
					"label"   => $this->language->global_text($type),
					"checked" => show_boolean($checked)));
			}
			$this->view->close_tag();

			/* Measure status
			 */
			$this->show_measure_statuses(true);

			/* Owners
			 */
			if (count($owners) > 0) {
				$this->view->open_tag("owners");

				foreach ($owners as $owner) {
					$this->view->add_tag("owner", $owner["fullname"], array("id" => $owner["id"]));
				}

				$this->view->close_tag();

				if (isset($_SESSION["measure_filter"]["owner"])) {
					$this->view->add_tag("owner", $_SESSION["measure_filter"]["owner"]);
				}
			}

			$this->view->close_tag();
		}

		public function execute() {
			if (isset($_SESSION["measure_filter"]) == false) {
				$_SESSION["measure_filter"] = array();
			}

			if ($_SERVER["REQUEST_METHOD"] == "POST") {
				if ($_POST["submit_button"] == $this->language->module_text("btn_set_filter")) {
					$this->model->set_filter($_POST);
					$this->show_overview();
				} else if ($_POST["submit_button"] == $this->language->module_text("btn_search")) {
					/* Search measures
					 */
					if (($measures = $this->model->search_measures($_POST["query"])) === false) {
						$this->view->add_system_message($this->language->module_text("error_measure_not_found"));
						$this->show_overview();
					} else if (count($measures) == 1) {
						$this->show_measure($measures[0]["type"], $measures[0]["id"]);
					} else if (count($measures) == 0) {
						$this->view->add_tag("result", $this->language->module_text("error_no_search_results"));
					} else {
						$this->measures_to_view($measures);
					}
				}
			} else if ($this->page->parameter_numeric(1)) {
				/* show measure
				 */
				$this->show_measure($this->page->parameters[0], $this->page->parameters[1]);
			} else if ($this->page->parameter_value(0, "attention")) {
				/* Risks requiring attention
				 */
				$this->show_measures_attention();
			} else if ($this->page->parameter_value(0, "filter")) {
				/* Filter form
				 */
				$this->show_filter_form();
			} else {
				/* Show overview
				 */
				$this->show_overview();
			}
		}
	}
?>
