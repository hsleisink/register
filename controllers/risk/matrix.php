<?php
	class risk_matrix_controller extends register_controller {
		private function show_matrix() {
			if (($matrix = $this->model->get_matrix()) == false) {
				$this->view->add_tag("result", $this->language->global_text("error_database"));
				return false;
			}

			if (($horizontal_labels = $this->model->get_horizontal_labels()) == false) {
				$this->view->add_tag("result", $this->language->global_text("error_database"));
				return false;
			}

			if (($vertical_labels = $this->model->get_vertical_labels()) == false) {
				$this->view->add_tag("result", $this->language->global_text("error_database"));
				return false;
			}
			$vertical_labels = array_reverse($vertical_labels);

			if (($level_labels = $this->model->get_level_labels()) == false) {
				$this->view->add_tag("result", $this->language->global_text("error_database"));
				return false;
			}

			if (($risks = $this->model->get_risks()) === false) {
				$this->view->add_tag("result", $this->language->global_text("error_database"));
				return false;
			}

			$levels = array();
			foreach ($risks as $risk) {
				$x = $risk["horizontal"] - 1;
				$y = $this->model->matrix_size - $risk["vertical"];

				if (isset($levels[$x]) == false) {
					$levels[$x] = array();
				}

				if (isset($levels[$x][$y]) == false) {
					$levels[$x][$y] = 1;
				} else {
					$levels[$x][$y]++;
				}
			}

			if (count($_SESSION["risk_filter"]) != 0) {
				$this->view->add_system_message("Filter active.");
			}

			$this->view->add_javascript("includes/risk_level.js");
			$this->view->add_javascript("risk/matrix.js");

			$this->view->open_tag("matrix");

			$this->view->open_tag("labels");

			$this->show_matrix_labels("horizontal", $this->model->matrix_horizontal, $horizontal_labels);
			$this->show_matrix_labels("vertical", $this->model->matrix_vertical, $vertical_labels);
			$this->show_level_labels($level_labels);

			$this->view->close_tag();

			$this->view->open_tag("risks");
			foreach ($risks as $risk) {
				$risk["level"] = $this->model->level_from_matrix($matrix, $risk["horizontal"], $risk["vertical"]);
				$risk["mitigated"] = max($risk["level"] - $risk["levels_mitigated"], 1);

				$this->view->record($risk, "risk");
			}
			$this->view->close_tag();

			$this->view->open_tag("values", array("size" => $this->model->matrix_size));
			foreach ($matrix as $y => $line) {
				$this->view->open_tag("row");
				foreach ($line as $x => $value) {
					$count = isset($levels[$x][$y]) ? $levels[$x][$y] : "";

					$this->view->add_tag("cell", $count, array("level" => $value));
				}
				$this->view->close_tag();
			}
			$this->view->close_tag();

			$this->view->close_tag();
		}

		public function execute() {
			$this->show_matrix();
		}
	}
?>
