<?php
	class risk_measure_controller extends register_controller {
		const DEFAULT_DEADLINE = "+1 month";

		/* List measures
		 */
		private function list_measures($measures, $type, $edit_risk) {
			$this->view->open_tag($type);

			foreach ($measures as $measure) {
				$status_okay = ($measure["status"] == MEASURE_STATUS_EFFECTIVE);
				$deadline_okay = (strtotime($measure["deadline"]) > time());

				$measure_statuses = array_keys(MEASURE_STATUSES);

				if (is_true(EDIT_OWN_MEASURE)) {
					$editable = $this->model->may_edit_measure($measure["id"], $type);
				} else {
					$editable = $edit_risk;
				}

				$measure["editable"] = show_boolean($editable);
				$measure["done"] = show_boolean($status_okay && $deadline_okay);
				$measure["deadline"] = date_string("j F Y", strtotime($measure["deadline"]));
				$measure["status"] = $measure_statuses[$measure["status"]];

				$this->view->record($measure, "measure");
			}

			$this->view->close_tag();
		}

		/* Show overview
		 */
		private function show_overview($risk_id) {
			if (($risk = $this->model->get_risk($risk_id)) == false) {
				$this->view->add_tag("result", $this->language->module_text("error_risk_not_found"), array("url" => "risk"));
				$this->user->log_action("unauthorized risk measures view attempt (risk id:%d)", $risk_id);
				return false;
			}

			$viewable = ($risk["creator_id"] == $this->user->id) || ($risk["owner_id"] == $this->user->id);
			$editable = $this->model->may_edit_risk($risk_id);
			$risk_manager = in_array($this->settings->role_id_risk_manager, $this->user->role_ids);
			$risk_viewer = in_array($this->settings->role_id_risk_viewer, $this->user->role_ids);

			if (is_false(EDIT_OWN_MEASURE)) {
				if (($viewable == false) && ($risk_manager == false) && ($risk_viewer == false)) {
					$this->view->add_tag("result", $this->language->module_text("error_measures_view_not_allowed"));
					$this->user->log_action("unauthorized risk measures view attempt (risk id:%d)", $risk_id);
					return false;
				}

				$back = true;
			} else {
				$back = $viewable || $risk_manager || $risk_viewer;
			}

			if (($preventive = $this->model->get_preventives($risk_id, $editable)) === false) {
				$this->view->add_tag("result", $this->language->global_text("error_database"));
				return false;
			}

			if (($detective = $this->model->get_detectives($risk_id, $editable)) === false) {
				$this->view->add_tag("result", $this->language->global_text("error_database"));
				return false;
			}

			if (($repressive = $this->model->get_repressives($risk_id, $editable)) === false) {
				$this->view->add_tag("result", $this->language->global_text("error_database"));
				return false;
			}

			$this->view->add_javascript("risk/measure_overview.js");

			$this->view->open_tag("overview", array(
				"measures" => show_boolean($this->user->access_allowed("measure")),
				"editable" => show_boolean($editable),
				"back"     => show_boolean($back)));

			$this->view->open_tag("risk", array("id" => $risk_id));
			$this->view->add_tag("title", $risk["title"]);
			$this->view->close_tag();

			$this->list_measures($preventive, "preventive", $editable);
			$this->list_measures($detective, "detective", $editable);
			$this->list_measures($repressive, "repressive", $editable);

			$this->show_measure_statuses();

			$this->view->close_tag();

			$measure_statuses = array_keys(MEASURE_STATUSES);
			$this->language->replace_global_text("help_measure_status", "design", $measure_statuses[0]);
			$this->language->replace_global_text("help_measure_status", "implemented", $measure_statuses[1]);
			$this->language->replace_global_text("help_measure_status", "effective", $measure_statuses[2]);
			$this->language->replace_global_text("help_measure_status", "ineffective", $measure_statuses[3]);
		}

		/* Add pulldown lists
		 */
		private function add_pulldown_lists($owners) {
			$this->view->open_tag("owners");
			foreach ($owners as $owner) {
				$this->view->add_tag("owner", $owner["fullname"], array("id" => $owner["id"]));
			}
			$this->view->close_tag();

			$this->view->open_tag("statuses");
			foreach (MEASURE_STATUSES as $status => $text) {
				$this->view->add_tag("status", $status, array("help" => $text));
			}
			$this->view->close_tag();
		}

		/* Add standard control lists
		 */
		private function add_control_standards($standards, $selected) {
			$this->view->add_javascript("risk/measure_controls.js");

			$param = array();
			if ($selected != false) {
				$param["selected"] = $selected;
			}

			$this->view->open_tag("standards", $param);

			foreach ($standards as $standard) {
				$this->view->open_tag("standard", array("id" => $standard["id"]));
				$this->view->add_tag("name", $standard["name"]);

				$this->view->open_tag("controls");
				foreach ($standard["controls"] as $control) {
					$this->view->record($control, "control");
				}
				$this->view->close_tag();

				$this->view->close_tag();
			}

			$this->view->close_tag();
		}

		/* Preventive
		 */
		private function show_preventive_form($preventive) {
			$url = array("url" => "risk/measure/".$preventive["risk_id"]);

			if ((isset($preventive["id"]) == false) || is_false(EDIT_OWN_MEASURE)) {
				$may_edit_measure = $this->model->may_edit_risk($preventive["risk_id"]);
			} else {
				$may_edit_measure = $this->model->may_edit_measure($preventive["id"], "preventive");
			}

			if ($may_edit_measure == false) {
				$this->view->add_tag("result", $this->language->module_text("error_measure_edit_not_allowed"));
				$this->user->log_action("unauthorized preventive measure edit attempt (id:%d, risk id:%d)", $preventive["id"], $preventive["risk_id"]);
				return false;
			}

			if (($risk = $this->model->get_risk($preventive["risk_id"])) == false) {
				$this->view->add_tag("result", $this->language->module_text("error_risk_not_found"), array("url" => "risk"));
				return false;
			}

			if (($owners = $this->model->get_measure_owners($preventive["risk_id"])) == false) {
				$this->view->add_tag("result", $this->language->module_text("error_measure_owner_not_available"), $url);
				return false;
			}

			if (isset($preventive["id"]) == false) {
				if (($causes = $this->model->get_risk_causes($preventive["risk_id"])) == false) {
					$this->view->add_tag("result", $this->language->module_text("error_causes_not_available"), $url);
					return false;
				}
			} else {
				if (($preventive["risk_cause"] = $this->model->get_risk_cause($preventive["id"])) == false) {
					$this->view->add_tag("result", $this->language->global_text("error_database"), $url);
					return false;
				}
			}

			if (($controls = $this->model->get_controls()) === false) {
				$this->view->add_tag("result", $this->language->global_text("error_database"));
				return false;
			}

			$selected = isset($preventive["id"]) ? $this->model->get_most_used_standard("preventive", $preventive["id"]) : false;

			if (isset($preventive["owner_id"]) == false) {
				$preventive["owner_id"] = $risk["owner_id"];
			}

			$this->view->add_css("webui/jquery-ui.css");
			$this->view->add_javascript("webui/jquery-ui.js");
			$this->view->add_javascript("banshee/datepicker.js");
			$this->view->add_javascript("banshee/jquery.windowframe.js");
			$this->view->add_javascript("risk/measure_edit.js");

			$this->view->open_tag("edit_preventive", array("maxlength_title" => MAXLENGTH_MEASURE_TITLE));

			$this->add_pulldown_lists($owners);
			$this->add_control_standards($controls, $selected);

			if (isset($preventive["id"]) == false) {
				$this->view->open_tag("causes");
				foreach ($causes as $cause) {
					$this->view->add_tag("cause", $cause["cause"]." | ".$cause["title"], array("id" => $cause["id"]));
				}
				$this->view->close_tag();
			}

			if (isset($preventive["controls"]) == false) {
				$preventive["controls"] = array();
			}

			$this->view->record($risk, "risk");
			$this->view->record($preventive, "preventive", array(), true);

			$this->view->close_tag();
		}

		/* Detective
		 */
		private function show_detective_form($detective) {
			$url = array("url" => "risk/measure/".$detective["risk_id"]);

			if ((isset($detective["id"]) == false) || is_false(EDIT_OWN_MEASURE)) {
				$may_edit_measure = $this->model->may_edit_risk($detective["risk_id"]);
			} else {
				$may_edit_measure = $this->model->may_edit_measure($detective["id"], "detective");
			}

			if ($may_edit_measure == false) {
				$this->view->add_tag("result", $this->language->module_text("error_measure_edit_not_allowed"));
				$this->user->log_action("unauthorized detective measure edit attempt (id:%d, risk id:%d)", $detective["id"], $detective["risk_id"]);
				return false;
			}

			if (($risk = $this->model->get_risk($detective["risk_id"])) == false) {
				$this->view->add_tag("result", $this->language->module_text("error_risk_not_found"), array("url" => "risk"));
				return false;
			}

			if (($owners = $this->model->get_measure_owners($detective["risk_id"])) == false) {
				$this->view->add_tag("result", $this->language->module_text("error_measure_owner_not_available"), $url);
				return false;
			}

			if (($controls = $this->model->get_controls()) === false) {
				$this->view->add_tag("result", $this->language->global_text("error_database"));
				return false;
			}

			$selected = isset($detective["id"]) ? $this->model->get_most_used_standard("detective", $detective["id"]) : false;

			if (isset($detective["owner_id"]) == false) {
				$detective["owner_id"] = $risk["owner_id"];
			}

			$this->view->add_css("webui/jquery-ui.css");
			$this->view->add_javascript("webui/jquery-ui.js");
			$this->view->add_javascript("banshee/datepicker.js");
			$this->view->add_javascript("risk/measure_edit.js");

			$this->view->open_tag("edit_detective", array("maxlength_title" => MAXLENGTH_MEASURE_TITLE));

			$this->add_pulldown_lists($owners);
			$this->add_control_standards($controls, $selected);

			if (isset($detective["controls"]) == false) {
				$detective["controls"] = array();
			}

			$this->view->record($risk, "risk");
			$this->view->record($detective, "detective", array(), true);

			$this->view->close_tag();
		}

		/* Repressive
		 */
		private function show_repressive_form($repressive) {
			$url = array("url" => "risk/measure/".$repressive["risk_id"]);

			if ((isset($repressive["id"]) == false) || is_false(EDIT_OWN_MEASURE)) {
				$may_edit_measure = $this->model->may_edit_risk($repressive["risk_id"]);
			} else {
				$may_edit_measure = $this->model->may_edit_measure($repressive["id"], "repressive");
			}

			if ($may_edit_measure == false) {
				$this->view->add_tag("result", $this->language->module_text("error_measure_edit_not_allowed"));
				$this->user->log_action("unauthorized repressive measure edit attempt (id:%d, risk id:%d)", $repressive["id"], $repressive["risk_id"]);
				return false;
			}

			if (($risk = $this->model->get_risk($repressive["risk_id"])) == false) {
				$this->view->add_tag("result", $this->language->module_text("error_risK_not_found"), array("url" => "risk"));
				return false;
			}

			if (($owners = $this->model->get_measure_owners($repressive["risk_id"])) == false) {
				$this->view->add_tag("result", $this->language->module_text("error_measure_owner_not_available"), $url);
				return false;
			}

			if (isset($repressive["id"]) == false) {
				if (($effects = $this->model->get_risk_effects($repressive["risk_id"])) == false) {
					$this->view->add_tag("result", $this->language->module_text("error_effects_not_availble"), $url);
					return false;
				}
			} else {
				if (($repressive["risk_effect"] = $this->model->get_risk_effect($repressive["id"])) == false) {
					$this->view->add_tag("result", $this->language->global_text("error_database"), $url);
					return false;
				}
			}

			if (($controls = $this->model->get_controls()) === false) {
				$this->view->add_tag("result", $this->language->global_text("error_database"));
				return false;
			}

			$selected = isset($repressive["id"]) ? $this->model->get_most_used_standard("repressive", $repressive["id"]) : false;

			if (isset($repressive["owner_id"]) == false) {
				$repressive["owner_id"] = $risk["owner_id"];
			}

			$this->view->add_css("webui/jquery-ui.css");
			$this->view->add_javascript("webui/jquery-ui.js");
			$this->view->add_javascript("banshee/datepicker.js");
			$this->view->add_javascript("risk/measure_edit.js");

			$this->view->open_tag("edit_repressive", array("maxlength_title" => MAXLENGTH_MEASURE_TITLE));

			if (isset($repressive["id"]) == false) {
				$this->view->open_tag("effects");
				foreach ($effects as $effect) {
					$this->view->add_tag("effect", $effect["effect"]." | ".$effect["title"], array("id" => $effect["id"]));
				}
				$this->view->close_tag();
			}

			$this->add_pulldown_lists($owners);
			$this->add_control_standards($controls, $selected);

			if (isset($repressive["controls"]) == false) {
				$repressive["controls"] = array();
			}

			$this->view->record($risk, "risk");
			$this->view->record($repressive, "repressive", array(), true);

			$this->view->close_tag();
		}

		/* Log measure changes
		 */
		private function log_measure_changes($old, $new, $type) {
			$event = $type." measure '%s' updated (id:%d)";
			$args = array($new["title"], $new["id"]);

			if ($old["owner_id"] != $new["owner_id"]) {
				$event .= ", owner %s (id:%d) -> %s (id:%d)";

				if (($old_name = $this->model->get_user_fullname($old["owner_id"])) == false) {
					$old_name = "id:".$old["owner_id"];
				}

				if (($new_name = $this->model->get_user_fullname($new["owner_id"])) == false) {
					$new_name = "id:".$new["owner_id"];
				}

				array_push($args, $old_name, $old["owner_id"]);
				array_push($args, $new_name, $new["owner_id"]);
			}

			$measure_statuses = array_keys(MEASURE_STATUSES);

			if ($old["status"] != $new["status"]) {
				$event .= ", status %s -> %s";
				array_push($args, $measure_statuses[$old["status"]], $measure_statuses[$new["status"]]);
			}

			if ($old["deadline"] != $new["deadline"]) {
				$event .= ", deadline %s -> %s";
				array_push($args, $old["deadline"], $new["deadline"]);
			}

			$this->model->log_risk_event($new["risk_id"], $event, $args);
		}

		/* AJAX request
		 */
		private function handle_ajax_request() {
			if (isset($_POST["cause_id"])) {
				if (($cause = $this->model->get_cause($_POST["cause_id"])) != false) {
					$this->view->record($cause);
				}
			} else if (isset($_POST["effect_id"])) {
				if (($effect = $this->model->get_effect($_POST["effect_id"])) != false) {
					$this->view->record($effect);
				}
			}
		}

		/* Main
		 */
		public function execute() {
			$url = array("url" => "risk");

			if ($_SERVER["REQUEST_METHOD"] == "POST") {
				if ($this->page->ajax_request) {
					/* AJAX call
					 */
					$this->handle_ajax_request();
				} else if ($_POST["submit_button"] == $this->language->module_text("btn_preventive_save")) {
					/* Save preventive measure
					 */
					if ($this->model->preventive_okay($_POST) == false) {
						$this->show_preventive_form($_POST);
					} else if (isset($_POST["id"]) === false) {
						/* Create preventive measure
						 */
						if (($preventive_id = $this->model->create_preventive($_POST)) === false) {
							$this->view->add_message($this->language->module_text("error_measure_create"));
							$this->show_preventive_form($_POST);
						} else {
							$this->user->log_action("preventive measure %d created", $preventive_id);
							$this->model->log_risk_event($_POST["risk_id"], "preventive measure '%s' created (id:%d)", $_POST["title"], $preventive_id);
							$this->show_overview($_POST["risk_id"]);
						}
					} else {
						/* Update preventive measure
						 */
						if (($old = $this->model->get_preventive($_POST["id"])) == false) {
							$this->view->add_tag("result", $this->language->module_text("error_measure_update"), array("url" => "risk"));
							$this->user->log_action("unauthorized preventive measure update attempt (measure id:%d)", $_POST["id"]);
						} else if ($this->model->update_preventive($_POST) == false) {
							$this->view->add_message($this->language->module_text("error_measure_update"));
							$this->show_preventive_form($_POST);
						} else {
							$this->log_measure_changes($old, $_POST, "preventive");
							$this->user->log_action("preventive measure %d updated", $_POST["id"]);
							$this->show_overview($_POST["risk_id"]);
						}
					}
				} else if ($_POST["submit_button"] == $this->language->module_text("btn_detective_save")) {
					/* Save detective measure
					 */
					if ($this->model->detective_okay($_POST) == false) {
						$this->show_detective_form($_POST);
					} else if (isset($_POST["id"]) === false) {
						/* Create detective measure
						 */
						if (($detective_id = $this->model->create_detective($_POST)) === false) {
							$this->view->add_message($this->language->module_text("error_measure_create"));
							$this->show_detective_form($_POST);
						} else {
							$this->user->log_action("detective measure %d created", $detective_id);
							$this->model->log_risk_event($_POST["risk_id"], "detective measure '%s' created (id:%d)", $_POST["title"], $detective_id);
							$this->show_overview($_POST["risk_id"]);
						}
					} else {
						/* Update detective measure
						 */
						if (($old = $this->model->get_detective($_POST["id"])) == false) {
							$this->view->add_tag("result", $this->language->module_text("error_measure_update"), array("url" => "risk"));
							$this->user->log_action("unauthorized detective measure update attempt (measure id:%d)", $_POST["id"]);
						} else if ($this->model->update_detective($_POST) == false) {
							$this->view->add_message($this->language->module_text("error_measure_update"));
							$this->show_detective_form($_POST);
						} else {
							$this->log_measure_changes($old, $_POST, "detective");
							$this->user->log_action("detective measure %d updated", $_POST["id"]);
							$this->show_overview($_POST["risk_id"]);
						}
					}
				} else if ($_POST["submit_button"] == $this->language->module_text("btn_repressive_save")) {
					/* Save repressive measure
					 */
					if ($this->model->repressive_okay($_POST) == false) {
						$this->show_repressive_form($_POST);
					} else if (isset($_POST["id"]) === false) {
						/* Create repressive measure
						 */
						if (($repressive_id = $this->model->create_repressive($_POST)) === false) {
							$this->view->add_message($this->language->module_text("error_measure_create"));
							$this->show_repressive_form($_POST);
						} else {
							$this->user->log_action("repressive measure %d created", $repressive_id);
							$this->model->log_risk_event($_POST["risk_id"], "repressive measure '%s' created (id:%d)", $_POST["title"], $repressive_id);
							$this->show_overview($_POST["risk_id"]);
						}
					} else {
						/* Update repressive measure
						 */
						if (($old = $this->model->get_repressive($_POST["id"])) == false) {
							$this->view->add_tag("result", $this->language->module_text("error_measure_update"), array("url" => "risk"));
							$this->user->log_action("unauthorized repressive measure update attempt (measure id:%d)", $_POST["id"]);
						} else if ($this->model->update_repressive($_POST) === false) {
							$this->view->add_message($this->language->module_text("error_measure_update"));
							$this->show_repressive_form($_POST);
						} else {
							$this->log_measure_changes($old, $_POST, "repressive");
							$this->user->log_action("repressive measure %d updated", $_POST["id"]);
							$this->show_overview($_POST["risk_id"]);
						}
					}
				} else if ($_POST["submit_button"] == $this->language->module_text("btn_preventive_delete")) {
					/* Delete preventive measure
					 */
					if (($risk_id = $this->model->delete_preventive($_POST["id"])) === false) {
						$this->view->add_message($this->language->module_text("error_measure_delete"));
						$this->show_preventive_form($_POST);
					} else {
						$this->user->log_action("preventive measure %d deleted", $_POST["id"]);
						$this->model->log_risk_event($risk_id, "preventive measure '%s' deleted (id:%d)", $_POST["title"], $_POST["id"]);
						$this->show_overview($_POST["risk_id"]);
					}
				} else if ($_POST["submit_button"] == $this->language->module_text("btn_detective_delete")) {
					/* Delete detective measure
					 */
					if (($risk_id = $this->model->delete_detective($_POST["id"])) === false) {
						$this->view->add_message($this->language->module_text("error_measure_delete"));
						$this->show_detective_form($_POST);
					} else {
						$this->user->log_action("detective measure %d deleted", $_POST["id"]);
						$this->model->log_risk_event($risk_id, "detective measure '%s' deleted (id:%d)", $_POST["title"], $_POST["id"]);
						$this->show_overview($_POST["risk_id"]);
					}
				} else if ($_POST["submit_button"] == $this->language->module_text("btn_repressive_delete")) {
					/* Delete repressive measure
					 */
					if (($risk_id = $this->model->delete_repressive($_POST["id"])) === false) {
						$this->view->add_message($this->language->module_text("error_measure_delete"));
						$this->show_repressive_form($_POST);
					} else {
						$this->user->log_action("repressive measure %d deleted", $_POST["id"]);
						$this->model->log_risk_event($risk_id, "repressive measure '%s' deleted (id:%d)", $_POST["title"], $_POST["id"]);
						$this->show_overview($_POST["risk_id"]);
					}
				} else if (isset($_POST["risk_id"])) {
					$this->show_overview($_POST["risk_id"]);
				} else {
					$this->view->add_tag("result", $this->language->module_text("error_no_risk_id"), $url);
				}
			} else if ($this->page->parameter_numeric(0)) {
				if ($this->page->parameter_value(1, "preventive")) {
					/* New preventive measure
					 */
					$preventive = array(
						"risk_id"  => $this->page->parameters[0],
						"deadline" => date("Y-m-d", strtotime(self::DEFAULT_DEADLINE)),
						"controls" => array());
					$this->show_preventive_form($preventive);
				} else if ($this->page->parameter_value(1, "detective")) {
					/* New detective measure
					 */
					$detective = array(
						"risk_id"  => $this->page->parameters[0],
						"deadline" => date("Y-m-d", strtotime(self::DEFAULT_DEADLINE)),
						"controls" => array());
					$this->show_detective_form($detective);
				} else if ($this->page->parameter_value(1, "repressive")) {
					/* New repressive measure
					 */
					$repressive = array(
						"risk_id"  => $this->page->parameters[0],
						"deadline" => date("Y-m-d", strtotime(self::DEFAULT_DEADLINE)),
						"controls" => array());
					$this->show_repressive_form($repressive);
				} else {
					/* show overview
					 */
					$this->show_overview($this->page->parameters[0]);
				}
			} else if ($this->page->parameter_numeric(1)) {
				if ($this->page->parameter_value(0, "preventive")) {
					/* Edit preventive
					 */
					if (($preventive = $this->model->get_preventive($this->page->parameters[1])) == false) {
						$this->view->add_tag("result", $this->language->module_text("error_measure_not_found"), $url);
						$this->user->log_action("unauthorized preventive measure edit attempt (id:%d)", $this->page->parameters[1]);
					} else if (($preventive["controls"] = $this->model->get_measure_controls("preventive", $preventive["id"])) === false) {
						$this->view->add_tag("result", $this->language->global_text("error_database"), $url);
						return false;
					} else {
						$this->show_preventive_form($preventive);
					}
				} else if ($this->page->parameter_value(0, "detective")) {
					/* Edit detective
					 */
					if (($detective = $this->model->get_detective($this->page->parameters[1])) == false) {
						$this->view->add_tag("result", $this->language->module_text("error_measure_not_found"), $url);
						$this->user->log_action("unauthorized detective measure edit attempt (id:%d)", $this->page->parameters[1]);
					} else if (($detective["controls"] = $this->model->get_measure_controls("detective", $detective["id"])) === false) {	
						$this->view->add_tag("result", $this->language->global_text("error_database"), $url);
						return false;
					} else {
						$this->show_detective_form($detective);
					}
				} else if ($this->page->parameter_value(0, "repressive")) {
					/* Edit repressive
					 */
					if (($repressive = $this->model->get_repressive($this->page->parameters[1])) == false) {
						$this->view->add_tag("result", $this->language->module_text("error_measure_not_found"), $url);
						$this->user->log_action("unauthorized repressive measure edit attempt (id:%d)", $this->page->parameters[1]);
					} else if (($repressive["controls"] = $this->model->get_measure_controls("repressive", $repressive["id"])) === false) {
						$this->view->add_tag("result", $this->language->global_text("error_database"), $url);
						return false;
					} else {
						$this->show_repressive_form($repressive);
					}
				}
			} else {
				/* No risk id supplied
				 */
				$this->view->add_tag("result", $this->language->module_text("error_no_risk_id"), $url);
			}
		}
	}
?>
