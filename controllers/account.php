<?php
	/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
	 * This file is part of the Banshee PHP framework
	 * https://www.banshee-php.org/
	 *
	 * Licensed under The MIT License
	 */

	class account_controller extends Banshee\controller {
		private function view_user($user_id) {
			if (($user = $this->model->get_user($user_id)) == false) {
				$this->view->add_tag("result", $this->language->module_text("error_user_not_found"), array("url" => ""));
				return;
			}

			$this->view->title = $user["fullname"];

			$this->view->open_tag("view");
			$this->view->record($user, "user");
			$this->view->add_tag("previous", $this->page->previous);
			$this->view->close_tag();
		}

		private function show_account_form($account = null) {
			if ($account === null) {
				$account = array(
					"fullname"             => $this->user->fullname,
					"email"                => $this->user->email,
					"authenticator_secret" => $this->user->authenticator_secret);
			}

			if (($organisation = $this->model->get_organisation()) === false) {
				$this->view->add_tag("result", $this->language->global_text("error_database"));
				return false;
			}

			if (is_true(USE_AUTHENTICATOR)) {
				$this->view->add_help_button();

				$this->view->add_javascript("webui/jquery-ui.js");
				$this->view->add_javascript("account.js");

				$this->view->add_css("webui/jquery-ui.css");
			}

			$this->view->open_tag("edit", array(
				"authenticator" => show_boolean(USE_AUTHENTICATOR),
				"logout"        => LOGOUT_MODULE));

			$this->view->add_tag("username", $this->user->username);
			$this->view->add_tag("organisation", $organisation);
			foreach (array_keys($account) as $key) {
				$this->view->add_tag($key, $account[$key]);
			}

			/* Action log
			 */
			if (($actionlog = $this->model->last_account_logs()) !== false) {
				$this->view->open_tag("actionlog");
				foreach ($actionlog as $log) {
					$this->view->record($log, "log");
				}
				$this->view->close_tag();
			}

			$this->view->close_tag();
		}

		private function show_totp() {
			if ($this->page->parameter_value(1) == false) {
				return false;
			}

			$totp = new \Banshee\TOTP($this->view, $this->settings);
			$totp->add_to_view($this->user->username, $this->page->parameters[1]);
		}

		public function execute() {
			if ($this->user->logged_in == false) {
				$this->view->add_tag("result", $this->language->module_text("error_not_logged_in"), array("url" => $this->settings->start_page));
				return;
			}

			if ($this->page->parameter_value(0, "totp")) {
				$this->show_totp();
				return;
			}

			$this->view->description = "Account";
			$this->view->keywords = "account";
			$this->view->title = $this->language->module_text("account");

			if ($this->user->status == USER_STATUS_ACTIVE) {
				if ($this->page->parameter_numeric(0)) {
					$this->view_user($this->page->parameters[0]);
					return;
				}
			}

			if ($this->user->status == USER_STATUS_CHANGEPWD) {
				$this->view->add_message($this->language->module_text("change_password"));
			}

			if (isset($_SESSION["account_next"]) == false) {
				if (($this->page->pathinfo[0] ?? null) == ACCOUNT_MODULE) {
					$_SESSION["account_next"] = $this->settings->start_page;
				} else {
					$_SESSION["account_next"] = substr($_SERVER["REQUEST_URI"], 1);
				}
			}

			if ($this->page->parameter_value(0, "authenticator") && $this->page->ajax_request) {
				$authenticator = new \Banshee\authenticator;
				$this->view->add_tag("secret", $authenticator->create_secret());
			} else if ($_SERVER["REQUEST_METHOD"] == "POST") {
				/* Update account
				 */
				if ($_POST["submit_button"] == $this->language->module_text("btn_update_account")) {
					if ($this->model->account_okay($_POST) == false) {
						$this->show_account_form($_POST);
					} else if ($this->model->update_account($_POST) === false) {
						$this->view->add_tag("result", $this->language->module_text("error_account_update"), array("url" => ACCOUNT_MODULE));
					} else {
						$this->view->add_tag("result", $this->language->module_text("account_updated"), array("url" => $_SESSION["account_next"]));
						$this->user->log_action("account updated");
						unset($_SESSION["account_next"]);
					}
				} else if ($_POST["submit_button"] == $this->language->module_text("btn_delete_account")) {
					if ($this->model->delete_okay($_POST) == false) {
						$this->show_account_form();
					} else if ($this->model->delete_account() == false) {
						$this->view->add_message($this->language->module_text("error_account_delete"));
						$this->show_account_form();
					} else {
						$this->view->add_tag("result", $this->language->module_text("account_deleted"), array("url" => ""));
						$this->user->logout();
					}
				}
			} else {
				$this->show_account_form();
			}
		}
	}
?>
