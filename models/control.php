<?php
	class control_model extends register_model {
		public function get_standards() {
			$query = "select * from control_standards order by name";

			return $this->db->execute($query);
		}

		public function get_controls($standard_id) {
			$query = "select id,number,title from controls where control_standard_id=%d";

			return $this->db->execute($query, $standard_id);
		}

		public function get_most_used_standard() {
			$query = "select control_standard_id, sum(count) as count from (".
				"select c.control_standard_id, count(*) as count ".
					"from controls c, control_preventive cp, measures_preventive mp, risk_cause rc, risks r ".
					"where c.id=cp.control_id and cp.measure_preventive_id=mp.risk_cause_id and rc.risk_id=r.id and r.organisation_id=%d ".
					"group by c.control_standard_id ".
				"union ".
				"select c.control_standard_id, count(*) as count ".
					"from controls c, control_detective cd, measures_detective md, risks r ".
					"where c.id=cd.control_id and cd.measure_detective_id=md.id and md.risk_id=r.id and r.organisation_id=%d ".
					"group by c.control_standard_id ".
				"union ".
				"select c.control_standard_id, count(*) as count ".
					"from controls c, control_repressive rr, measures_repressive mr, risk_effect re, risks r ".
					"where c.id=rr.control_id and rr.measure_repressive_id=mr.id and mr.risk_effect_id=re.id and re.risk_id=r.id and r.organisation_id=%d ".
					"group by c.control_standard_id".
				") standards group by control_standard_id";

			if (($result = $this->db->execute($query, $this->user->organisation_id, $this->user->organisation_id, $this->user->organisation_id)) == false) {
				return false;
			}

			return $result[0]["control_standard_id"];
		}

		public function get_control_measures() {
			$args = array();

			/* Preventive
			 */
			$where = array("c.measure_preventive_id=m.id", "m.risk_cause_id=rc.id", "rc.risk_id=r.id", "r.organisation_id=%d");
			array_push($args, "preventive", $this->organisation_id);

			$this->query_measure_access($where, $args);

			$preventive = "select control_id, m.id, m.title, status, %s as type ".
			              "from control_preventive c, measures_preventive m, risk_cause rc, risks r ".
			              "where ".implode(" and ", $where);

			/* Detective
			 */
			$where = array("c.measure_detective_id=m.id", "m.risk_id=r.id", "r.organisation_id=%d");
			array_push($args, "detective", $this->organisation_id);

			$this->query_measure_access($where, $args);

			$detective =  "select control_id, m.id, m.title, status, %s as type ".
			              "from control_detective c, measures_detective m, risks r ".
			              "where ".implode(" and ", $where);

			/* Repressive
			 */
			$where = array("c.measure_repressive_id=m.id", "m.risk_effect_id=re.id", "re.risk_id=r.id", "r.organisation_id=%d");
			array_push($args, "repressive", $this->organisation_id);

			$this->query_measure_access($where, $args);

			$repressive = "select control_id, m.id, m.title, status, %s as type ".
			              "from control_repressive c, measures_repressive m, risk_effect re, risks r ".
			              "where ".implode(" and ", $where);

			/* Query
			 */
			$query = implode(" union ", array($preventive, $detective, $repressive));

			if (($result = $this->db->execute($query, $args)) === false) {
				return false;
			}

			$measures = array();
			foreach ($result as $item) {
				$control_id = $item["control_id"];

				if (isset($measures[$control_id]) == false) {
					$measures[$control_id] = array();
				}

				unset($item["control_id"]);

				$item["title"] = $this->decrypt($item["title"]);

				array_push($measures[$control_id], $item);
			}

			return $measures;
		}
	}
?>
