<?php
	/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
	 * This file is part of the Banshee PHP framework
	 * https://gitlab.com/hsleisink/banshee/
	 *
	 * Licensed under The MIT License
	 */

	class measure_calendar_model extends register_model {
		public function get_measure_deadlines_for_month($month, $year) {
			$begin_timestamp = $this->monday_before($month, $year);
			$begin = date("Y-m-d 00:00:00", $begin_timestamp - 6 * DAY);

			$end_timestamp = $this->sunday_after($month, $year);
			$end = date("Y-m-d 23:59:59", $end_timestamp + 6 * DAY);

			$where = array("m.risk_id=r.id", "r.organisation_id=%d", "deadline>=%s", "deadline<=%s");
			$args = array($this->organisation_id, $begin, $end);

			$this->query_measure_access($where, $args);

			$query = "select m.id, m.title, type, UNIX_TIMESTAMP(m.deadline) as deadline, m.status ".
					 "from measures m, risks r where ".implode(" and ", $where);

			array_push($args);

			if (($measures = $this->db->execute($query, $args)) === false) {
				return false;
			}

			if (is_true(ENCRYPT_DATA)) {
				foreach ($measures as $i => $measure) {
					$measures[$i]["title"] = $this->decrypt($measure["title"]);
				}
			}

			return $measures;
		}

		public function monday_before($month, $year) {
			$timestamp = strtotime($year."-".$month."-01 00:00:00");
			$dow = date("N", $timestamp) - 1;
			$timestamp -= $dow * DAY;

			return $timestamp;
		}

		public function sunday_after($month, $year) {
			$timestamp = strtotime($year."-".$month."-01 00:00:00 +1 month") - DAY;
			if (($dow = date("N", $timestamp)) < 7) {
				$timestamp += (7 - $dow) * DAY;
			}

			return $timestamp;
		}
	}
?>
