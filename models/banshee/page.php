<?php
	/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
	 * This file is part of the Banshee PHP framework
	 * https://www.banshee-php.org/
	 *
	 * Licensed under The MIT License
	 */

	class banshee_page_model extends Banshee\model {
		private function get_page_for_language($url, $language = null) {
			static $pages = null;

			if ($pages === null) {
				$query = "select * from pages where url=%s";
				$pages = $this->db->execute($query, $url);
			}

			if ($pages == false) {
				return false;
			}

			foreach ($pages as $page) {
				if ($page["language"] == $language) {
					return $page;
				}

				if ($language === null) {
					return $page;
				}
			}

			return false;
		}

		public function get_page($url) {
			if (($page = $this->get_page_for_language($url, $this->view->language)) != false) {
				return $page;
			}

			$languages = array_keys(config_array(SUPPORTED_LANGUAGES));
			if (($page = $this->get_page_for_language($url, $languages[0])) != false) {
				return $page;
			}

			if (($page = $this->get_page_for_language($url)) != false) {
				return $page;
			}

			return false;
		}
	}
?>
