<?php
	/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
	 * This file is part of the Banshee PHP framework
	 * https://gitlab.com/hsleisink/banshee/
	 *
	 * Licensed under The MIT License
	 */

	class issue_calendar_model extends register_model {
		public function get_task_deadlines_for_month($month, $year) {
			$begin_timestamp = $this->monday_before($month, $year);
			$begin = date("Y-m-d 00:00:00", $begin_timestamp - 6 * DAY);

			$end_timestamp = $this->sunday_after($month, $year);
			$end = date("Y-m-d 23:59:59", $end_timestamp + 6 * DAY);

			$where = array("t.issue_id=i.id", "t.type=i.type", "i.measure_id=m.id", "i.type=m.type", "m.risk_id=r.id",
			               "r.organisation_id=%d", "t.deadline>=%s", "t.deadline<=%s", "done=%d");
			$args = array($this->organisation_id, $begin, $end, NO);

			$this->query_measure_access($where, $args);

			$query = "select distinct i.id, t.title, i.type, UNIX_TIMESTAMP(t.deadline) as deadline ".
					 "from issue_tasks t, issues i, measures m, risks r where ".implode(" and ", $where);

			if (($actions = $this->db->execute($query, $args)) === false) {
				return false;
			}

			if (is_true(ENCRYPT_DATA)) {
				foreach ($actions as $i => $action) {
					$actions[$i]["title"] = $this->decrypt($action["title"]);
				}
			}

			return $actions;
		}

		public function monday_before($month, $year) {
			$timestamp = strtotime($year."-".$month."-01 00:00:00");
			$dow = date("N", $timestamp) - 1;
			$timestamp -= $dow * DAY;

			return $timestamp;
		}

		public function sunday_after($month, $year) {
			$timestamp = strtotime($year."-".$month."-01 00:00:00 +1 month") - DAY;
			if (($dow = date("N", $timestamp)) < 7) {
				$timestamp += (7 - $dow) * DAY;
			}

			return $timestamp;
		}
	}
?>
