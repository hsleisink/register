<?php
	class cms_cause_model extends Banshee\model {
		public function get_causes() {
			$query = "select * from causes where organisation_id=%d";

			if (($causes = $this->db->execute($query, $this->user->organisation_id)) === false) {
				return false;
			}

			if (is_true(ENCRYPT_DATA)) {
				foreach ($causes as $i => $cause) {
					$causes[$i]["cause"] = $this->decrypt($cause["cause"]);
				}
			}

			usort($causes, function($a, $b) {
				return strcmp($a["cause"], $b["cause"]);
			});

			return $causes;
		}

		public function get_cause($cause_id) {
			$query = "select * from causes where id=%d and organisation_id=%d";

			if (($result = $this->db->execute($query, $cause_id, $this->user->organisation_id)) == false) {
				return false;
			}
			$cause = $result[0];

			if (is_true(ENCRYPT_DATA)) {
				$cause["cause"] = $this->decrypt($cause["cause"]);
			}

			return $cause;
		}

		public function save_okay($cause) {
			if (isset($cause["id"])) {
				if ($this->get_cause($cause["id"]) == false) {
					$this->view->add_message("Cause not found.");
					$this->user->log_action("unauthorized delete attempt of cause %d", $cause["id"]);
					return false;
				}
			}

			$result = true;

			if (trim($cause["cause"]) == "") {
				$this->view->add_message("Specify the risk cause.");
				$result = false;
			}

			return $result;
		}

		public function create_cause($cause) {
			$keys = array("id", "organisation_id", "cause");

			$cause["id"] = null;
			$cause["organisation_id"] = $this->user->organisation_id;
			$cause["cause"] = substr($cause["cause"], 0, MAXLENGTH_BOWTIE_TITLE);

			if (is_true(ENCRYPT_DATA)) {
				$cause["cause"] = $this->encrypt($cause["cause"]);
			}

			return $this->db->insert("causes", $cause, $keys);
		}

		public function update_cause($cause) {
			$keys = array("cause");

			$cause["cause"] = substr($cause["cause"], 0, MAXLENGTH_BOWTIE_TITLE);

			if (is_true(ENCRYPT_DATA)) {
				$cause["cause"] = $this->encrypt($cause["cause"]);
			}

			return $this->db->update("causes", $cause["id"], $cause, $keys);
		}

		public function delete_okay($cause) {
			if ($this->get_cause($cause["id"]) == false) {
				$this->view->add_message("Cause not found.");
				$this->user->log_action("unauthorized delete attempt of cause %d", $cause["id"]);
				return false;
			}

			$result = true;

			$query = "select count(*) as count from risk_cause where cause_id=%d";
			if (($count = $this->db->execute($query, $cause["id"])) === false) {
				$this->view->add_message("Database error.");
				$result = false;
			} else if ($count[0]["count"] > 0) {
				$this->view->add_message("This cause is in use.");
				$result = false;
			}

			return $result;
		}

		public function delete_cause($cause_id) {
			return $this->db->delete("causes", $cause_id);
		}
	}
?>
