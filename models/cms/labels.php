<?php
	class cms_labels_model extends register_model {
		public function labels_okay($labels) {
			$result = true;

			for ($i = 0; $i < $this->matrix_size; $i++) {
				if (trim($labels["horizontal"][$i] ?? "") == "") {
					$this->view->add_message($this->matrix_horizontal." label ".($i + 1)." can't be empty.");
					$result = false;
				}

				if (trim($labels["vertical"][$i] ?? "") == "") {
					$this->view->add_message($this->matrix_vertical." label ".($i + 1)." can't be empty.");
					$result = false;
				}
			}

			for ($i = 0; $i < $this->risk_levels; $i++) {
				if (trim($labels["level"]["label"][$i] ?? "") == "") {
					$this->view->add_message("Risk level label ".($i + 1)." can't be empty.");
					$result = false;
				}

				$color = $labels["level"]["color"][$i] ?? "";
				if ((strlen($color) != 7) || (substr($color, 0, 1) != "#")) {
					$this->view->add_message("Invalid risk level color ".($i + 1).".");
					$result = false;
				}
			}

			return $result;
		}

		public function save_labels($labels) {
			$queries = array();

			$query = "update organisations set impact_axis=%s where id=%s";
			array_push($queries, array($query, $labels["impact_axis"], $this->user->organisation_id));

			/* Matrixe labels
			 */
			foreach (array("horizontal", "vertical") as $type) {
				$set = implode(", ", array_fill(1, $this->matrix_size, "%S=%s"));
				$query = "update %S set ".$set." where organisation_id=%d";

				$values = array("labels_".$type);
				for ($i = 0; $i < $this->matrix_size; $i++) {
					array_push($values, "label".($i + 1));
					array_push($values, substr(trim($labels[$type][$i]), 0, 15));
				}
				array_push($values, $this->user->organisation_id);
				array_push($queries, array($query, $values));
			}

			/* Risk level labels
			 */
			$set = implode(", ", array_fill(1, $this->risk_levels * 2, "%S=%s"));
			$query = "update labels_risk set ".$set." where organisation_id=%d";

			$values = array();
			for ($i = 0; $i < $this->risk_levels; $i++) {
				array_push($values, "label".($i + 1));
				array_push($values, substr(trim($labels["level"]["label"][$i]), 0, 15));
				array_push($values, "color".($i + 1));
				array_push($values, substr(trim($labels["level"]["color"][$i]), 0, 7));
			}
			array_push($values, $this->user->organisation_id);
			array_push($queries, array($query, $values));

			return $this->db->transaction($queries) !== false;
		}
	}
?>
