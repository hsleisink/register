<?php
	class cms_tag_category_model extends Banshee\model {
		private $columns = array();

		public function get_categories() {
			$query = "select * from tag_categories where organisation_id=%d";

			if (($categories = $this->db->execute($query, $this->user->organisation_id)) === false) {
				return false;
			}

			if (is_true(ENCRYPT_DATA)) {
				foreach ($categories as $i => $category) {
					$categories[$i]["name"] = $this->decrypt($category["name"]);
				}
			}

			usort($categories, function($a, $b) {
				return strcmp($a["name"], $b["name"]);
			});

			return $categories;
		}

		public function get_category($category_id) {
			$query = "select * from tag_categories where id=%d and organisation_id=%d";

			if (($result = $this->db->execute($query, $category_id, $this->user->organisation_id)) == false) {
				return false;
			}
			$category = $result[0];

			if (is_true(ENCRYPT_DATA)) {
				$category["name"] = $this->decrypt($category["name"]);
			}

			return $category;
		}

		public function save_oke($category) {
			if (isset($category["id"])) {
				if ($this->get_category($category["id"]) == false) {
					$this->view->add_message("Category not found.");
					$this->user->log_action("unauthorized update attempt of tag category %d", $category["id"]);
					return false;
				}
			}

			$result = true;

			if (trim($category["name"]) == "") {
				$this->view->add_message("Enter the category name.");
				$result = false;
			} else {
				$query = "select count(*) as count from tag_categories ".
				         "where organisation_id=%d and name=%s";
				$args = array($this->user->organisation_id, trim($category["name"]));

				if (isset($category["id"])) {
					$query .= " and id!=%d";
					array_push($args, $category["id"]);
				}

				if (($categories = $this->db->execute($query, $args)) === false) {
					$result = false;
				} else if ($categories[0]["count"] > 0) {
					$this->view->add_message("This tag category already exists.");
					$result = false;
				}
			}

			return $result;
		}

		public function create_category($category) {
			$keys = array("id", "organisation_id", "name");

			$category["id"] = null;
			$category["name"] = trim($category["name"]);
			$category["organisation_id"] = $this->user->organisation_id;

			if (is_true(ENCRYPT_DATA)) {
				$category["name"] = $this->encrypt($category["name"]);
			}

			return $this->db->insert("tag_categories", $category, $keys) !== false;
		}

		public function update_category($category) {
			$keys = array("name");

			$category["name"] = trim($category["name"]);

			if (is_true(ENCRYPT_DATA)) {
				$category["name"] = $this->encrypt($category["name"]);
			}

			return $this->db->update("tag_categories", $category["id"], $category, $keys) !== false;
		}

		public function delete_oke($category) {
			$result = true;

			if ($this->get_category($category["id"]) == false) {
				$this->view->add_message("Category not found.");
				$this->user->log_action("unauthorized delete attempt of tag category %d", $category["id"]);
				$result = false;
			}

			return $result;
		}

		public function delete_category($category_id) {
			unset($_SESSION["tag_category"]);

			$queries = array(
				array("delete from tags where tag_category_id=%d", $category_id),
				array("delete from tag_categories where id=%d", $category_id));

			return $this->db->transaction($queries) !== false;
		}
	}
?>
