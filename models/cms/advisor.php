<?php
	class cms_advisor_model extends Banshee\model {
		public function get_advisors() {
			$query = "select a.id, u.fullname, o.name as organisation, a.crypto_key ".
			         "from advisors a, users u ".
			         "left join organisations o on o.id=u.organisation_id ".
			         "where a.user_id=u.id and a.organisation_id=%d";

			return $this->db->execute($query, $this->user->organisation_id);
		}

		public function add_advisor($user) {
			$query = "select * from users u, user_role r where u.id=r.user_id and r.role_id=%d and (username=%s or email=%s)";

			if (($users = $this->db->execute($query, $this->settings->role_id_advisor, $user, $user)) == false) {
				$this->view->add_message("User not found.");
				return false;
			}
			$advisor = $users[0];

			if ($advisor["organisation_id"] == $this->user->organisation_id) {
				$this->view->add_message("That user belongs to your organisation.");
				return false;
			}

			$query = "select count(*) as count from advisors where user_id=%d and organisation_id=%d";

			if (($advisors = $this->db->execute($query, $advisor["id"], $this->user->organisation_id)) === false) {
				$this->view->add_message("Database error.");
				return false;
			}

			if ($advisors[0]["count"] > 0) {
				$this->view->add_message("That user is already an advisor for your organisation.");
				return false;
			}

			$rsa = new \Banshee\Protocol\RSA($advisor["public_key"]);
			$cookie = new \Banshee\secure_cookie($this->settings);

			$data = array(
				"id"              => null,
				"organisation_id" => $this->user->organisation_id,
				"user_id"         => $advisor["id"],
				"crypto_key"      => $rsa->encrypt_with_public_key($cookie->crypto_key));

			return $this->db->insert("advisors", $data) !== false;

			return true;
		}

		public function activate_advisor($id) {
			$query = "select user_id, public_key from advisors a, users u ".
			         "where a.user_id=u.id and a.id=%d and a.organisation_id=%d and a.crypto_key is null";
			if (($result = $this->db->execute($query, $id, $this->user->organisation_id)) === false) {
				return false;
			}
			$advisor = $result[0];

			$rsa = new \Banshee\Protocol\RSA($advisor["public_key"]);
			$cookie = new \Banshee\secure_cookie($this->settings);
			$data = array("crypto_key" => $rsa->encrypt_with_public_key($cookie->crypto_key));

			$this->user->log_action("accepted advisor request for ".$advisor["user_id"]);

			return $this->db->update("advisors", $id, $data) !== false;
		}

		public function delete_advisor($id) {
			$query = "delete from advisors where id=%d and organisation_id=%d";

			return $this->db->query($query, $id, $this->user->organisation_id) !== false;
		}
	}
?>
