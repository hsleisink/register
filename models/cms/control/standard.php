<?php
	class cms_control_standard_model extends Banshee\model {
		private $columns = array();

		public function get_standards() {
			$query = "select * from control_standards order by name";

			return $this->db->execute($query);
		}

		public function get_standard($standard_id) {
			return $this->db->entry("control_standards", $standard_id);
		}

		public function save_oke($standard) {
			$result = true;

			if (trim($standard["name"]) == "") {
				$this->view->add_message("Specify the standard name.");
				$result = false;
			}

			return $result;
		}

		public function create_standard($standard) {
			$keys = array("id", "name");

			$standard["id"] = null;
			$standard["name"] = trim($standard["name"]);

			return $this->db->insert("control_standards", $standard, $keys) !== false;
		}

		public function update_standard($standard) {
			$keys = array("name");

			$standard["name"] = trim($standard["name"]);

			return $this->db->update("control_standards", $standard["id"], $standard, $keys) !== false;
		}

		public function delete_oke($standard) {
			$result = true;

			$query = "select ".
				"(select count(*) from control_preventive p, controls c where p.control_id=c.id and control_standard_id=%d) + ".
				"(select count(*) from control_detective d, controls c where d.control_id=c.id and control_standard_id=%d) + ".
				"(select count(*) from control_repressive r, controls c where r.control_id=c.id and control_standard_id=%d) as count";

			if (($count = $this->db->execute($query, $standard["id"], $standard["id"], $standard["id"])) == false) {
				return false;
			}

			if ($count[0]["count"] > 0) {
				$this->view->add_message("This control standard is in use.");
				return false;
			}


			return $result;
		}

		public function delete_standard($standard_id) {
			unset($_SESSION["control_standard"]);

			$queries = array(
				array("delete from controls where control_standard_id=%d", $standard_id),
				array("delete from control_standards where id=%d", $standard_id));

			return $this->db->transaction($queries) !== false;
		}
	}
?>
