<?php
	/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
	 * This file is part of the Banshee PHP framework
	 * https://www.banshee-php.org/
	 *
	 * Licensed under The MIT License
	 */

	class cms_organisation_model extends Banshee\model {
		public function count_organisations() {
			$query = "select count(*) as count from organisations";

			if (($result = $this->db->execute($query)) == false) {
				return false;
			}

			return $result[0]["count"];
		}

		public function get_organisations($offset, $limit) {
			$query = "select * from organisations order by name limit %d,%d";

			return $this->db->execute($query, $offset, $limit);
		}

		public function get_organisation($organisation_id) {
			return $this->db->entry("organisations", $organisation_id);
		}

		public function get_users($organisation_id) {
			$query = "select * from users where organisation_id=%d order by fullname";

			return $this->db->execute($query, $organisation_id);
		}

		public function save_okay($organisation) {
			$result = true;

			if (trim($organisation["name"]) == "") {
				$this->view->add_message("Empty name is not allowed.");
				$result = false;
			}

			if (($check = $this->db->entry("organisations", $organisation["name"], "name")) === false) {
				$this->view->add_message("Database error.");
				$result = false;
			} else if ($check != false) {
				if ($check["id"] != $organisation["id"]) {
					$this->view->add_message("Organisation name already exists.");
					$result = false;
				}
			}

			if (isset($organisation["id"]) == false) {
				if (is_true(ENCRYPT_DATA)) {
					$this->view->add_message("Can't create organisations with database encryption enabled.");
					$result = false;
				}

				if (trim($organisation["matrix_vertical"]) == "") {
					$this->view->add_message("Empty vertical label is not allowed.");
					$result = false;
				}

				if (trim($organisation["matrix_horizontal"]) == "") {
					$this->view->add_message("Empty horizontal label is not allowed.");
					$result = false;
				}

				$size = (int)$organisation["matrix_size"];
				if (($size < MATRIX_MIN_SIZE) || ($size > MATRIX_MAX_SIZE)) {
					$this->view->add_message("Invalid matrix size.");
					$result = false;
				}

				$levels = (int)$organisation["risk_levels"];
				if (($levels < MATRIX_MIN_SIZE) || ($levels > $size)) {
					$this->view->add_message("Invalid amount of risk levels.");
					$result = false;
				}
			}

			return $result;
		}

		public function add_register_settings($organisation_id, $organisation) {
			/* Labels
			 */
			$horizontal = array(
				"id"              => null,
				"organisation_id" => $organisation_id);

			$label = strtolower($organisation["matrix_horizontal"]);
			for ($i = 1; $i <= 7; $i++) {
				$horizontal["label".$i] = $label." ".$i;
			}

			if ($this->db->insert("labels_horizontal", $horizontal) == false) {
				return false;
			}

			$vertical = array(
				"id"              => null,
				"organisation_id" => $organisation_id);

			$label = strtolower($organisation["matrix_vertical"]);
			for ($i = 1; $i <= 7; $i++) {
				$vertical["label".$i] = $label." ".$i;
			}

			if ($this->db->insert("labels_vertical", $vertical) == false) {
				return false;
			}

			/* Levels
			 */
			$risk = array(
				"id"              => null,
				"organisation_id" => $organisation_id);
				$matrix["column".$i] = 1;

			for ($i = 1; $i <= 7; $i++) {
				$risk["label".$i] = "risk level ".$i;
				$risk["color".$i] = "#f0f0f0";
			}

			if ($this->db->insert("labels_risk", $risk) == false) {
				return false;
			}

			/* Matrix
			 */
			$matrix = array(
				"id"              => null,
				"organisation_id" => $organisation_id);
			for ($i = 1; $i <= 7; $i++) {
				$matrix["column".$i] = 1;
			}
			for ($i = 0; $i < $organisation["matrix_size"]; $i++) {
				if ($this->db->insert("matrix", $matrix) == false) {
					return false;
				}
			}

			return true;
		}

		public function create_organisation($organisation, $register = false) {
			$keys = array("id", "name", "invitation_code", "matrix_horizontal", "matrix_vertical",
			              "impact_axis", "matrix_size", "risk_levels", "risk_appetite");

			$organisation["id"] = null;
			$organisation["invitation_code"] = null;
			$organisation["risk_appetite"] = 1;
			$organisation["impact_axis"] = "horizontal";

			if ($register == false) {
				if ($this->db->query("begin") === false) {
					return false;
				}
			}

			if ($this->db->insert("organisations", $organisation, $keys) == false) {
				$this->db->query("rollback");
				return false;
			}

			$organisation_id = $this->db->last_insert_id;

			if ($this->add_register_settings($organisation_id, $organisation) == false) {
				$this->db->query("rollback");
				return false;
			}

			if ($register == false) {
				if ($this->db->query("commit") === false) {
					return false;
				}
			}

			return $organisation_id;
		}

		public function update_organisation($organisation) {
			$keys = array("name");

			return $this->db->update("organisations", $organisation["id"], $organisation, $keys);
		}

		public function delete_organisation($organisation_id) {
			$queries = array(
				/* Preventive
				 */
				array("delete from control_preventive where measure_preventive_id in ".
					"(select id from measures_preventive where risk_cause_id in ".
						"(select id from risk_cause where risk_id in ".
							"(select id from risks where organisation_id=%d)))", $organisation_id),
				array("delete from issue_preventive_tasks where issue_id in ".
					"(select id from issues_preventive where measure_id in ".
						"(select id from measures_preventive where risk_cause_id in ".
							"(select id from risk_cause where risk_id in ".
								"(select id from risks where organisation_id=%d))))", $organisation_id),
				array("delete from issues_preventive where measure_id in ".
					"(select id from measures_preventive where risk_cause_id in ".
						"(select id from risk_cause where risk_id in ".
							"(select id from risks where organisation_id=%d)))", $organisation_id),
				array("delete from measures_preventive where risk_cause_id in ".
					"(select id from risk_cause where risk_id in ".
						"(select id from risks where organisation_id=%d))", $organisation_id),
				/* Detective
				 */
				array("delete from control_detective where measure_detective_id in ".
					"(select id from measures_detective where risk_id in ".
						"(select id from risks where organisation_id=%d))", $organisation_id),
				array("delete from issue_detective_tasks where issue_id in ".
					"(select id from issues_detective where measure_id in ".
						"(select id from measures_detective where risk_id in ".
							"(select id from risks where organisation_id=%d)))", $organisation_id),
				array("delete from issues_detective where measure_id in ".
					"(select id from measures_detective where risk_id in ".
						"(select id from risks where organisation_id=%d))", $organisation_id),
				array("delete from measures_detective where risk_id in ".
					"(select id from risks where organisation_id=%d)", $organisation_id),
				/* Repressive
				 */
				array("delete from control_repressive where measure_repressive_id in ".
					"(select id from measures_repressive where risk_effect_id in ".
						"(select id from risk_effect where risk_id in ".
							"(select id from risks where organisation_id=%d)))", $organisation_id),
				array("delete from issue_repressive_tasks where issue_id in ".
					"(select id from issues_repressive where measure_id in ".
						"(select id from measures_repressive where risk_effect_id in ".
							"(select id from risk_effect where risk_id in ".
								"(select id from risks where organisation_id=%d))))", $organisation_id),
				array("delete from issues_repressive where measure_id in ".
					"(select id from measures_repressive where risk_effect_id in ".
						"(select id from risk_effect where risk_id in ".
							"(select id from risks where organisation_id=%d)))", $organisation_id),
				array("delete from measures_repressive where risk_effect_id in ".
					"(select id from risk_effect where risk_id in ".
						"(select id from risks where organisation_id=%d))", $organisation_id),
				/* Risks
				 */
				array("delete from risk_tag where risk_id in ".
					"(select id from risks where organisation_id=%d)", $organisation_id),
				array("delete from risk_log where risk_id in ".
					"(select id from risks where organisation_id=%d)", $organisation_id),
				array("delete from risk_effect where risk_id in ".
					"(select id from risks where organisation_id=%d)", $organisation_id),
				array("delete from risk_cause where risk_id in ".
					"(select id from risks where organisation_id=%d)", $organisation_id),
				array("delete from risks where organisation_id=%d", $organisation_id),
				/* Tags
				 */
				array("delete from tags where tag_category_id in ".
					"(select id from tag_categories where organisation_id=%d)", $organisation_id),
				array("delete from tag_categories where organisation_id=%d", $organisation_id),
				/* Labels
				 */
				array("delete from labels_horizontal where organisation_id=%d", $organisation_id),
				array("delete from labels_vertical where organisation_id=%d", $organisation_id),
				array("delete from labels_risk where organisation_id=%d", $organisation_id),
				/* matrix
				 */
				array("delete from causes where organisation_id=%d", $organisation_id),
				array("delete from effects where organisation_id=%d", $organisation_id),
				array("delete from matrix where organisation_id=%d", $organisation_id),
				/* Users
				 */
				array("delete from sessions where user_id in ".
					"(select id from users where organisation_id=%d)", $organisation_id),
				array("delete from user_role where user_id in ".
					"(select id from users where organisation_id=%d)", $organisation_id),
				array("delete from users where organisation_id=%d", $organisation_id),
				array("delete from organisations where id=%d", $organisation_id));

			return $this->db->transaction($queries) !== false;
		}
	}
?>
