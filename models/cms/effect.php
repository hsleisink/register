<?php
	class cms_effect_model extends register_model {
		public function get_impact_labels() {
			return ($this->impact_axis == "horizontal") ? $this->get_horizontal_labels() : $this->get_vertical_labels();
		}

		public function get_effects() {
			$query = "select * from effects where organisation_id=%d";

			if (($effects = $this->db->execute($query, $this->user->organisation_id)) === false) {
				return false;
			}

			if (is_true(ENCRYPT_DATA)) {
				foreach ($effects as $i => $effect) {
					$effects[$i]["effect"] = $this->decrypt($effect["effect"]);
				}
			}

			usort($effects, function($a, $b) {
				return strcmp($a["effect"], $b["effect"]);
			});

			return $effects;
		}

		public function get_effect($effect_id) {
			$query = "select * from effects where id=%d and organisation_id=%d";

			if (($result = $this->db->execute($query, $effect_id, $this->user->organisation_id)) == false) {
				return false;
			}
			$effect = $result[0];

			for ($i = $this->matrix_size + 1; $i <= 7; $i++) {
				unset($effect["impact".$i]);
			}

			if (is_true(ENCRYPT_DATA)) {
				$effect["effect"] = $this->decrypt($effect["effect"]);

				for ($i = 1; $i <= $this->matrix_size; $i++) {
					$key = "impact".$i;

					if ($effect[$key] != "") {
						$effect[$key] = $this->decrypt($effect[$key]);
					}
				}
			}

			return $effect;
		}

		public function save_okay($effect) {
			if (isset($effect["id"])) {
				if ($this->get_effect($effect["id"]) == false) {
					$this->view->add_message("Effect not found.");
					$this->user->log_action("unauthorized update attempt of effect %d", $effect["id"]);
					$result = false;
				}
			}

			$result = true;

			if (trim($effect["effect"]) == "") {
				$this->view->add_message("Specify the risk effect.");
				$result = false;
			}

			return $result;
		}

		private function encrypt_effect($effect) {
			$effect["effect"] = $this->encrypt($effect["effect"]);

			for ($i = 1; $i <= $this->matrix_size; $i++) {
				$effect["impact".$i] = $this->encrypt($effect["impact".$i]);
			}
			
			return $effect;
		}

		public function create_effect($effect) {
			$keys = array("id", "organisation_id", "effect");

			for ($i = 1; $i <= $this->matrix_size; $i++) {
				array_push($keys, "impact".$i);
			}

			$effect["id"] = null;
			$effect["organisation_id"] = $this->user->organisation_id;
			$effect["effect"] = substr($effect["effect"], 0, MAXLENGTH_BOWTIE_TITLE);

			if (is_true(ENCRYPT_DATA)) {
				$effect = $this->encrypt_effect($effect);
			}

			return $this->db->insert("effects", $effect, $keys);
		}

		public function update_effect($effect) {
			$keys = array("effect");

			for ($i = 1; $i <= $this->matrix_size; $i++) {
				array_push($keys, "impact".$i);
			}

			$effect["effect"] = substr($effect["effect"], 0, MAXLENGTH_BOWTIE_TITLE);

			if (is_true(ENCRYPT_DATA)) {
				$effect = $this->encrypt_effect($effect);
			}

			return $this->db->update("effects", $effect["id"], $effect, $keys);
		}

		public function delete_okay($effect) {
			if ($this->get_effect($effect["id"]) == false) {
				$this->view->add_message("Effect not found.");
				$this->user->log_action("unauthorized delete attempt of effect %d", $effect["id"]);
				return false;
			}

			$result = true;

			$query = "select count(*) as count from risk_effect where effect_id=%d";
			if (($count = $this->db->execute($query, $effect["id"])) === false) {
				$this->view->add_message("Database error.");
				$result = false;
			} else if ($count[0]["count"] > 0) {
				$this->view->add_message("This effect is in use.");
				$result = false;
			}

			return $result;
		}

		public function delete_effect($effect_id) {
			return $this->db->delete("effects", $effect_id);
		}
	}
?>
