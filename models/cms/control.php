<?php
	class cms_control_model extends Banshee\model {
		public function get_standards() {
			static $standards = null;

			if ($standards === null) {
				$query = "select id,name from control_standards order by name";
				if (($standards = $this->db->execute($query)) === false) {
					return false;
				}
			}

			return $standards;
		}

		public function get_controls($standard_id) {
			$query = "select l.* from controls l, control_standards c ".
			         "where l.control_standard_id=c.id and l.control_standard_id=%d ".
			         "order by id";

			return $this->db->execute($query, $standard_id);
		}

		public function get_control($control_id) {
			return $this->db->entry("controls", $control_id);
		}

		public function save_oke($control) {
			$result = true;

			if (trim($control["number"]) == "") {
				$this->view->add_message("Specify the number.");
				$result = false;
			}

			if (trim($control["title"]) == "") {
				$this->view->add_message("specify the title.");
				$result = false;
			}

			return $result;
		}

		public function create_control($control) {
			$keys = array("id", "control_standard_id", "number", "title");

			$control["id"] = null;
			$control["number"] = trim($control["number"]);
			$control["title"] = trim($control["title"]);
			$control["control_standard_id"] = $_SESSION["control_standard"];

			return $this->db->insert("controls", $control, $keys) !== false;
		}

		public function update_control($control) {
			$keys = array("number", "title");

			$control["number"] = trim($control["number"]);
			$control["title"] = trim($control["title"]);

			return $this->db->update("controls", $control["id"], $control, $keys) !== false;
		}

		public function delete_oke($control) {
			$result = true;

			$query = "select ".
				"(select count(*) from control_preventive where control_id=%d) + ".
				"(select count(*) from control_detective where control_id=%d) + ".
				"(select count(*) from control_repressive where control_id=%d) as count";

			if (($count = $this->db->execute($query, $control["id"], $control["id"], $control["id"])) == false) {
				return false;
			}

			if ($count[0]["count"] > 0) {
				$this->view->add_message("This control is in use.");
				return false;
			}

			return $result;
		}

		public function delete_control($control_id) {
			return $this->db->delete("controls", $control_id) != false;
		}
	}
?>
