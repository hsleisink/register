<?php
	/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
	 * Licensed under the RAFIS license.
	 G*/

	class cms_data_model extends Banshee\model {
		private $root_tables = array("labels_horizontal", "labels_vertical", "labels_risk", "matrix",
			"causes", "effects", "tag_categories", "risks");
		private $table_structure = array(
				"labels_horizontal" => array(),
				"labels_vertical" => array(),
				"labels_risk" => array(),
				"matrix" => array(),
				"causes" => array(
					"encrypted"    => array("cause")),
				"effects" => array(
					"encrypted"    => array("effect", "impact1", "impact2", "impact3", "impact4", "impact5", "impact6", "impact7")),
				"tag_categories" => array(
					"encrypted"    => array("name")),
				"tags" => array(
					"encrypted"    => array("name"),
					"foreign_keys" => array("tag_category_id" => "tag_categories")),
				"risks" => array(
					"encrypted"    => array("title", "description"),
					"foreign_keys" => array("creator_id" => "users", "owner_id" => "users", "acceptor_id" => "users")),
				"risk_cause" => array(
					"encrypted"    => array("title", "description"),
					"foreign_keys" => array("risk_id" => "risks", "cause_id" => "causes")),
				"risk_effect" => array(
					"encrypted"    => array("title", "description"),
					"foreign_keys" => array("risk_id" => "risks", "effect_id" => "effects")),
				"risk_tag" => array(
					"foreign_keys" => array("risk_id" => "risks", "tag_id" => "tags")),
				"measures_preventive" => array(
					"encrypted"    => array("title", "description"),
					"foreign_keys" => array("risk_cause_id" => "risk_cause", "owner_id" => "users")),
				"measures_detective" => array(
					"encrypted"    => array("title", "description"),
					"foreign_keys" => array("risk_id" => "risks", "owner_id" => "users")),
				"measures_repressive" => array(
					"encrypted"    => array("title", "description"),
					"foreign_keys" => array("risk_effect_id" => "risk_effect", "owner_id" => "users")),
				"control_preventive" => array(
					"foreign_keys" => array("measure_preventive_id" => "measures_preventive", "control_id" => "controls")),
				"control_detective" => array(
					"foreign_keys" => array("measure_detective_id" => "measures_detective", "control_id" => "controls")),
				"control_repressive" => array(
					"foreign_keys" => array("measure_repressive_id" => "measures_repressive", "control_id" => "controls")),
				"issues_preventive" => array(
					"encrypted"    => array("title", "description"),
					"foreign_keys" => array("measure_id" => "measures_preventive")),
				"issues_detective" => array(
					"encrypted"    => array("title", "description"),
					"foreign_keys" => array("measure_id" => "measures_detective")),
				"issues_repressive" => array(
					"encrypted"    => array("title", "description"),
					"foreign_keys" => array("measure_id" => "measures_repressive")),
				"issue_preventive_tasks" => array(
					"encrypted"    => array("title", "description", "executor"),
					"foreign_keys" => array("issue_id" => "issues_preventive")),
				"issue_detective_tasks" => array(
					"encrypted"    => array("title", "description", "executor"),
					"foreign_keys" => array("issue_id" => "issues_detective")),
				"issue_repressive_tasks" => array(
					"encrypted"    => array("title", "description", "executor"),
					"foreign_keys" => array("issue_id" => "issues_repressive")),
				"risk_log" => array(
					"encrypted"    => array("event"),
					"foreign_keys" => array("risk_id" => "risks", "user_id" => "users")));

		public function get_organisation($id) {
			if (($organisation = $this->db->entry("organisations", $id)) == false) {
				return false;
			}

			return $organisation["name"];
		}

		private function export_table($table, $column, $value) {
			if (is_array($value) == false) {
				$query = "select * from %S where %S=%s";
			} else if (($count = count($value)) > 0) {
				$filter = implode(",", array_fill(1, $count, "%d"));
				$query = "select * from %S where %S in (".$filter.")";
			} else {
				return array();
			}

			if (($data = $this->db->execute($query, $table, $column, $value)) === false) {
				return false;
			}

			$columns = $this->table_structure[$table]["encrypted"] ?? array();

			if (is_true(ENCRYPT_DATA)) {
				foreach ($columns as $column) {
					foreach ($data as $i => $entry) {
						if ($entry[$column] !== null) {
							$data[$i][$column] = $this->decrypt($entry[$column]);
						}
					}
				}
			}

			return $data;
		}

		private function get_id_list($items) {
			$list = array();

			foreach ($items as $item) {
				array_push($list, (int)$item["id"]);
			}

			return $list;
		}

		private function user_foreign_keys($structure) {
			$foreign_keys = $structure["foreign_keys"] ?? array();
			$columns = array();

			foreach ($foreign_keys as $column => $table) {
				if ($table == "users") {
					array_push($columns, $column);
				}
			}

			return $columns;
		}

		public function get_export() {
			$export = array("database_version" => $this->settings->database_version);

			/* Organisation settings
			 */
			if (($organisation = $this->db->entry("organisations", $this->user->organisation_id)) === false) {
				return false;
			}

			$export["settings"] = array(
				"matrix_horizontal" => $organisation["matrix_horizontal"],
				"matrix_vertical"   => $organisation["matrix_vertical"],
				"impact_axis"       => $organisation["impact_axis"],
				"matrix_size"       => $organisation["matrix_size"],
				"risk_levels"       => $organisation["risk_levels"],
				"risk_appetite"     => $organisation["risk_appetite"]);

			/* Root tables
			 */
			foreach ($this->root_tables as $table) {
				if (($export[$table] = $this->export_table($table, "organisation_id", $this->user->organisation_id)) === false) {
					return false;
				}
			}

			/* Tags
			 */
			$tag_categories_ids = $this->get_id_list($export["tag_categories"]);
			if (($export["tags"] = $this->export_table("tags", "tag_category_id", $tag_categories_ids)) === false) {
				return false;
			}

			/* Users
			 */
			$query = "select id, fullname from users where organisation_id=%d";
			if (($users = $this->db->execute($query, $this->user->organisation_id)) === false) {
				return false;
			}

			/* Other tables
			 */
			foreach ($this->table_structure as $table => $structure) {
				$foreign_keys = $this->table_structure[$table]["foreign_keys"] ?? array();

				if (in_array($table, $this->root_tables) || (count($foreign_keys) == 0)) {
					continue;
				}

				$columns = array_keys($foreign_keys);
				$column = $columns[0];
				$other_table = $foreign_keys[$column];

				$ids = $this->get_id_list($export[$other_table]);
				if (($export[$table] = $this->export_table($table, $column, $ids)) === false) {
					return false;
				}
			}

			/* User id to fullname
			 */
			foreach ($this->table_structure as $table => $structure) {
				$columns = $this->user_foreign_keys($structure);

				foreach ($columns as $column) {
					foreach ($export[$table] as $i => $entry) {
						foreach ($users as $user) {
							if ($user["id"] == $entry[$column]) {
								$export[$table][$i][$column] = $user["fullname"];
								break;
							}
						}
					}
				}
			}

			return $export;
		}

		public function signature($export) {
			$hash = hash("sha256", json_encode($export));

			return hash_hmac("sha256", $hash, $this->settings->secret_website_code);
		}

		public function generate_filename($str) {
			$valid = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789() ";

			$result = "";
			$len = strlen($str);
			for ($i = 0; $i < $len; $i++) {
				$c = substr($str, $i, 1);
				if (strpos($valid, $c) !== false) {
					$result .= $c;
				}
			}

			return $result;
		}

		private function set_organisation($data, $table) {
			if (isset($data[$table])) {
				foreach ($data[$table] as $i => $item) {
					$data[$table][$i]["organisation_id"] = $this->user->organisation_id;
				}
			}

			return $data;
		}

		private function get_user($fullname) {
			static $users = false;
			static $mentioned = array();

			if ($users === false) {
				$query = "select id,fullname from users where organisation_id=%d";
				if (($result = $this->db->execute($query, $this->user->organisation_id)) === false) {
					return $this->user->id;
				}

				$users = array();
				foreach ($result as $item) {
					$users[$item["fullname"]] = $item["id"];
				}
			}

			if (isset($users[$fullname])) {
				return $users[$fullname];
			}

			if (in_array($fullname, $mentioned) == false) {
				$this->view->add_message("No account found for '%s'.", $fullname);
				array_push($mentioned, $fullname);
			}

			return $this->user->id;
		}

		public function test_import($data) {
			$query = "select id,fullname from users where organisation_id=%d";
			if (($result = $this->db->execute($query, $this->user->organisation_id)) === false) {
				return $this->user->id;
			}

			$users = array();
			foreach ($result as $item) {
				array_push($users, $item["fullname"]);
			}

			$mentioned = array();
			foreach ($this->table_structure as $table => $structure) {
				if (is_array($data[$table]) == false) {
					continue;
				}

				$columns = $this->user_foreign_keys($structure);

				foreach ($columns as $column) {
					foreach ($data[$table] as $entry) {
						if (($user = $entry[$column]) == null) {
							continue;
						}

						if (in_array($user, $users) == false) {
							if (in_array($user, $mentioned) == false) {
								$this->view->add_message("No account for user '".$user."' available.");
								array_push($mentioned, $user);
							}
						}
					}
				}
			}
			
			return true;
		}

		private function import_table($table, $data, $foreign_keys, &$inserted_keys) {
			foreach ($data as $item) {
				$id = null;
				if (isset($item["id"])) {
					$id = (int)$item["id"];
					$item["id"] = null;
				}

				foreach ($foreign_keys as $column => $other_table) {
					if ($item[$column] === null) {
						continue;
					}

					if ($other_table == "controls") {
						continue;
					}

					if ($other_table == "users") {
						$item[$column] = $this->get_user($item[$column]);
						continue;
					}

					if (($item[$column] = $inserted_keys[$other_table][$item[$column]]) == null) {
						return false;
					}
				}

				if ($this->db->insert($table, $item) === false) {
					return false;
				}

				if ($id !== null) {
					$inserted_keys[$table][$id] = $this->db->last_insert_id;
				}
			}

			return true;
		}

		public function import_data($data) {
			if ($this->db->query("begin") === false) {
				return false;
			}

			if ($this->delete_data() == false) {
				$this->db->query("rollback");
				return false;
			}

			/* Settings
			 */
			if (isset($data["settings"])) {
				if ($this->db->update("organisations", $this->user->organisation_id, $data["settings"]) === false) {
					$this->db->query("rollback");
					return false;
				}
			}

			/* Adjust data
			 */
			foreach ($this->root_tables as $table) {
				$data = $this->set_organisation($data, $table);
			}

			/* Store data
			 */
			$inserted_keys = array();
			foreach (array_keys($this->table_structure) as $table) {
				$inserted_keys[$table] = array();
			}

			foreach ($this->table_structure as $table => $structure) {
				if (is_array($data[$table]) == false) {
					continue;
				}

				if (is_true(ENCRYPT_DATA)) {
					$columns = $structure["encrypted"] ?? array();
					foreach ($columns as $column) {
						foreach ($data[$table] as $i => $entry) {
							if ($entry[$column] !== null) {
								$data[$table][$i][$column] = $this->encrypt($entry[$column]);
							}
						}
					}
				}

				$foreign_keys = $structure["foreign_keys"] ?? array();
				if ($this->import_table($table, $data[$table], $foreign_keys, $inserted_keys) == false) {
					$this->db->query("rollback");
					return false;
				}
			}

			return $this->db->query("commit") !== false;
		}

		public function delete_oke($data) {
			return $data["confirmation"] == "Delete all data";
		}

		private function delete_data() {
			$queries = array();

			$tables = array_reverse(array_keys($this->table_structure));
			foreach ($tables as $table) {
				$foreign_keys = $this->table_structure[$table]["foreign_keys"] ?? array();

				if (in_array($table, $this->root_tables) || (count($foreign_keys) == 0)) {
					continue;
				}

				$columns = array_keys($foreign_keys);
				$column = $columns[0];
				$query = "delete from ".$table." where ".$columns[0]." in ";

				$nested = 0;
				$continue = true;
				do {
					$table = $foreign_keys[$column];

					if (in_array($table, $this->root_tables)) {
						$query .= "(select id from ".$table." where organisation_id=%d";
						$continue = false;
					} else {
						$foreign_keys = $this->table_structure[$table]["foreign_keys"];
						$columns = array_keys($foreign_keys);
						$column = $columns[0];

						$query .= "(select id from ".$table." where ".$column." in ";
					}

					$nested++;
				} while ($continue);
				$query .= str_repeat(")", $nested);

				array_push($queries, $query);
			}

			array_push($this->root_tables, "advisors");
			foreach ($this->root_tables as $table) {
				array_push($queries, "delete from ".$table." where organisation_id=%d");
			}

			foreach ($queries as $query) {
				$this->db->query($query, $this->user->organisation_id);
			}

			return true;
		}

		public function reset_data() {
			if ($this->db->query("begin") === false) {
				return false;
			}

			if ($this->delete_data() == false) {
				$this->db->query("rollback");
				return false;
			}

			if (($organisation = $this->db->entry("organisations", $this->user->organisation_id)) == false) {
				$this->db->query("rollback");
				return false;
			}

			if ($this->borrow("cms/organisation")->add_register_settings($this->user->organisation_id, $organisation) == false) {
				$this->db->query("rollback");
				return false;
			}

			return $this->db->query("commit") !== false;
		}
	}
?>
