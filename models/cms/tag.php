<?php
	class cms_tag_model extends Banshee\model {
		public function get_categories() {
			static $categories = null;

			if ($categories === null) {
				$query = "select id,name from tag_categories where organisation_id=%d";
				if (($categories = $this->db->execute($query, $this->user->organisation_id)) === false) {
					return false;
				}

				if (is_true(ENCRYPT_DATA)) {
					foreach ($categories as $i => $category) {
						$categories[$i]["name"] = $this->decrypt($category["name"]);
					}
				}

				usort($categories, function($a, $b) {
					return strcmp($a["name"], $b["name"]);
				});
			}

			return $categories;
		}

		public function get_tags($category_id) {
			$query = "select l.* from tags l, tag_categories c ".
			         "where l.tag_category_id=c.id and l.tag_category_id=%d and c.organisation_id=%d ".
			         "order by name";

			if (($tags = $this->db->execute($query, $category_id, $this->user->organisation_id)) === false) {
				return false;
			}

			if (is_true(ENCRYPT_DATA)) {
				foreach ($tags as $i => $tag) {
					$tags[$i]["name"] = $this->decrypt($tag["name"]);
				}
			}

			usort($tags, function($a, $b) {
				return strcmp($a["name"], $b["name"]);
			});

			return $tags;
		}

		public function get_tag($tag_id) {
			$query = "select l.* from tags l, tag_categories c ".
			         "where l.id=%d and l.tag_category_id=c.id and c.organisation_id=%d";

			if (($result = $this->db->execute($query, $tag_id, $this->user->organisation_id)) == false) {
				return false;
			}
			$tag = $result[0];

			if (is_true(ENCRYPT_DATA)) {
				$tag["name"] = $this->decrypt($tag["name"]);
			}

			return $tag;
		}

		public function save_oke($tag) {
			if (isset($tag["id"])) {
				if ($this->get_tag($tag["id"]) == false) {
					$this->view->add_message("Tag not found.");
					$this->user->log_action("unauthorized update attempt of tag %d", $tag["id"]);
					return false;
				}
			}

			$result = true;

			if (trim($tag["name"]) == "") {
				$this->view->add_message("Enter the tag name.");
				$result = false;
			} else {
				$query = "select count(*) as count from tags l, tag_categories c ".
				         "where l.tag_category_id=c.id and l.name=%s and l.tag_category_id=%d";
				$args = array(trim($tag["name"]), $_SESSION["tag_category"]);

				if (isset($tag["id"])) {
					$query .= " and l.id!=%d";
					array_push($args, $tag["id"]);
				}

				if (($tags = $this->db->execute($query, $args)) === false) {
					$result = false;
				} else if ($tags[0]["count"] > 0) {
					$this->view->add_message("This tag already exists.");
					$result = false;
				}
			}

			return $result;
		}

		public function create_tag($tag) {
			$keys = array("id", "tag_category_id", "name");

			$tag["id"] = null;
			$tag["name"] = trim($tag["name"]);
			$tag["tag_category_id"] = $_SESSION["tag_category"];

			if (is_true(ENCRYPT_DATA)) {
				$tag["name"] = $this->encrypt($tag["name"]);
			}

			return $this->db->insert("tags", $tag, $keys) !== false;
		}

		public function update_tag($tag) {
			$keys = array("name");

			$tag["name"] = trim($tag["name"]);

			if (is_true(ENCRYPT_DATA)) {
				$tag["name"] = $this->encrypt($tag["name"]);
			}

			return $this->db->update("tags", $tag["id"], $tag, $keys) !== false;
		}

		public function delete_oke($tag) {
			$result = true;

			if ($this->get_tag($tag["id"]) == false) {
				$this->view->add_message("Tag not found.");
				$this->user->log_action("unauthorized delete attempt of tag %d", $tag["id"]);
				$result = false;
			}

			return $result;
		}

		public function delete_tag($tag_id) {
			$queries = array(
				array("delete from risk_tag where tag_id=%d", $tag_id),
				array("delete from tags where id=%d", $tag_id));

			return $this->db->transaction($queries) !== false;
		}
	}
?>
