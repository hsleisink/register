<?php
	class cms_matrix_model extends register_model {
		public function matrix_okay($matrix) {
			$result = true;

			$matrix = array_reverse($matrix["matrix"]);

			if ($matrix[0]["column1"] != 1) {
				$this->view->add_message("The left bottom corner of the matrix must contain the lowest risk level.");
				$result = false;
			}

			if ($matrix[$this->matrix_size - 1]["column".$this->matrix_size] != $this->risk_levels) {
				$this->view->add_message("The right top corner of the matrix must contain the highest risk level.");
				$result = false;
			}

			for ($i = 0; $i < $this->matrix_size; $i++) {
				for ($c = 2; $c <= $this->matrix_size; $c++) {
					$d = $matrix[$i]["column".$c] - $matrix[$i]["column".($c - 1)];
					if (($d < 0) || ($d > 1)) {
						$this->view->add_message("Levels may only go up one step.");
						$result = false;
					}
				}
			}

			for ($c = 1; $c <= $this->matrix_size; $c++) {
				for ($i = 1; $i < $this->matrix_size; $i++) {
					$d = $matrix[$i]["column".$c] - $matrix[$i - 1]["column".$c];
					if (($d < 0) || ($d > 1)) {
						$this->view->add_message("Levels may only go up one step.");
						$result = false;
					}
				}
			}

			return $result;
		}

		public function save_matrix($matrix) {
			$query = "select id from matrix where organisation_id=%d order by id";

			if (($result = $this->db->execute($query, $this->user->organisation_id)) == false) {
				return false;
			}

			$keys = array();
			for ($k = 1; $k <= $this->matrix_size; $k++) {
				array_push($keys, "column".$k);
			}

			if ($this->db->query("begin") == false) {
				return false;
			}

			for ($i = 0; $i < $this->matrix_size; $i++) {
				if ($this->db->update("matrix", $result[$i]["id"], $matrix["matrix"][$this->matrix_size - $i], $keys) === false) {
					$this->db->query("rollback");
					return false;
				}
			}
			
			return $this->db->query("commit");
		}
	}
?>
