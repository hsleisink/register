<?php
	class statistics_model extends register_model {
		public function dates_okay($dates) {
			$result = true;

			if (valid_date($dates["first"]) == false) {
				$this->view->add_message($this->language->module_text("error_invalid_from_date"));
				$result = false;
			}

			if (valid_date($dates["last"]) == false) {
				$this->view->add_message($this->language->module_text("error_invalid_to_date"));
				$result = false;
			}

			if ($result) {
				if (strtotime($dates["first"]) > strtotime($dates["last"])) {
					$this->view->add_message($this->language->module_text("error_invalid_date_order"));
					$result = false;
				}
			}

			return $result;
		}

		public function count_risks($first, $last) {
			$query = "select count(*) as count from risks where organisation_id=%d and created>=%s and created<=%s";

			if (($result = $this->db->execute($query, $this->organisation_id, $first, $last)) == false) {
				return false;
			}

			return $result[0]["count"];
		}

		public function measure_count($first, $last) {
			$query = "select type, count(*) as count from measures m, risks r where m.risk_id=r.id ".
			         "and organisation_id=%d and created>=%s and created<=%s group by type order by type";

			if (($types = $this->db->execute($query, $this->organisation_id, $first, $last)) === false) {
				return false;
			}

			$result = array();
			foreach ($types as $type) {
				$result[$type["type"]] = $type["count"];
			}

			return $result;
		}

		private function decrypt_and_sort($list, $key) {
			if (is_true(ENCRYPT_DATA)) {
				foreach ($list as $i => $item) {
					$list[$i][$key] = $this->decrypt($item[$key]);
				}
			}

			usort($list, function($a, $b) use ($key) {
				return strcmp(strtolower($a[$key]), strtolower($b[$key]));
			});

			return $list;
		}

		public function tag_count($first, $last) {
			$query = "select id, name from tag_categories where organisation_id=%d";

			if (($result = $this->db->execute($query, $this->organisation_id)) === false) {
				return false;
			}

			$categories = array();

			$query = "select id, name, (select count(*) from risk_tag rt, risks r where rt.risk_id=r.id and tag_id=t.id ".
			         "and created>=%s and created<=%s) as count from tags t where tag_category_id=%d";
			foreach ($result as $item) {
				if (is_true(ENCRYPT_DATA)) {
					$item["name"] = $this->decrypt($item["name"]);
				}

				if (($tags = $this->db->execute($query, $first, $last, $item["id"])) === false) {
					return false;
				}

				$tags = $this->decrypt_and_sort($tags, "name");

				$categories[$item["name"]] = $tags;
			}

			ksort($categories);

			return $categories;
		}

		public function cause_count($first, $last) {
			$query = "select id, cause, (select count(*) from risk_cause rc, risks r where rc.risk_id=r.id and cause_id=c.id ".
			         "and created>=%s and created<=%s) as count from causes c where organisation_id=%d group by cause";

			if (($causes = $this->db->execute($query, $first, $last, $this->organisation_id)) === false) {
				return false;
			}

			return $this->decrypt_and_sort($causes, "cause");
		}

		public function effect_count($first, $last) {
			$query = "select id, effect, (select count(*) from risk_effect re, risks r where re.risk_id=r.id and effect_id=e.id ".
			         "and created>=%s and created<=%s) as count from effects e where organisation_id=%d group by effect";

			if (($effects = $this->db->execute($query, $first, $last, $this->organisation_id)) === false) {
				return false;
			}

			return $this->decrypt_and_sort($effects, "effect");
		}

		public function get_cause($cause_id) {
			$query = "select cause from causes where id=%d and organisation_id=%d";

			if (($result = $this->db->execute($query, $cause_id, $this->user->organisation_id)) == false) {
				return false;
			}
			$cause = $result[0]["cause"];

			if (is_true(ENCRYPT_DATA)) {
				$cause = $this->decrypt($cause);
			}

			return $cause;
		}

		public function get_causes($cause_id, $first, $last) {
			$query = "select c.title from risk_cause c, risks r where c.risk_id=r.id and c.cause_id=%d ".
			         "and created>=%s and created<=%s and r.organisation_id=%d";

			if (($causes = $this->db->execute($query, $cause_id, $first, $last, $this->user->organisation_id)) === false) {
				return false;
			}

			return $this->decrypt_and_sort($causes, "title");
		}

		public function get_effect($effect_id) {
			$query = "select effect from effects where id=%d and organisation_id=%d";

			if (($result = $this->db->execute($query, $effect_id, $this->user->organisation_id)) == false) {
				return false;
			}
			$effect = $result[0]["effect"];

			if (is_true(ENCRYPT_DATA)) {
				$effect = $this->decrypt($effect);
			}

			return $effect;
		}

		public function get_effects($effect_id, $first, $last) {
			$query = "select e.title from risk_effect e, risks r where e.risk_id=r.id and e.effect_id=%d ".
			         "and created>=%s and created<=%s and r.organisation_id=%d";

			if (($effects = $this->db->execute($query, $effect_id, $first, $last, $this->user->organisation_id)) === false) {
				return false;
			}

			return $this->decrypt_and_sort($effects, "title");
		}
	}
?>
