<?php
	/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
	 * This file is part of the Banshee PHP framework
	 * https://www.banshee-php.org/
	 *
	 * Licensed under The MIT License
	 */

	class register_model extends Banshee\splitform_model {
		const MINIMUM_USERNAME_LENGTH = 4;
		const MINIMUM_FULLNAME_LENGTH = 4;

		protected $forms = array(
			"invitation"    => array("invitation"),
			"matrix"        => array("matrix_horizontal", "matrix_vertical", "matrix_size", "risk_levels"),
			"email"         => array("email"),
			"code"          => array("code"),
			"account"       => array("fullname", "username", "password", "organisation"),
			"authenticator" => array("authenticator_secret"));

		public function __construct() {
			if ((DEFAULT_ORGANISATION_ID != 0) || is_true(ENCRYPT_DATA)) {
				unset($this->forms["invitation"]);
			}

			if (is_false(USE_AUTHENTICATOR)) {
				unset($this->forms["authenticator"]);
			}

			$arguments = func_get_args();
			call_user_func_array(array(parent::class, "__construct"), $arguments);
		}

		public function reset_form_progress() {
			unset($_SESSION["register_email"]);
			unset($_SESSION["register_code"]);

			parent::reset_form_progress();

			parent::default_value("matrix_horizontal", MATRIX_DEFAULT_HORIZONTAL);
			parent::default_value("matrix_vertical", MATRIX_DEFAULT_VERTICAL);
			parent::default_value("matrix_size", 5);
			parent::default_value("risk_levels", 4);
		}

		public function validate_invitation($data) {
			if (empty($data["invitation"])) {
				return true;
			}

			if (strpos($data["invitation"], "-") === false) {
				$this->view->add_message($this->language->module_text("error_invalid_invitation_code"));
				return false;
			}

			list($id, $code) = explode("-", $data["invitation"]);

			$query = "select * from organisations where id=%d and invitation_code=%s";
			if ($this->db->execute($query, $id, $code) == false) {
				$this->view->add_message($this->language->module_text("error_invalid_invitation_code"));
				return false;
			}

			return true;
		}

		public function validate_matrix($data) {
			$result = true;

			$size = (int)$data["matrix_size"];
			if (($size < MATRIX_MIN_SIZE) || ($size > MATRIX_MAX_SIZE)) {
				$this->view->add_message($this->language->module_text("error_invalid_matrix_size"));
				$result = false;
			}

			$levels = (int)$data["risk_levels"];
			if (($levels < MATRIX_MIN_SIZE) || ($levels > $size)) {
				$this->view->add_message($this->language->module_text("error_invalid_risk_levels"));
				$result = false;
			}

			return $result;
		}

		public function validate_email($data) {
			$result = true;

			if (valid_email($data["email"]) == false) {
				$this->view->add_message($this->language->module_text("error_invalid_email_address"));
				$result = false;
			}

			$query = "select * from users where email=%s";
			if ($this->db->execute($query, $data["email"]) != false) {
				$this->view->add_message($this->language->module_text("error_email_in_use"));
				$result = false;
			}

			return $result;
		}

		public function validate_code($data) {
			if ($data["code"] != $_SESSION["register_code"]) {
				$this->view->add_message($this->language->module_text("error_invalid_verification_code"));
				return false;
			}

			return true;
		}

		public function validate_account($data) {
			$result = true;

			$length_okay = (strlen($data["username"]) >= self::MINIMUM_USERNAME_LENGTH);
			$format_okay = valid_input($data["username"], VALIDATE_NONCAPITALS.VALIDATE_NUMBERS."@.-_", VALIDATE_NONEMPTY);

			if (($length_okay == false) || ($format_okay == false)) {
				$this->view->add_message($this->language->module_text("error_lowercase"), self::MINIMUM_USERNAME_LENGTH);
				$result = false;
			}

			$query = "select * from users where username=%s";
			if ($this->db->execute($query, $data["username"]) != false) {
				$this->view->add_message($this->language->module_text("error_username_taken"));
				$result = false;
			}

			if (is_secure_password($data["password"], $this->view, $this->language) == false) {
				$result = false;
			}

			if (strlen($data["fullname"]) < self::MINIMUM_FULLNAME_LENGTH) {
				$this->view->add_message($this->language->module_text("error_name_length"), self::MINIMUM_FULLNAME_LENGTH);
				$result = false;
			}

			if ((DEFAULT_ORGANISATION_ID == 0) && empty($this->values["invitation"])) {
				if (trim($data["organisation"] == "")) {
					$this->view->add_message($this->language->module_text("error_organisation_name_empty"));
					$result = false;
				} else {
					$query = "select * from organisations where name=%s";
					if ($this->db->execute($query, $data["organisation"]) != false) {
						$this->view->add_message($this->language->module_text("error_organisation_name_taken"));
						$result = false;
					}
				}
			}

			return $result;
		}

		public function validate_authenticator($data) {
			$result = true;

			if ((strlen($data["authenticator_secret"]) > 0) && ($data["authenticator_secret"] != str_repeat("*", 16))) {
				if (valid_input($data["authenticator_secret"], Banshee\authenticator::BASE32_CHARS, 16) == false) {
					$this->view->add_message($this->language->module_text("error_invalid_authenticator_secret"));
					$result = false;
				}
			}

			return $result;
		}

		public function process_form_data($data) {
			if ($this->db->query("begin") === false) {
				return false;
			}

			if (DEFAULT_ORGANISATION_ID != 0) {
				$organisation_id = DEFAULT_ORGANISATION_ID;
			} else if (empty($data["invitation"]) == false) {
				list($organisation_id) = explode("-", $data["invitation"]);
			} else {
				$organisation = array(
					"name"              => $data["organisation"],
					"matrix_horizontal" => $data["matrix_horizontal"],
					"matrix_vertical"   => $data["matrix_vertical"],
					"matrix_size"       => $data["matrix_size"],
					"risk_levels"       => $data["risk_levels"]);

				if (($organisation_id = $this->borrow("cms/organisation")->create_organisation($organisation, true)) == false) {
					return false;
				}
			}

			if (is_true(ENCRYPT_DATA)) {
				$crypto_key = random_string(CRYPTO_KEY_SIZE);

				$rsa = new \Banshee\Protocol\RSA((int)RSA_KEY_SIZE);
				$aes = new \Banshee\Protocol\AES256($crypto_key);
				$private_key = $aes->encrypt($rsa->private_key);
				$public_key = $rsa->public_key;

				$aes = new \Banshee\Protocol\AES256($data["password"]);
				$crypto_key = $aes->encrypt($crypto_key);
			} else {
				$private_key = null;
				$public_key = null;
				$crypto_key = null;
			}

			$roles = array(
				$this->settings->role_id_risk_manager,
				$this->settings->role_id_employee);

			$user = array(
				"organisation_id" => $organisation_id,
				"username"        => $data["username"],
				"password"        => $data["password"],
			    "status"          => USER_STATUS_ACTIVE,
				"fullname"        => $data["fullname"],
				"email"           => $data["email"],
				"private_key"     => $private_key,
				"public_key"      => $public_key,
				"crypto_key"      => $crypto_key,
				"risk_accept"     => 1,
				"roles"           => $roles);

			if (is_true(USE_AUTHENTICATOR)) {
				$user["authenticator_secret"] = $data["authenticator_secret"];
			}

			if ($this->borrow("cms/user")->create_user($user, true) == false) {
				return false;
			}

			$this->db->query("commit");

			$this->user->log_action("user %s registered", $data["username"]);

			unset($_SESSION["register_email"]);
			unset($_SESSION["register_code"]);

			$email = new register_email("New account registered at ".$_SERVER["SERVER_NAME"], $this->settings->webmaster_email);
			$email->set_message_fields(array(
				"ORGANISATION" => $data["organisation"],
				"FULLNAME"     => $data["fullname"],
				"EMAIL"        => $data["email"],
				"USERNAME"     => $data["username"],
				"WEBSITE"      => $this->settings->head_title,
				"IP_ADDR"      => $_SERVER["REMOTE_ADDR"]));
			$email->message(file_get_contents("../extra/account_registered.txt"));
			$email->send($this->settings->webmaster_email);

			return true;
		}
	}
?>
