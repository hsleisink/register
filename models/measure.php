<?php
	class measure_model extends register_model {
		public function set_filter($filter) {
			/* Type
			 */
			$_SESSION["measure_filter"]["type"] = array();

			if (is_array($filter["type"] ?? null)) {
				foreach ($filter["type"] as $type) {
					array_push($_SESSION["measure_filter"]["type"], $type);
				}
			}

			if (count($_SESSION["measure_filter"]["type"]) == 0) {
				unset($_SESSION["measure_filter"]["type"]);
			}

			/* Status
			 */
			$_SESSION["measure_filter"]["status"] = array();

			if (is_array($filter["status"] ?? null)) {
				foreach ($filter["status"] as $status) {
					array_push($_SESSION["measure_filter"]["status"], (int)$status);
				}
			}

			if (count($_SESSION["measure_filter"]["status"]) == 0) {
				unset($_SESSION["measure_filter"]["status"]);
			}

			/* owner
			 */
			if (isset($filter["owner"])) {
				if ($filter["owner"] != 0) {
					$_SESSION["measure_filter"]["owner"] = $filter["owner"];
				} else {
					unset($_SESSION["measure_filter"]["owner"]);
				}
			}
		}

		private function query_filter(&$where, &$args) {
			/* Types
			 */
			if (isset($_SESSION["measure_filter"]["type"])) {
				$filter = array();
				foreach ($_SESSION["measure_filter"]["type"] as $type) {
					array_push($filter, "type=%s");
					array_push($args, $type);
				}
				array_push($where, "(".implode(" or ", $filter).")");
			}

			/* Status
			 */
			if (isset($_SESSION["measure_filter"]["status"])) {
				$filter = array();
				foreach ($_SESSION["measure_filter"]["status"] as $status) {
					array_push($filter, "m.status=%d");
					array_push($args, $status);
				}
				array_push($where, "(".implode(" or ", $filter).")");
			}

			/* Owner
			 */
			if (isset($_SESSION["measure_filter"]["owner"])) {
				array_push($where, "m.owner_id=%d");
				array_push($args, $_SESSION["measure_filter"]["owner"]);
			}
		}

		public function count_measures() {
			$where = array("m.risk_id=r.id and r.organisation_id=%d");
			$args = array($this->organisation_id);

			$this->query_measure_access($where, $args);
			$this->query_filter($where, $args);

			$query = "select count(*) as count from measures m, risks r where ".implode(" and ", $where);

			if (($result = $this->db->execute($query, $args)) == false) {
				return false;
			}

			return $result[0]["count"];
		}

		private function decrypt_and_sort($measures) {
			if (is_true(ENCRYPT_DATA)) {
				foreach ($measures as $i => $measure) {
					$measures[$i]["title"] = $this->decrypt($measure["title"]);
				}
			}

			usort($measures, function($a, $b) {
				if ($a["deadline"] > $b["deadline"]) {
					return 1;
				}

				if ($a["deadline"] < $b["deadline"]) {
					return -1;
				}

				return strcmp(strtolower($a["title"]), strtolower($b["title"]));
			});

			return $measures;
		}

		public function get_measures($offset, $limit) {
			$where = array("m.risk_id=r.id", "m.owner_id=u.id", "r.organisation_id=%d");
			$args = array($this->organisation_id);

			$this->query_measure_access($where, $args);
			$this->query_filter($where, $args);

			$query = "select m.*, UNIX_TIMESTAMP(m.deadline) as deadline, u.fullname as owner, r.identifier, r.horizontal, r.vertical, ".
			         "r.owner_id as risk_owner_id, r.creator_id as risk_creator_id, r.acceptor_id as risk_acceptor_id ".
			         "from measures m, risks r, users u where ".implode(" and ", $where)." order by deadline,id limit %d,%d";

			array_push($args, $offset, $limit);

			if (($measures = $this->db->execute($query, $args)) === false) {
				return false;
			}

			return $this->decrypt_and_sort($measures);
		}

		public function get_measures_attention() {
			$select = "select m.*, UNIX_TIMESTAMP(deadline) as deadline, u.fullname as owner, r.identifier, r.horizontal, r.vertical, ".
			          "r.owner_id as risk_owner_id, r.creator_id as risk_creator_id, r.acceptor_id as risk_acceptor_id";
			$from = "from measures m, risks r, users u";

			/* First selection
			 */
			$where = array("m.risk_id=r.id", "r.organisation_id=%d", "m.owner_id=u.id");
			$args = array($this->organisation_id);

			$this->query_measure_access($where, $args);
			$this->query_filter($where, $args);

			$attention = array();

			// Deadline passed
			array_push($attention, "deadline<=curdate()");

			// Measure not effective
			array_push($attention, "m.status!=%d");
			array_push($args, MEASURE_STATUS_EFFECTIVE);

			array_push($where, "(".implode(" or ", $attention).")");

			$query = $select." ".$from." where ".implode(" and ", $where);

			if (($measures = $this->db->execute($query, $args)) === false) {
				return false;
			}

			/* Second selection, that require a different way of database querying
			 */
			$where = array("m.risk_id=r.id", "r.organisation_id=%d", "m.owner_id=u.id");
			$args = array(ISSUE_STATUS_CLOSED, $this->organisation_id);

			$this->query_measure_access($where, $args);
			$this->query_filter($where, $args);

			$having = array();

			// Active issues
			array_push($having, "issues>%d");
			array_push($args, 0);

			$query = $select.", (select count(*) from issues where measure_id=m.id and type=m.type and status<%d) as issues ".
			         $from." where ".implode(" and ", $where)." having ".implode(" or ", $having);

			if (($extras = $this->db->execute($query, $args)) === false) {
				return false;
			}

			/* Merge two selections
			 */
			foreach ($extras as $extra) {
				$present = false;

				foreach ($measures as $measure) {
					if (($extra["id"] == $measure["id"]) && ($extra["type"] == $measure["type"])) {
						$present = true;
						break;
					}
				}

				if ($present == false) {
					array_push($measures, $extra);
				}
			}

			return $this->decrypt_and_sort($measures);
		}

		/* Search measures
		 */
		public function search_measures($search) {
			$where = array("m.risk_id=r.id", "m.owner_id=u.id", "r.organisation_id=%d");
			$args = array($this->organisation_id);

			$this->query_measure_access($where, $args);

			$filter = array("m.title like %s", "m.description like %s");
			array_push($args, "%".$search."%", "%".$search."%");

			$query = "select m.*, UNIX_TIMESTAMP(m.deadline) as deadline, u.fullname as owner, r.identifier, r.horizontal, r.vertical, ".
			         "r.owner_id as risk_owner_id, r.creator_id as risk_creator_id, r.acceptor_id as risk_acceptor_id ".
			         "from measures m, risks r, users u where ".implode(" and ", $where)." and (".implode(" or ", $filter).")";

			if (($measures = $this->db->execute($query, $args)) === false) {
				return false;
			}

			return $this->decrypt_and_sort($measures);
		}

		private function get_measure($query, $where, $args) {
			$this->query_measure_access($where, $args);

			$query .= " where ".implode(" and ", $where);

			if (($result = $this->db->execute($query, $args)) == false) {
				return false;
			}
			$measure = $result[0];

			if (is_true(ENCRYPT_DATA)) {
				$measure["title"] = $this->decrypt($measure["title"]);
				$measure["description"] = $this->decrypt($measure["description"]);
				$measure["risk"] = $this->decrypt($measure["risk"]);

				foreach (array("cause", "risk_cause", "effect", "risk_effect") as $field) {
					if (isset($measure[$field])) {
						$measure[$field] = $this->decrypt($measure[$field]);
					}
				}
			}

			return $measure;
		}

		public function get_preventive_measure($measure_id) {
			$query = "select m.*, rc.title as risk_cause, c.cause, r.id as risk_id, r.identifier, r.title as risk, u.fullname as owner, o.fullname as risk_owner, ".
			         "r.owner_id as risk_owner_id, r.creator_id as risk_creator_id, r.acceptor_id as risk_acceptor_id, %s as type ".
			         "from measures_preventive m, risk_cause rc, causes c, users u, risks r left join users o on r.owner_id=o.id";
			$where = array("m.risk_cause_id=rc.id", "rc.cause_id=c.id", "rc.risk_id=r.id", "m.owner_id=u.id", "m.id=%d", "r.organisation_id=%d");
			$args = array("preventive", $measure_id, $this->organisation_id);

			return $this->get_measure($query, $where, $args);
		}

		public function get_detective_measure($measure_id) {
			$query = "select m.*, r.identifier, r.title as risk, u.fullname as owner, o.fullname as risk_owner, %s as type, ".
			         "r.owner_id as risk_owner_id, r.creator_id as risk_creator_id, r.acceptor_id as risk_acceptor_id ".
			         "from measures_detective m, users u, risks r left join users o on r.owner_id=o.id";
			$where = array("m.owner_id=u.id", "m.risk_id=r.id", "m.id=%d", "r.organisation_id=%d");
			$args = array("detective", $measure_id, $this->organisation_id);

			return $this->get_measure($query, $where, $args);
		}

		public function get_repressive_measure($measure_id) {
			$query = "select m.*, re.title as risk_effect, e.effect, r.id as risk_id, r.identifier, r.title as risk, u.fullname as owner, o.fullname as risk_owner, ".
			         "r.owner_id as risk_owner_id, r.creator_id as risk_creator_id, r.acceptor_id as risk_acceptor_id, %s as type ".
			         "from measures_repressive m, risk_effect re, effects e, users u, risks r left join users o on r.owner_id=o.id";
			$where = array("m.risk_effect_id=re.id", "re.effect_id=e.id", "re.risk_id=r.id", "m.owner_id=u.id", "m.id=%d", "r.organisation_id=%d");
			$args = array("repressive", $measure_id, $this->organisation_id);

			return $this->get_measure($query, $where, $args);
		}

		/* Measure owner
		 */
		public function get_measure_owners() {
			$query = "select distinct u.id, u.fullname from measures m, risks r, users u ".
			         "where m.risk_id=r.id and m.owner_id=u.id and r.organisation_id=%d ".
			         "order by fullname";

			return $this->db->execute($query, $this->organisation_id);
		}

		/* Measure controls
		 */
		public function get_measure_controls($type, $measure_id) {
			$control_table = "control_".$type;
			$measure_table = "measures_".$type;
			$column = "measure_".$type."_id";

			$query = "select c.id, s.name, c.number, c.title from control_standards s, controls c, %S mc, %S m ".
					 "where s.id=c.control_standard_id and c.id=mc.control_id and mc.%S=m.id and m.id=%d ".
					 "order by s.name, c.id";

			if (($result = $this->db->execute($query, $control_table, $measure_table, $column, $measure_id)) === false) {
				return false;
			
			}

			$controls = array();

			foreach ($result as $item) {
				$standard = $item["name"];
				unset($item["name"]);

				if (isset($controls[$standard]) == false) {
					$controls[$standard] = array();
				}

				array_push($controls[$standard], $item);
			}

			return $controls;
		}

		/* Measure issues
		 */
		public function get_measure_issues($type, $measure_id) {
			$query = "select id, type, title, UNIX_TIMESTAMP(created) as created, status ".
			         "from issues where type=%s and measure_id=%d order by created desc, status";

			if (($issues = $this->db->execute($query, $type, $measure_id)) === false) {
				return false;
			}

			if (is_true(ENCRYPT_DATA)) {
				foreach ($issues as $i => $issue) {
					$issues[$i]["title"] = $this->decrypt($issue["title"]);
				}
			}

			return $issues;
		}
	}
?>
