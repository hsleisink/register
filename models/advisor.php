<?php
	class advisor_model extends Banshee\model {
		public function get_roles() {
			$query = "select a.id, o.name, a.crypto_key, o.id as organisation_id ".
			         "from advisors a, organisations o ".
			         "where a.organisation_id=o.id and a.user_id=%d order by name";

			return $this->db->execute($query, $this->user->id);
		}

		public function request_adviser_role($user) {
			$query = "select o.id from organisations o, users u ".
			         "where o.id=u.organisation_id and (u.username=%s or u.email=%s) and o.id!=%d";
			if (($result = $this->db->execute($query, $user, $user, $this->user->organisation_id)) == false) {
				$this->view->add_message($this->language->module_text("user_not_found"));
				return false;
			}
			$organisation_id = $result[0]["id"];

			$query = "select count(*) as count from advisors where organisation_id=%d and user_id=%d";
			if (($result = $this->db->execute($query, $organisation_id, $this->user->id)) == false) {
				$this->view->add_message($this->language->global_text("error_database"));
				return false;
			}
			if ($result[0]["count"] > 0) {
				$this->view->add_message($this->language->module_text("act_as_advisor"));
				return false;
			}

			$data = array(
				"id"              => null,
				"organisation_id" => $organisation_id,
				"user_id"         => $this->user->id,
				"crypto_key"      => null);

			return $this->db->insert("advisors", $data) !== false;
		}

		public function activate_role($id) {
			$query = "select * from advisors where id=%d and user_id=%d";
			if (($result = $this->db->execute($query, $id, $this->user->id)) === false) {
				return false;
			}
			$advisor = $result[0];

			$private_key = $this->decrypt($this->user->private_key);
			$rsa = new \Banshee\Protocol\RSA($private_key);
			$crypto_key = $rsa->decrypt_with_private_key($advisor["crypto_key"]);

			$cookie = new \Banshee\secure_cookie($this->settings);
			$cookie->advisor_crypto_key = $crypto_key;

			$_SESSION["advisor_organisation_id"] = (int)$advisor["organisation_id"];

			$this->user->log_action("advisor role activated for ".$this->get_organisation($advisor["organisation_id"]));

			$this->reset_pagination_and_filters();

			if (is_true(MENU_PERSONALIZED)) {
				$cache = new \Banshee\Core\cache($this->db, "banshee_menu");
				$cache->store("last_updated", time(), 365 * DAY);
			}

			return true;
		}

		public function get_organisation($id) {
			if (($organisation = $this->db->entry("organisations", $id)) == false) {
				return false;
			}

			return $organisation["name"];
		}

		public function deactivate_role() {
			unset($_SESSION["advisor_organisation_id"]);
			$cookie = new \Banshee\secure_cookie($this->settings);
			$cookie->advisor_crypto_key = null;

			$this->user->log_action("advisor role deactivated");

			$this->reset_pagination_and_filters();

			if (is_true(MENU_PERSONALIZED)) {
				$cache = new \Banshee\Core\cache($this->db, "banshee_menu");
				$cache->store("last_updated", time(), 365 * DAY);
			}
		}

		public function delete_role($id) {
			$query = "delete from advisors where id=%d and user_id=%d";

			return $this->db->query($query, $id, $this->user->id) !== false;
		}
	}
?>
