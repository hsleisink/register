<?php
	class risk_model extends register_model {
		public function set_filter($filter) {
			/* Level
			 */
			if (isset($filter["level"]) == false) {
				unset($_SESSION["risk_filter"]["level"]);
			} else if ($filter["level"] == 0) {
				unset($_SESSION["risk_filter"]["level"]);
			} else {
				$_SESSION["risk_filter"]["level"] = $filter["level"];
			}

			/* Tags
			 */
			$_SESSION["risk_filter"]["tags"] = array();

			if (is_array($filter["tags"] ?? null)) {
				foreach ($filter["tags"] as $tag) {
					array_push($_SESSION["risk_filter"]["tags"], (int)$tag);
				}
			}

			if (count($_SESSION["risk_filter"]["tags"]) == 0) {
				unset($_SESSION["risk_filter"]["tags"]);
			}

			/* owner
			 */
			if (isset($filter["owner"])) {
				if ($filter["owner"] != 0) {
					$_SESSION["risk_filter"]["owner"] = $filter["owner"];
				} else {
					unset($_SESSION["risk_filter"]["owner"]);
				}
			}
		}

		private function query_filter(&$where, &$args) {
			/* Risk level
			 */
			if (isset($_SESSION["risk_filter"]["level"])) {
				if ($this->query_level_filter($_SESSION["risk_filter"]["level"], $where, $args) == false) {
					return false;
				}
			}

			/* Tags
			 */
			if (isset($_SESSION["risk_filter"]["tags"])) {
				$tag_count = count($_SESSION["risk_filter"]["tags"]);
				$tag_filter = implode(" or ", array_fill(1, $tag_count, "tag_id=%d"));
				array_push($where, "(select count(*) from risk_tag where risk_id=r.id and (".$tag_filter."))=".$tag_count);
				array_push($args, $_SESSION["risk_filter"]["tags"]);
			}

			/* Owner
			 */
			if (isset($_SESSION["risk_filter"]["owner"])) {
				array_push($where, "owner_id=%d");
				array_push($args, $_SESSION["risk_filter"]["owner"]);
			}

			return true;
		}

		public function count_risks() {
			$where = array("r.organisation_id=%d");
			$args = array($this->organisation_id);

			$this->query_risk_access($where, $args);
			if ($this->query_filter($where, $args) == false) {
				return false;
			}

			$query = "select count(*) as count from risks r where ".implode(" and ", $where);

			if (($result = $this->db->execute($query, $args)) == false) {
				return false;
			}

			return $result[0]["count"];
		}

		/* Drcrypt and sort
		 */
		private function decrypt_and_sort($risks) {
			if (is_true(ENCRYPT_DATA)) {
				foreach ($risks as $i => $risk) {
					$risks[$i]["title"] = $this->decrypt($risk["title"]);
				}
			}

			usort($risks, function($a, $b) {
				if ($a["created"] > $b["created"]) {
					return -1;
				}

				if ($a["created"] < $b["created"]) {
					return 1;
				}

				return strcmp(strtolower($a["title"]), strtolower($b["title"]));
			});

			return $risks;
		}

		/* Get risks
		 */
		public function get_risks($offset, $limit) {
			$where = array("r.organisation_id=%d");
			$args = array(MEASURE_STATUS_EFFECTIVE, $this->organisation_id);

			$this->query_risk_access($where, $args);
			if ($this->query_filter($where, $args) == false) {
				return false;
			}

			$query = "select distinct r.id, r.identifier, r.title, r.horizontal, r.vertical, r.approach, levels_mitigated, ".
			         "UNIX_TIMESTAMP(created) as created, u.fullname as owner, acceptor_id, accept_end_date, ".
			         "(select count(*) from measures where risk_id=r.id) as measure_count, ".
			         "(select count(*) from measures where risk_id=r.id and status=%d) as measures_done ".
			         "from risks r left join users u on r.owner_id=u.id where ".implode(" and ", $where)." ".
			         "order by created desc,id limit %d,%d";

			array_push($args, $offset, $limit);

			if (($risks = $this->db->execute($query, $args)) === false) {
				return false;
			}

			return $this->decrypt_and_sort($risks);
		}

		public function get_risks_attention() {
			/* First selection
			 */
			$select = "select distinct r.id, r.identifier, r.title, r.horizontal, r.vertical, r.approach, ".
			          "levels_mitigated, UNIX_TIMESTAMP(created) as created, u.fullname as owner, ".
			          "(select count(*) from measures where risk_id=r.id) as measure_count, ".
			          "(select count(*) from measures where risk_id=r.id and status=%d) as measures_done";
			$from = "from risks r left join users u on r.owner_id=u.id";

			$where = array("r.organisation_id=%d");
			$args = array(MEASURE_STATUS_EFFECTIVE, $this->organisation_id);

			$this->query_risk_access($where, $args);
			if ($this->query_filter($where, $args) == false) {
				return false;
			}
			if ($this->query_level_filter($this->risk_appetite + 1, $where, $args, true) == false) {
				return false;
			}

			$attention = array();

			// Risk has no owner
			array_push($attention, "r.owner_id is null");

			// Approach decided later
			array_push($attention, "approach=%d");
			array_push($args, RISK_APPROACH_DECIDE_LATER);

			// Risk acceptance needs approval
			array_push($attention, "(approach=%d and accept_end_date<=curdate())");
			array_push($args, RISK_APPROACH_ACCEPT);

			array_push($where, "(".implode(" or ", $attention).")");

			$query = $select." ".$from." where ".implode(" and ", $where);

			if (($risks = $this->db->execute($query, $args)) === false) {
				return false;
			}

			/* Second selection, that require a different way of database querying
			 */
			$where = array("r.organisation_id=%d");
			$args = array(MEASURE_STATUS_EFFECTIVE, ISSUE_STATUS_CLOSED, $this->organisation_id);

			$this->query_risk_access($where, $args);
			if ($this->query_filter($where, $args) == false) {
				return false;
			}
			if ($this->query_level_filter($this->risk_appetite + 1, $where, $args, true) == false) {
				return false;
			}

			$having = array();

			// No measures for not-accepted risk
			array_push($having, "(measure_count=%d and approach!=%d)");
			array_push($args, 0, RISK_APPROACH_ACCEPT);

			// Measures for accepted risk
			array_push($having, "(measure_count>%d and approach=%d)");
			array_push($args, 0, RISK_APPROACH_ACCEPT);

			// Not all measures effective
			array_push($having, "measures_done!=measure_count");

			// No causes assigned
			array_push($having, "cause_count=%d");
			array_push($args, 0);

			// No effects assigned
			array_push($having, "effect_count=%d");
			array_push($args, 0);

			// Issues
			array_push($having, "issues>%d");
			array_push($args, 0);

			$query = $select.", ".
				"(select count(*) from risk_cause where risk_id=r.id) as cause_count, ".
				"(select count(*) from risk_effect where risk_id=r.id) as effect_count, ".
				"(select count(*) from issues where risk_id=r.id and status<%d) as issues ".
				$from." where ".implode(" and ", $where)." having ".implode(" or ", $having);

			if (($extras = $this->db->execute($query, $args)) === false) {
				return false;
			}

			/* Merge two selections
			 */
			foreach ($extras as $extra) {
				$present = false;

				foreach ($risks as $risk) {
					if ($extra["id"] == $risk["id"]) {
						$present = true;
						break;
					}
				}

				if ($present == false) {
					array_push($risks, $extra);
				}
			}

			return $this->decrypt_and_sort($risks);
		}

		public function get_risk($risk_id) {
			$query = "select distinct r.*, UNIX_TIMESTAMP(created) as created, c.fullname as creator, o.fullname as owner, ".
			         "(select count(*) from measures where risk_id=r.id) as measure_count, ".
			         "(select count(*) from measures where risk_id=r.id and status=%d) as measures_done, ".
			         "(select count(*) from issues where risk_id=r.id and status!=%d) as issues ".
			         "from users c, risks r left join users o on r.owner_id=o.id ".
			         "where r.creator_id=c.id and r.id=%d and r.organisation_id=%d";

			if (($result = $this->db->execute($query, MEASURE_STATUS_EFFECTIVE, ISSUE_STATUS_CLOSED, $risk_id, $this->organisation_id)) == false) {
				return false;
			}
			$risk = $result[0];

			if (is_true(ENCRYPT_DATA)) {
				$risk["title"] = $this->decrypt($risk["title"]);
				$risk["description"] = $this->decrypt($risk["description"]);
			}

			if ($risk["acceptor_id"] != null) {
				$risk["acceptor"] = $this->get_user_fullname($risk["acceptor_id"]);
			}

			return $risk;
		}

		public function get_all_identifiers() {
			$query = "select identifier from risks where organisation_id=%d order by identifier";

			if (($result = $this->db->execute($query, $this->user->organisation_id)) === false) {
				return false;
			}

			$identifiers = array();
			foreach ($result as $item) {
				array_push($identifiers, $item["identifier"]);
			}

			return $identifiers;
		}

		public function get_impact_labels() {
			return ($this->impact_axis == "horizontal") ? $this->get_horizontal_labels() : $this->get_vertical_labels();
		}

		public function get_reference_table() {
			$columns = array();
			for ($i = 1; $i <= $this->matrix_size; $i++) {
				array_push($columns, "impact".$i);
			}

			$query = "select effect,".implode(",", $columns)." from effects where organisation_id=%d";

			if (($table = $this->db->execute($query, $this->organisation_id)) === false) {
				return false;
			}

			foreach ($table as $e => $impact) {
				$table[$e]["effect"] = $this->decrypt($impact["effect"]);
				for ($i = 1; $i <= $this->matrix_size; $i++) {
					if ($impact["impact".$i] != "") {
						$table[$e]["impact".$i] = $this->decrypt($impact["impact".$i]);
					}
				}
			}

			return $table;
		}

		/* Get risk level
		 */
		public function get_risk_level($risk_id) {
			$query = "select horizontal, vertical  from risks where id=%d and organisation_id";

			if (($result = $this->db->execute($query, $risk_id, $this->organisation_id)) == false) {
				return false;
			}
			$risk = $result[0];

			if (($matrix = $this->get_matrix()) == false) {
				return false;
			}

			return $this->level_from_matrix($matrix, $risk["horizontal"], $risk["vertical"]);
		}

		/* Search risks
		 */
		public function search_risks($search) {
			$args = array(MEASURE_STATUS_EFFECTIVE, ISSUE_STATUS_CLOSED);

			$where = array("r.organisation_id=%d");
			array_push($args, $this->organisation_id);

			$this->query_risk_access($where, $args);

			$filter = array("r.identifier like %s");
			array_push($args, $search."%");

			if (is_false(ENCRYPT_DATA)) {
				array_push($filter, "r.title like %s", "r.description like %s");
				array_push($args, "%".$search."%", "%".$search."%");
			}

			$query = "select r.*, UNIX_TIMESTAMP(created) as created, o.fullname as owner, ".
			         "(select count(*) from measures where risk_id=r.id) as measure_count, ".
			         "(select count(*) from measures where risk_id=r.id and status=%d) as measures_done, ".
			         "(select count(*) from issues where risk_id=r.id and status<%d) as issues ".
			         "from risks r left join users o on r.owner_id=o.id ".
			         "where ".implode(" and ", $where)." and (".implode(" or ", $filter).")";

			if (($risks = $this->db->execute($query, $args)) === false) {
				return false;
			}

			return $this->decrypt_and_sort($risks);
		}

		/* Risk acceptance approving
		 */
		public function get_risk_acceptors($level) {
			$query = "select id, fullname from users where risk_accept>=%d and organisation_id=%d order by fullname";

			return $this->db->execute($query, $level, $this->organisation_id);
		}

		/* Get risk accept status
		 */
		public function get_risk_accept_status($risk_id) {
			$query = "select approach, acceptor_id, accept_end_date, horizontal, vertical from risks where id=%d and organisation_id";

			if (($result = $this->db->execute($query, $risk_id, $this->organisation_id)) == false) {
				return false;
			}

			return $result[0];
		}

		/* Accept end date passed
		 */
		public function accept_end_date_passed($accept_end_date) {
			if ($accept_end_date == DATE_NOT_SET) {
				return true;
			}

			return strtotime($accept_end_date) <= time();
		}

		/* Set risk acceptor
		 */
		public function set_risk_acceptor($values) {
			if ($this->may_edit_risk($values["risk_id"]) == false) {
				$this->user->log_action("unauthorized set risk acceptor attempt %d", $values["risk_id"]);
				return false;
			}

			if (($level = $this->get_risk_level($values["risk_id"])) == false) {
				$this->view->add_message($this->language->module_text("error_risk_not_found"));
				return false;
			}

			if ($level <= $this->risk_appetite) {
				$this->view->add_message($this->language->module_text("error_risk_level_not_high_enough"));
				return false;
			}

			if (($acceptors = $this->get_risk_acceptors($level)) == false) {
				$this->view->add_message($this->language->module_text("error_no_risk_acceptance_approvers_available"));
				return false;
			}

			foreach ($acceptors as $acceptor) {
				if ($acceptor["id"] == $values["acceptor_id"]) {
					$keys = array("acceptor_id", "approach");

					$values["approach"] = RISK_APPROACH_ACCEPT;

					return $this->db->update("risks", $values["risk_id"], $values, $keys);
				}
			}

			$this->user->log_action("attempt to set invalid risk acceptance approver (risk id:%d, approver user id:%d)", $values["risk_id"], $values["acceptor_id"]);
			$this->view->add_message($this->language->module_text("error_risk_acceptance_aprover_invalid"));

			return false;
		}

		/* Approve risk acceptance
		 */
		public function approve_risk_acceptance($risk_id, $period) {
			$query = "select approach, acceptor_id, accept_end_date from risks where id=%d and organisation_id=%d";
			if (($result = $this->db->execute($query, $risk_id, $this->organisation_id)) == false) {
				return false;
			}
			$risk = $result[0];

			if ($risk["acceptor_id"] != $this->user->id) {
				$this->user->log_action("unauthorized attempt to approve risk acceptance (risk id:%d, user id:%d)", $risk_id, $this->user->id);
				return false;
			}

			if ($risk["approach"] != RISK_APPROACH_ACCEPT) {
				return false;
			}

			if ($this->accept_end_date_passed($risk["accept_end_date"]) == false) {
				return false;
			}

			if (($period < 1) || ($period > 12)) { 
				return false;
			}

			$data = array("accept_end_date" => date("Y-m-d", strtotime("+".$period." months")));

			return $this->db->update("risks", $risk_id, $data) !== false;
		}

		/* Deny risk acceptance
		 */
		public function deny_risk_acceptance($risk_id) {
			$query = "select approach, acceptor_id, accept_end_date from risks where id=%d and organisation_id=%d";
			if (($result = $this->db->execute($query, $risk_id, $this->organisation_id)) == false) {
				return false;
			}
			$risk = $result[0];

			if ($risk["acceptor_id"] != $this->user->id) {
				$this->user->log_action("unauthorized attempt to deny risk acceptance (risk id:%d, user id:%d)", $risk_id, $this->user->id);
				return false;
			}

			if ($risk["approach"] != RISK_APPROACH_ACCEPT) {
				return false;
			}

			$data = array(
				"acceptor_id"     => NULL,
				"approach"        => RISK_APPROACH_DECIDE_LATER,
				"accept_end_date" => DATE_NOT_SET);

			return $this->db->update("risks", $risk_id, $data) !== false;
		}

		/* Send accept notification 
		 */
		public function send_accept_notification($risk_id, $acceptor_id) {
			if (($risk = $this->get_risk($risk_id)) == false) {
				return false;
			}

			if (($acceptor = $this->db->entry("users", $acceptor_id)) == false) {
				return false;
			}

			$message = "<p>Hello ".$acceptor["fullname"].",</p>".
			           "<p>The acceptance of a risk with a risk level higher than the risk appetite awaits your approval. That risk has the following title.</p>".
			           "<p>\"%s\"</p>".
			           "<p>Click <a href=\"https://%s/risk/%d\">here</a> to visit the Risk Register and view this risk.</p>";
			$message = sprintf($message, $risk["title"], $_SERVER["HTTP_HOST"], $risk_id);

			$email = new register_email("Risk acceptance awaits your approval", $this->settings->webmaster_email);
			$email->message($message);

			$this->cc_risk_managers($email);

			return $email->send($acceptor["email"], $acceptor["fullname"]);
		}

		/* Get risk owners
		 */
		public function get_risk_owners($risk_id = null) {
			$query = "select distinct u.id, u.fullname from users u, user_role r where u.organisation_id=%d ".
			         "and u.id=r.user_id and (r.role_id=%d";
			$args = array($this->organisation_id, $this->settings->role_id_risk_owner);
			if ($risk_id != null) {
				$query .= " or u.id in (select owner_id from risks where id=%d and organisation_id=%d)";
				array_push($args, $risk_id, $this->organisation_id);
			}
			$query .= ") order by fullname";

			return $this->db->execute($query, $args);
		}

		/* Get risk owner
		 */
		public function get_risk_owner($risk_id) {
			$query = "select owner_id from risks where id=%d and organisation_id";

			if (($result = $this->db->execute($query, $risk_id, $this->organisation_id)) == false) {
				return false;
			}

			return $result[0]["owner_id"];
		}

		/* Set risk owner
		 */
		public function set_risk_owner($values) {
			if (isset($values["owner_id"]) == false) {
				$this->view->add_message($this->language->module_text("error_no_owner_set"));
				return false;
			}

			if ($this->may_edit_risk($values["risk_id"]) == false) {
				$this->user->log_action("unauthorized set risk owner attempt %d", $values["risk_id"]);
				return false;
			}

			if (($owners = $this->get_risk_owners($values["risk_id"])) != false) {
				foreach ($owners as $owner) {
					if ($values["owner_id"] == $owner["id"]) {
						$keys = array("owner_id");
						return $this->db->update("risks", $values["risk_id"], $values, $keys) !== false;
					}
				}
			}

			$this->user->log_action("attempt to set invalid risk owner (risk id:%d, owner id:%d)", $values["risk_id"], $values["owner_id"]);

			$this->view->add_message($this->language->module_text("error_owner_invalid"));

			return false;
		}

		/* Send owner notification 
		 */
		public function send_owner_notification($risk_id, $owner_id) {
			if (($risk = $this->get_risk($risk_id)) == false) {
				return false;
			}

			if (($owner = $this->db->entry("users", $owner_id)) == false) {
				return false;
			}

			$message = $this->language->module_text("email_owner_notification_body");
			$message = sprintf($message, $owner["fullname"], $risk["title"], $_SERVER["HTTP_HOST"], $risk_id);

			$email = new register_email($this->language->module_text("email_owner_notification_subject"), $this->settings->webmaster_email);
			$email->message($message);

			$this->cc_risk_managers($email, $owner["email"]);

			return $email->send($owner["email"], $owner["fullname"]);
		}

		/* Risk okay
		 */
		public function risk_okay($risk) {
			$result = true;

			if (trim($risk["title"]) == "") {
				$this->view->add_message($this->language->module_text("error_specify_title"));
				$result = false;
			}

			if (trim($risk["identifier"]) == "") {
				$this->view->add_message($this->language->module_text("error_specify_identifier"));
				$result = false;
			} else {
				$query = "select id from risks where identifier=%s and organisation_id=%d";

				if (($identifier = $this->db->execute($query, $risk["identifier"], $this->organisation_id)) === false) {
					$this->view->add_message($this->language->global_text("error_database"));
					$result = false;
				} else if (count($identifier) > 0) {
					if ((isset($risk["id"]) == false) || (isset($risk["id"]) && ($identifier[0]["id"] != $risk["id"]))) {
						$this->view->add_message($this->language->module_text("error_identifier_exists"));
						$result = false;
					}
				}
			}

			if (($risk["vertical"] < 1) || ($risk["vertical"] > $this->matrix_size)) {
				$this->view->add_message($this->language->module_text("error_invalid")." ".$this->matrix_vertical);
				$result = false;
			}

			if (($risk["horizontal"] < 1) || ($risk["horizontal"] > $this->matrix_size)) {
				$this->view->add_message($this->language->module_text("error_invalid")." ".$this->matrix_horizontal);
				$result = false;
			}

			if (($risk["approach"] < 0) || ($risk["approach"] >= count(RISK_APPROACHES))) {
				$this->view->add_message($this->language->module_text("error_invalid")." ".$this->language->module_text("error_approach"));
				$result = false;
			}

			if ($result) {
				if (($matrix = $this->get_matrix()) == false) {
					$this->view->add_message($this->language->global_text("error_database"));
					return false;
				}

				$risk_level = $this->level_from_matrix($matrix, $risk["horizontal"], $risk["vertical"]);

				if (($risk["approach"] == RISK_APPROACH_ACCEPT) && ($risk_level > $this->risk_appetite)) {
					if (isset($risk["id"]) == false) {
						$this->view->add_message($this->language->module_text("error_risk_level_too_high"));
						$result = false;
					} else if (($current = $this->get_risk($risk["id"])) == false) {
						$this->view->add_message($this->language->global_text("error_database"));
						return false;
					} else {
						if ($current["owner_id"] == null) {
							$this->view->add_message($this->language->module_text("error_owner_and_high_level"));
							$result = false;
						}

						if (($causes = $this->get_risk_causes($risk["id"])) === false) {
							$this->view->add_message($this->language->global_text("error_database"));
							return false;
						} else if (count($causes) == 0) {
							$this->view->add_message($this->language->module_text("error_risk_no_causes_and_high_level"));
							$result = false;
						}

						if (($effects = $this->get_risk_effects($risk["id"])) === false) {
							$this->view->add_message($this->language->global_text("error_database"));
							return false;
						} else if (count($effects) == 0) {
							$this->view->add_message($this->language->module_text("error_risk_no_effects_and_high_level"));
							$result = false;
						}
					}
				}
			}

			return $result;
		}

		/* Create risk
		 */
		public function create_risk($risk) {
			if (in_array($this->settings->role_id_employee, $this->user->role_ids) == false) {
				$this->user->log_action("unauthorized risk create attempt %d", $risk["id"]);
				return false;
			}

			$keys = array("id", "organisation_id", "identifier", "title", "description", "created", "creator_id", "owner_id", "horizontal", "vertical", "approach", "levels_mitigated", "acceptor_id", "accept_end_date");

			$risk["id"] = null;
			$risk["organisation_id"] = $this->organisation_id;
			$risk["identifier"] = substr($risk["identifier"], 0, MAXLENGTH_RISK_IDENTIFIER);
			$risk["title"] = substr($risk["title"], 0, MAXLENGTH_RISK_TITLE);
			$risk["created"] = date("Y-m-d H:i;s");
			$risk["creator_id"] = $this->user->id;
			$risk["owner_id"] = NULL;
			$risk["levels_mitigated"] = 0;
			$risk["acceptor_id"] = NULL;
			$risk["accept_end_date"] = DATE_NOT_SET;

			if (is_true(ENCRYPT_DATA)) {
				$risk["title"] = $this->encrypt($risk["title"]);
				$risk["description"] = $this->encrypt($risk["description"]);
			}

			if ($this->db->insert("risks", $risk, $keys) === false) {
				return false;
			}

			return $this->db->last_insert_id;
		}

		/* Update risk
		 */
		public function update_risk($risk) {
			if ($this->may_edit_risk($risk["id"]) == false) {
				$this->user->log_action("unauthorized risk update attempt %d", $risk["id"]);
				return false;
			}

			if (($current = $this->get_risk($risk["id"])) == false) {
				return false;
			}

			$risk["identifier"] = substr($risk["identifier"], 0, MAXLENGTH_RISK_IDENTIFIER);
			$risk["title"] = substr($risk["title"], 0, MAXLENGTH_RISK_TITLE);

			$keys = array("identifier", "title", "description", "horizontal", "vertical", "approach");
			if (isset($_POST["levels_mitigated"])) {
				array_push($keys, "levels_mitigated");

				if (($matrix = $this->get_matrix()) == false) {
					return false;
				}

				$level_old = $this->level_from_matrix($matrix, $current["horizontal"], $current["vertical"]);
				$level_new = $this->level_from_matrix($matrix, $risk["horizontal"], $risk["vertical"]);

				$risk["levels_mitigated"] = max($risk["levels_mitigated"] + $level_new - $level_old, 0);
			}

			if (($matrix = $this->get_matrix()) == false) {
				return false;
			}

			if (($risk["approach"] != RISK_APPROACH_ACCEPT) || ($this->level_from_matrix($matrix, $risk["horizontal"], $risk["vertical"]) == 1)) {
				array_push($keys, "acceptor_id", "accept_end_date");

				$risk["acceptor_id"] = null;
				$risk["accept_end_date"] = DATE_NOT_SET;
			}

			if (is_true(ENCRYPT_DATA)) {
				$risk["title"] = $this->encrypt($risk["title"]);
				$risk["description"] = $this->encrypt($risk["description"]);
			}

			return $this->db->update("risks", $risk["id"], $risk, $keys) !== false;
		}

		/* Delete risk
		 */
		public function delete_risk($risk_id) {
			if ($this->may_edit_risk($risk_id) == false) {
				$this->user->log_action("unauthorized risk delete attempt %d", $risk_id);
				return false;
			}

			if ($this->db->query("begin") === false) {
				return false;
			}

			$query = "select id, type from measures where risk_id=%d";
			if (($measures = $this->db->execute($query, $risk_id)) === false) {
				$this->db->query("rollback");
				return false;
			}

			foreach ($measures as $measure) {
				switch ($measure["type"]) {
					case "preventive":
						$result = $this->borrow("risk/measure")->delete_preventive($measure["id"], false);
						break;
					case "detective":
						$result = $this->borrow("risk/measure")->delete_detective($measure["id"], false);
						break;
					case "repressive":
						$result = $this->borrow("risk/measure")->delete_repressive($measure["id"], false);
						break;
				}

				if ($result === false) {
					$this->db->query("rollback");
					return false;
				}
			}

			$queries = array(
				"delete from risk_log where risk_id=%d",
				"delete from risk_cause where risk_id=%d",
				"delete from risk_effect where risk_id=%d",
				"delete from risk_tag where risk_id=%d",
				"delete from risks where id=%d");

			foreach ($queries as $query) {
				if ($this->db->execute($query, $risk_id) === false) {
					$this->db->query("rollback");
					return false;
				}
			}

			return $this->db->query("commit") !== false;
		}

		/* Risk tags
		 */
		public function get_tags() {
			$query = "select id, name from tag_categories where organisation_id=%d";

			if (($result = $this->db->execute($query, $this->organisation_id)) === false) {
				return false;
			}

			$categories = array();

			$query = "select * from tags where tag_category_id=%d";
			foreach ($result as $item) {
				if (is_true(ENCRYPT_DATA)) {
					$item["name"] = $this->decrypt($item["name"]);
				}

				if (($tags = $this->db->execute($query, $item["id"])) === false) {
					return false;
				}

				if (is_true(ENCRYPT_DATA)) {
					foreach ($tags as $i => $tag) {
						$tags[$i]["name"] = $this->decrypt($tag["name"]);
					}
				}

				$category = array();
				foreach ($tags as $tag) {
					$category[$tag["id"]] = $tag["name"];
				}

				asort($category);

				$categories[$item["name"]] = $category;
			}

			ksort($categories);

			return $categories;
		}

		public function get_risk_tags($risk_id) {
			$query = "select tag_id from risk_tag where risk_id=%d";

			if (($result = $this->db->execute($query, $risk_id)) === false) {
				return false;
			}

			$tags = array();
			foreach ($result as $item) {
				array_push($tags, $item["tag_id"]);
			}

			return array("risk_id" => $risk_id, "tags" => $tags);
		}

		public function assign_tags($tags) {
			if ($this->may_edit_risk($tags["risk_id"]) == false) {
				$this->user->log_action("unauthorized risk tag attempt %d", $risk_id);
				return false;
			}

			$query = "delete from risk_tag where risk_id=%d";
			if ($this->db->query($query, $tags["risk_id"]) === false) {
				return false;
			}

			if (is_array($tags["tags"] ?? null) == false) {
				return true;
			}

			$query = "select t.id from tags t, tag_categories c where t.tag_category_id=c.id and c.organisation_id=%d";
			if (($result = $this->db->execute($query, $this->organisation_id)) == false) {
				return false;
			}

			$tag_ids = array();
			foreach ($result as $item) {
				array_push($tag_ids, $item["id"]);
			}

			$values = array("risk_id" => $tags["risk_id"]);
			foreach ($tags["tags"] as $tag) {
				if (in_array($tag, $tag_ids) == false) {
					$this->user->log_action("attempt to assign invalid tag (risk id: %d, tag id: %d)", $tags["risk_id"], $tag);
					continue;
				}

				$values["tag_id"] = $tag;
				$this->db->insert("risk_tag", $values);
			}

			return true;
		}

		/* Risk cause
		 */
		public function get_risk_causes($risk_id) {
			$query = "select r.*, c.cause from risk_cause r, causes c where r.cause_id=c.id and risk_id=%d";

			if (($causes = $this->db->execute($query, $risk_id)) === false) {
				return false;
			}

			if (is_true(ENCRYPT_DATA)) {
				foreach ($causes as $i => $cause) {
					$causes[$i]["cause"] = $this->decrypt($cause["cause"]);
					$causes[$i]["title"] = $this->decrypt($cause["title"]);
					$causes[$i]["description"] = $this->decrypt($cause["description"]);
				}
			}

			usort($causes, function($a, $b) {
				if (($result = strcmp($a["cause"], $b["cause"])) != 0) {
					return $result;
				}

				return strcmp($a["title"], $b["title"]);
			});

			return $causes;
		}

		public function get_causes() {
			return $this->borrow("cms/cause")->get_causes();
		}

		public function get_risk_cause($cause_id) {
			$query = "select c.* from risk_cause c, risks r where c.risk_id=r.id and c.id=%d and r.organisation_id=%d";

			if (($result = $this->db->execute($query, $cause_id, $this->organisation_id)) == false) {
				$this->user->log_action("unauthorized risk cause view attempt %d", $cause_id);
				return false;
			}
			$cause = $result[0];

			if (is_true(ENCRYPT_DATA)) {
				$cause["title"] = $this->decrypt($cause["title"]);
				$cause["description"] = $this->decrypt($cause["description"]);
			}

			return $cause;
		}

		public function cause_okay($cause) {
			$result = true;

			if (trim($cause["title"]) == "") {
				$this->view->add_message($this->language->module_text("error_specify_title"));
				$result = false;
			}

			return $result;
		}

		private function valid_cause_id($cause_id) {
			$query = "select count(*) as count from causes where id=%d and organisation_id=%d";

			if (($result = $this->db->execute($query, $cause_id, $this->organisation_id)) == false) {
				return false;
			}

			return $result[0]["count"] == 1;
		}

		public function create_cause($cause) {
			if ($this->may_edit_risk($cause["risk_id"]) == false) {
				$this->user->log_action("unauthorized risk cause create attempt %d", $cause["risk_id"]);
				return false;
			}

			if ($this->valid_cause_id($cause["cause_id"]) == false) {
				$this->user->log_action("attempt to assign invalid cause %d to risk %d", $cause["cause_id"], $cause["risk_id"]);
				return false;
			}

			$keys = array("id", "risk_id", "cause_id", "title", "description");

			$cause["id"] = null;
			$cause["title"] = substr($cause["title"], 0, MAXLENGTH_CAUSE_EFFECT_TITLE);

			if (is_true(ENCRYPT_DATA)) {
				$cause["title"] = $this->encrypt($cause["title"]);
				$cause["description"] = $this->encrypt($cause["description"]);
			}

			if ($this->db->insert("risk_cause", $cause, $keys) === false) {
				return false;
			}

			return $this->db->last_insert_id;
		}

		public function update_cause($cause) {
			if (($current = $this->get_risk_cause($cause["id"])) == false) {
				$this->user->log_action("invalid risk cause update attempt %d", $cause["id"]);
				return false;
			}

			if ($this->may_edit_risk($current["risk_id"]) == false) {
				$this->user->log_action("unauthorized risk cause update attempt %d", $cause["id"]);
				return false;
			}

			if ($this->valid_cause_id($cause["cause_id"]) == false) {
				$this->user->log_action("attempt to assign invalid cause %d to risk %d", $cause["cause_id"], $current["risk_id"]);
				return false;
			}

			$keys = array("cause_id", "title", "description");

			$cause["title"] = substr($cause["title"], 0, MAXLENGTH_CAUSE_EFFECT_TITLE);

			if (is_true(ENCRYPT_DATA)) {
				$cause["title"] = $this->encrypt($cause["title"]);
				$cause["description"] = $this->encrypt($cause["description"]);
			}

			return $this->db->update("risk_cause", $cause["id"], $cause, $keys) !== false;
		}

		public function delete_cause_okay($cause_id) {
			$query = "select count(*) as count from measures_preventive where risk_cause_id=%d";
			if (($result = $this->db->execute($query, $cause_id)) === false) {
				$this->view->add_message($this->language->global_text("error_database"));
				return false;
			}
			if ($result[0]["count"] > 0) {
				$this->view->add_message($this->language->module_text("error_cause_has_measures"));
				return false;
			}

			return true;
		}

		public function delete_cause($cause_id) {
			if (($current = $this->get_risk_cause($cause_id)) == false) {
				$this->user->log_action("invalid risk cause delete attempt %d", $cause_id);
				return false;
			}

			if ($this->may_edit_risk($current["risk_id"]) == false) {
				$this->user->log_action("unauthorized risk cause delete attempt %d", $cause_id);
				return false;
			}

			return $this->db->delete("risk_cause", $cause_id);
		}

		/* Risk effect
		 */
		public function get_risk_effects($risk_id) {
			$query = "select r.*, c.effect from risk_effect r, effects c where r.effect_id=c.id and risk_id=%d";

			if (($effects = $this->db->execute($query, $risk_id)) === false) {
				$this->user->log_action("unauthorized risk effect view attempt %d", $effect["id"]);
				return false;
			}

			if (is_true(ENCRYPT_DATA)) {
				foreach ($effects as $i => $effect) {
					$effects[$i]["effect"] = $this->decrypt($effect["effect"]);
					$effects[$i]["title"] = $this->decrypt($effect["title"]);
					$effects[$i]["description"] = $this->decrypt($effect["description"]);
				}
			}

			usort($effects, function($a, $b) {
				if (($result = strcmp($a["effect"], $b["effect"])) != 0) {
					return $result;
				}

				return strcmp($a["title"], $b["title"]);
			});


			return $effects;
		}

		public function get_effects() {
			return $this->borrow("cms/effect")->get_effects();
		}

		public function get_risk_effect($effect_id) {
			$query = "select c.* from risk_effect c, risks r where c.risk_id=r.id and c.id=%d and r.organisation_id=%d";

			if (($result = $this->db->execute($query, $effect_id, $this->organisation_id)) == false) {
				$this->user->log_action("unauthorized risk effect view attempt %d", $effect_id);
				return false;
			}
			$effect = $result[0];

			if (is_true(ENCRYPT_DATA)) {
				$effect["title"] = $this->decrypt($effect["title"]);
				$effect["description"] = $this->decrypt($effect["description"]);
			}

			return $effect;
		}

		public function effect_okay($effect) {
			$result = true;

			if (trim($effect["title"]) == "") {
				$this->view->add_message($this->language->module_text("error_specify_title"));
				$result = false;
			}

			return $result;
		}

		private function valid_effect_id($effect_id) {
			$query = "select count(*) as count from effects where id=%d and organisation_id=%d";

			if (($result = $this->db->execute($query, $effect_id, $this->organisation_id)) == false) {
				return false;
			}

			return $result[0]["count"] == 1;
		}

		public function create_effect($effect) {
			if ($this->may_edit_risk($effect["risk_id"]) == false) {
				$this->user->log_action("unauthorized risk effect create attempt %d", $effect["risk_id"]);
				return false;
			}

			if ($this->valid_effect_id($effect["effect_id"]) == false) {
				$this->user->log_action("attempt to assign invalid effect %d to risk %d", $effect["cause_id"], $effect["risk_id"]);
				return false;
			}

			$keys = array("id", "risk_id", "effect_id", "title", "description");

			$effect["id"] = null;
			$effect["title"] = substr($effect["title"], 0, MAXLENGTH_CAUSE_EFFECT_TITLE);

			if (is_true(ENCRYPT_DATA)) {
				$effect["title"] = $this->encrypt($effect["title"]);
				$effect["description"] = $this->encrypt($effect["description"]);
			}

			if ($this->db->insert("risk_effect", $effect, $keys) === false) {
				return false;
			}

			return $this->db->last_insert_id;
		}

		public function update_effect($effect) {
			if (($current = $this->get_risk_effect($effect["id"])) == false) {
				$this->user->log_action("invalid risk effect update attempt %d", $effect["id"]);
				return false;
			}

			if ($this->may_edit_risk($current["risk_id"]) == false) {
				$this->user->log_action("unauthorized risk effect update attempt %d", $effect["id"]);
				return false;
			}

			if ($this->valid_effect_id($effect["effect_id"]) == false) {
				$this->user->log_action("attempt to assign invalid effect %d to risk %d", $effect["cause_id"], $current["risk_id"]);
				return false;
			}

			$keys = array("effect_id", "title", "description");

			$effect["title"] = substr($effect["title"], 0, MAXLENGTH_CAUSE_EFFECT_TITLE);

			if (is_true(ENCRYPT_DATA)) {
				$effect["title"] = $this->encrypt($effect["title"]);
				$effect["description"] = $this->encrypt($effect["description"]);
			}

			return $this->db->update("risk_effect", $effect["id"], $effect, $keys);
		}

		public function delete_effect_okay($effect_id) {
			$query = "select count(*) as count from measures_repressive where risk_effect_id=%d";
			if (($result = $this->db->execute($query, $effect_id)) === false) {
				$this->view->add_message($this->language->global_text("error_database"));
				return false;
			}
			if ($result[0]["count"] > 0) {
				$this->view->add_message($this->language->module_text("error_effect_has_measures"));
				return false;
			}

			return true;
		}

		public function delete_effect($effect_id) {
			if (($current = $this->get_risk_effect($effect_id)) == false) {
				$this->user->log_action("invalid risk effect delete attempt %d", $effect_id);
				return false;
			}

			if ($this->may_edit_risk($current["risk_id"]) == false) {
				$this->user->log_action("unauthorized risk effect delete attempt %d", $effect_id);
				return false;
			}

			return $this->db->delete("risk_effect", $effect_id);
		}
	}
?>
