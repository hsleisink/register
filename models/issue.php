<?php
	class issue_model extends register_model {
		private $measure_types = array("preventive", "detective", "repressive");

		public function __get($key) {
			switch ($key) {
				case "measure_types": return $this->measure_types;
			}

			return null;
		}

		/* Issues
		 */
		public function count_issues() {
			$where = array("i.measure_id=m.id", "i.type=m.type", "m.risk_id=r.id", "organisation_id=%d");
			$args = array($this->organisation_id);

			$this->query_measure_access($where, $args);

			$query = "select count(*) as count from issues i, measures m, risks r ".
			         "where ".implode(" and ", $where);


			if (($result = $this->db->execute($query, $args)) == false) {
				return false;
			}

			return $result[0]["count"];
		}

		private function decrypt_and_sort($issues) {
			if (is_true(ENCRYPT_DATA)) {
				foreach ($issues as $i => $issue) {
					$issues[$i]["title"] = $this->decrypt($issue["title"]);
					$issues[$i]["measure"] = $this->decrypt($issue["measure"]);
				}
			}

			usort($issues, function($a, $b) {
				if ($a["created"] > $b["created"]) {
					return -1;
				}

				if ($a["created"] < $b["created"]) {
					return 1;
				}

				return strcmp(strtolower($a["title"]), strtolower($b["title"]));
			});

			return $issues;
		}

		public function get_issues($offset, $limit) {
			$where = array("i.measure_id=m.id", "i.type=m.type", "m.risk_id=r.id", "organisation_id=%d");
			$args = array(YES, $this->organisation_id);

			$this->query_measure_access($where, $args);

			array_push($args, $offset, $limit);

			$query = "select i.*, UNIX_TIMESTAMP(i.created) as created, m.title as measure, ".
			         "(select count(*) from issue_tasks where issue_id=i.id and type=i.type) as tasks, ".
			         "(select count(*) from issue_tasks where issue_id=i.id and type=i.type and done=%d) as tasks_done ".
			         "from issues i, measures m, risks r ".
			         "where ".implode(" and ", $where)." order by created desc,id limit %d,%d";

			if (($issues = $this->db->execute($query, $args)) === false) {
				return false;
			}

			return $this->decrypt_and_sort($issues);
		}

		public function get_issues_attention() {
			$where = array("i.measure_id=m.id", "i.type=m.type", "m.risk_id=r.id", "i.status<%d", "organisation_id=%d");
			$args = array(YES, ISSUE_STATUS_CLOSED, $this->organisation_id);

			$this->query_measure_access($where, $args);

			$query = "select i.*, UNIX_TIMESTAMP(i.created) as created, m.title as measure, ".
			         "(select count(*) from issue_tasks where issue_id=i.id and type=i.type) as tasks, ".
			         "(select count(*) from issue_tasks where issue_id=i.id and type=i.type and done=%d) as tasks_done ".
			         "from issues i, measures m, risks r ".
			         "where ".implode(" and ", $where);

			if (($issues = $this->db->execute($query, $args)) === false) {
				return false;
			}

			return $this->decrypt_and_sort($issues);
		}

		public function get_issue($issue_id, $type) {
			$where = array("i.measure_id=m.id", "i.type=m.type", "m.risk_id=r.id", "i.id=%d", "i.type=%s", "organisation_id=%d");
			$args = array($issue_id, $type, $this->organisation_id);

			$this->query_measure_access($where, $args);

			$query = "select i.*, m.type as mtype, UNIX_TIMESTAMP(i.created) as created from issues i, measures m, risks r ".
			         "where ".implode(" and ", $where);

			if (($issues = $this->db->execute($query, $args)) == false) {
				return false;
			}
			$issue = $issues[0];

			if (is_true(ENCRYPT_DATA)) {
				$issue["title"] = $this->decrypt($issue["title"]);
				$issue["description"] = $this->decrypt($issue["description"]);
			}

			return $issue;
		}

		public function get_measure($measure_id, $type) {
			$query = "select m.id, m.title from measures m, risks r ".
			         "where m.risk_id=r.id and m.id=%d and type=%s and organisation_id=%d";

			if (($measures = $this->db->execute($query, $measure_id, $type, $this->organisation_id)) == false) {
				return false;
			}
			$measure = $measures[0];

			if (is_true(ENCRYPT_DATA)) {
				$measure["title"] = $this->decrypt($measure["title"]);
			}

			return $measure;
		}

		public function issue_to_risk($issue_id, $type) {
			$query = "select risk_id from issues where id=%d and type=%s";

			if (($result = $this->db->execute($query, $issue_id, $type)) == false) {
				return false;
			}

			return $result[0]["risk_id"];
		}

		public function issue_okay($issue) {
			$result = true;

			if (trim($issue["title"]) == "") {
				$this->view->add_message($this->language->module_text("error_title_empty"));
				$result = false;
			}

			if (isset($issue["id"])) {
				if ($issue["status"] == ISSUE_STATUS_CLOSED) {
					$query = "select count(*) as count from issue_tasks where issue_id=%d and type=%s and done=%d";

					if (($tasks = $this->db->execute($query, $issue["id"], $issue["type"], NO)) === false) {
						$result = false;
					} else if ($tasks[0]["count"] > 0) {
						$this->view->add_message($this->language->module_text("error_cant_close_issue"));
						$result = false;
					}
				}
			}

			return $result;
		}

		public function may_add_issue_to_measure($measure_id, $type) {
			if (in_array($type, $this->measure_types) == false) {
				return false;
			}

			$query = "select m.owner_id, risk_id from measures m, risks r ".
			         "where m.risk_id=r.id and m.id=%d and type=%s and organisation_id=%d";
			if (($measures = $this->db->execute($query, $measure_id, $type, $this->organisation_id)) == false) {
				return false;
			}
			$measure = $measures[0];

			if ($measure["owner_id"] == $this->user->id) {
				return true;
			}

			return $this->may_edit_risk($measure["risk_id"]);
		}

		public function may_edit_issue($issue_id, $type) {
			if (in_array($type, $this->measure_types) == false) {
				return false;
			}

			$query = "select measure_id from %S where id=%d";

			if (($issues = $this->db->execute($query, "issues_".$type, $issue_id)) == false) {
				return false;
			}
			$issue = $issues[0];

			return $this->may_add_issue_to_measure($issue["measure_id"], $type);
		}

		private function mark_measure_ineffective($measure_id, $type) {
			$query = "update %S set status=%d where id=%d and status=%d";

			$this->db->query($query, "measures_".$type, MEASURE_STATUS_INEFFECTIVE, $measure_id, MEASURE_STATUS_EFFECTIVE);
		}

		public function create_issue($issue) {
			if ($this->may_add_issue_to_measure($issue["measure_id"], $issue["type"]) == false) {
				$this->user->log_action("issue create attempt for invalid %s measure %d", $issue["type"], $issue["measure_id"]);
				return false;
			}

			$keys = array("id", "measure_id", "title", "description", "created", "status");

			$issue["id"] = null;
			$issue["created"] = date("Y-m-d H:i:s");
			$issue["status"] = 0;

			if (is_true(ENCRYPT_DATA)) {
				$issue["title"] = $this->encrypt($issue["title"]);
				$issue["description"] = $this->encrypt($issue["description"]);
			}

			if ($this->db->insert("issues_".$issue["type"], $issue, $keys) == false) {
				return false;
			}
			$issue_id = $this->db->last_insert_id;

			$this->mark_measure_ineffective($issue["measure_id"], $issue["type"]);

			return $issue_id;
		}

		public function update_issue($issue) {
			if ($this->may_edit_issue($issue["id"], $issue["type"]) == false) {
				$this->user->log_action("unauthorized issue (id:%d) update attempt issue for %s measure", $issue["id"], $issue["type"]);
				return false;
			}

			if (($current = $this->get_issue($issue["id"], $issue["type"])) == false) {
				return false;
			}

			$new = $issue;

			$keys = array("title", "description", "status");

			if (is_true(ENCRYPT_DATA)) {
				$issue["title"] = $this->encrypt($issue["title"]);
				$issue["description"] = $this->encrypt($issue["description"]);
			}

			if ($this->db->update("issues_".$issue["type"], $issue["id"], $issue, $keys) === false) {
				return false;
			}

			if ($issue["status"] != ISSUE_STATUS_CLOSED) {
				$this->mark_measure_ineffective($issue["measure_id"], $issue["type"]);
			}

			return true;
		}

		public function delete_issue($issue_id, $type, $transaction = true) {
			if (in_array($type, $this->measure_types) == false) {
				return false;
			}

			if ($this->may_edit_issue($issue_id, $type) == false) {
				$this->user->log_action("unauthorized issue (id:%d) delete attempt for %s measure", $issue_id, $type);
				return false;
			}

			$query = "select id from %S where issue_id=%d";
			if (($tasks = $this->db->execute($query, "issue_".$type."_tasks", $issue_id)) === false) {
				return false;
			}

			if ($transaction) {
				if ($this->db->query("begin") === false) {
					return false;
				}
			}

			foreach ($tasks as $task) {
				if ($this->db->query("delete from %S where id=%d", "issue_".$type."_tasks", $task["id"]) === false) {
					if ($transaction) {
						$this->db->query("rollback");
					}
					return false;
				}
			}

			if ($this->db->query("delete from %S where id=%d", "issues_".$type, $issue_id) === false) {
				if ($transaction) {
					$this->db->query("rollback");
				}
				return false;
			}

			return ($transaction) ? ($this->db->query("commit") !== false) : true;
		}

		/* Tasks
		 */
		public function task_to_risk($task_id, $type) {
			$query = "select risk_id from issue_tasks t, issues i ".
			         "where t.issue_id=i.id and t.type=i.type and t.id=%d and t.type=%s";

			if (($result = $this->db->execute($query, $task_id, $type)) == false) {
				return false;
			}

			return $result[0]["risk_id"];
		}

		public function get_tasks($issue_id, $type) {
			if (in_array($type, $this->measure_types) == false) {
				return false;
			}

			$query = "select *, UNIX_TIMESTAMP(deadline) as deadline from %S where issue_id=%d";

			if (($tasks = $this->db->execute($query, "issue_".$type."_tasks", $issue_id)) === false) {
				return false;
			}

			if (is_true(ENCRYPT_DATA)) {
				foreach ($tasks as $i => $task) {
					$tasks[$i]["title"] = $this->decrypt($task["title"]);
					$tasks[$i]["description"] = $this->decrypt($task["description"]);
					$tasks[$i]["executor"] = $this->decrypt($task["executor"]);
				}
			}

			return $tasks;
		}

		public function get_task($task_id, $type) {
			$query = "select t.*, i.risk_id, measure_id from issue_tasks t, issues i ".
			         "where t.issue_id=i.id and t.type=i.type and t.id=%d and t.type=%s";

			if (($tasks = $this->db->execute($query, $task_id, $type)) == false) {
				return false;
			}
			$task = $tasks[0];

			if ($this->may_add_issue_to_measure($task["measure_id"], $type) == false) {
				return false;
			}

			if (is_true(ENCRYPT_DATA)) {
				$task["title"] = $this->decrypt($task["title"]);
				$task["description"] = $this->decrypt($task["description"]);
				$task["executor"] = $this->decrypt($task["executor"]);
			}

			return $task;
		}

		public function task_okay($task) {
			$result = true;

			if (trim($task["title"]) == "") {
				$this->view->add_message($this->language->module_text("error_title_empty"));
				$result = false;
			}

			if (trim($task["executor"]) == "") {
				$this->view->add_message($this->language->module_text("error_executor_empty"));
				$result = false;
			}

			if (valid_date($task["deadline"]) == false) {
				$this->view->add_message($this->language->module_text("error_deadline_invalid"));
				$result = false;
			} else if ((strtotime($task["deadline"]) < strtotime("tomorrow")) && is_false($task["done"] ?? false)) {
				$this->view->add_message($this->language->module_text("error_deadline_past"));
				$result = false;
			}

			return $result;
		}

		private function valid_task($task_id, $type) {
			if (in_array($type, $this->measure_types) == false) {
				return false;
			}

			$query = "select issue_id from %S where id=%d";

			if (($tasks = $this->db->execute($query, "issue_".$type."_tasks", $task_id)) == false) {
				return false;
			}
			$task = $tasks[0];

			return $this->may_edit_issue($task["issue_id"], $type);
		}

		private function unclose_issue($issue_id, $type) {
			$table = "issues_".$type;

			$query = "update %S set status=status-1 where id=%d and status=%d";

			$this->db->query($query, $table, $issue_id, ISSUE_STATUS_CLOSED);

			$query = "select measure_id from %S where id=%d";

			if (($issue = $this->db->execute($query, $table, $issue_id)) != false) {
				$this->mark_measure_ineffective($issue[0]["measure_id"], $type);
			}
		}

		public function create_task($task) {
			if ($this->may_edit_issue($task["issue_id"], $task["type"]) == false) {
				$this->user->log_action("task create attempt for invalid %s issue %d", $task["type"], $task["issue_id"]);
				return false;
			}

			$keys = array("id", "issue_id", "title", "description", "executor", "deadline", "done");

			$task["id"] = null;
			$task["done"] = NO;

			if (is_true(ENCRYPT_DATA)) {
				$task["title"] = $this->encrypt($task["title"]);
				$task["description"] = $this->encrypt($task["description"]);
				$task["executor"] = $this->encrypt($task["executor"]);
			}

			if ($this->db->insert("issue_".$task["type"]."_tasks", $task, $keys) == false) {
				return false;
			}

			$task_id = $this->db->last_insert_id;

			$this->unclose_issue($task["issue_id"], $task["type"]);

			return $task_id;
		}

		public function update_task($task) {
			if ($this->valid_task($task["id"], $task["type"]) == false) {
				$this->user->log_action("unauthorized task (id:%d) update attempt for %s measure", $task["id"], $task["type"]);
				return false;
			}

			$keys = array("title", "description", "executor", "deadline", "done");

			if (is_true(ENCRYPT_DATA)) {
				$task["title"] = $this->encrypt($task["title"]);
				$task["description"] = $this->encrypt($task["description"]);
				$task["executor"] = $this->encrypt($task["executor"]);
			}

			$task["done"] = is_true($task["done"] ?? false) ? YES : NO;

			$table = "issue_".$task["type"]."_tasks";

			if ($this->db->update($table, $task["id"], $task, $keys) === false) {
				return false;
			}

			if (is_false($task["done"])) {
				$query = "select issue_id from %S where id=%d";
				if (($current = $this->db->execute($query, $table, $task["id"])) !== false) {
					$this->unclose_issue($current[0]["issue_id"], $task["type"]);
				}
			}

			return true;
		}

		public function delete_task($task_id, $type) {
			if ($this->valid_task($task_id, $type) == false) {
				$this->user->log_action("unauthorized task (id:%d) delete attempt for %s issue (%d)", $task_id, $type);
				return false;
			}

			$query = "delete from %S where id=%d";

			return $this->db->query($query, "issue_".$type."_tasks", $task_id) != false;
		}
	}
?>
