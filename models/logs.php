<?php
	class logs_model extends register_model {
		public function count_logs() {
			$where = array("l.risk_id=r.id", "l.user_id=u.id", "u.organisation_id=%d");
			$args = array($this->organisation_id);

			$this->query_risk_access($where, $args);

			$query = "select count(*) as count from risk_log l, risks r, users u where ".implode(" and ", $where);

			if (($result = $this->db->execute($query, $args)) == false) {
				return false;
			}

			return $result[0]["count"];
		}

		public function get_logs($offset, $limit) {
			$where = array("l.risk_id=r.id", "l.user_id=u.id", "u.organisation_id=%d");
			$args = array($this->organisation_id);

			$this->query_risk_access($where, $args);

			array_push($args, $offset, $limit);

			$query = "select l.*, UNIX_TIMESTAMP(l.timestamp) as timestamp, u.fullname, r.identifier ".
			         "from risk_log l, risks r, users u where ".implode(" and ", $where)." order by id desc limit %d,%d";

			if (($logs = $this->db->execute($query, $args)) === false) {
				return false;
			}

			foreach ($logs as $i => $log) {
				$logs[$i]["event"] = $this->decrypt($log["event"]);
			}

			return $logs;
		}

		public function get_risk_logs($identifier) {
			$where = array("l.risk_id=r.id", "l.user_id=u.id", "u.organisation_id=%d");
			$args = array($this->organisation_id);

			if (is_int($identifier)) {
				array_push($where, "r.id=%d");
				array_push($args, $identifier);
			} else {
				array_push($where, "r.identifier like %s");
				array_push($args, $identifier."%");
			}

			$this->query_risk_access($where, $args);

			$query = "select l.*, UNIX_TIMESTAMP(l.timestamp) as timestamp, u.fullname, r.identifier ".
			         "from risk_log l, risks r, users u where ".implode(" and ", $where)." order by id desc";

			if (($logs = $this->db->execute($query, $args)) === false) {
				return false;
			}

			foreach ($logs as $i => $log) {
				$logs[$i]["event"] = $this->decrypt($log["event"]);
			}

			return $logs;
		}
	}
?>
