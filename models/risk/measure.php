<?php
	class risk_measure_model extends register_model {
		public function get_risk($risk_id) {
			$query = "select title, description, creator_id, owner_id from risks where id=%d and organisation_id=%d";

			if (($result = $this->db->execute($query, $risk_id, $this->organisation_id)) == false) {
				return false;
			}
			$risk = $result[0];

			if (is_true(ENCRYPT_DATA)) {
				$risk["title"] = $this->decrypt($risk["title"]);
				$risk["description"] = $this->decrypt($risk["description"]);
			}

			return $risk;
		}

		public function get_measure_owners($risk_id) {
			$query = "select distinct u.id, u.fullname from users u, user_role r where u.organisation_id=%d and ".
			         "u.id=r.user_id and (r.role_id=%d or r.role_id=%d or r.user_id in ".
			         "(select m.owner_id from measures m, risks r where m.risk_id=r.id and r.id=%d and r.organisation_id=%d)) order by fullname";
			$args = array($this->organisation_id, $this->settings->role_id_measure_owner, $this->settings->role_id_risk_owner,
			              $risk_id, $this->organisation_id);

			return $this->db->execute($query, $args);
		}

		public function valid_measure_owner($owner_id, $risk_id) {
			if (($owners = $this->get_measure_owners($risk_id)) != false) {
				foreach ($owners as $owner) {
					if ($owner_id == $owner["id"]) {
						return true;
					}
				}
			}

			return false;
		}

		private function count_open_issues($measure_id, $type) {
			$query = "select count(*) as count from %S where measure_id=%d and status!=%d";

			if (($result = $this->db->execute($query, "issues_".$type, $measure_id, ISSUE_STATUS_CLOSED)) == false) {
				return false;
			}

			return $result[0]["count"];
		}

		private function measure_okay($measure, $type) {
			$result = true;

			if (trim($measure["title"]) == "") {
				$this->view->add_message($this->language->module_text("error_specify_title"));
				$result = false;
			}

			if (valid_date($measure["deadline"]) == false) {
				$this->view->add_message($this->language->module_text("error_deadline_invalid"));
				$result = false;
			} else if (strtotime($measure["deadline"]) < strtotime("tomorrow")) {
				$this->view->add_message($this->language->module_text("error_deadline_in_past"));
				$result = false;
			}

			$measure_statuses = array_keys(MEASURE_STATUSES);
			if (($measure["status"] < 0) || ($measure["status"] >= count($measure_statuses))) {
				$this->view->add_message($this->language->module_text("error_status_invalid"));
				$result = false;
			}

			if (isset($measure["id"]) && ($measure["status"] == MEASURE_STATUS_EFFECTIVE)) {
				if (($issue_count = $this->count_open_issues($measure["id"], $type)) !== false) {
					if ($issue_count > 0) {
						$this->view->add_message($this->language->module_text("error_effective_with_open_issues"));
						$result = false;
					}
				}
			}

			return $result;
		}

		public function get_cause($risk_cause_id) {
			$query = "select r.id as risk_id, cause, rc.title, rc.description from causes c, risk_cause rc, risks r ".
			         "where c.id=rc.cause_id and rc.risk_id=r.id and rc.id=%d and r.organisation_id=%d";

			if (($result = $this->db->execute($query, $risk_cause_id, $this->organisation_id)) == false) {
				return false;
			}
			$cause = $result[0];

			if ($this->may_edit_risk($cause["risk_id"]) == false) {
				return false;
			}

			if (is_true(ENCRYPT_DATA)) {
				$cause["cause"] = $this->decrypt($cause["cause"]);
				$cause["title"] = $this->decrypt($cause["title"]);
				$cause["description"] = $this->decrypt($cause["description"]);
			}

			return $cause;
		}

		public function get_effect($risk_effect_id) {
			$query = "select r.id as risk_id, effect, re.title, re.description from effects e, risk_effect re, risks r ".
			         "where e.id=re.effect_id and re.risk_id=r.id and re.id=%d and r.organisation_id=%d";

			if (($result = $this->db->execute($query, $risk_effect_id, $this->organisation_id)) == false) {
				return false;
			}
			$effect = $result[0];

			if ($this->may_edit_risk($effect["risk_id"]) == false) {
				return false;
			}

			if (is_true(ENCRYPT_DATA)) {
				$effect["effect"] = $this->decrypt($effect["effect"]);
				$effect["title"] = $this->decrypt($effect["title"]);
				$effect["description"] = $this->decrypt($effect["description"]);
			}

			return $effect;
		}

		private function delete_issues($measure_id, $type, $transaction) {
			$query = "select id from %S where measure_id=%d";

			if (($issues = $this->db->execute($query, "issues_".$type, $measure_id)) === false) {
				return false;
			}

			foreach ($issues as $issue) {
				if ($this->borrow("issue")->delete_issue($issue["id"], $type, $transaction) == false) {
					return false;
				}
			}

			return true;
		}

		private function log_measure_changes($old, $new, $type) {
			$event = $type." measure '%s' updated (id:%d)";
			$args = array($new["title"], $new["id"]);

			if ($old["owner_id"] != $new["owner_id"]) {
				$event .= ", owner %s (id:%d) -> %s (id:%d)";

				if (($old_name = $this->get_user_fullname($old["owner_id"])) == false) {
					$old_name = "id:".$old["owner_id"];
				}

				if (($new_name = $this->get_user_fullname($new["owner_id"])) == false) {
					$new_name = "id:".$new["owner_id"];
				}

				array_push($args, $old_name, $old["owner_id"], $new_name, $new["owner_id"]);
			}

			if ($old["status"] != $new["status"]) {
				$event .= ", status %s -> %s";
				$measure_statuses = array_keys(MEASURE_STATUSES);
				array_push($args, $measure_statuses[$old["status"]], $measure_statuses[$new["status"]]);
			}

			if ($old["deadline"] != $new["deadline"]) {
				$event .= ", deadline %s -> %s";
				array_push($args, $old["deadline"], $new["deadline"]);
			}

			$this->log_risk_event($new["risk_id"], $event, $args);
		}

		/* Preventive measures
		 */
		public function get_preventives($risk_id) {
			$query = "select m.*, u.fullname as owner, c.cause, rc.title as risk_cause, ".
			         "(select count(*) from issues where type=%s and measure_id=m.id and status<%d) as issues ".
			         "from measures_preventive m, risk_cause rc, causes c, risks r, users u ".
			         "where m.risk_cause_id=rc.id and rc.risk_id=r.id and rc.cause_id=c.id ".
			         "and m.owner_id=u.id and r.id=%d and r.organisation_id=%d ";
			$args = array("preventive", ISSUE_STATUS_CLOSED, $risk_id, $this->organisation_id);

			if (is_true(EDIT_OWN_MEASURE)) {
				$query .= "and m.owner_id=%d ";
				array_push($args, $this->user->id);
			}

			$query .= "order by title";

			if (($preventives = $this->db->execute($query, $args)) === false) {
				return false;
			}

			if (is_true(ENCRYPT_DATA)) {
				foreach ($preventives as $i => $preventive) {
					$preventives[$i]["title"] = $this->decrypt($preventive["title"]);
					$preventives[$i]["description"] = $this->decrypt($preventive["description"]);
					$preventives[$i]["cause"] = $this->decrypt($preventive["cause"]);
					$preventives[$i]["risk_cause"] = $this->decrypt($preventive["risk_cause"]);
				}
			}

			usort($preventives, function($a, $b) {
				return strcmp($a["title"], $b["title"]);
			});

			return $preventives;
		}

		public function get_preventive($preventive_id) {
			$query = "select m.*, c.risk_id from measures_preventive m, risk_cause c, risks r ".
			         "where m.risk_cause_id=c.id and c.risk_id=r.id and m.id=%d and r.organisation_id=%d";

			if (($result = $this->db->execute($query, $preventive_id, $this->organisation_id)) == false) {
				return false;
			}
			$preventive = $result[0];

			if (is_true(ENCRYPT_DATA)) {
				$preventive["title"] = $this->decrypt($preventive["title"]);
				$preventive["description"] = $this->decrypt($preventive["description"]);
			}

			return $preventive;
		}

		public function get_risk_causes($risk_id) {
			return $this->borrow("risk")->get_risk_causes($risk_id);
		}

		public function get_risk_cause($preventive_id) {
			$query = "select c.cause, rc.title from measures_preventive m, risk_cause rc, causes c ".
			         "where m.risk_cause_id=rc.id and rc.cause_id=c.id and m.id=%d and organisation_id=%d";

			if (($result = $this->db->execute($query, $preventive_id, $this->organisation_id)) == false) {
				return false;
			}
			$risk_cause = $result[0];

			if (is_true(ENCRYPT_DATA)) {
				$risk_cause["cause"] = $this->decrypt($risk_cause["cause"]);
				$risk_cause["title"] = $this->decrypt($risk_cause["title"]);
			}

			return $risk_cause["cause"]." | ".$risk_cause["title"];
		}

		public function preventive_okay($preventive) {
			$result = $this->measure_okay($preventive, "preventive");

			return $result;
		}

		public function create_preventive($preventive) {
			if ($this->may_edit_risk($preventive["risk_id"]) == false) {
				$this->user->log_action("unauthorized preventive measure create attempt (risk id:%d)", $preventive["risk_id"]);
				return false;
			}

			if ($this->valid_measure_owner($preventive["owner_id"], $preventive["risk_id"]) == false) {
				return false;
			}

			$keys = array("id", "risk_cause_id", "owner_id", "title", "description", "deadline", "status");

			$preventive["id"] = null;
			$preventive["title"] = substr($preventive["title"], 0, MAXLENGTH_MEASURE_TITLE);

			if (is_true(ENCRYPT_DATA)) {
				$preventive["title"] = $this->encrypt($preventive["title"]);
				$preventive["description"] = $this->encrypt($preventive["description"]);
			}

			if ($this->db->query("begin") === false) {
				return false;
			}

			if ($this->db->insert("measures_preventive", $preventive, $keys) == false) {
				$this->db->query("rollback");
				return true;
			}
			$preventive_id = $this->db->last_insert_id;

			if ($this->add_controls("preventive", $preventive_id, $preventive["controls"] ?? array()) == false) {
				$this->db->query("rollback");
				return true;
			}

			if ($this->db->query("commit") === false) {
				return false;
			}

			return $preventive_id;
		}

		public function update_preventive($preventive) {
			if (($current = $this->get_preventive($preventive["id"])) == false) {
				return false;
			}

			if (is_false(EDIT_OWN_MEASURE)) {
				$may_edit_measure = $this->may_edit_risk($preventive["risk_id"]);
			} else {
				$may_edit_measure = $this->may_edit_measure($preventive["id"], "preventive");
			}

			if ($may_edit_measure == false) {
				$this->user->log_action("unauthorized preventive measure update attempt (id:%d, risk id:%d)", $preventive["id"], $preventive["risk_id"]);
				return false;
			}

			if ($this->valid_measure_owner($preventive["owner_id"], $current["risk_id"]) == false) {
				return false;
			}

			$keys = array("owner_id", "title", "description", "deadline", "status");

			$preventive["title"] = substr($preventive["title"], 0, MAXLENGTH_MEASURE_TITLE);

			$new = $preventive;

			if (is_true(ENCRYPT_DATA)) {
				$preventive["title"] = $this->encrypt($preventive["title"]);
				$preventive["description"] = $this->encrypt($preventive["description"]);
			}

			if ($this->db->query("begin") === false) {
				return false;
			}

			if ($this->db->update("measures_preventive", $preventive["id"], $preventive, $keys) == false) {
				$this->db->query("rollback");
				return false;
			}

			if ($this->add_controls("preventive", $preventive["id"], $preventive["controls"] ?? array()) == false) {
				$this->db->query("rollback");
				return false;
			}

			return $this->db->query("commit") !== false;
		}

		public function delete_preventive($preventive_id, $transaction = true) {
			if (($current = $this->get_preventive($preventive_id)) == false) {
				return false;
			}

			if (is_false(EDIT_OWN_MEASURE)) {
				$may_delete_measure = $this->may_edit_risk($current["risk_id"]);
			} else {
				$may_delete_measure = $this->may_edit_measure($preventive_id, "preventive");
			}

			if ($may_delete_measure == false) {
				return false;
			}

			if ($transaction) {
				if ($this->db->query("begin") === false) {
					return false;
				}
			}

			if ($this->delete_issues($preventive_id, "preventive", $transaction) == false) {
				if ($transaction) {
					$this->db->query("rollback");
				}
				return false;
			}

			$queries = array(
				"delete from control_preventive where measure_preventive_id=%d",
				"delete from measures_preventive where id=%d");

			foreach ($queries as $query) {
				if ($this->db->execute($query, $preventive_id) === false) {
					if ($transaction) {
						$this->db->query("rollback");
					}
					return false;
				}
			}

			if ($transaction) {
				if ($this->db->query("commit") === false) {
					return false;
				}
			}

			return $current["risk_id"];
		}

		/* Detective measures
		 */
		public function get_detectives($risk_id) {
			$query = "select m.*, u.fullname as owner, (select count(*) from issues where type=%s and measure_id=m.id and status<%d) as issues ".
			         "from measures_detective m, risks r, users u ".
			         "where m.risk_id=r.id and r.id=%d and m.owner_id=u.id and r.organisation_id=%d ";
			$args = array("detective", ISSUE_STATUS_CLOSED, $risk_id, $this->organisation_id);

			if (is_true(EDIT_OWN_MEASURE)) {
				$query .= "and m.owner_id=%d ";
				array_push($args, $this->user->id);
			}

			$query .= "order by title";

			if (($detectives = $this->db->execute($query, $args)) === false) {
				return false;
			}

			if (is_true(ENCRYPT_DATA)) {
				foreach ($detectives as $i => $detective) {
					$detectives[$i]["title"] = $this->decrypt($detective["title"]);
					$detectives[$i]["description"] = $this->decrypt($detective["description"]);
				}
			}

			usort($detectives, function($a, $b) {
				return strcmp($a["title"], $b["title"]);
			});

			return $detectives;
		}

		public function get_detective($detective_id) {
			$query = "select m.* from measures_detective m, risks r ".
			         "where m.risk_id=r.id and m.id=%d and r.organisation_id=%d";

			if (($result = $this->db->execute($query, $detective_id, $this->organisation_id)) == false) {
				return false;
			}
			$detective = $result[0];

			if (is_true(ENCRYPT_DATA)) {
				$detective["title"] = $this->decrypt($detective["title"]);
				$detective["description"] = $this->decrypt($detective["description"]);
			}

			return $detective;
		}

		public function detective_okay($detective) {
			$result = $this->measure_okay($detective, "detective");

			return $result;
		}

		public function create_detective($detective) {
			if ($this->may_edit_risk($detective["risk_id"]) == false) {
				$this->user->log_action("unauthorized detective measure create attempt (risk id:%d)", $detective["risk_id"]);
				return false;
			}

			if ($this->valid_measure_owner($detective["owner_id"], $detective["risk_id"]) == false) {
				return false;
			}

			$keys = array("id", "risk_id", "owner_id", "title", "description", "deadline", "status");

			$detective["id"] = null;
			$detective["title"] = substr($detective["title"], 0, MAXLENGTH_MEASURE_TITLE);

			if (is_true(ENCRYPT_DATA)) {
				$detective["title"] = $this->encrypt($detective["title"]);
				$detective["description"] = $this->encrypt($detective["description"]);
			}

			if ($this->db->query("begin") === false) {
				return false;
			}

			if ($this->db->insert("measures_detective", $detective, $keys) == false) {
				$this->db->query("rollback");
				return true;
			}
			$detective_id = $this->db->last_insert_id;

			if ($this->add_controls("detective", $detective_id, $detective["controls"] ?? array()) == false) {
				$this->db->query("rollback");
				return false;
			}

			if ($this->db->query("commit") === false) {
				return false;
			}

			return $detective_id;
		}

		public function update_detective($detective) {
			if (($current = $this->get_detective($detective["id"])) == false) {
				return false;
			}

			if (is_false(EDIT_OWN_MEASURE)) {
				$may_edit_measure = $this->may_edit_risk($detective["risk_id"]);
			} else {
				$may_edit_measure = $this->may_edit_measure($detective["id"], "detective");
			}

			if ($may_edit_measure == false) {
				$this->user->log_action("unauthorized detective measure update attempt (id:%d, risk id:%d)", $detective["id"], $detective["risk_id"]);
				return false;
			}

			if ($this->valid_measure_owner($detective["owner_id"], $current["risk_id"]) == false) {
				return false;
			}

			$keys = array("owner_id", "title", "description", "deadline", "status");

			$detective["title"] = substr($detective["title"], 0, MAXLENGTH_MEASURE_TITLE);

			$new = $detective;

			if (is_true(ENCRYPT_DATA)) {
				$detective["title"] = $this->encrypt($detective["title"]);
				$detective["description"] = $this->encrypt($detective["description"]);
			}

			if ($this->db->query("begin") === false) {
				return false;
			}

			if ($this->db->update("measures_detective", $detective["id"], $detective, $keys) == false) {
				$this->db->query("rollback");
				return false;
			}

			if ($this->add_controls("detective", $detective["id"], $detective["controls"] ?? array()) == false) {
				$this->db->query("rollback");
				return false;
			}

			return $this->db->query("commit") !== false;
		}

		public function delete_detective($detective_id, $transaction = true) {
			if (($current = $this->get_detective($detective_id)) == false) {
				return false;
			}

			if (is_false(EDIT_OWN_MEASURE)) {
				$may_delete_measure = $this->may_edit_risk($current["risk_id"]);
			} else {
				$may_delete_measure = $this->may_edit_measure($detective_id, "detective");
			}

			if ($may_delete_measure == false) {
				return false;
			}

			if ($transaction) {
				if ($this->db->query("begin") === false) {
					return false;
				}
			}

			if ($this->delete_issues($detective_id, "detective", $transaction) == false) {
				if ($transaction) {
					$this->db->query("rollback");
				}
				return false;
			}

			$queries = array(
				"delete from control_detective where measure_detective_id=%d",
				"delete from measures_detective where id=%d");

			foreach ($queries as $query) {
				if ($this->db->execute($query, $detective_id) === false) {
					if ($transaction) {
						$this->db->query("rollback");
					}
					return false;
				}
			}

			if ($transaction) {
				if ($this->db->query("commit") === false) {
					return false;
				}
			}

			return $current["risk_id"];
		}

		/* Repressive measures
		 */
		public function get_repressives($risk_id) {
			$query = "select m.*, u.fullname as owner, e.effect, re.title as risk_effect, ".
			         "(select count(*) from issues where type=%s and measure_id=m.id and status<%d) as issues ".
			         "from measures_repressive m, risk_effect re, effects e, risks r, users u ".
			         "where m.risk_effect_id=re.id and re.risk_id=r.id and re.effect_id=e.id ".
			         "and m.owner_id=u.id and r.id=%d and r.organisation_id=%d ";
			$args = array("repressive", ISSUE_STATUS_CLOSED, $risk_id, $this->organisation_id);

			if (is_true(EDIT_OWN_MEASURE)) {
				$query .= "and m.owner_id=%d ";
				array_push($args, $this->user->id);
			}

			$query .= "order by title";

			if (($repressives = $this->db->execute($query, $args)) === false) {
				return false;
			}

			if (is_true(ENCRYPT_DATA)) {
				foreach ($repressives as $i => $repressive) {
					$repressives[$i]["title"] = $this->decrypt($repressive["title"]);
					$repressives[$i]["description"] = $this->decrypt($repressive["description"]);
					$repressives[$i]["effect"] = $this->decrypt($repressive["effect"]);
					$repressives[$i]["risk_effect"] = $this->decrypt($repressive["risk_effect"]);
				}
			}

			usort($repressives, function($a, $b) {
				return strcmp($a["title"], $b["title"]);
			});

			return $repressives;
		}

		public function get_repressive($repressive_id) {
			$query = "select m.*, e.risk_id from measures_repressive m, risk_effect e, risks r ".
			         "where m.risk_effect_id=e.id and e.risk_id=r.id and m.id=%d and r.organisation_id=%d";

			if (($result = $this->db->execute($query, $repressive_id, $this->organisation_id)) == false) {
				return false;
			}
			$repressive = $result[0];

			if (is_true(ENCRYPT_DATA)) {
				$repressive["title"] = $this->decrypt($repressive["title"]);
				$repressive["description"] = $this->decrypt($repressive["description"]);
			}

			return $repressive;
		}

		public function get_risk_effects($risk_id) {
			return $this->borrow("risk")->get_risk_effects($risk_id);
		}

		public function get_risk_effect($repressive_id) {
			$query = "select e.effect, re.title from measures_repressive m, risk_effect re, effects e ".
			         "where m.risk_effect_id=re.id and re.effect_id=e.id and m.id=%d and organisation_id=%d";

			if (($result = $this->db->execute($query, $repressive_id, $this->organisation_id)) == false) {
				return false;
			}
			$risk_effect = $result[0];

			if (is_true(ENCRYPT_DATA)) {
				$risk_effect["effect"] = $this->decrypt($risk_effect["effect"]);
				$risk_effect["title"] = $this->decrypt($risk_effect["title"]);
			}

			return $risk_effect["effect"]." | ".$risk_effect["title"];
		}

		public function repressive_okay($repressive) {
			$result = $this->measure_okay($repressive, "repressive");

			return $result;
		}

		public function create_repressive($repressive) {
			if ($this->may_edit_risk($repressive["risk_id"]) == false) {
				$this->user->log_action("unauthorized repressive measure create attempt (risk id:%d)", $repressive["risk_id"]);
				return false;
			}

			if ($this->valid_measure_owner($repressive["owner_id"], $repressive["risk_id"]) == false) {
				return false;
			}

			$keys = array("id", "risk_effect_id", "owner_id", "title", "description", "deadline", "status");

			$repressive["id"] = null;
			$repressive["title"] = substr($repressive["title"], 0, MAXLENGTH_MEASURE_TITLE);

			if (is_true(ENCRYPT_DATA)) {
				$repressive["title"] = $this->encrypt($repressive["title"]);
				$repressive["description"] = $this->encrypt($repressive["description"]);
			}

			if ($this->db->query("begin") === false) {
				return false;
			}

			if ($this->db->insert("measures_repressive", $repressive, $keys) == false) {
				$this->db->query("rollback");
				return false;
			}
			$repressive_id = $this->db->last_insert_id;

			if ($this->add_controls("repressive", $repressive_id, $repressive["controls"] ?? array()) == false) {
				$this->db->query("rollback");
				return false;
			}

			if ($this->db->query("commit") === false) {
				return false;
			}

			return $repressive_id;
		}

		public function update_repressive($repressive) {
			if (($current = $this->get_repressive($repressive["id"])) == false) {
				return false;
			}

			if (is_false(EDIT_OWN_MEASURE)) {
				$may_edit_measure = $this->may_edit_risk($repressive["risk_id"]);
			} else {
				$may_edit_measure = $this->may_edit_measure($repressive["id"], "repressive");
			}

			if ($may_edit_measure == false) {
				$this->user->log_action("unauthorized repressive measure update attempt (id:%d, risk id:%d)", $repressive["id"], $repressive["risk_id"]);
				return false;
			}

			if ($this->valid_measure_owner($repressive["owner_id"], $current["risk_id"]) == false) {
				return false;
			}

			$keys = array("owner_id", "title", "description", "deadline", "status");

			$repressive["title"] = substr($repressive["title"], 0, MAXLENGTH_MEASURE_TITLE);

			$new = $repressive;

			if (is_true(ENCRYPT_DATA)) {
				$repressive["title"] = $this->encrypt($repressive["title"]);
				$repressive["description"] = $this->encrypt($repressive["description"]);
			}

			if ($this->db->query("begin") === false) {
				return false;
			}

			if ($this->db->update("measures_repressive", $repressive["id"], $repressive, $keys) == false) {
				$this->db->query("rollback");
				return false;
			}

			if ($this->add_controls("repressive", $repressive["id"], $repressive["controls"] ?? array()) == false) {
				$this->db->query("rollback");
				return false;
			}

			return $this->db->query("commit") !== false;
		}

		public function delete_repressive($repressive_id, $transaction = true) {
			if (($current = $this->get_repressive($repressive_id)) == false) {
				return false;
			}

			if (is_false(EDIT_OWN_MEASURE)) {
				$may_delete_measure = $this->may_edit_risk($current["risk_id"]);
			} else {
				$may_delete_measure = $this->may_edit_measure($repressive_id, "repressive");
			}

			if ($may_delete_measure == false) {
				return false;
			}

			if ($transaction) {
				if ($this->db->query("begin") === false) {
					return false;
				}
			}

			if ($this->delete_issues($repressive_id, "repressive", $transaction) == false) {
				if ($transaction) {
					$this->db->query("rollback");
				}
				return false;
			}

			$queries = array(
				"delete from control_repressive where measure_repressive_id=%d",
				"delete from measures_repressive where id=%d");

			foreach ($queries as $query) {
				if ($this->db->execute($query, $repressive_id) === false) {
					if ($transaction) {
						$this->db->query("rollback");
					}
					return false;
				}
			}

			if ($transaction) {
				if ($this->db->query("commit") === false) {
					return false;
				}
			}

			return $current["risk_id"];
		}

		/* Controls
		 */
		public function get_controls() {
			$query = "select * from control_standards order by name";
			if (($standards = $this->db->execute($query)) === false) {
				return false;
			}

			$query = "select id,number,title from controls where control_standard_id=%d order by id";
			foreach ($standards as $i => $standard) {
				if (($controls = $this->db->execute($query, $standard["id"])) === false) {
					return false;
				}

				$standards[$i]["controls"] = $controls;
			}

			return $standards;
		}

		public function get_most_used_standard($type, $measure_id) {
			if (in_array($type, array("preventive", "detective", "repressive")) == false) {
				return false;
			}

			$query = "select c.control_standard_id, count(*) as count ".
			         "from controls c, control_".$type." l, measures_".$type." m ".
			         "where c.id=l.control_id and l.measure_".$type."_id=m.id and m.id=%d ".
			         "group by c.control_standard_id order by count desc";

			if (($result = $this->db->execute($query, $measure_id)) == false) {
				return false;
			}

			return $result[0]["control_standard_id"];
		}

		public function get_measure_controls($type, $measure_id) {
			$control_table = "control_".$type;
			$measure_table = "measures_".$type;
			$column = "measure_".$type."_id";

			$query = "select control_id from %S c, %S m where c.%S=m.id and m.id=%d order by control_id";

			if (($result = $this->db->execute($query, $control_table, $measure_table, $column, $measure_id)) === false) {
				return false;
			}

			$controls = array();
			foreach ($result as $item) {
				array_push($controls, $item["control_id"]);
			}

			return $controls;
		}

		public function add_controls($type, $id, $controls) {
			$table = "control_".$type;
			$column = "measure_".$type."_id";

			$query = "delete from %S where %S=%d";
			if ($this->db->query($query, $table, $column, $id) === false) {
				return false;
			}

			foreach ($controls as $control_id) {
				$data = array(
					"control_id" => $control_id,
					$column      => $id);
				if ($this->db->insert($table, $data) == false) {
					return false;
				}
			}

			return true;
		}
	}
?>
