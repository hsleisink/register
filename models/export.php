<?php
	class export_model extends register_model {
		public function first_risk_date() {
			$query = "select DATE(created) as created from risks ".
			         "where organisation_id=%d order by created limit 1";

			if (($result = $this->db->execute($query, $this->user->organisation_id)) == false) {
				return "2000-01-01";
			}

			return $result[0]["created"];
		}

		public function dates_okay($dates) {
			$result = true;

			if (valid_date($dates["first"]) == false) {
				$this->view->add_message($this->language->module_text("error_invalid_from_date"));
				$result = false;
			}

			if (valid_date($dates["last"]) == false) {
				$this->view->add_message($this->language->module_text("error_invalid_to_date"));
				$result = false;
			}

			if ($result) {
				if (strtotime($dates["first"]) > strtotime($dates["last"])) {
					$this->view->add_message($this->language->module_text("error_invalid_date_order"));
					$result = false;
				}
			}

			return $result;
		}

		private function rewrite_date($date) {
			$parts = explode(" ", $date, 2);

			$date = explode("-", $parts[0]);
			$date = array_reverse($date);
			$parts[0] = implode("-", $date);

			return implode(" ", $parts);
		}

		private function index_by_risk($data) {
			$result = array();

			foreach ($data as $item) {
				$risk_id = $item["risk_id"];
				if (isset($result[$risk_id]) == false) {
					$result[$risk_id] = array();
				}

				array_push($result[$risk_id], $item);
			}

			return $result;
		}

		/* Export risks
		 */
		public function export_risks($dates) {
			$where = array("r.creator_id=u.id", "r.organisation_id=%d", "created>=%s", "created<=%s");
			$args = array($this->organisation_id, $dates["first"]." 00:00:00", $dates["last"]." 23:59:59");

			$this->query_risk_access($where, $args);

			$query = "select r.*, u.fullname as creator, o.fullname as owner, a.fullname as acceptor ".
			         "from users u, risks r left join users o on r.owner_id=o.id left join users a on r.acceptor_id=a.id ".
			         "where ".implode(" and ", $where)." order by created desc";

			if (($risks = $this->db->execute($query, $args)) === false) {
				return false;
			}

			/* Get risk causes
			 */
			$where = array("rc.risk_id=r.id", "rc.cause_id=c.id", "r.organisation_id=%d");
			$args = array($this->organisation_id);

			$this->query_risk_access($where, $args);

			$query = "select rc.*, c.cause from risk_cause rc, risks r, causes c where ".implode(" and ", $where);

			if (($causes = $this->db->execute($query, $args)) === false) {
				return false;
			}

			$causes = $this->index_by_risk($causes);

			/* Get risk effects
			 */
			$where = array("re.risk_id=r.id", "re.effect_id=e.id", "r.organisation_id=%d");
			$args = array($this->organisation_id);

			$this->query_risk_access($where, $args);

			$query = "select re.*, e.effect from risk_effect re, risks r, effects e where ".implode(" and ", $where);

			if (($effects = $this->db->execute($query, $args)) === false) {
				return false;
			}

			$effects = $this->index_by_risk($effects);

			/* Get matrix and labels
			 */
			if (($matrix = $this->get_matrix()) == false) {
				return false;
			}

			if (($horizontal_labels = $this->get_horizontal_labels()) == false) {
				$this->view->add_tag("result", $this->language->global_text("error_database"));
				return false;
			}

			if (($vertical_labels = $this->get_vertical_labels()) == false) {
				$this->view->add_tag("result", $this->language->global_text("error_database"));
				return false;
			}

			if (($level_labels = $this->get_level_labels()) == false) {
				$this->view->add_tag("result", $this->language->global_text("error_database"));
				return false;
			}

			$approach_labels = array_keys(RISK_APPROACHES);

			$add_description = is_true($dates["description"] ?? false);

			/* Create export
			 */
			$csv = new \Banshee\csvfile();

			$line = array("Identifier", "Risk");
			if ($add_description) {
				array_push($line, "Description");
			}
			array_push($line, "Created", "Creator", "Owner");
			array_push($line, $this->matrix_horizontal, $this->matrix_vertical, "Level", "Mitigated to");
			array_push($line, "Approach", "Acceptance approver", "Accept end date");

			$risk_offset = count($line);

			array_push($line, "Cause");
			if ($add_description) {
				array_push($line, "Description");
			}

			array_push($line, "Effect");
			if ($add_description) {
				array_push($line, "Description");
			}
			$csv->add_line($line);

			foreach ($risks as $risk) {
				$risk["title"] = $this->decrypt($risk["title"]);
				if ($add_description) {
					$risk["description"] = $this->decrypt($risk["description"]);
				}

				$risk["created"] = $this->rewrite_date($risk["created"]);
				if ($risk["accept_end_date"] == DATE_NOT_SET) {
					$risk["accept_end_date"] = "";
				} else {
					$risk["accept_end_date"] = $this->rewrite_date($risk["accept_end_date"]);
				}

				$risk_level = $this->level_from_matrix($matrix, $risk["horizontal"], $risk["vertical"]);

				$line = array($risk["identifier"], $risk["title"]);
				if ($add_description) {
					array_push($line, $risk["description"]);
				}
				array_push($line, $risk["created"], $risk["creator"], $risk["owner"]);
				array_push($line, $horizontal_labels[$risk["horizontal"] - 1]);
				array_push($line, $vertical_labels[$risk["vertical"] - 1]);
				array_push($line, $level_labels["label"][$risk_level - 1]);

				if ($risk["levels_mitigated"] != 0) {
					$mitigated_level = max($risk_level - $risk["levels_mitigated"], 1);
					array_push($line, $level_labels["label"][$mitigated_level - 1]);
				} else {
					array_push($line, "");
				}

				array_push($line, $approach_labels[$risk["approach"]]);
				array_push($line, $risk["acceptor"], $risk["accept_end_date"]);

				$csv->add_line($line);

				/* Causes and effects
				 */
				$risk_id = $risk["id"];
				$cause_count = isset($causes[$risk_id]) ? count($causes[$risk_id]) : 0;
				$effect_count = isset($effects[$risk_id]) ? count($effects[$risk_id]) : 0;
				$causes_effects = max($cause_count, $effect_count);

				for ($i = 0; $i < $causes_effects; $i++) {
					$line = array_fill(0, $risk_offset, "");

					if (isset($causes[$risk_id][$i])) {
						$cause = $this->decrypt($causes[$risk_id][$i]["cause"]);
						$title = $this->decrypt($causes[$risk_id][$i]["title"]);
						array_push($line, $cause." | ".$title);
						if ($add_description) {
							array_push($line, $this->decrypt($causes[$risk_id][$i]["description"]));
						}
					} else {
						array_push($line, "");
						if ($add_description) {
							array_push($line, "");
						}
					}

					if (isset($effects[$risk_id][$i])) {
						$effect = $this->decrypt($effects[$risk_id][$i]["effect"]);
						$title = $this->decrypt($effects[$risk_id][$i]["title"]);
						array_push($line, $effect." | ".$title);
						if ($add_description) {
							array_push($line, $this->decrypt($effects[$risk_id][$i]["description"]));
						}
					} else {
						array_push($line, "");
						if ($add_description) {
							array_push($line, "");
						}
					}

					$csv->add_line($line);
				}
			}

			$csv->add_to_view($this->view, "Risks ".$dates["first"]." - ".$dates["last"]);

			return true;
		}

		/* Export tags
		 */
		public function export_tags($dates) {
			$query = "select id, name from tag_categories where organisation_id=%d";

			if (($categories = $this->db->execute($query, $this->organisation_id)) === false) {
				return false;
			}

			foreach ($categories as $c => $category) {
				$categories[$c]["name"] = $this->decrypt($category["name"]);
			}
			usort($categories, function($a, $b) {
				return strcmp($a["name"], $b["name"]);
			});

			$csv = new \Banshee\csvfile();

			$cline = array("");
			$tline = array("");
			$tag_ids = array();

			$query = "select id, name from tags where tag_category_id=%d";

			foreach ($categories as $c => $category) {
				if (($tags = $this->db->execute($query, $category["id"])) === false) {
					return false;
				}

				if (($tag_count = count($tags)) == 0) {
					continue;
				}

				$ctags = array();
				foreach ($tags as $t => $tag) {
					$tags[$t]["name"] = $this->decrypt($tag["name"]);
				}

				usort($tags, function($a, $b) {
					return strcmp($a["name"], $b["name"]);
				});

				foreach ($tags as $t => $tag) {
					array_push($ctags, $tag["name"]);
					array_push($tag_ids, $tag["id"]);
				}

				array_push($cline, $category["name"]);
				if ($tag_count > 1) {
					$cline = array_merge($cline, array_fill(1, $tag_count - 1, ""));
				}

				$tline = array_merge($tline, $ctags);
			}

			$csv->add_line($cline);
			$csv->add_line($tline);

			/* Get risks
			 */
			$where = array("r.organisation_id=%d", "created>=%s", "created<=%s");
			$args = array($this->organisation_id, $dates["first"]." 00:00:00", $dates["last"]." 23:59:59");

			$this->query_risk_access($where, $args);

			$query = "select id, identifier from risks r where ".implode(" and ", $where)." order by created desc";

			if (($risks = $this->db->execute($query, $args)) === false) {
				return false;
			}

			$query = "select tag_id from risk_tag where risk_id=%d";

			foreach ($risks as $risk) {
				$line = array($risk["identifier"]);

				if (($tags = $this->db->execute($query, $risk["id"])) === false) {
					return false;
				}

				$risk_tags = array();
				foreach ($tags as $tag) {
					array_push($risk_tags, $tag["tag_id"]);
				}

				foreach ($tag_ids as $tag_id) {
					array_push($line, in_array($tag_id, $risk_tags) ? "X" : "");
				}

				$csv->add_line($line);
			}

			$csv->add_to_view($this->view, "Tags ".$dates["first"]." - ".$dates["last"]);

			return true;
		}

		/* Export measures
		 */
		public function export_measures($dates) {
			$where = array("m.owner_id=u.id", "m.risk_id=r.id", "r.organisation_id=%d", "created>=%s", "created<=%s");
			$args = array($this->organisation_id, $dates["first"]." 00:00:00", $dates["last"]." 23:59:59");

			$this->query_measure_access($where, $args);

			$query = "select m.*, r.identifier, u.fullname as owner from measures m, users u, risks r ".
			         "where ".implode(" and ", $where)." order by created desc";

			if (($measures = $this->db->execute($query, $args)) === false) {
				return false;
			}

			$add_description = is_true($dates["description"] ?? false);

			$csv = new \Banshee\csvfile();

			$line = array("Measure");
			if ($add_description) {
				array_push($line, "Description");
			}
			array_push($line, "Type", "Owner", "Deadline", "Status", "Identifier", "Cause", "Effect");
			if ($add_description) {
				array_push($line, "Description");
			}
			$csv->add_line($line);

			foreach ($measures as $measure) {
				/* Cause
				 */
				if ($measure["type"] == "preventive") {
					$query = "select cause, title, description from causes c, risk_cause rc ".
					         "where c.id=rc.cause_id and rc.id=%d";
					if (($result = $this->db->execute($query, $measure["ref_id"])) == false) {
						return false;
					}
					$cause = $result[0];

					$measure["cause"] = $this->decrypt($cause["cause"])." | ".$this->decrypt($cause["title"]);
					$measure["cause_effect_description"] = $this->decrypt($cause["description"]);
				} else {
					$measure["cause"] = $measure["cause_effect_description"] = "";
				}

				/* Effect
				 */
				if ($measure["type"] == "repressive") {
					$query = "select effect, title, description from effects e, risk_effect re ".
					         "where e.id=re.effect_id and re.id=%d";
					if (($result = $this->db->execute($query, $measure["ref_id"])) == false) {
						return false;
					}
					$effect = $result[0];

					$measure["effect"] = $this->decrypt($effect["effect"])." | ".$this->decrypt($effect["title"]);
					$measure["cause_effect_description"] = $this->decrypt($effect["description"]);
				} else {
					$measure["effect"] = "";
				}

				$measure["title"] = $this->decrypt($measure["title"]);
				if ($add_description) {
					$measure["description"] = $this->decrypt($measure["description"]);
				}

				$measure["deadline"] = $this->rewrite_date($measure["deadline"]);
				$measure_statuses = array_keys(MEASURE_STATUSES);
				$measure["status"] = $measure_statuses[$measure["status"]];

				$line = array($measure["title"]);
				if ($add_description) {
					array_push($line, $measure["description"]);
				}
				array_push($line, $measure["type"], $measure["owner"], $measure["deadline"], $measure["status"], $measure["identifier"], $measure["cause"], $measure["effect"]);
				if ($add_description) {
					array_push($line, $measure["cause_effect_description"]);
				}

				$csv->add_line($line);
			}

			$csv->add_to_view($this->view, "Measures ".$dates["first"]." - ".$dates["last"]);

			return true;
		}

		/* Export issues
		 */
		public function export_issues($dates) {
			$where = array("i.risk_id=r.id", "i.measure_id=m.id", "i.type=m.type", "organisation_id=%d", "i.created>=%s", "i.created<=%s");
			$args = array($this->organisation_id, $dates["first"]." 00:00:00", $dates["last"]." 23:59:59");

			$this->query_measure_access($where, $args);

			$query = "select i.*, r.identifier, m.title as measure ".
					 "from issues i, measures m, risks r where ".implode(" and ", $where);

			if (($issues = $this->db->execute($query, $args)) === false) {
				return false;
			}

			$add_description = is_true($dates["description"] ?? false);

			$csv = new \Banshee\csvfile();

			$line = array("Issue", "Status", "Measure", "Identifier", "Created");
			if ($add_description) {
				array_push($line, "Description");
			}
			array_push($line, "Task", "Executor", "Deadline", "Done");
			if ($add_description) {
				array_push($line, "Description");
			}
			$csv->add_line($line);

			foreach ($issues as $issue) {
				$issue["title"] = $this->decrypt($issue["title"]);
				$issue["measure"] = $this->decrypt($issue["measure"]);
				if ($add_description) {
					$issue["description"] = $this->decrypt($issue["description"]);
				}

				$issue_statuses = array_keys(ISSUE_STATUSES);
				$issue["status"] = $issue_statuses[$issue["status"]];

				$line = array($issue["title"], $issue["status"], $issue["measure"], $issue["identifier"], $issue["created"]);

				if ($add_description) {
					array_push($line, $issue["description"]);
				}

				$csv->add_line($line);

				/* Tasks
				 */
				$query = "select * from issue_tasks where issue_id=%d and type=%s order by deadline desc";

				if (($tasks = $this->db->execute($query, $issue["id"], $issue["type"])) === false) {
					return false;
				}

				foreach ($tasks as $task) {
					$task["title"] = $this->decrypt($task["title"]);
					$task["executor"] = $this->decrypt($task["executor"]);
					if ($add_description) {
						$task["description"] = $this->decrypt($task["description"]);
					}
					$task["done"] = show_boolean($task["done"]);

					$line = array_fill(1, $add_description ? 6 : 5, "");

					array_push($line, $task["title"], $task["executor"], $task["deadline"], $task["done"]);
					if ($add_description) {
						array_push($line, $task["description"]);
					}

					$csv->add_line($line);
				}
			}

			$csv->add_to_view($this->view, "Issues ".$dates["first"]." - ".$dates["last"]);

			return true;
		}

		/* Export logs
		 */
		public function export_logs($dates) {
			$where = array("l.user_id=u.id", "l.risk_id=r.id", "r.organisation_id=%d", "timestamp>=%s", "timestamp<=%s");
			$args = array($this->organisation_id, $dates["first"]." 00:00:00", $dates["last"]." 23:59:59");

			$this->query_risk_access($where, $args);

			$query = "select l.*, r.identifier, u.fullname from risk_log l, users u, risks r ".
			         "where ".implode(" and ", $where)." order by id desc";

			if (($logs = $this->db->execute($query, $args)) === false) {
				return false;
			}

			$csv = new \Banshee\csvfile();
			$csv->add_line("Identifier", "Timestamp", "User", "Event");

			foreach ($logs as $log) {
				$log["event"] = $this->decrypt($log["event"]);

				$csv->add_line($log["identifier"], $log["timestamp"], $log["fullname"], $log["event"]);
			}

			$csv->add_to_view($this->view, "Logs ".$dates["first"]." - ".$dates["last"]);

			return true;
		}
	}
?>
