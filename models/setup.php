<?php
	/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
	 * This file is part of the Banshee PHP framework
	 * https://www.banshee-php.org/
	 *
	 * Licensed under The MIT License
	 */

	class setup_model extends Banshee\model {
		private $required_php_extensions = array("gd", "libxml", "mysqli", "xsl");

		/* Determine next step
		 */
		public function step_to_take() {
			$missing = $this->missing_php_extensions();
			if (count($missing) > 0) {
				return "php_extensions";
			}

			if ($this->db->connected == false) {
				$db = new \Banshee\Database\MySQLi_connection(DB_HOSTNAME, DB_DATABASE, DB_USERNAME, DB_PASSWORD);
			} else {
				$db = $this->db;
			}

			if ($db->connected == false) {
				/* No database connection
				 */
				if ((DB_HOSTNAME == "localhost") && (DB_DATABASE == "register") && (DB_USERNAME == "register") && (DB_PASSWORD == "register")) {
					return "db_settings";
				} else if (strpos(DB_PASSWORD, "'") !== false) {
					$this->view->add_system_message("A single quote is not allowed in the password!");
					return "db_settings";
				}

				return "create_db";
			}

			$result = $db->execute("show tables like %s", "settings");
			if (count($result) == 0) {
				return "import_database";
			}

			if ($this->settings->database_version < $this->latest_database_version()) {
				return "update_db";
			}

			$result = $db->execute("select count(*) as count from matrix");
			if ($result[0]["count"] == 0) {
				return "matrix";
			}

			if (($result = $db->execute("select password from users where username=%s", "admin")) != false) {
				if ($result[0]["password"] == "none") {
					return "credentials";
				}
			}

			return "done";
		}

		/* Missing PHP extensions
		 */
		public function missing_php_extensions() {
			static $missing = null;

			if ($missing !== null) {
				return $missing;
			}

			$missing = array();
			foreach ($this->required_php_extensions as $extension) {
				if (extension_loaded($extension) == false) {
					array_push($missing, $extension);
				}
			}

			return $missing;
		}

		/* Remove datase related error messages
		 */
		public function remove_database_errors() {
			$errors = explode("\n", rtrim(ob_get_contents()));
			ob_clean();

			foreach ($errors as $error) {
				if (strpos(strtolower($error), "mysqli_connect") === false) {
					print $error."\n";
				}
			}
		}

		/* Create the MySQL database
		 */
		public function create_database($username, $password) {
			$db = new \Banshee\Database\MySQLi_connection(DB_HOSTNAME, "mysql", $username, $password);

			if ($db->connected == false) {
				$this->view->add_message("Error connecting to database.");
				return false;
			}

			$db->query("begin");

			/* Create database
			 */
			$query = "create database if not exists %S character set utf8mb4";
			if ($db->query($query, DB_DATABASE) == false) {
				$db->query("rollback");
				$this->view->add_message("Error creating database.");
				return false;
			}

			/* Create user
			 */
			$query = "select count(*) as count from user where User=%s";
			if (($users = $db->execute($query, DB_USERNAME)) === false) {
				$db->query("rollback");
				$this->view->add_message("Error checking for user.");
				return false;
			}

			if ($users[0]["count"] == 0) {
				$query = "create user %s@%s identified by %s";
				if ($db->query($query, DB_USERNAME, DB_HOSTNAME, DB_PASSWORD) == false) {
					$db->query("rollback");
					$this->view->add_message("Error creating user.");
					return false;
				}
			}

			/* Set access rights
			 */
			$rights = array(
				"select", "insert", "update", "delete",
				"create", "drop", "alter", "index", "lock tables",
				"create view", "show view");

			$query = "grant ".implode(", ", $rights)." on %S.* to %s@%s";
			if ($db->query($query, DB_DATABASE, DB_USERNAME, DB_HOSTNAME) == false) {
				$db->query("rollback");
				$this->view->add_message("Error setting access rights.");
				return false;
			}

			/* Test login for existing user
			 */
			if ($users[0]["count"] > 0) {
				$login_test = new \Banshee\Database\MySQLi_connection(DB_HOSTNAME, DB_DATABASE, DB_USERNAME, DB_PASSWORD);
				if ($login_test->connected == false) {
					$db->query("rollback");
					$this->view->add_message("Invalid credentials in settings/banshee.conf.");
					return false;
				}
			}

			/* Commit changes
			 */
			$db->query("commit");
			$db->query("flush privileges");
			unset($db);

			return true;
		}

		/* Import database tables from file
		 */
		public function import_database() {
			if (($queries = file("../database/mysql.sql")) === false) {
				$this->view->add_message("Can't read the database/mysql.sql file.");
				return false;
			}

			if (($db_link = mysqli_connect(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE)) === false) {
				$this->view->add_message("Error while connecting to the database.");
				return false;
			}

			$query = "";
			foreach ($queries as $line) {
				if (($line = trim($line)) == "") {
					continue;
				}
				if (substr($line, 0, 2) == "--") {
					continue;
				}

				$query .= $line;
				if (substr($query, -1) != ";") {
					continue;
				}

				if (mysqli_query($db_link, $query) === false) {
					$this->view->add_message("Error while executing query [%s].", $query);
					return false;
				}

				$query = "";
			}

			mysqli_close($db_link);

			$this->db->query("update users set status=%d", USER_STATUS_CHANGEPWD);
			$this->settings->secret_website_code = random_string(32);

			return true;
		}

		/* Collect latest database version from update_database() function
		 */
		private function latest_database_version() {
			$old_db = $this->db;
			$old_settings = $this->settings;
			$this->db = new dummy_object();
			$this->settings = new dummy_object();
			$this->settings->database_version = 0.1;

			$this->update_database();
			$version = $this->settings->database_version;

			unset($this->db);
			unset($this->settings);
			$this->db = $old_db;
			$this->settings = $old_settings;

			return $version;
		}

		/* Execute query and report errors
		 */
		private function db_query($query) {
			static $first = true;
			static $logfile = null;

			$args = func_get_args();
			array_shift($args);

			if ($this->db->query($query, $args) === false) {
				if ($first) {
					$this->view->add_message("The following queries failed (also added to debug logfile):");
					$first = false;
				}

				$query = str_replace("%s", "'%s'", $query);
				$query = str_replace("%S", "`%s`", $query);
				$query = vsprintf($query, $args);

				$this->view->add_message(" - %s", $query);

				if ($logfile === null) {
					$logfile = new \Banshee\logfile("debug");
				}

				$logfile->add_entry("Failed query: %s", $query);
			}
		}

		/* Update database
		 */
		public function update_database() {
			if ($this->settings->database_version == 0.1) {
				$this->db_query("ALTER TABLE measures_preventive ADD %S TINYINT UNSIGNED NOT NULL AFTER deadline", "status");
				$this->db_query("ALTER TABLE measures_detective ADD %S TINYINT UNSIGNED NOT NULL AFTER deadline", "status");
				$this->db_query("ALTER TABLE measures_repressive ADD %S TINYINT UNSIGNED NOT NULL AFTER deadline", "status");
				$this->db_query("ALTER TABLE risks CHANGE %S created DATETIME NOT NULL", "timestamp");
				$this->db_query("ALTER TABLE risk_log CHANGE risk_id risk_id INT(10) UNSIGNED NOT NULL");

				$this->settings->database_version = 0.2;
			}

			if ($this->settings->database_version == 0.2) {
				$this->db_query("CREATE TABLE control_standards (id int(10) unsigned NOT NULL AUTO_INCREMENT, %S tinytext NOT NULL, ".
				                "PRIMARY KEY (id)) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci", "name");
				$this->db_query("CREATE TABLE controls (id int(10) unsigned NOT NULL AUTO_INCREMENT, control_standard_id int(10) unsigned NOT NULL, ".
				                "number varchar(25) NOT NULL, title tinytext NOT NULL, PRIMARY KEY (id), KEY tag_category_id (control_standard_id), ".
				                "CONSTRAINT controls_ibfk_1 FOREIGN KEY (control_standard_id) REFERENCES control_standards (id)) ".
				                "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci", "number");
				$this->db_query("CREATE TABLE control_preventive (control_id int(10) unsigned NOT NULL, measure_preventive_id int(10) unsigned NOT NULL, ".
				                "KEY control_id (control_id), KEY measure_preventive (measure_preventive_id), ".
				                "CONSTRAINT control_preventive_ibfk_1 FOREIGN KEY (control_id) REFERENCES controls (id), ".
				                "CONSTRAINT control_preventive_ibfk_2 FOREIGN KEY (measure_preventive_id) REFERENCES measures_preventive (id)) ".
				                "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci");
				$this->db_query("CREATE TABLE control_detective (control_id int(10) unsigned NOT NULL, measure_detective_id int(10) unsigned NOT NULL, ".
				                "KEY control_id (control_id), KEY measure_detective_id (measure_detective_id), ".
				                "CONSTRAINT control_detective_ibfk_1 FOREIGN KEY (control_id) REFERENCES controls (id), ".
				                "CONSTRAINT control_detective_ibfk_2 FOREIGN KEY (measure_detective_id) REFERENCES measures_detective (id)) ".
				                "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci");
				$this->db_query("CREATE TABLE control_repressive ( control_id int(10) unsigned NOT NULL, measure_repressive_id int(10) unsigned NOT NULL, ".
				                "KEY control_id (control_id), KEY measure_repressive_id (measure_repressive_id), ".
				                "CONSTRAINT control_repressive_ibfk_1 FOREIGN KEY (control_id) REFERENCES controls (id), ".
				                "CONSTRAINT control_repressive_ibfk_2 FOREIGN KEY (measure_repressive_id) REFERENCES measures_repressive (id)) ".
				                "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci");
				$this->db_query("INSERT INTO menu (text, link) VALUES (%s,%s),(%s,%s)", "Controls", "/control", "Export", "/export");

				$this->db->import("../database/measures.sql");

				$this->settings->database_version = 0.3;
			}

			if ($this->settings->database_version == 0.3) {
				$this->db_query("ALTER TABLE risks CHANGE accept_date accept_end_date DATE NOT NULL");
				$this->db_query("ALTER TABLE risks ADD levels_mitigated TINYINT UNSIGNED NOT NULL AFTER approach");

				$this->settings->database_version = 0.4;
			}

			if ($this->settings->database_version == 0.4) {
				$this->db_query("INSERT INTO settings VALUES(null, %s, %s, %d)", "risk_page_size", "integer", 15);

				$this->db_query("UPDATE menu SET text=%s, link=%s WHERE text=%s", "Issues", "/issue", "Matrix");

				$this->db_query("CREATE TABLE issues_preventive (id int(10) unsigned NOT NULL AUTO_INCREMENT, ".
				                "measure_id int(10) unsigned NOT NULL, title text NOT NULL, description text NOT NULL, ".
				                "created datetime NOT NULL, %S tinyint(3) unsigned NOT NULL, PRIMARY KEY (id), ".
				                "KEY measure_id (measure_id), CONSTRAINT issues_preventive_ibfk_1 FOREIGN KEY (measure_id) ".
				                "REFERENCES measures_preventive (id)) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci", "status");
				$this->db_query("CREATE TABLE issues_detective (id int(10) unsigned NOT NULL AUTO_INCREMENT, ".
				                "measure_id int(10) unsigned NOT NULL, title text NOT NULL, description text NOT NULL, ".
				                "created datetime NOT NULL, %S tinyint(3) unsigned NOT NULL, PRIMARY KEY (id), ".
				                "KEY measure_id (measure_id), CONSTRAINT issues_detective_ibfk_1 FOREIGN KEY (measure_id) ".
				                "REFERENCES measures_detective (id)) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci", "status");
				$this->db_query("CREATE TABLE issues_repressive (id int(10) unsigned NOT NULL AUTO_INCREMENT, ".
				                "measure_id int(10) unsigned NOT NULL, title text NOT NULL, description text NOT NULL, ".
				                "created datetime NOT NULL, %S tinyint(3) unsigned NOT NULL, PRIMARY KEY (id), ".
				                "KEY measure_id (measure_id), CONSTRAINT issues_repressive_ibfk_1 FOREIGN KEY (measure_id) ".
				                "REFERENCES measures_repressive (id)) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci", "status");

				$this->db_query("CREATE TABLE issue_detective_tasks ( id int(10) unsigned NOT NULL AUTO_INCREMENT, ".
				                "issue_id int(10) unsigned NOT NULL, title text NOT NULL, description text NOT NULL, ".
				                "executor text NOT NULL, deadline date NOT NULL, done tinyint(1) NOT NULL, PRIMARY KEY (id), ".
				                "KEY issue_id (issue_id), CONSTRAINT issue_detective_tasks_ibfk_1 FOREIGN KEY (issue_id) ".
				                "REFERENCES issues_detective (id)) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci");
				$this->db_query("CREATE TABLE issue_preventive_tasks ( id int(10) unsigned NOT NULL AUTO_INCREMENT, ".
				                "issue_id int(10) unsigned NOT NULL, title text NOT NULL, description text NOT NULL, ".
				                "executor text NOT NULL, deadline date NOT NULL, done tinyint(1) NOT NULL, PRIMARY KEY (id), ".
				                "KEY issue_id (issue_id), CONSTRAINT issue_preventive_tasks_ibfk_1 FOREIGN KEY (issue_id) ".
				                "REFERENCES issues_preventive (id)) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci");
				$this->db_query("CREATE TABLE issue_repressive_tasks ( id int(10) unsigned NOT NULL AUTO_INCREMENT, ".
				                "issue_id int(10) unsigned NOT NULL, title text NOT NULL, description text NOT NULL, ".
				                "executor text NOT NULL, deadline date NOT NULL, done tinyint(1) NOT NULL, PRIMARY KEY (id), ".
				                "KEY issue_id (issue_id), CONSTRAINT issue_repressive_tasks_ibfk_1 FOREIGN KEY (issue_id) ".
				                "REFERENCES issues_repressive (id)) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci");

				$this->db->import("../database/view_issues.sql");
				$this->db->import("../database/view_issue_tasks.sql");

				$this->settings->database_version = 0.5;
			}

			if ($this->settings->database_version == 0.5) {
				$this->settings->database_version = 0.6;
			}

			if ($this->settings->database_version == 0.6) {
				$this->db_query("CREATE TABLE advisors (id int(10) unsigned NOT NULL AUTO_INCREMENT, organisation_id int(10) unsigned ".
				                "NOT NULL, user_id int(10) unsigned NOT NULL, crypto_key text DEFAULT NULL, PRIMARY KEY (id), KEY organisation_id ".
				                "(organisation_id), KEY advisors_ibfk_2 (user_id), CONSTRAINT advisors_ibfk_1 FOREIGN KEY (organisation_id) ".
				                "REFERENCES organisations (id), CONSTRAINT advisors_ibfk_2 FOREIGN KEY (user_id) REFERENCES users (id)) ".
				                "ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci");

				$this->settings->database_version = 0.7;
			}

			if ($this->settings->database_version == 0.7) {
				$this->db_query("UPDATE settings SET %S=%s WHERE %S=%s", "key", "role_id_advisor", "key", "advisor_role_id");
				$this->db_query("UPDATE settings SET %S=%s WHERE %S=%s", "key", "role_id_employee", "key", "employee_role_id");
				$this->db_query("UPDATE settings SET %S=%s WHERE %S=%s", "key", "role_id_measure_owner", "key", "measure_owner_role_id");
				$this->db_query("UPDATE settings SET %S=%s WHERE %S=%s", "key", "role_id_risk_manager", "key", "risk_manager_role_id");
				$this->db_query("UPDATE settings SET %S=%s WHERE %S=%s", "key", "role_id_risk_owner", "key", "risk_owner_role_id");
				$this->db_query("UPDATE settings SET %S=%s WHERE %S=%s", "key", "role_id_risk_viewer", "key", "risk_viewer_role_id");
				$this->db_query("ALTER TABLE organisations ADD impact ENUM(%s,%s) NOT NULL AFTER matrix_vertical", "horizontal", "vertical");
				$this->db_query("ALTER TABLE effects ADD impact1 TEXT NULL, ADD impact2 TEXT NULL, ADD impact3 TEXT NULL, ".
				                "ADD impact4 TEXT NULL, ADD impact5 TEXT NULL, ADD impact6 TEXT NULL, ADD impact7 TEXT NULL");

				$this->settings->database_version = 0.8;
			}

			if ($this->settings->database_version == 0.8) {
				$this->settings->database_version = 0.9;
			}

			if ($this->settings->database_version == 0.9) {
				$this->settings->database_version = 1.0;
			}

			return true;
		}

		/* Import INSERTs from mysql.sql
		 */
		public function import_inserts($tables) {
			if (count($tables) == 0) {
				return;
			}

			if (($queries = file("../database/mysql.sql")) === false) {
				$this->view->add_message("Can't read the database/mysql.sql file.");
				return false;
			}

			if (($db_link = mysqli_connect(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE)) === false) {
				$this->view->add_message("Error while connecting to the database.");
				return false;
			}

			foreach ($tables as $table) {
				mysqli_query($db_link, "DELETE FROM `".$table."`");
				mysqli_query($db_link, "ALTER TABLE `".$table."` AUTO_INCREMENT=1");
			}

			$line = "";
			foreach ($queries as $part) {
				if (($part = trim($part)) == "") {
					continue;
				}

				if (substr($part, 0, 2) == "--") {
					continue;
				}

				$line .= $part;
				if (substr($part, -1) != ";") {
					continue;
				}

				$query = $line;
				$line = "";

				if (substr($query, 0, 6) != "INSERT") {
					continue;
				}

				list(, $table) = explode("`", $query, 3);
				if (in_array($table, $tables) == false) {
					continue;
				}

				if (mysqli_query($db_link, $query) === false) {
					$this->view->add_message("Error while executing query [%s].", $query);
					return false;
				}
			}

			mysqli_close($db_link);
		}

		/* Define matrix
		 */
		public function define_matrix($matrix) {
			if ((trim($matrix["matrix_vertical"]) == "") || (trim($matrix["matrix_horizontal"]) == "")) {
				$this->view->add_message("Specify the matrix labels.");
				return false;
			}

			$data = array(
				"matrix_vertical"   => $matrix["matrix_vertical"],
				"matrix_horizontal" => $matrix["matrix_horizontal"],
				"matrix_size"       => $matrix["matrix_size"],
				"risk_levels"       => $matrix["risk_levels"]);
			if ($this->db->update("organisations", 1, $data) === false) {
				$this->view->add_message("Error updating register settings.");
				return false;
			}

			$this->borrow("cms/organisation")->add_register_settings(1, $data);

			return true;
		}

		/* Set administrator password
		 */
		public function set_admin_credentials($username, $password, $repeat) {
			$result = true;

			if (valid_input($username, VALIDATE_NONCAPITALS.VALIDATE_NUMBERS."@.-_", VALIDATE_NONEMPTY) == false) {
				$this->view->add_message("The username must consist of lowercase letters or an e-mail address.");
				$result = false;
			}

			if ($password != $repeat) {
				$this->view->add_message("The passwords do not match.");
				$result = false;
			}

			if (is_secure_password($password, $this->view) == false) {
				$result = false;
			}

			if ($result == false) {
				return false;
			}

			if (is_true(ENCRYPT_DATA)) {
				$crypto_key = random_string(CRYPTO_KEY_SIZE);

				$rsa = new \Banshee\Protocol\RSA((int)RSA_KEY_SIZE);
				$aes = new \Banshee\Protocol\AES256($crypto_key);
				$private_key = $aes->encrypt($rsa->private_key);
				$public_key = $rsa->public_key;

				$aes = new \Banshee\Protocol\AES256($password);
				$crypto_key = $aes->encrypt($crypto_key);
			} else {
				$private_key = null;
				$public_key = null;
				$crypto_key = null;
			}

			$password = password_hash($password, PASSWORD_ALGORITHM);

			$data = array(
				"username"	=> $username,
				"password"	=> $password,
				"status"	  => USER_STATUS_ACTIVE,
				"private_key" => $private_key,
				"public_key"  => $public_key,
				"crypto_key"  => $crypto_key);
			if ($this->db->update("users", 1, $data) === false) {
				$this->view->add_message("Error while setting password.");
				return false;
			}

			return true;
		}
	}

	class dummy_object {
		private $cache = array();

		public function __set($key, $value) {
			$this->cache[$key] = $value;
		}

		public function __get($key) {
			return $this->cache[$key];
		}

		public function __call($func, $args) {
			 return true;
		}
	}
?>
