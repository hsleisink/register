<p>A new account has been created at the [WEBSITE] website for:</p>
<p>[ORGANISATION]</p>
<p>[FULLNAME] &lt;[EMAIL]&gt;: [USERNAME]</p>
<p>IP address: [IP_ADDR]</p>
