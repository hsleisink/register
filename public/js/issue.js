$(document).ready(function() {
	$('li.list-group-item div.title').on('click', function() {
		$(this).parent().find('div.details').toggleClass('collapsed');
		$(this).find('span.glyphicon').toggleClass('glyphicon-chevron-up');
		$(this).find('span.glyphicon').toggleClass('glyphicon-chevron-down');
	});

	$('li.list-group-item').each(function() {
		if ($(this).find('img.attention').length > 0) {
			$(this).find('div.title').trigger('click');
		}
	});

	if ($('p.description').text().split(' ').length >= 100) {
		$('p.description').css('column-count', 2);
	}
});
