/* Show control selector
 */
function show_controls() {
	$('div.controls').addClass('hidden');

	var standard_id = $('select.standards').val();
	$('div.controls[standard_id="' + standard_id + '"]').removeClass('hidden');
}

/* Add control
 */
function add_control(control_id) {
	if ($('div.measure_controls input[value="' + control_id + '"]').length > 0) {
		return;
	}

	var option = $('select.controls option[value="' + control_id + '"]');
	var standard_id = option.parent().attr('standard_id');

	var standard = $('select.standards option[value="' + standard_id +'"]').text();
	var control = option.text();

	var measure_control = $('<div class="input-group">' +
		'<input type="hidden" name="controls[]" value="' + control_id + '" />' +
		'<input type="text" value="' + standard + ' - ' + control + '" readonly="readonly" class="form-control" />' +
		'<span class="input-group-btn"><button class="btn btn-default" type="button">Delete</button></span>' +
		'</div>');
	
	measure_control.find('button').on('click', function() {
		$(this).parent().parent().remove();

		if ($('div.measure_controls div').length == 0) {
			add_none_marker();
		}
	});

	$('div.measure_controls div.none').remove();
	$('div.measure_controls').append(measure_control);
}

/* Add none-marker
 */
function add_none_marker() {
	$('div.measure_controls').append('<div class="none">' + language_module['controls_none'] + '</div>');
}

$(document).ready(function() {
	$('select.standards').on('change', show_controls);
	$('select.standards').trigger('change');

	/* Add-control button 
	*/
	$('div.input-group input.add-control').on('click', function() {
		var control_id = $(this).parent().parent().find('select').val();
		add_control(control_id);

		return false;
	});

	/* Add current controls
	 */
	$('div.measure_controls span').each(function(span) {
		var control_id = $(this).text();
		add_control(control_id);
	});

	if ($('div.measure_controls div').length == 0) {
		add_none_marker();
	}
});
