$(document).ready(function() {
	$('li.list-group-item div.title').on('click', function() {
		$(this).parent().find('div.details').toggleClass('collapsed');
		$(this).find('span.glyphicon').toggleClass('glyphicon-chevron-up');
		$(this).find('span.glyphicon').toggleClass('glyphicon-chevron-down');
	});
});
