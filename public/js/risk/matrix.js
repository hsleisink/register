function show_risks(horizontal, vertical) {
	$('table.risks').removeClass('hidden');

	$('table.risks tbody tr').each(function(row) {
		var h = parseInt($(this).attr('horizontal')) == horizontal;
		var v = parseInt($(this).attr('vertical')) == vertical;

		if (h && v) {
			$(this).show();
		} else {
			$(this).hide();
		}
	});
}

$(document).ready(function() {
	$('table.matrix td.cell').each(function() {
		var bgcolor = $(this).attr('color');

		$(this).css('background-color', bgcolor);
		$(this).css('color', background_to_font_color(bgcolor));

		$(this).append('<div class="background-hack" style="border-color:' + bgcolor + '"></div>');
	});

	$('table.matrix td.cell span').each(function() {
		if ($(this).text() == '') {
			return true;
		}

		$(this).parent().addClass('number').on('click', function() {
			var horizontal = $(this).prevAll().length;
			var vertical = 7 - $(this).parent().prevAll().length;

			show_risks(horizontal, vertical);
		});
	});
});
