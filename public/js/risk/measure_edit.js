const WINDOW_WIDTH = 800;

function show_cause_effect_info(title, description) {
	var content = '<div>' + description + '</div>';

	var info = $(content).windowframe({
		header: title,
		width: WINDOW_WIDTH,
		close: function() {
			info.destroy();
		}
	});

	info.open();
}

$(document).ready(function() {
	/* Textarea size
	 */
	if ($(window).width() >= 768) {
		if ($('div.row:first-of-type div.col-xs-12:first-child div.form-group').length == 5) {
			$('textarea').css('height', '330px');
		}
	}

	/* Status
	 */
	$('select[name="status"]').on('change', function() {
		var measure_status = $('select[name="status"]').val();

		if (measure_status == 0) {
			$('label[for="deadline"]').text(language_module['deadline_implementation'] + ':');
		} else if (measure_status == 1) {
			$('label[for="deadline"]').text(language_module['deadline_effectiveness_check'] + ':');
		} else {
			$('label[for="deadline"]').text(language_module['deadline_effectiveness_recheck'] + ':');
		}
	});

	$('select[name="status"]').trigger('change');

	/* Risk information
	 */
	var risk_info = $('div.risk').windowframe({
		header: $('div.risk').attr('title'),
		width: WINDOW_WIDTH
	});

	$('input.risk').on('click', function() {
		risk_info.open();
	});

	/* Cause
	 */
	$('button.cause').on('click', function() {
		var cause_id = $('input[cause_id]').attr('cause_id');
		if (cause_id == undefined) {
			cause_id = $('select[name="risk_cause_id"]').val();
		}

		$.post('/risk/measure', {
			cause_id: cause_id
		}).done(function(data) {
			var cause = $(data).find('cause').text();
			var title = $(data).find('title').text();
			var description = $(data).find('description').text();

			show_cause_effect_info(cause + ' | ' + title, description);
		});
	});

	/* Effect
	 */
	$('button.effect').on('click', function() {
		var effect_id = $('input[effect_id]').attr('effect_id');
		if (effect_id == undefined) {
			effect_id = $('select[name="risk_effect_id"]').val();
		}

		$.post('/risk/measure', {
			effect_id: effect_id
		}).done(function(data) {
			var effect = $(data).find('effect').text();
			var title = $(data).find('title').text();
			var description = $(data).find('description').text();

			show_cause_effect_info(effect + ' | ' + title, description);
		});
	});
});
