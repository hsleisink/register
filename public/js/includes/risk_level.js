function background_to_font_color(bgcolor) {
	var red = Number('0x' + bgcolor.substr(1, 2));
	var green = Number('0x' + bgcolor.substr(3, 2));
	var blue = Number('0x' + bgcolor.substr(5, 2));

	return ((red * 0.299) + (green * 0.587) + (blue * 0.114)) > 186 ? '#000000' : '#ffffff';
}

function select_risk_level_color(selector) {
	var bgcolor = $(this).find('option:selected').attr('color');

	$(this).css('background-color', bgcolor);
	$(this).css('color', background_to_font_color(bgcolor));
}
