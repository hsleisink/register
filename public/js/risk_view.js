$(document).ready(function() {
	$('table.matrix td').each(function() {
		var bgcolor = $(this).attr('color');
		$(this).css('background-color', bgcolor);

		if ($(this).attr('marked') != undefined) {
			$(this).css('color', background_to_font_color(bgcolor));
		}

		if ($(this).hasClass('cell')) {
			$(this).append('<div class="background-hack" style="border-color:' + bgcolor + '"></div>');
		}
	});

	$('li.list-group-item div.title').on('click', function() {
		$(this).parent().find('div.description').toggleClass('collapsed');
		$(this).find('span.glyphicon').toggleClass('glyphicon-chevron-up');
		$(this).find('span.glyphicon').toggleClass('glyphicon-chevron-down');
	});

	if ($('p.description').text().split(' ').length < 100) {
		$('p.description').css('column-count', '');
	}

	/* Mitigated color
	 */
	var mitigated = $('span.mitigated');
	var bgcolor = mitigated.attr('color');
	if (bgcolor != undefined) {
		mitigated.css('background-color', bgcolor);
		mitigated.css('color', background_to_font_color(bgcolor));
	}
});
