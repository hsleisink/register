$(document).ready(function() {
	$('table.controls tr.click').on('click', function() {
		$(this).next().toggle();
	});

	/* Entry colors
	 */
	$('table.controls tr.click').each(function() {
		var space = $('<span>&#160;</span>').text();
		var measures = $(this).find('td:nth-child(3)').text().split(space);
		var effective = parseInt(measures[0].trim());
		var total = parseInt(measures[2].trim());

		var color = (effective != total) ? 'ffffd0' : 'd0ffd0';
		$(this).css('background-color', '#' + color);
	});
});
