$(document).ready(function() {
	if ($('#help').length == 0) {
		return;
	}

	var label = 'Help';
	if (typeof language_global != 'undefined') {
		if (typeof language_global['btn_help'] != 'undefined') {
			label = language_global['btn_help'];
		}
	}

	var help_button = $('<button type="button" class="btn btn-default btn-xs help" data-toggle="modal" data-target="#help_message">' + label + '</button>');

	$('div#help').windowframe({
		activator: help_button,
		header: label,
		top: '50px'
	});

	var content = $('body div.content');
	var container = $(content).find('div.container');
	if (container.length != 0) {
		content = container;
	}

	var title = $(content).find('h1');
	var icon = $(content).find('img.title_icon');
	var mesg = $(content).find('div.alert');

	if (title.length != 0) {
		$(title).first().before(help_button);
	} else if (icon.length != 0) {
		$(icon).first().after(help_button);
	} else if (mesg.length != 0) {
		$(mesg).first().after(help_button);
	} else {
		$(help_button).prependTo(content);
	}
});
