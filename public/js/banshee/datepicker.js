$(document).ready(function() {
	$("input.datepicker").datepicker({
		dateFormat: "yy-mm-dd",
		firstDay: 1,
		showButtonPanel: true,
		showWeek: true,
		beforeShow: function() {
			setTimeout(function(){
				$('.ui-datepicker').css('z-index', 100);
			}, 0)
		}
	});
});
