function set_authenticator_code() {
	clear_authenticator_code();

	$.get('/register/authenticator', function(data) {
		var totp = encodeURIComponent($(data).find('secret').text());

		$('input#secret').val(totp);
		$('div.qrcode').append('<img src="/register/totp/' + totp + '" />');
	});
}

function clear_authenticator_code() {
	$('input#secret').val('');
	$('div.secret_set').remove();
	$('div.qrcode').empty();
}

function add_delay_warning() {
	var text_added = false;

	$('input.submit').on('click', function() {
		if (text_added == false) {
			$('body').append('<div class="creating"><div>' + language_module['creating_account'] + '</div></div>');
			text_added = true;
		}

		$('div.creating').css('display', 'block');
	});
}
