$(document).ready(function() {
	$('table.measures td a').each(function() {
		var link = $(this).attr('href');

		$(this).parent().on('click', function(event) {
			document.location = link;
			event.stopPropagation();
			return false;
		});
	});
});
