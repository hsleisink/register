$(document).ready(function() {
	var count = 0;
	$('div.chapter').each(function() {
		var chapter = 'chapter' + count.toString();
		$(this).addClass(chapter);

		var link = $('<li chapter="' + chapter + '">' + $(this).find('h2').text() + '</li>');
		link.on('click', function() {
			$('div.chapter').hide();
			var chapter = $(this).attr('chapter');
			$('div.' + chapter).show();
		});

		$('ul.index').append(link);

		count++;
	});
});
