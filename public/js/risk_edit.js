$(document).ready(function() {
	$('select#levels_mitigated').on('change', select_risk_level_color);
	$('select#levels_mitigated').trigger('change');

	var identifiers = {};

	$('ul.identifiers li').each(function() {
		var identifier = $(this).text();

		while (identifier.length > 0) {
			var ascii = identifier.charCodeAt(identifier.length - 1);
			if ((ascii >= 48) && (ascii <= 57)) {
				identifier = identifier.substr(0, identifier.length - 1);
			} else {
				break;
			}
		}

		if (identifier.length > 0) {
			var value = $(this).text().substr(identifier.length);
			var value_len = value.length;

			if (value_len > 0) {
				value = parseInt(value);

				if (identifiers[identifier] == undefined) {
					identifiers[identifier] = {
						value: value,
						length: value_len
					};
				} else if (identifiers[identifier].value < value) {
					identifiers[identifier].value = value;
					identifiers[identifier].length = value_len;
				}
			}
		}
	});


	var identifier_count = Object.keys(identifiers).length;

	if (identifier_count > 0) {
		var input = $('div.identifier-input');
		input.addClass('input-group');

		var btn = '<span class="input-group-btn"><button class="btn btn-default" type="button">. . .</button></span>';
		input.append(btn);

		if (identifier_count > 40) {
			col = 3;
			width = 800;
		} else if (identifier_count > 20) {
			col = 4;
			width = 700;
		} else {
			col = 6;
			width = 600;
		}

		var list = $('<div class="row identifier-select"></div>');
		Object.entries(identifiers).forEach(function([key, value]) {
			list.append('<div class="col-xs-' + col + ' item" value="' + key + '">' + key + ' .'.repeat(value.length) + '</div>');
		});

		var list_window = $(list).windowframe({
			activator: input.find('button'),
			header: language_module['identifier'],
			width: width
		});

		var delay = 300;

		list.find('div.item').css('cursor', 'pointer').on('click', function() {
			$('input#identifier').val($(this).attr('value'));
			list_window.close();

			delay = 0;
			$('input#identifier').trigger('keyup');
		});

		var timer = null;

		$('input#identifier').on('keyup', function(event) {
			if ([8, 16, 20].includes(event.which)) {
				return;
			}

			if (timer != null) {
				clearTimeout(timer);
			}

			var input_obj = $(this);

			timer = setTimeout(function() {
				timer = null;

				var input = input_obj.val().toLowerCase();

				Object.entries(identifiers).forEach(function([key, value]) {
					if (input == key.toLowerCase()) {
						input = key + (value.value + 1).toString().padStart(value.length, '0');
						$('input#identifier').val(input);
					}
				});
			}, delay);

			delay = 300;
		});
	}

	if (identifier_count == 1) {
		var key = Object.keys(identifiers)[0];
		var value = (identifiers[key].value + 1).toString().padStart(identifiers[key].length, '0');

		$('input#identifier').val(key + value);
	}
});
