var account_status = null;

function password_field() {
	if ($('input#generate:checked').length > 0) {
		$('input#password').val('');
		$('input#password').prop('disabled', true);

		if ($('select#status').prop('disabled') == false) {
			account_status = $('select#status').val();
			$('select#status').val(1);
		}
	} else {
		$('input#password').prop('disabled', false);

		if ($('select#status').prop('disabled') == false) {
			$('select#status').val(account_status);
		}
	}
}

function set_authenticator_code() {
	clear_authenticator_code();

	$.get('/cms/user/authenticator', function(data) {
		var username = encodeURIComponent($('input#username').val());
		var totp = encodeURIComponent($(data).find('secret').text());

		$('input#secret').val(totp);
		$('div.qrcode').append('<img src="/cms/user/totp/' + username + '/' + totp + '" />');
	});
}

function clear_authenticator_code() {
	$('input#secret').val('');
	$('div.secret_set').remove();
	$('div.qrcode').empty();
}

$(document).ready(function() {
	$('select.risk').on('change', select_risk_level_color);
	$('select.risk').trigger('change');
});
