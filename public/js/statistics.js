$(document).ready(function() {
	var max = 0;

	$('div.tags').each(function() {
		var height = $(this).outerHeight();
		if (height > max) {
			max = height;
		}
	});

	$('div.tags').each(function() {
		var height = $(this).outerHeight();

		$(this).append('<div class="padding" style="height:' + (max - height + 15) + 'px" />');
	});

	$('div.filter button').on('click', function() {
		$('div.well').toggle();
	});
});
