function reset_filter() {
	$('select.level').val(0);
	$('div.tag_category input').prop('checked', false);
	$('select.owner').val(0);

	return false;
}

$(document).ready(function() {
	$('div.btn-group button').on('click', reset_filter);
});
