function reset_filter() {
	$('input[type="checkbox"]').prop('checked', false);
	$('select.owner').val(0);

	return false;
}

$(document).ready(function() {
	$('div.btn-group button').on('click', reset_filter);
});
