CREATE VIEW issues AS

SELECT i.id, 'preventive' COLLATE utf8mb4_general_ci as type, i.measure_id, i.title, i.description, i.created, i.status, r.id as risk_id
FROM issues_preventive i, measures_preventive m, risk_cause rc, risks r
WHERE i.measure_id=m.id and m.risk_cause_id=rc.id and rc.risk_id=r.id

UNION

SELECT i.id, 'detective' COLLATE utf8mb4_general_ci as type, i.measure_id, i.title, i.description, i.created, i.status, r.id as risk_id
FROM issues_detective i, measures_detective m, risks r
WHERE i.measure_id=m.id and m.risk_id=r.id

UNION

SELECT i.id, 'repressive' COLLATE utf8mb4_general_ci as type, i.measure_id, i.title, i.description, i.created, i.status, r.id as risk_id
FROM issues_repressive i, measures_repressive m, risk_effect re, risks r
WHERE i.measure_id=m.id and m.risK_effect_id=re.id and re.risk_id=r.id
