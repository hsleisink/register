5.1.1	Beleidsregels voor informatiebeveiliging
5.1.2	Beoordeling van het informatiebeveiligingsbeleid
6.1.1	Rollen en verantwoordelijkheden bij informatiebeveiliging
6.1.2	Scheiding van taken
6.1.3	Contact met overheidsinstanties
6.1.5	Informatiebeveiliging in projectbeheer
6.2.1	Beleid voor mobiele apparatuur
6.2.2	Telewerken
7.1.1	Screening
7.1.2	Arbeidsvoorwaarden
7.2.1	Directieverantwoordelijkheden
7.2.2	Bewustzijn, opleiding en training ten aanzien van informatiebeveiliging
7.2.3	Disciplinaire procedure
7.3.1	Beëindiging of wijziging van verantwoordelijkheden van het dienstverband
8.1.1	Inventariseren van bedrijfsmiddelen
8.1.2	Eigendom van bedrijfsmiddelen
8.1.3	Aanvaardbaar gebruik van bedrijfsmiddelen
8.1.4	Teruggeven van bedrijfsmiddelen
8.2.1	Classificatie van informatie
8.2.2	Informatie labelen
8.2.3	Behandelen van bedrijfsmiddelen
8.3.1	Beheer van verwijderbare media
8.3.2	Verwijderen van media
8.3.3	Media fysiek overdragen
9.1.1	Beleid voor toegangsbeveiliging
9.1.2	Toegang tot netwerken en netwerkdiensten
9.2.1	Registratie en afmelden van gebruikers
9.2.2	Gebruikers toegang verlenen
9.2.3	Beheren van speciale toegangsrechten
9.2.4	Beheer van geheime authenticatie-informatie van gebruikers
9.2.5	Beoordeling van toegangsrechten van gebruikers
9.2.6	Toegangsrechten intrekken of aanpassen
9.3.1	Geheime authenticatie-informatie gebruiken
9.4.1	Beperking toegang tot informatie
9.4.2	Beveiligde inlogprocedures
9.4.3	Systeem voor wachtwoordbeheer
9.4.4	Speciale systeemhulpmiddelen gebruiken
9.4.5	Toegangsbeveiliging op programmabroncode
10.1.1	Beleid inzake het gebruik van cryptografische beheersmaatregelen
10.1.2	Sleutelbeheer
11.1.1	Fysieke beveiligingszone
11.1.2	Fysieke toegangsbeveiliging
11.1.3	Kantoren, ruimten en faciliteiten beveiligen
11.1.4	Beschermen tegen bedreigingen van buitenaf
11.1.5	Werken in beveiligde gebieden
11.1.6	Laad- en loslocatie
11.2.1	Plaatsing en bescherming van apparatuur
11.2.2	Nutsvoorzieningen
11.2.3	Beveiliging van bekabeling
11.2.4	Onderhoud van apparatuur
11.2.5	Verwijdering van bedrijfsmiddelen
11.2.6	Beveiliging van apparatuur en bedrijfsmiddelen buiten het terrein
11.2.7	Veilig verwijderen of hergebruiken van apparatuur
11.2.8	Onbeheerde gebruikersapparatuur
11.2.9	‘Clear desk’- en ‘clear screen’-beleid
12.1.1	Gedocumenteerde bedieningsprocedures
12.1.2	Wijzigingsbeheer
12.1.3	Capaciteitsbeheer
12.1.4	Scheiding van ontwikkel-, test- en productieomgevingen
12.2.1	Beheersmaatregelen tegen malware
12.3.1	Back-up van informatie
12.4.1	Gebeurtenissen registreren
12.4.2	Beschermen van informatie in logbestanden
12.4.3	Logbestanden van beheerders en operators
12.4.4	Kloksynchronisatie
12.5.1	Software installeren op operationele systemen
12.6.1	Beheer van technische kwetsbaarheden
12.6.2	Beperkingen voor het installeren van software
12.7.1	Beheersmaatregelen betreffende audits van informatiesystemen
13.1.1	Beheersmaatregelen voor netwerken
13.1.2	Beveiliging van netwerkdiensten
13.1.3	Scheiding in netwerken
13.2.1	Beleid en procedures voor informatietransport
13.2.2	Overeenkomsten over informatietransport
13.2.3	Elektronische berichten
13.2.4	Vertrouwelijkheids- of geheimhoudingsovereenkomst
14.1.1	Analyse en specificatie van informatiebeveiligingseisen
14.1.2	Toepassingen op openbare netwerken beveiligen
14.1.3	Transacties van toepassingen beschermen
14.2.1	Beleid voor beveiligd ontwikkelen
14.2.2	Procedures voor wijzigingsbeheer met betrekking tot systemen
14.2.3	Technische beoordeling van toepassingen na wijzigingen besturingsplatform
14.2.5	Principes voor engineering van beveiligde systemen
14.2.6	Beveiligde ontwikkelomgeving
14.2.7	Uitbestede softwareontwikkeling
14.2.8	Testen van systeembeveiliging
14.2.9	Systeemacceptatietests
14.3.1	Bescherming van testgegevens
15.1.1	Informatiebeveiligingsbeleid voor leveranciersrelaties
15.1.2	Opnemen van beveiligingsaspecten in leveranciersovereenkomsten
15.1.3	Toeleveringsketen van informatie- en communicatietechnologie
15.2.1	Monitoring en beoordeling van dienstverlening van leveranciers
15.2.2	Beheer van veranderingen in dienstverlening van leveranciers
16.1.1	Verantwoordelijkheden en procedures
16.1.2	Rapportage van informatiebeveiligingsgebeurtenissen
16.1.3	Rapportage van zwakke plekken in de informatiebeveiliging
16.1.4	Beoordeling van en besluitvorming over informatiebeveiligingsgebeurtenissen
16.1.5	Respons op informatiebeveiligingsincidenten
16.1.6	Lering uit informatiebeveiligingsincidenten
16.1.7	Verzamelen van bewijsmateriaal
17.1.1	Informatiebeveiligingscontinuïteit plannen
17.1.2	Informatiebeveiligingscontinuïteit implementeren
17.1.3	Informatiebeveiligingscontinuïteit verifiëren, beoordelen en evalueren
17.2.1	Beschikbaarheid van informatieverwerkende faciliteiten
18.1.1	Vaststellen van toepasselijke wetgeving en contractuele eisen
18.1.2	Intellectuele-eigendomsrechten
18.1.3	Beschermen van registraties
18.1.4	Privacy en bescherming van persoonsgegevens
18.1.5	Voorschriften voor het gebruik van cryptografische beheersmaatregelen
18.2.1	Onafhankelijke beoordeling van informatiebeveiliging
18.2.2	Naleving van beveiligingsbeleid en -normen
18.2.3	Beoordeling van technische naleving
