CREATE VIEW issue_tasks AS

SELECT t.id, 'preventive' COLLATE utf8mb4_general_ci as type, i.id as issue_id, t.title, t.description, t.executor, t.deadline, t.done
FROM issue_preventive_tasks t, issues_preventive i
WHERE t.issue_id=i.id

UNION

SELECT t.id, 'detective' COLLATE utf8mb4_general_ci as type, i.id as issue_id, t.title, t.description, t.executor, t.deadline, t.done
FROM issue_detective_tasks t, issues_detective i
WHERE t.issue_id=i.id

UNION

SELECT t.id, 'repressive' COLLATE utf8mb4_general_ci as type, i.id as issue_id, t.title, t.description, t.executor, t.deadline, t.done
FROM issue_repressive_tasks t, issues_repressive i
WHERE t.issue_id=i.id
