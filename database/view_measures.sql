CREATE VIEW measures AS

SELECT m.id, 'preventive' COLLATE utf8mb4_general_ci as type, risk_cause_id as ref_id, owner_id, m.title, m.description, deadline, status, c.risk_id
FROM measures_preventive m, risk_cause c
WHERE m.risk_cause_id=c.id

UNION

SELECT id, 'detective' COLLATE utf8mb4_general_ci as type, risk_id as ref_id, owner_id, title, description, deadline, status, risk_id
FROM measures_detective

UNION

SELECT m.id, 'repressive' COLLATE utf8mb4_general_ci as type, risk_effect_id as ref_id, owner_id, m.title, m.description, deadline, status, e.risk_id
FROM measures_repressive m, risk_effect e
WHERE m.risk_effect_id=e.id
