<?xml version="1.0" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="banshee/main.xslt" />
<xsl:import href="banshee/pagination.xslt" />

<!--
//
//  Overview template
//
//-->
<xsl:template match="overview">
<table class="table table-hover table-xs issues">
<thead class="table-xs">
<tr>
<th><xsl:value-of select="/output/language/module/issue" /></th>
<th><xsl:value-of select="/output/language/module/measure" /></th>
<th><xsl:value-of select="/output/language/module/created" /></th>
<th><xsl:value-of select="/output/language/module/status" /></th>
<th><xsl:value-of select="/output/language/module/tasks" /></th>
</tr>
</thead>
<tbody>
<xsl:for-each select="issues/issue">
<tr class="click" onClick="javascript:document.location='/{/output/page}/{type}/{@id}'">
<td><span class="table-xs"><xsl:value-of select="/output/language/module/issue" /></span><xsl:value-of select="title" /></td>
<td><span class="table-xs"><xsl:value-of select="/output/language/module/measure" /></span><xsl:value-of select="measure" /></td>
<td><span class="table-xs"><xsl:value-of select="/output/language/module/created" /></span><xsl:value-of select="created" /></td>
<td><span class="table-xs"><xsl:value-of select="/output/language/module/status" /></span><xsl:value-of select="status" /></td>
<td><span class="table-xs"><xsl:value-of select="/output/language/module/tasks" /></span><xsl:value-of select="tasks_done" />&#160;<xsl:value-of select="/output/language/global/of" />&#160;<xsl:value-of select="tasks" /></td>
</tr>
</xsl:for-each>
</tbody>
<xsl:if test="@subpage='yes'">
<tfoot>
<tr><td></td><td></td><td></td><td></td>
<td><xsl:value-of select="/output/language/global/total" />: <xsl:value-of select="count(issues/issue)" /></td></tr>
</tfoot>
</xsl:if>
</table>

<div class="btn-group left">
<xsl:if test="@subpage='no'">
<a href="/{/output/page}/attention" class="btn btn-default"><xsl:value-of select="/output/language/module/btn_attention" /></a>
<xsl:if test="@calendar='yes'">
<a href="/{/output/page}/calendar" class="btn btn-default"><xsl:value-of select="/output/language/module/btn_view_deadline_calender" /></a>
</xsl:if>
</xsl:if>
<xsl:if test="@subpage='yes'">
<a href="/{/output/page}" class="btn btn-default"><xsl:value-of select="/output/language/global/btn_back" /></a>
</xsl:if>
</div>

<div class="right">
<xsl:apply-templates select="pagination" />
</div>

<div id="help">
<xsl:value-of select="/output/language/module/help_overview" disable-output-escaping="yes" />
</div>
</xsl:template>

<!--
//
//  View template
//
//-->
<xsl:template match="view">
<div class="panel panel-primary">
<div class="panel-heading"><xsl:value-of select="issue/title" /></div>
<div class="panel-body">
<xsl:if test="issue/description!=''">
<p class="description"><xsl:value-of select="issue/description" /></p>
<hr />
</xsl:if>
<div class="row">
<div class="col-xs-12 col-sm-6 col-lg-4"><span class="field"><xsl:value-of select="/output/language/module/created_at" />:</span> <xsl:value-of select="issue/created" /></div>
<div class="col-xs-12 col-sm-6 col-lg-4"><span class="field"><xsl:value-of select="/output/language/module/status" />:</span> <xsl:value-of select="statuses/status[(position()-1)=../../issue/status]" /><xsl:if test="issue/status!=(count(statuses/status)-1)"><img src="/images/warning.png" class="attention" /></xsl:if></div>
<div class="col-xs-12 col-lg-4"><span class="field"><xsl:value-of select="/output/language/module/for_measure" />:</span> <a href="/measure/{issue/type}/{measure/@id}"><xsl:value-of select="measure/title" /></a></div>
</div>

<xsl:if test="@edit='yes'">
<div class="btn-group">
<a href="/{/output/page}/{issue/type}/{issue/@id}/edit" class="btn btn-default btn-sm"><xsl:value-of select="/output/language/module/btn_issue_edit" /></a>
</div>
</xsl:if>
</div>
</div>

<div class="panel panel-default">
<div class="panel-heading"><xsl:if test="@edit='yes'"><div class="right"><a href="/{/output/page}/task/{issue/@id}/{issue/type}" class="btn btn-primary btn-xs">+</a></div></xsl:if><xsl:value-of select="/output/language/module/tasks" /></div>
<ul class="list-group">
<xsl:for-each select="tasks/task">
<li class="list-group-item">
<div class="title"><span class="glyphicon glyphicon-chevron-down" /><xsl:if test="done='yes'"><img src="/images/check.png" class="check" /></xsl:if><xsl:value-of select="title" /></div>
<div class="details collapsed">
<xsl:if test="description!=''">
<div class="description"><xsl:value-of select="description" /></div>
<hr />
</xsl:if>
<div class="row">
<div class="col-xs-12 col-sm-6"><span class="field"><xsl:value-of select="/output/language/module/executor" />:</span> <xsl:value-of select="executor" /></div>
<div class="col-xs-12 col-sm-6"><span class="field"><xsl:value-of select="/output/language/module/deadline" />:</span> <xsl:value-of select="deadline" /><xsl:if test="deadline_passed='yes'"><img src="/images/warning.png" class="attention" /></xsl:if></div>
</div>
<xsl:if test="../../@edit='yes'">
<div class="btn-group">
<a href="/{/output/page}/task/{../../issue/type}/{@id}" class="btn btn-default btn-sm"><xsl:value-of select="/output/language/module/btn_task_edit" /></a>
</div>
</xsl:if>
</div>
</li>
</xsl:for-each>
</ul>
</div>

<div class="btn-group">
<a href="/issue" class="btn btn-default"><xsl:value-of select="/output/language/global/btn_back" /></a>
</div>
</xsl:template>

<!--
//
//  Edit issue template
//
//-->
<xsl:template match="edit_issue">
<xsl:call-template name="show_messages" />
<form action="/{/output/page}" method="post">
<xsl:if test="issue/@id">
<input type="hidden" name="id" value="{issue/@id}" />
</xsl:if>
<input type="hidden" name="measure_id" value="{issue/measure_id}" />
<input type="hidden" name="type" value="{issue/type}" />

<xsl:if test="measure">
<div class="form-group">
<label><xsl:value-of select="/output/language/module/measure" />:</label>
<p><xsl:value-of select="measure" /></p>
</div>
</xsl:if>
<div class="form-group">
<label for="title"><xsl:value-of select="/output/language/module/title" />:</label>
<input type="text" id="title" name="title" value="{issue/title}" class="form-control" />
</div>
<div class="form-group">
<label for="description"><xsl:value-of select="/output/language/global/description" />:</label>
<textarea id="description" name="description" class="form-control"><xsl:value-of select="issue/description" /></textarea>
</div>
<xsl:if test="issue/@id">
<div class="form-group">
<label for="status"><xsl:value-of select="/output/language/module/status" />:</label>
<select id="status" name="status" class="form-control">
<xsl:for-each select="statuses/status">
<option value="{position()-1}"><xsl:if test="(position()-1)=../../issue/status"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if><xsl:value-of select="." /></option>
</xsl:for-each>
</select>
</div>
</xsl:if>

<div class="btn-group">
<input type="submit" name="submit_button" value="{/output/language/module/btn_issue_save}" class="btn btn-default" />
<xsl:if test="not(issue/@id)">
<a href="/{/output/page}" class="btn btn-default"><xsl:value-of select="/output/language/global/btn_cancel" /></a>
</xsl:if>
<xsl:if test="issue/@id">
<a href="/{/output/page}/{issue/type}/{issue/@id}" class="btn btn-default"><xsl:value-of select="/output/language/global/btn_cancel" /></a>
</xsl:if>
<xsl:if test="issue/@id">
<input type="submit" name="submit_button" value="{/output/language/module/btn_issue_delete}" class="btn btn-default" onClick="javascript:return confirm('{/output/language/global/are_you_sure}')" />
</xsl:if>
</div>
</form>

<xsl:if test="issue/@id">
<div id="help">
<xsl:value-of select="/output/language/module/help_edit" disable-output-escaping="yes" />
<ul>
<xsl:for-each select="statuses/status">
<li><b><xsl:value-of select="." /></b>: <xsl:value-of select="@help" /></li>
</xsl:for-each>
</ul>
</div>
</xsl:if>
</xsl:template>

<!--
//
//  Edit task template
//
//-->
<xsl:template match="edit_task">
<xsl:call-template name="show_messages" />
<form action="/{/output/page}" method="post">
<xsl:if test="task/@id">
<input type="hidden" name="id" value="{task/@id}" />
</xsl:if>
<input type="hidden" name="issue_id" value="{task/issue_id}" />
<input type="hidden" name="type" value="{task/type}" />

<div class="row">
<div class="col-xs-12 col-sm-6">
<xsl:if test="issue">
<div class="form-group">
<label>Issue:</label>
<p><xsl:value-of select="issue" /></p>
</div>
</xsl:if>
<div class="form-group">
<label for="title"><xsl:value-of select="/output/language/module/task" />:</label>
<input type="text" id="title" name="title" value="{task/title}" class="form-control" />
</div>
<div class="form-group">
<label for="executor"><xsl:value-of select="/output/language/module/executor" />:</label>
<input type="text" id="executor" name="executor" value="{task/executor}" class="form-control" />
</div>
<div class="form-group">
<label for="deadline"><xsl:value-of select="/output/language/module/deadline" />:</label>
<input type="text" id="deadline" name="deadline" value="{task/deadline}" class="form-control datepicker" />
</div>
<div class="form-group">
<label for="done"><xsl:value-of select="/output/language/module/done" />:</label>
<input type="checkbox" id="done" name="done"><xsl:if test="task/done='yes'"><xsl:attribute name="checked">checked</xsl:attribute></xsl:if></input>
</div>
</div>
<div class="col-xs-12 col-sm-6">
<div class="form-group">
<label for="description"><xsl:value-of select="/output/language/global/description" />:</label>
<textarea id="description" name="description" class="form-control task"><xsl:value-of select="task/description" /></textarea>
</div>
</div>
</div>

<div class="btn-group">
<input type="submit" name="submit_button" value="{/output/language/module/btn_task_save}" class="btn btn-default" />
<a href="/{/output/page}/{task/type}/{task/issue_id}" class="btn btn-default"><xsl:value-of select="/output/language/global/btn_cancel" /></a>
<xsl:if test="task/@id">
<input type="submit" name="submit_button" value="{/output/language/module/btn_task_delete}" class="btn btn-default" onClick="javascript:return confirm('{/output/language/global/are_you_sure}')" />
</xsl:if>
</div>
</form>
</xsl:template>

<!--
//
//  Content template
//
//-->
<xsl:template match="content">
<h1><xsl:value-of select="/output/language/module/issues" /></h1>
<xsl:apply-templates select="overview" />
<xsl:apply-templates select="view" />
<xsl:apply-templates select="edit_issue" />
<xsl:apply-templates select="edit_task" />
<xsl:apply-templates select="result" />
</xsl:template>

</xsl:stylesheet>
