<?xml version="1.0" ?>
<xsl:stylesheet version="1.1" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="../banshee/main.xslt" />
<!--
//
//  Matrix template
//
//-->
<xsl:template match="matrix">
<xsl:call-template name="show_messages" />
<table class="matrix">
<tbody>
<tr><td colspan="2"></td><td colspan="{values/@size}" class="horizontal"><xsl:value-of select="labels/horizontal/@label" /></td></tr>
<tr><td rowspan="{values/@size + 1}" class="vertical"><div><xsl:value-of select="labels/vertical/@label" /></div></td><td></td>
<xsl:for-each select="labels/horizontal/label">
<td class="mlabel"><xsl:value-of select="." /></td>
</xsl:for-each>
</tr>
<xsl:for-each select="values/row">
<xsl:variable name="vertical" select="position()" />
<tr><td class="mlabel"><xsl:value-of select="../../labels/vertical/label[position()=$vertical]" /></td>
<xsl:for-each select="cell">
<xsl:variable name="level" select="@level" />
<td color="{../../../labels/levels/level[position()=$level]/@color}" class="cell"><span><xsl:value-of select="." /></span></td>
</xsl:for-each>
</tr>
</xsl:for-each>
</tbody>
</table>

<table class="table table-hover risks hidden">
<thead>
<tr><th></th><th><xsl:value-of select="/output/language/module/identifier" /></th><th><xsl:value-of select="/output/language/module/title" /></th></tr>
</thead>
<tbody>
<xsl:for-each select="risks/risk">
<xsl:variable name="level" select="level" />
<xsl:variable name="mitigated" select="mitigated" />
<tr horizontal="{horizontal}" vertical="{vertical}" onClick="javascript:document.location='/risk/{@id}'">
<td><div class="levelbox" style="background-color:{../../labels/levels/level[position()=$level]/@color}" title="Original risk" /><xsl:if test="mitigated!=level"><div class="levelbox" style="background-color:{../../labels/levels/level[position()=$mitigated]/@color}" title="Mitigated risk" /></xsl:if></td>
<td><xsl:value-of select="identifier" /></td>
<td><xsl:value-of select="title" /></td>
</tr>
</xsl:for-each>
</tbody>
</table>

<div class="btn-group">
<a href="/risk/{risk/@id}" class="btn btn-default"><xsl:value-of select="/output/language/global/btn_back" /></a>
</div>

<div id="help">
<xsl:value-of select="/output/language/module/help_text" disable-output-escaping="yes" />
</div>
</xsl:template>
<!--
//
//  Content template
//
//-->
<xsl:template match="content">
<h1><xsl:value-of select="/output/language/module/risk_matrix" /></h1>
<xsl:apply-templates select="matrix" />
<xsl:apply-templates select="result" />
</xsl:template>

</xsl:stylesheet>
