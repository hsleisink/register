<?xml version="1.0" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="../banshee/main.xslt" />

<!--
//
//  Overview template
//
//-->
<xsl:template match="overview">
<h2><xsl:value-of select="risk/title" /></h2>
<div class="row"><div class="col-sm-12 col-md-4">

<div class="panel panel-default">
<div class="panel-heading">
<xsl:if test="@editable='yes'"><div class="right"><a href="/{/output/page}/{risk/@id}/preventive" class="btn btn-primary btn-xs">+</a></div></xsl:if><xsl:value-of select="/output/language/module/preventive_measures" /></div>
<ul class="list-group">
<xsl:for-each select="preventive/measure">
<li class="list-group-item">
<div class="title"><span class="glyphicon glyphicon-chevron-down" /><xsl:if test="done='yes'"><img src="/images/check.png" class="check" /></xsl:if><xsl:value-of select="title" /></div>
<div class="details collapsed">
<p><span class="field"><xsl:value-of select="/output/language/module/cause" />:</span> <xsl:value-of select="cause" /> | <xsl:value-of select="risk_cause" /></p>
<p class="description"><xsl:value-of select="description" /></p>
<p class="owner"><span class="field"><xsl:value-of select="/output/language/module/owner" />:</span> <xsl:value-of select="owner" /></p>
<p class="status"><span class="field"><xsl:value-of select="/output/language/module/status" />:</span> <xsl:value-of select="status" /></p>
<p class="deadline"><span class="field"><xsl:value-of select="/output/language/module/deadline" />:</span> <xsl:value-of select="deadline" /></p>
<p class="issues"><span class="field"><xsl:value-of select="/output/language/module/open_issues" />:</span> <xsl:value-of select="issues" /></p>
<xsl:if test="(../../@measures='yes') or (editable='yes')">
<div class="btn-group">
<xsl:if test="../../@measures='yes'">
<a href="/measure/preventive/{@id}" class="btn btn-default btn-sm"><xsl:value-of select="/output/language/module/btn_measure_view" /></a>
</xsl:if>
<xsl:if test="editable='yes'">
<a href="/{/output/page}/preventive/{@id}" class="btn btn-default btn-sm"><xsl:value-of select="/output/language/module/btn_measure_edit" /></a>
</xsl:if>
</div>
</xsl:if>
</div>
</li>
</xsl:for-each>
</ul>
</div>

</div><div class="col-sm-12 col-md-4">

<div class="panel panel-default">
<div class="panel-heading">
<xsl:if test="@editable='yes'"><div class="right"><a href="/{/output/page}/{risk/@id}/detective" class="btn btn-primary btn-xs">+</a></div></xsl:if><xsl:value-of select="/output/language/module/detective_measures" /></div>
<ul class="list-group">
<xsl:for-each select="detective/measure">
<li class="list-group-item">
<div class="title"><span class="glyphicon glyphicon-chevron-down" /><xsl:if test="done='yes'"><img src="/images/check.png" class="check" /></xsl:if><xsl:value-of select="title" /></div>
<div class="details collapsed">
<p class="description"><xsl:value-of select="description" /></p>
<p class="owner"><span class="field"><xsl:value-of select="/output/language/module/owner" />:</span> <xsl:value-of select="owner" /></p>
<p class="status"><span class="field"><xsl:value-of select="/output/language/module/status" />:</span> <xsl:value-of select="status" /></p>
<p class="deadline"><span class="field"><xsl:value-of select="/output/language/module/deadline" />:</span> <xsl:value-of select="deadline" /></p>
<p class="issues"><span class="field"><xsl:value-of select="/output/language/module/open_issues" />:</span> <xsl:value-of select="issues" /></p>
<xsl:if test="(../../@measures='yes') or (editable='yes')">
<div class="btn-group">
<xsl:if test="../../@measures='yes'">
<a href="/measure/detective/{@id}" class="btn btn-default btn-sm"><xsl:value-of select="/output/language/module/btn_measure_view" /></a>
</xsl:if>
<xsl:if test="editable='yes'">
<a href="/{/output/page}/detective/{@id}" class="btn btn-default btn-sm"><xsl:value-of select="/output/language/module/btn_measure_edit" /></a>
</xsl:if>
</div>
</xsl:if>
</div>
</li>
</xsl:for-each>
</ul>
</div>

</div><div class="col-sm-12 col-md-4">

<div class="panel panel-default">
<div class="panel-heading">
<xsl:if test="@editable='yes'"><div class="right"><a href="/{/output/page}/{risk/@id}/repressive" class="btn btn-primary btn-xs">+</a></div></xsl:if><xsl:value-of select="/output/language/module/repressive_measures" /></div>
<ul class="list-group">
<xsl:for-each select="repressive/measure">
<li class="list-group-item">
<div class="title"><span class="glyphicon glyphicon-chevron-down" /><xsl:if test="done='yes'"><img src="/images/check.png" class="check" /></xsl:if><xsl:value-of select="title" /></div>
<div class="details collapsed">
<p><span class="field"><xsl:value-of select="/output/language/module/effect" />:</span> <xsl:value-of select="effect" /> | <xsl:value-of select="risk_effect" /></p>
<p class="description"><xsl:value-of select="description" /></p>
<p class="owner"><span class="field"><xsl:value-of select="/output/language/module/owner" />:</span> <xsl:value-of select="owner" /></p>
<p class="status"><span class="field"><xsl:value-of select="/output/language/module/status" />:</span> <xsl:value-of select="status" /></p>
<p class="deadline"><span class="field"><xsl:value-of select="/output/language/module/deadline" />:</span> <xsl:value-of select="deadline" /></p>
<p class="issues"><span class="field"><xsl:value-of select="/output/language/module/open_issues" />:</span> <xsl:value-of select="issues" /></p>
<xsl:if test="(../../@measures='yes') or (editable='yes')">
<div class="btn-group">
<xsl:if test="../../@measures='yes'">
<a href="/measure/repressive/{@id}" class="btn btn-default btn-sm"><xsl:value-of select="/output/language/module/btn_measure_view" /></a>
</xsl:if>
<xsl:if test="editable='yes'">
<a href="/{/output/page}/repressive/{@id}" class="btn btn-default btn-sm"><xsl:value-of select="/output/language/module/btn_measure_edit" /></a>
</xsl:if>
</div>
</xsl:if>
</div>
</li>
</xsl:for-each>
</ul>
</div>

</div></div>

<xsl:if test="@back='yes'">
<div class="btn-group">
<a href="/risk/{risk/@id}" class="btn btn-default"><xsl:value-of select="/output/language/global/btn_back" /></a>
</div>
</xsl:if>

<div id="help">
<xsl:value-of select="/output/language/module/help_overview" disable-output-escaping="yes" />
<xsl:value-of select="/output/language/global/help_measure_status" disable-output-escaping="yes" />
</div>
</xsl:template>

<!--
//
//  Controls template
//
//-->
<xsl:template match="controls">
<div class="form-group">
<h2><xsl:value-of select="/output/language/module/controls" /></h2>
<label><xsl:value-of select="/output/language/module/controls_linked_to_measure" />:</label>
<div class="row"><div class="col-xs-12">
<div class="measure_controls"><xsl:for-each select="item">
<span class="control"><xsl:value-of select="." /></span>
</xsl:for-each></div>
</div></div>
</div>
</xsl:template>

<!--
//
//  Standards template
//
//-->
<xsl:template match="standards">
<div class="row control_panel"><div class="col-xs-12 col-sm-6">
<div class="form-group">
<label for="standard"><xsl:value-of select="/output/language/module/standard" />:</label>
<select id="standard" class="form-control standards">
<xsl:for-each select="standard">
<option value="{@id}"><xsl:if test="@id=../@selected"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if><xsl:value-of select="name" /></option>
</xsl:for-each>
</select>
</div>
</div><div class="col-xs-12 col-sm-6">
<div class="form-group">
<label><xsl:value-of select="/output/language/module/control" />:</label>
<xsl:for-each select="standard">
<div class="input-group controls hidden" standard_id="{@id}">
<select standard_id="{@id}" class="form-control controls controls_hidden">
<xsl:for-each select="controls/control">
<option value="{@id}"><xsl:value-of select="number" /> - <xsl:value-of select="title" /></option>
</xsl:for-each>
</select>
<span class="input-group-btn">
<input type="button" value="{/output/language/module/btn_add}" class="btn btn-default add-control" />
</span>
</div>
</xsl:for-each>
</div>
</div></div>
</xsl:template>

<!--
//
//  Measure help template
//
//-->
<xsl:template name="measure_help">
<div id="help">
<xsl:value-of select="/output/language/module/help_edit_1" disable-output-escaping="yes" />
<ul>
<xsl:for-each select="statuses/status">
<li><b><xsl:value-of select="." /></b>: <xsl:value-of select="@help" /></li>
</xsl:for-each>
</ul>
<xsl:value-of select="/output/language/module/help_edit_2" disable-output-escaping="yes" />
</div>
</xsl:template>

<!--
//
//  Edit preventive template
//
//-->
<xsl:template match="edit_preventive">
<xsl:call-template name="show_messages" />
<form action="/{/output/page}" method="post">
<xsl:if test="preventive/@id">
<input type="hidden" name="id" value="{preventive/@id}" />
</xsl:if>
<input type="hidden" name="risk_id" value="{preventive/risk_id}" />

<h2><xsl:value-of select="/output/language/module/preventive_measure" /></h2>
<div class="row"><div class="col-xs-12 col-sm-6">
<div class="form-group">
<label for="owner_id"><xsl:value-of select="/output/language/module/cause" />:</label>
<div class="input-group">
<xsl:if test="not(preventive/@id)">
<select name="risk_cause_id" class="form-control">
<xsl:for-each select="causes/cause">
<option value="{@id}"><xsl:if test="@id=../../preventive/risk_cause_id"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if><xsl:value-of select="." /></option>
</xsl:for-each>
</select>
</xsl:if>
<xsl:if test="preventive/@id">
<input type="text" disabled="disabled" value="{preventive/risk_cause}" cause_id="{preventive/risk_cause_id}" class="form-control" />
</xsl:if>
<span class="input-group-btn"><button class="btn btn-default cause" type="button"><xsl:value-of select="/output/language/global/description" /></button></span>
</div>
</div>
<div class="form-group">
<label for="title"><xsl:value-of select="/output/language/module/title" />:</label>
<input type="text" id="title" name="title" value="{preventive/title}" maxlength="{@maxlength_title}" class="form-control" />
</div>
<div class="form-group">
<label for="owner_id"><xsl:value-of select="/output/language/module/measure_owner" />:</label>
<select name="owner_id" class="form-control">
<xsl:for-each select="owners/owner">
<option value="{@id}"><xsl:if test="@id=../../preventive/owner_id"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if><xsl:value-of select="." /></option>
</xsl:for-each>
</select>
</div>
<div class="form-group">
<label for="deadline"><xsl:value-of select="/output/language/module/deadline" />:</label>
<input type="text" id="deadline" name="deadline" value="{preventive/deadline}" class="form-control datepicker" />
</div>
<div class="form-group">
<label for="status"><xsl:value-of select="/output/language/module/status" />:</label>
<select name="status" class="form-control">
<xsl:for-each select="statuses/status">
<option value="{position()-1}"><xsl:if test="(position()-1)=../../preventive/status"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if><xsl:value-of select="." /></option>
</xsl:for-each>
</select>
</div>
</div><div class="col-xs-12 col-sm-6">
<div class="form-group">
<label for="description"><xsl:value-of select="/output/language/global/description" />:</label>
<textarea id="description" name="description" class="form-control"><xsl:value-of select="preventive/description" /></textarea>
</div>
</div></div>

<xsl:apply-templates select="preventive/controls" />
<xsl:apply-templates select="standards" />

<div class="btn-group right">
<input type="button" class="btn btn-default risk" value="{/output/language/module/risk_description}" />
</div>

<div class="btn-group">
<input type="submit" name="submit_button" value="{/output/language/module/btn_preventive_save}" class="btn btn-default" />
<a href="/{/output/page}/{preventive/risk_id}" class="btn btn-default"><xsl:value-of select="/output/language/global/btn_cancel" /></a>
<xsl:if test="preventive/@id">
<input type="submit" name="submit_button" value="{/output/language/module/btn_preventive_delete}" class="btn btn-default" onClick="javascript:return confirm('{/output/language/global/are_you_sure}')" />
</xsl:if>
</div>
</form>

<div class="risk" title="{risk/title}"><xsl:value-of select="risk/description" /></div>

<xsl:call-template name="measure_help" />
</xsl:template>

<!--
//
//  Edit detective template
//
//-->
<xsl:template match="edit_detective">
<xsl:call-template name="show_messages" />
<form action="/{/output/page}" method="post">
<xsl:if test="detective/@id">
<input type="hidden" name="id" value="{detective/@id}" />
</xsl:if>
<input type="hidden" name="risk_id" value="{detective/risk_id}" />

<h2><xsl:value-of select="/output/language/module/detective_measure" /></h2>
<div class="row"><div class="col-xs-12 col-sm-6">
<div class="form-group">
<label for="title"><xsl:value-of select="/output/language/module/title" />:</label>
<input type="text" id="title" name="title" value="{detective/title}" maxlength="{@maxlength_title}" class="form-control" />
</div>
<div class="form-group">
<label for="owner_id"><xsl:value-of select="/output/language/module/measure_owner" />:</label>
<select name="owner_id" class="form-control">
<xsl:for-each select="owners/owner">
<option value="{@id}"><xsl:if test="@id=../../detective/owner_id"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if><xsl:value-of select="." /></option>
</xsl:for-each>
</select>
</div>
<div class="form-group">
<label for="deadline"><xsl:value-of select="/output/language/module/deadline" />:</label>
<input type="text" id="deadline" name="deadline" value="{detective/deadline}" class="form-control datepicker" />
</div>
<div class="form-group">
<label for="status"><xsl:value-of select="/output/language/module/status" />:</label>
<select name="status" class="form-control">
<xsl:for-each select="statuses/status">
<option value="{position()-1}"><xsl:if test="(position()-1)=../../detective/status"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if><xsl:value-of select="." /></option>
</xsl:for-each>
</select>
</div>
</div><div class="col-xs-12 col-sm-6">
<div class="form-group">
<label for="description"><xsl:value-of select="/output/language/global/description" />:</label>
<textarea id="description" name="description" class="form-control"><xsl:value-of select="detective/description" /></textarea>
</div>
</div></div>

<xsl:apply-templates select="detective/controls" />
<xsl:apply-templates select="standards" />

<div class="btn-group right">
<input type="button" class="btn btn-default risk" value="{/output/language/module/risk_description}" />
</div>

<div class="btn-group">
<input type="submit" name="submit_button" value="{/output/language/module/btn_detective_save}" class="btn btn-default" />
<a href="/{/output/page}/{detective/risk_id}" class="btn btn-default"><xsl:value-of select="/output/language/global/btn_cancel" /></a>
<xsl:if test="detective/@id">
<input type="submit" name="submit_button" value="{/output/language/module/btn_detective_delete}" class="btn btn-default" onClick="javascript:return confirm('{/output/language/global/are_you_sure}')" />
</xsl:if>
</div>
</form>

<div class="risk" title="{risk/title}"><xsl:value-of select="risk/description" /></div>
</xsl:template>

<!--
//
//  Edit repressive template
//
//-->
<xsl:template match="edit_repressive">
<xsl:call-template name="show_messages" />
<form action="/{/output/page}" method="post">
<xsl:if test="repressive/@id">
<input type="hidden" name="id" value="{repressive/@id}" />
</xsl:if>
<input type="hidden" name="risk_id" value="{repressive/risk_id}" />

<h2><xsl:value-of select="/output/language/module/repressive_measure" /></h2>
<div class="row"><div class="col-xs-12 col-sm-6">
<div class="form-group">
<label for="owner_id"><xsl:value-of select="/output/language/module/effect" />:</label>
<div class="input-group">
<xsl:if test="not(repressive/@id)">
<select name="risk_effect_id" class="form-control">
<xsl:for-each select="effects/effect">
<option value="{@id}"><xsl:if test="@id=../../repressive/risk_effect_id"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if><xsl:value-of select="." /></option>
</xsl:for-each>
</select>
</xsl:if>
<xsl:if test="repressive/@id">
<input type="text" disabled="disabled" value="{repressive/risk_effect}" effect_id="{repressive/risk_effect_id}" class="form-control" />
</xsl:if>
<span class="input-group-btn"><button class="btn btn-default effect" type="button"><xsl:value-of select="/output/language/global/description" /></button></span>
</div>
</div>
<div class="form-group">
<label for="title"><xsl:value-of select="/output/language/module/title" />:</label>
<input type="text" id="title" name="title" value="{repressive/title}" maxlength="{@maxlength_title}" class="form-control" />
</div>
<div class="form-group">
<label for="owner_id"><xsl:value-of select="/output/language/module/measure_owner" />:</label>
<select name="owner_id" class="form-control">
<xsl:for-each select="owners/owner">
<option value="{@id}"><xsl:if test="@id=../../repressive/owner_id"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if><xsl:value-of select="." /></option>
</xsl:for-each>
</select>
</div>
<div class="form-group">
<label for="deadline"><xsl:value-of select="/output/language/module/deadline" />:</label>
<input type="text" id="deadline" name="deadline" value="{repressive/deadline}" class="form-control datepicker" />
</div>
<div class="form-group">
<label for="status"><xsl:value-of select="/output/language/module/status" />:</label>
<select name="status" class="form-control">
<xsl:for-each select="statuses/status">
<option value="{position()-1}"><xsl:if test="(position()-1)=../../repressive/status"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if><xsl:value-of select="." /></option>
</xsl:for-each>
</select>
</div>
</div><div class="col-xs-12 col-sm-6">
<div class="form-group">
<label for="description"><xsl:value-of select="/output/language/global/description" />:</label>
<textarea id="description" name="description" class="form-control"><xsl:value-of select="repressive/description" /></textarea>
</div>
</div></div>

<xsl:apply-templates select="repressive/controls" />
<xsl:apply-templates select="standards" />

<div class="btn-group right">
<input type="button" class="btn btn-default risk" value="{/output/language/module/risk_description}" />
</div>

<div class="btn-group">
<input type="submit" name="submit_button" value="{/output/language/module/btn_repressive_save}" class="btn btn-default" />
<a href="/{/output/page}/{repressive/risk_id}" class="btn btn-default"><xsl:value-of select="/output/language/global/btn_cancel" /></a>
<xsl:if test="repressive/@id">
<input type="submit" name="submit_button" value="{/output/language/module/btn_repressive_delete}" class="btn btn-default" onClick="javascript:return confirm('{/output/language/global/are_you_sure}')" />
</xsl:if>
</div>
</form>

<div class="risk" title="{risk/title}"><xsl:value-of select="risk/description" /></div>
</xsl:template>

<!--
//
//  Content template
//
//-->
<xsl:template match="content">
<h1><xsl:value-of select="/output/language/module/measures" /></h1>
<xsl:apply-templates select="overview" />
<xsl:apply-templates select="edit_preventive" />
<xsl:apply-templates select="edit_detective" />
<xsl:apply-templates select="edit_repressive" />
<xsl:apply-templates select="result" />
</xsl:template>

</xsl:stylesheet>
