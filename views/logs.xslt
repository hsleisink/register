<?xml version="1.0" ?>
<xsl:stylesheet version="1.1" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="banshee/main.xslt" />
<xsl:import href="banshee/pagination.xslt" />

<!--
//
//  Overview template
//
//-->
<xsl:template match="overview">
<xsl:call-template name="show_messages" />
<table class="table table-xs logs">
<thead class="table-xs">
<tr>
<th><xsl:value-of select="/output/language/module/identifier" /></th>
<th><xsl:value-of select="/output/language/module/timestamp" /></th>
<th><xsl:value-of select="/output/language/module/by_user" /></th>
<th><xsl:value-of select="/output/language/module/event" /></th>
</tr>
</thead>
<tbody>
<xsl:for-each select="logs/log">
<tr>
<td><span class="table-xs"><xsl:value-of select="/output/language/module/identifier" /></span><a href="/risk/{risk_id}"><xsl:value-of select="identifier" /></a></td>
<td><span class="table-xs"><xsl:value-of select="/output/language/module/timestamp" /></span><xsl:value-of select="timestamp" /></td>
<td><span class="table-xs"><xsl:value-of select="/output/language/module/by_user" /></span><xsl:value-of select="fullname" /></td>
<td><xsl:value-of select="event" /></td>
</tr>
</xsl:for-each>
</tbody>
</table>

<xsl:choose>
<xsl:when test="pagination">
<div class="right">
<xsl:apply-templates select="pagination" />
</div>

<form action="/{/output/page}" method="post">
<input type="hidden" name="submit_button" value="filter" />
<div class="input-group filter">
<input type="text" name="identifier" placeholder="{/output/language/module/filter_by_risk_identifier}" class="form-control" />
<span class="input-group-btn">
<input type="submit" name="submit_button" value="{/output/language/module/btn_search}" class="btn btn-default" />
</span>
</div>
</form>
</xsl:when>
<xsl:otherwise>
<div class="btn-group">
<a href="/{/output/page}" class="btn btn-default"><xsl:value-of select="/output/language/global/btn_back" /></a>
</div>
</xsl:otherwise>
</xsl:choose>
</xsl:template>

<!--
//
//  Content template
//
//-->
<xsl:template match="content">
<h1>Logs</h1>
<xsl:apply-templates select="overview" />
<xsl:apply-templates select="result" />
</xsl:template>

</xsl:stylesheet>
