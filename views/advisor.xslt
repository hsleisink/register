<?xml version="1.0" ?>
<xsl:stylesheet version="1.1" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="banshee/main.xslt" />

<!--
//
//  Overview template
//
//-->
<xsl:template match="overview">
<xsl:call-template name="show_messages" />

<table class="table table-condensed">
<thead>
<tr>
<th><xsl:value-of select="/output/language/module/organisation" /></th>
<th><xsl:value-of select="/output/language/module/status" /></th>
<th></th></tr>
</thead>
<tbody>
<xsl:for-each select="role">
<tr>
<td><xsl:value-of select="." /></td>
<td><xsl:if test="@ready='no'"><xsl:value-of select="/output/language/module/requested" /></xsl:if><xsl:if test="@ready='yes'"><form action="/{/output/page}" method="post"><input type="hidden" name="id" value="{@id}" /><input type="submit" name="submit_button" value="{/output/language/module/activate}" class="btn btn-primary btn-xs" /></form></xsl:if><xsl:if test="@ready='active'"><form action="/{/output/page}" method="post"><input type="submit" name="submit_button" value="{/output/language/module/deactivate}" class="btn btn-info btn-xs" /></form></xsl:if></td>
<td><form action="/{/output/page}" method="post"><input type="hidden" name="id" value="{@id}" /><input type="submit" name="submit_button" value="{/output/language/module/remove}" class="btn btn-danger btn-xs" onClick="javascript:return confirm('{/output/language/global/are_you_sure}')" /></form></td>
</tr>
</xsl:for-each>
</tbody>
</table>

<h2><xsl:value-of select="/output/language/module/request_advisor_role" /></h2>
<p><xsl:value-of select="/output/language/module/request_explained" /></p>
<form action="/{/output/page}" method="post" class="request">
<div class="input-group">
<input type="text" id="user" name="user" placeholder="{/output/language/module/username_or_email}" class="form-control datepicker" />
<span class="input-group-btn"><input type="submit" name="submit_button" value="{/output/language/module/request}" class="btn btn-default" /></span>
</div>
</form>

<div id="help">
<p><xsl:value-of select="/output/language/module/help_text" /></p>
</div>
</xsl:template>

<!--
//
//  Content template
//
//-->
<xsl:template match="content">
<img src="/images/icons/advisor.png" class="title_icon" />
<h1>Advisor role</h1>
<xsl:apply-templates select="overview" />
<xsl:apply-templates select="result" />
</xsl:template>

</xsl:stylesheet>
