<?xml version="1.0" ?>
<!--
//
//  Copyright (c) by Hugo Leisink <hugo@leisink.net>
//  This file is part of the Banshee PHP framework
//  https://gitlab.com/hsleisink/banshee/
//
//  Licensed under The MIT License
//
//-->
<xsl:stylesheet version="1.1" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="../banshee/main.xslt" />

<!--
//
//  Month template
//
//-->
<xsl:template match="month">
<div class="row">
<div class="col-sm-4"><h2><xsl:value-of select="@title" /></h2></div>
<div class="col-sm-8"><div class="btn-group btn-responsive">
	<a href="/{/output/page}/{prev}" class="btn btn-xs btn-primary"><xsl:value-of select="/output/language/module/previous_month" /></a>
	<a href="/{/output/page}/current" class="btn btn-xs btn-primary"><xsl:value-of select="/output/language/module/current_month" /></a>
	<a href="/{/output/page}/{next}" class="btn btn-xs btn-primary"><xsl:value-of select="/output/language/module/next_month" /></a>
</div></div>
</div>

<table class="month" cellspacing="0">
<thead>
<tr>
<xsl:for-each select="days_of_week/day">
<th><xsl:value-of select="." /></th>
</xsl:for-each>
</tr>
</thead>
<tbody>
<xsl:for-each select="week">
	<tr class="week">
	<xsl:for-each select="day">
		<td class="day dow{@dow}{@today}">
			<div class="nr"><xsl:value-of select="@nr" /></div>
			<xsl:for-each select="task">
				<div class="task"><a href="/issue/{@type}/{@id}"><xsl:value-of select="." /></a></div>
			</xsl:for-each>
		</td>
	</xsl:for-each>
	</tr>
</xsl:for-each>
</tbody>
</table>

<div class="btn-group left">
<a href="/issue" class="btn btn-default"><xsl:value-of select="/output/language/global/btn_back" /></a>
</div>

<div id="help">
<p><xsl:value-of select="/output/language/module/help_text" /></p>
</div>
</xsl:template>

<!--
//
//  Content template
//
//-->
<xsl:template match="content">
<h1><xsl:value-of select="/output/language/module/task_deadline_calender" /></h1>
<xsl:apply-templates select="month" />
<xsl:apply-templates select="result" />
</xsl:template>

</xsl:stylesheet>
