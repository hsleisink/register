
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="banshee/main.xslt" />
<xsl:import href="banshee/pagination.xslt" />

<!--
//
//  Overview template
//
//-->
<xsl:template match="overview">
<table class="table table-hover table-xs risks">
<thead class="table-xs">
<tr>
<th></th>
<th><xsl:value-of select="/output/language/module/identifier" /></th>
<th><xsl:value-of select="/output/language/module/risk" /></th>
<th><xsl:value-of select="/output/language/module/owner" /></th>
<th><xsl:value-of select="/output/language/module/created" /></th>
<th><xsl:value-of select="/output/language/module/level" /></th>
<th><xsl:value-of select="/output/language/module/approach" /></th>
<th><xsl:value-of select="/output/language/module/measures" /></th>
</tr>
</thead>
<tbody>
<xsl:for-each select="risks/risk">
<xsl:variable name="level" select="level" />
<xsl:variable name="mitigated" select="mitigated" />
<tr onClick="javascript:document.location='/{/output/page}/{@id}'">
<td><div class="levelbox" style="background-color:{../../levels/level[position()=$level]/@color}" title="Original risk" /><xsl:if test="mitigated!=level"><div class="levelbox" style="background-color:{../../levels/level[position()=$mitigated]/@color}" title="Mitigated risk" /></xsl:if></td>
<td><span class="table-xs"><xsl:value-of select="/output/language/module/identifier" /></span><xsl:value-of select="identifier" /></td>
<td><span class="table-xs"><xsl:value-of select="/output/language/module/risk" /></span><xsl:value-of select="title" /></td>
<td><span class="table-xs"><xsl:value-of select="/output/language/module/owner" /></span><xsl:value-of select="owner" /></td>
<td><span class="table-xs"><xsl:value-of select="/output/language/module/created" /></span><xsl:value-of select="created" /></td>
<td><span class="table-xs"><xsl:value-of select="/output/language/module/level" /></span><xsl:value-of select="../../levels/level[position()=$level]" /> &gt; <xsl:value-of select="../../levels/level[position()=$mitigated]" /></td>
<td><span class="table-xs"><xsl:value-of select="/output/language/module/approach" /></span><xsl:value-of select="approach_text" /></td>
<td><span class="table-xs"><xsl:value-of select="/output/language/module/measures" /></span><xsl:value-of select="measures_done" />&#160;<xsl:value-of select="/output/language/global/of" />&#160;<xsl:value-of select="measure_count" /></td>
</tr>
</xsl:for-each>
</tbody>
<xsl:if test="@subpage='yes'">
<tfoot>
<tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
<td><xsl:value-of select="/output/language/global/total" />: <xsl:value-of select="count(risks/risk)" /></td></tr>
</tfoot>
</xsl:if>
</table>

<div class="right">
<xsl:apply-templates select="pagination" />
</div>

<div class="btn-group left">
<xsl:if test="@subpage='no'">
<xsl:if test="@employee='yes'">
<a href="/{/output/page}/new" class="btn btn-default"><xsl:value-of select="/output/language/module/btn_new_risk" /></a>
</xsl:if>
<a href="/{/output/page}/attention" class="btn btn-default"><xsl:value-of select="/output/language/module/btn_attention" /></a>
<a href="/{/output/page}/filter" class="btn btn-default"><xsl:value-of select="/output/language/module/btn_filter" /></a>
<xsl:if test="@matrix='yes'">
<a href="/{/output/page}/matrix" class="btn btn-default"><xsl:value-of select="/output/language/module/btn_show_matrix" /></a>
</xsl:if>
</xsl:if>
<xsl:if test="@subpage='yes'">
<a href="/{/output/page}" class="btn btn-default"><xsl:value-of select="/output/language/global/btn_back" /></a>
</xsl:if>
</div>

<xsl:if test="@subpage='no'">
<div class="btn-group search">
<form action="/{/output/page}" method="post">
<div class="input-group">
<input type="text" name="query" placeholder="{../search_placeholder}" class="form-control" />
<span class="input-group-btn">
<input type="submit" name="submit_button" value="{/output/language/module/btn_search}" class="btn btn-default" />
</span>
</div>
</form>
</div>
</xsl:if>

<div id="help">
<xsl:if test="@subpage='no'">
<xsl:value-of select="/output/language/module/help_overview_1" disable-output-escaping="yes" />
</xsl:if>
<xsl:if test="@subpage='yes'">
<xsl:value-of select="/output/language/module/help_overview_2" disable-output-escaping="yes" />
</xsl:if>
<xsl:value-of select="/output/language/module/help_overview_criteria" disable-output-escaping="yes" />
<xsl:if test="@subpage='no'">
<xsl:value-of select="/output/language/module/help_overview_3" disable-output-escaping="yes" />
</xsl:if>
</div>
</xsl:template>

<!--
//
//  Filter template
//
//-->
<xsl:template match="filter">
<form action="/{/output/page}" method="post">
<h2><xsl:value-of select="/output/language/module/filter_risk_level" /></h2>
<select name="level" class="form-control level">
<option value="0">-</option>
<xsl:for-each select="levels/level">
<option value="{position()}" color="{@color}"><xsl:if test="position()=../../level"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if><xsl:value-of select="." /></option>
</xsl:for-each>
</select>

<xsl:if test="tags">
<h2><xsl:value-of select="/output/language/module/filter_tags" /></h2>
<xsl:for-each select="tags/category">
<div class="tag_category"><div class="title"><xsl:value-of select="@title" />:</div>
<xsl:for-each select="tag">
<span class="tag"><input type="checkbox" name="tags[]" value="{@id}"><xsl:if test="@id=../../set"><xsl:attribute name="checked">checked</xsl:attribute></xsl:if></input><xsl:value-of select="." /></span>
</xsl:for-each>
</div>
</xsl:for-each>
</xsl:if>

<xsl:if test="owners">
<h2><xsl:value-of select="/output/language/module/owner" /></h2>
<select name="owner" class="form-control owner">
<option value="0">-</option>
<xsl:for-each select="owners/owner">
<option value="{@id}"><xsl:if test="@id=../../owner"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if><xsl:value-of select="." /></option>
</xsl:for-each>
</select>
</xsl:if>

<div class="btn-group">
<input type="submit" name="submit_button" value="{/output/language/module/filter_set_filter}" class="btn btn-default" />
<button class="btn btn-default"><xsl:value-of select="/output/language/module/filter_reset_filter" /></button>
<a href="/{/output/page}" class="btn btn-default"><xsl:value-of select="/output/language/global/btn_cancel" /></a>
</div>
</form>
</xsl:template>

<!--
//
//  View template
//
//-->
<xsl:template match="view">
<xsl:call-template name="show_messages" />
<div class="panel panel-primary">
<div class="panel-heading"><div class="identifier"><xsl:value-of select="risk/identifier" /></div><xsl:value-of select="risk/title" /></div>
<div class="panel-body">
<xsl:if test="risk/description!=''">
<p class="description"><xsl:value-of select="risk/description" /></p>
<hr />
</xsl:if>
<div class="row"><div class="col-md-12 col-lg-4 details">
<p><span class="field"><xsl:value-of select="/output/language/module/owner" />:</span> <xsl:value-of select="risk/owner" /><xsl:if test="risk/owner='-'"><img src="/images/warning.png" class="attention" /></xsl:if></p>
<p><span class="field"><xsl:value-of select="vertical/@label" />:</span> <xsl:value-of select="vertical/label[position()=../../risk/vertical]" /></p>
<p><span class="field"><xsl:value-of select="horizontal/@label" />:</span> <xsl:value-of select="horizontal/label[position()=../../risk/horizontal]" /></p>
<p><span class="field"><xsl:value-of select="/output/language/module/initial_risk_level" />:</span> <xsl:value-of select="levels/level[position()=../../risk/level]" /></p>
<xsl:if test="risk/mitigated!=risk/level">
<p><span class="field"><xsl:value-of select="/output/language/module/risk_mitigated_to" />:</span> <span class="mitigated" color="{levels/level[position()=../../risk/mitigated]/@color}"><xsl:value-of select="levels/level[position()=../../risk/mitigated]" /></span></p>
</xsl:if>
<p><span class="field"><xsl:value-of select="/output/language/module/approach" />:</span> <xsl:value-of select="risk/approach_text" /><xsl:if test="risk/approach=0"><img src="/images/warning.png" class="attention" /></xsl:if></p>
<xsl:if test="risk/acceptor_id!=''">
<p><span class="field"><xsl:value-of select="/output/language/module/acceptance_approver" />:</span> <xsl:value-of select="risk/acceptor" /></p>
<p><span class="field"><xsl:value-of select="/output/language/module/acceptance_approval_ends_at" />:</span> <xsl:value-of select="risk/accept_end_date" /><xsl:if test="risk/accept_end_date_passed='yes'"><img src="/images/warning.png" class="attention" /></xsl:if></p>
</xsl:if>
<p><span class="field"><xsl:value-of select="/output/language/module/creator" />:</span> <xsl:value-of select="risk/creator" /></p>
<p><span class="field"><xsl:value-of select="/output/language/module/created_at" />:</span> <xsl:value-of select="risk/created" /></p>
<p><span class="field"><xsl:value-of select="/output/language/module/measures_assigned" />:</span> <xsl:value-of select="risk/measure_count" /><xsl:if test="((risk/measure_count=0) and (risk/approach>1)) or ((risk/measure_count>0) and (risk/approach=1))"><img src="/images/warning.png" class="attention" /></xsl:if></p>
<p><span class="field"><xsl:value-of select="/output/language/module/measures_done" />:</span> <xsl:value-of select="risk/measures_done" /><xsl:if test="risk/measures_done!=risk/measure_count"><img src="/images/warning.png" class="attention" /></xsl:if></p>
<p><span class="field"><xsl:value-of select="/output/language/module/open_issues" />:</span> <xsl:value-of select="risk/issues" /><xsl:if test="risk/issues>0"><img src="/images/warning.png" class="attention" /></xsl:if></p>
</div><div class="col-md-12 col-lg-8">

<table class="matrix">
<tbody>
<tr><td colspan="2"></td><td colspan="{matrix/@size}" class="horizontal"><xsl:value-of select="horizontal/@label" /></td></tr>
<tr><td rowspan="{matrix/@size + 1}" class="vertical"><div><xsl:value-of select="vertical/@label" /></div></td><td></td>
<xsl:for-each select="horizontal/label">
<td class=""><xsl:value-of select="." /></td>
</xsl:for-each>
</tr>
<xsl:for-each select="matrix/row">
<xsl:variable name="vertical" select="count(../row) - position() + 1" />
<tr><td><xsl:value-of select="../../vertical/label[position()=$vertical]" /></td>
<xsl:for-each select="cell"><xsl:variable name="level" select="." /><td color="{../../../levels/level[position()=$level]/@color}" class="cell"><xsl:if test="(position()=../../../risk/horizontal) and ($vertical=../../../risk/vertical)"><xsl:attribute name="marked">marked</xsl:attribute><span>&#9673;</span></xsl:if></td></xsl:for-each>
</tr>
</xsl:for-each>
</tbody>
</table>

</div></div>

<xsl:if test="risk/accept_procedure='yes'">
<form action="/{/output/page}" method="post" class="acceptance">
<input type="hidden" name="risk_id" value="{risk/@id}" />
<div class="input-group">
<select name="period" class="form-control">
<option value="12">12 <xsl:value-of select="/output/language/module/months" /></option>
<option value="11">11 <xsl:value-of select="/output/language/module/months" /></option>
<option value="10">10 <xsl:value-of select="/output/language/module/months" /></option>
<option value="9">9 <xsl:value-of select="/output/language/module/months" /></option>
<option value="8">8 <xsl:value-of select="/output/language/module/months" /></option>
<option value="7">7 <xsl:value-of select="/output/language/module/months" /></option>
<option value="6">6 <xsl:value-of select="/output/language/module/months" /></option>
<option value="5">5 <xsl:value-of select="/output/language/module/months" /></option>
<option value="4">4 <xsl:value-of select="/output/language/module/months" /></option>
<option value="3">3 <xsl:value-of select="/output/language/module/months" /></option>
<option value="2">2 <xsl:value-of select="/output/language/module/months" /></option>
<option value="1">1 <xsl:value-of select="/output/language/module/month" /></option>
</select>
<span class="input-group-btn">
<input type="submit" name="submit_button" value="{/output/language/module/btn_approve_acceptance}" class="btn btn-success" onClick="javascript:return confirm('{/output/language/global/are_you_sure}')" />
<input type="submit" name="submit_button" value="{/output/language/module/btn_deny_acceptance}" class="btn btn-danger" onClick="javascript:return confirm('{/output/language/global/are_you_sure}')" />
</span>
</div>
</form>
</xsl:if>

<xsl:for-each select="tags/category">
<div class="tag_category">
<div class="title"><xsl:value-of select="@title" />:</div>
<xsl:for-each select="tag">
<div class="label label-primary"><xsl:value-of select="." /></div>
</xsl:for-each>
</div>
</xsl:for-each>

<xsl:if test="risk/@editable='yes'">
<div class="btn-group">
<a href="/{/output/page}/{risk/@id}/edit" class="btn btn-default btn-sm"><xsl:value-of select="/output/language/module/btn_edit_risk" /></a>
<a href="/{/output/page}/{risk/@id}/tags" class="btn btn-default btn-sm"><xsl:value-of select="/output/language/module/btn_assign_tags" /></a>
<a href="/{/output/page}/{risk/@id}/owner" class="btn btn-default btn-sm"><xsl:value-of select="/output/language/module/btn_assign_to_owner" /></a>
</div>
</xsl:if>

</div>
</div>

<div class="row"><div class="col-sm-12 col-md-6">
<div class="panel panel-default">
<div class="panel-heading">
<xsl:if test="risk/@editable='yes'"><div class="right"><a href="/{/output/page}/{risk/@id}/cause" class="btn btn-primary btn-xs">+</a></div></xsl:if><xsl:value-of select="/output/language/module/causes" /><xsl:if test="count(causes/cause)=0"><img src="/images/warning.png" class="attention" /></xsl:if></div>
<ul class="list-group">
<xsl:for-each select="causes/cause">
<li class="list-group-item">
<div class="title"><span class="glyphicon glyphicon-chevron-down" /><span><xsl:value-of select="cause" /></span><xsl:value-of select="title" /></div>
<div class="description collapsed">
<div><xsl:value-of select="description" /></div>
<xsl:if test="../../risk/@editable='yes'">
<div class="btn-group"><a href="/{/output/page}/cause/{@id}" class="btn btn-default btn-sm"><xsl:value-of select="/output/language/module/btn_edit_cause" /></a></div>
</xsl:if>
</div>
</li>
</xsl:for-each>
</ul>
</div>
</div><div class="col-sm-12 col-md-6">
<div class="panel panel-default">
<div class="panel-heading"><xsl:if test="risk/@editable='yes'"><div class="right"><a href="/{/output/page}/{risk/@id}/effect" class="btn btn-primary btn-xs">+</a></div></xsl:if><xsl:value-of select="/output/language/module/effects" /><xsl:if test="count(effects/effect)=0"><img src="/images/warning.png" class="attention" /></xsl:if></div>
<ul class="list-group">
<xsl:for-each select="effects/effect">
<li class="list-group-item">
<div class="title"><span class="glyphicon glyphicon-chevron-down" /><span><xsl:value-of select="effect" /></span><xsl:value-of select="title" /></div>
<div class="description collapsed">
<div><xsl:value-of select="description" /></div>
<xsl:if test="../../risk/@editable='yes'">
<div class="btn-group"><a href="/{/output/page}/effect/{@id}" class="btn btn-default btn-sm"><xsl:value-of select="/output/language/module/btn_edit_effect" /></a></div>
</xsl:if>
</div>
</li>
</xsl:for-each>
</ul>
</div>
</div></div>

<div class="btn-group">
<xsl:if test="@measures='yes'">
<a href="/{/output/page}/measure/{risk/@id}" class="btn btn-default"><xsl:value-of select="/output/language/module/btn_measures" /></a>
</xsl:if>
<xsl:if test="@log_access='yes'">
<a href="/logs/{risk/@id}" class="btn btn-default"><xsl:value-of select="/output/language/module/btn_risk_logs" /></a>
</xsl:if>
<a href="/risk" class="btn btn-default"><xsl:value-of select="/output/language/global/btn_back" /></a>
</div>
</xsl:template>

<!--
//
//  Edit risk template
//
//-->
<xsl:template match="edit_risk">
<xsl:call-template name="show_messages" />
<form action="/{/output/page}" method="post">
<xsl:if test="risk/@id">
<input type="hidden" name="id" value="{risk/@id}" />
</xsl:if>

<div class="row noprint"><div class="col-xs-12 col-sm-6">
<div class="form-group">
<label for="title"><xsl:value-of select="/output/language/module/title" />:</label>
<input type="text" id="title" name="title" value="{risk/title}" maxlength="{@maxlength_title}" class="form-control" />
</div>
<div class="form-group">
<label for="identifier"><xsl:value-of select="/output/language/module/identifier" />:</label>
<div class="identifier-input">
<input type="text" id="identifier" name="identifier" value="{risk/identifier}" maxlength="{@maxlength_identifier}" class="form-control" />
</div>
</div>
<div class="form-group">
<label for="vertical"><xsl:value-of select="vertical/@label" />:</label>
<select id="vertical" name="vertical" class="form-control">
<xsl:for-each select="vertical/label">
<option value="{position()}"><xsl:if test="position()=../../risk/vertical"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if><xsl:value-of select="." /></option>
</xsl:for-each>
</select>
</div>
<div class="form-group">
<label for="horizontal"><xsl:value-of select="horizontal/@label" />:</label>
<select id="horizontal" name="horizontal" class="form-control">
<xsl:for-each select="horizontal/label">
<option value="{position()}"><xsl:if test="position()=../../risk/horizontal"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if><xsl:value-of select="." /></option>
</xsl:for-each>
</select>
</div>
<div class="form-group">
<label for="approach"><xsl:value-of select="/output/language/module/approach" />:</label>
<select id="approach" name="approach" class="form-control">
<xsl:for-each select="approaches/approach">
<option value="{position()-1}"><xsl:if test="(position()-1)=../../risk/approach"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if><xsl:value-of select="." /></option>
</xsl:for-each>
</select>
</div>
<xsl:if test="risk/@id and mitigated">
<div class="form-group">
<label for="levels_mitigated"><xsl:value-of select="/output/language/module/risk_mitigated_to" />:</label>
<select id="levels_mitigated" name="levels_mitigated" class="form-control">
<xsl:for-each select="mitigated/level">
<option value="{@level}" color="{@color}"><xsl:if test="@level=../../risk/levels_mitigated"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if><xsl:value-of select="." /></option>
</xsl:for-each>
</select>
</div>
</xsl:if>
</div><div class="col-xs-12 col-sm-6">
<div class="form-group">
<label for="description"><xsl:value-of select="/output/language/global/description" />:</label>
<textarea id="description" name="description" class="form-control risk"><xsl:value-of select="risk/description" /></textarea>
</div>
</div></div>

<div class="btn-group right">
<input type="button" value="{/output/language/module/show_reference_table}" class="btn btn-default" onClick="javascript:$('div.references').toggleClass('collapsed'); return false;" />
</div>

<div class="btn-group">
<input type="submit" name="submit_button" value="{/output/language/module/btn_save_risk}" class="btn btn-default"><xsl:if test="risk/accept_end_date!='0000-00-00'"><xsl:attribute name="onClick">javascript:return confirm('<xsl:value-of select="/output/language/global/are_you_sure" />')</xsl:attribute></xsl:if></input>
<xsl:if test="not(risk/@id)">
<a href="/{/output/page}" class="btn btn-default"><xsl:value-of select="/output/language/global/btn_cancel" /></a>
</xsl:if>
<xsl:if test="risk/@id">
<a href="/{/output/page}/{risk/@id}" class="btn btn-default"><xsl:value-of select="/output/language/global/btn_cancel" /></a>
</xsl:if>
<xsl:if test="(@deletable='yes') and risk/@id">
<input type="submit" name="submit_button" value="{/output/language/module/btn_delete_risk}" class="btn btn-danger" onClick="javascript:return confirm('{/output/language/global/are_you_sure}')" />
</xsl:if>
</div>
</form>

<div class="references collapsed">
<h2><xsl:value-of select="/output/language/module/impact_reference_table" /></h2>
<table class="table table-condensed table-striped table-xs">
<thead class="table-xs">
<tr>
<th></th><th colspan="{count(impact/level)}" class="impact"><xsl:value-of select="impact/@text" /></th>
</tr>
<tr>
<th><xsl:value-of select="/output/language/module/effect" /></th>
<xsl:for-each select="impact/level">
<th><xsl:value-of select="." /></th>
</xsl:for-each>
</tr>
</thead>
<tbody>
<xsl:for-each select="references/reference">
<tr>
<td><span class="table-xs"><xsl:value-of select="/output/language/module/effect" /></span><xsl:value-of select="effect" /></td>
<xsl:for-each select="impact">
<xsl:variable name="level" select="position()" />
<td><span class="table-xs"><xsl:value-of select="../../../impact/@text" />&#160;<xsl:value-of select="../../../impact/level[position()=$level]" /></span><xsl:value-of select="." /></td>
</xsl:for-each>
</tr>
</xsl:for-each>
</tbody>
</table>
</div>

<ul class="identifiers">
<xsl:for-each select="identifiers/identifier">
<li><xsl:value-of select="." /></li>
</xsl:for-each>
</ul>

<div id="help">
<xsl:value-of select="/output/language/module/help_edit_1" disable-output-escaping="yes" />
<ul>
<xsl:for-each select="approaches/approach">
<li><span class="field"><xsl:value-of select="." />:</span> <xsl:value-of select="@description" /></li>
</xsl:for-each>
</ul>
<xsl:value-of select="/output/language/module/help_edit_2" disable-output-escaping="yes" />
<xsl:if test="risk/@id">
<xsl:value-of select="/output/language/module/help_edit_3" disable-output-escaping="yes" />
</xsl:if>
</div>
</xsl:template>

<!--
//
//  Assign tags template
//
//-->
<xsl:template match="assign_tags">
<xsl:call-template name="show_messages" />
<form action="/{/output/page}" method="post">
<input type="hidden" name="risk_id" value="{risk_id}" />

<div class="row">
<xsl:for-each select="categories/category">
<div class="col-xs-12 col-sm-6 col-md-4">
<div class="panel panel-default tags">
<div class="panel-heading"><xsl:value-of select="@title" /></div>
<div class="panel-body"><xsl:for-each select="tag">
<div><input type="checkbox" name="tags[]" value="{@id}"><xsl:if test="@id=../../../tags/tag"><xsl:attribute name="checked">checked</xsl:attribute></xsl:if></input><xsl:value-of select="." /></div>
</xsl:for-each></div>
</div>
</div>
</xsl:for-each>
</div>

<div class="btn-group">
<input type="submit" name="submit_button" value="{/output/language/module/btn_assign_tags}" class="btn btn-default" />
<a href="/{/output/page}/{risk_id}" class="btn btn-default"><xsl:value-of select="/output/language/global/btn_cancel" /></a>
</div>
</form>
</xsl:template>

<!--
//
//  Set acceptor template
//
//-->
<xsl:template match="set_acceptor">
<xsl:call-template name="show_messages" />
<form action="/{/output/page}" method="post">
<input type="hidden" name="risk_id" value="{risk_id}" />

<label for="acceptor_id"><xsl:value-of select="/output/language/module/risk_acceptance_approver" />:</label>
<select id="acceptor_id" name="acceptor_id" class="form-control">
<xsl:for-each select="acceptors/acceptor">
<option value="{@id}"><xsl:if test="@id=../../acceptor_id"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if><xsl:value-of select="." /></option>
</xsl:for-each>
</select>

<div class="btn-group">
<input type="submit" name="submit_button" value="{/output/language/module/btn_set_acceptance_approver}" class="btn btn-default" />
<a href="/{/output/page}/{risk_id}" class="btn btn-default"><xsl:value-of select="/output/language/global/btn_cancel" /></a>
</div>
</form>
</xsl:template>

<!--
//
//  Set owner template
//
//-->
<xsl:template match="set_owner">
<xsl:call-template name="show_messages" />
<form action="/{/output/page}" method="post">
<input type="hidden" name="risk_id" value="{risk_id}" />

<div class="form-group">
<label for="owner_id"><xsl:value-of select="/output/language/module/risk_owner" />:</label>
<select id="owner_id" name="owner_id" class="form-control">
<xsl:for-each select="owners/owner">
<option value="{@id}"><xsl:if test="@id=../../owner_id"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if><xsl:value-of select="." /></option>
</xsl:for-each>
</select>
</div>

<div class="btn-group">
<input type="submit" name="submit_button" value="{/output/language/module/btn_set_owner}" class="btn btn-default" />
<a href="/{/output/page}/{risk_id}" class="btn btn-default"><xsl:value-of select="/output/language/global/btn_cancel" /></a>
</div>
</form>
</xsl:template>

<!--
//
//  Edit cause template
//
//-->
<xsl:template match="edit_cause">
<h2><xsl:value-of select="/output/language/module/cause" /></h2>
<xsl:call-template name="show_messages" />
<form action="/{/output/page}" method="post">
<xsl:if test="cause/@id">
<input type="hidden" name="id" value="{cause/@id}" />
</xsl:if>
<input type="hidden" name="risk_id" value="{cause/risk_id}" />
<div class="form-group">
<label for="cause"><xsl:value-of select="/output/language/module/category" />:</label>
<select id="cause" name="cause_id" class="form-control">
<xsl:for-each select="causes/cause">
<option value="{@id}"><xsl:if test="@id=../../cause/cause_id"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if><xsl:value-of select="." /></option>
</xsl:for-each>
</select>
</div>
<div class="form-group">
<label for="title"><xsl:value-of select="/output/language/module/title" />:</label>
<input type="text" id="title" name="title" value="{cause/title}" maxlength="{@maxlength_title}" class="form-control" />
</div>
<div class="form-group">
<label for="description"><xsl:value-of select="/output/language/global/description" />:</label>
<textarea id="description" name="description" class="form-control cause"><xsl:value-of select="cause/description" /></textarea>
</div>

<div class="btn-group">
<input type="submit" name="submit_button" value="{/output/language/module/btn_save_cause}" class="btn btn-default" />
<xsl:if test="cause/@id">
<input type="submit" name="submit_button" value="{/output/language/module/btn_delete_cause}" class="btn btn-default" onClick="javascript:return confirm('{/output/language/global/are_you_sure}')" />
</xsl:if>
<a href="/{/output/page}/{cause/risk_id}" class="btn btn-default"><xsl:value-of select="/output/language/global/btn_cancel" /></a>
</div>
</form>
</xsl:template>

<!--
//
//  Edit effect template
//
//-->
<xsl:template match="edit_effect">
<h2><xsl:value-of select="/output/language/module/effect" /></h2>
<xsl:call-template name="show_messages" />
<form action="/{/output/page}" method="post">
<xsl:if test="effect/@id">
<input type="hidden" name="id" value="{effect/@id}" />
</xsl:if>
<input type="hidden" name="risk_id" value="{effect/risk_id}" />
<div class="form-group">
<label for="effect"><xsl:value-of select="/output/language/module/category" />:</label>
<select id="effect" name="effect_id" class="form-control">
<xsl:for-each select="effects/effect">
<option value="{@id}"><xsl:if test="@id=../../effect/effect_id"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if><xsl:value-of select="." /></option>
</xsl:for-each>
</select>
</div>
<div class="form-group">
<label for="title"><xsl:value-of select="/output/language/module/title" />:</label>
<input type="text" id="title" name="title" value="{effect/title}" maxlength="{@maxlength_title}" class="form-control" />
</div>
<div class="form-group">
<label for="description"><xsl:value-of select="/output/language/global/description" />:</label>
<textarea id="description" name="description" class="form-control effect"><xsl:value-of select="effect/description" /></textarea>
</div>

<div class="btn-group">
<input type="submit" name="submit_button" value="{/output/language/module/btn_save_effect}" class="btn btn-default" />
<xsl:if test="effect/@id">
<input type="submit" name="submit_button" value="{/output/language/module/btn_delete_effect}" class="btn btn-default" onClick="javascript:return confirm('{/output/language/global/are_you_sure}')" />
</xsl:if>
<a href="/{/output/page}/{effect/risk_id}" class="btn btn-default"><xsl:value-of select="/output/language/global/btn_cancel" /></a>
</div>
</form>
</xsl:template>

<!--
//
//  Content template
//
//-->
<xsl:template match="content">
<h1><xsl:value-of select="/output/language/module/risks" /></h1>
<xsl:apply-templates select="overview" />
<xsl:apply-templates select="view" />
<xsl:apply-templates select="filter" />
<xsl:apply-templates select="edit_risk" />
<xsl:apply-templates select="edit_cause" />
<xsl:apply-templates select="edit_effect" />
<xsl:apply-templates select="assign_tags" />
<xsl:apply-templates select="set_acceptor" />
<xsl:apply-templates select="set_owner" />
<xsl:apply-templates select="result" />
</xsl:template>

</xsl:stylesheet>
