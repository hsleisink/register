<?xml version="1.0" ?>
<xsl:stylesheet version="1.1" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="banshee/main.xslt" />

<!--
//
//  Standards template
//
//-->
<xsl:template match="standards">
<form action="/{/output/page}" method="post">
<select name="standard_id" class="form-control standard" onChange="javascript:submit()">
<xsl:for-each select="standard">
<option value="{@id}"><xsl:if test="@id=../@selected"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if><xsl:value-of select="." /></option>
</xsl:for-each>
</select>
</form>
</xsl:template>

<!--
//
//  Controls template
//
//-->
<xsl:template match="controls">
<table class="table table-condensed controls">
<thead>
<tr>
<th><xsl:value-of select="/output/language/module/number" /></th>
<th><xsl:value-of select="/output/language/module/title" /></th>
<th><xsl:value-of select="/output/language/module/measures" /></th>
</tr>
</thead>
<tbody>
<xsl:for-each select="control">
<tr><xsl:if test="measures"><xsl:attribute name="class">click</xsl:attribute></xsl:if>
<td><xsl:value-of select="number" /></td>
<td><xsl:value-of select="title" /></td>
<td><xsl:value-of select="count(measures/measure[effective='yes'])" />&#160;<xsl:value-of select="/output/language/global/of" />&#160;<xsl:value-of select="count(measures/measure)" /></td>
</tr>
<xsl:if test="measures">
<tr class="measures"><td></td><td colspan="2">
<xsl:for-each select="measures/measure">
<xsl:sort select="title" />
<span class="measure"><a href="/measure/{type}/{@id}"><xsl:value-of select="title" /></a> (<xsl:value-of select="measure_type" />, <xsl:value-of select="status" />)</span>
</xsl:for-each>
</td></tr>
</xsl:if>
</xsl:for-each>
</tbody>
</table>

<div id="help">
<xsl:value-of select="/output/language/module/help_text" disable-output-escaping="yes" />
</div>
</xsl:template>

<!--
//
//  Content template
//
//-->
<xsl:template match="content">
<h1><xsl:value-of select="/output/language/module/controls" /></h1>
<xsl:apply-templates select="standards" />
<xsl:apply-templates select="controls" />
<xsl:apply-templates select="result" />
</xsl:template>

</xsl:stylesheet>
