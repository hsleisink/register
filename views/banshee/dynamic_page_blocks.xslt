<?xml version="1.0" ?>
<!--
//
//  Copyright (c) by Hugo Leisink <hugo@leisink.net>
//  This file is part of the Banshee PHP framework
//  https://www.banshee-php.org/
//
//  Licensed under The MIT License
//
//-->
<xsl:stylesheet version="1.1" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" doctype-system="about:legacy-compat" />

<!--
//
//  Approaches template
//
//-->
<xsl:template match="approaches">
<ul>
<xsl:for-each select="approach">
<li><b><xsl:value-of select="@label" /></b>: <xsl:value-of select="." /></li>
</xsl:for-each>
</ul>
</xsl:template>

<!--
//
//  Measure statuses template
//
//-->
<xsl:template match="measure_statuses">
<ul>
<xsl:for-each select="status">
<li><b><xsl:value-of select="@label" /></b>: <xsl:value-of select="." /></li>
</xsl:for-each>
</ul>
</xsl:template>

<!--
//
//  Issue statuses template
//
//-->
<xsl:template match="issue_statuses">
<ul>
<xsl:for-each select="status">
<li><b><xsl:value-of select="@label" /></b>: <xsl:value-of select="." /></li>
</xsl:for-each>
</ul>
</xsl:template>

</xsl:stylesheet>
