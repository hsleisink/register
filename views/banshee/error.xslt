<?xml version="1.0" ?>
<!--
//
//  Copyright (c) by Hugo Leisink <hugo@leisink.net>
//  This file is part of the Banshee PHP framework
//  https://www.banshee-php.org/
//
//  Licensed under The MIT License
//
//-->
<xsl:stylesheet version="1.1" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="main.xslt" />

<xsl:template match="content">
<h1><xsl:value-of select="/output/language/module/error" /></h1>
<img src="/images/error.png" alt="error" class="error" />
<p><xsl:apply-templates select="website_error" /></p>
<p><xsl:value-of select="/output/language/module/believe_bug_in_website" />&#160;<a href="mailto:{webmaster_email}"><xsl:value-of select="/output/language/module/webmaster" /></a>. <xsl:value-of select="/output/language/module/click" />&#160;<a href="/"><xsl:value-of select="/output/language/module/here" /></a>&#160;<xsl:value-of select="/output/language/module/return_to_homepage" /></p>
</xsl:template>

</xsl:stylesheet>
