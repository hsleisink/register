<?xml version="1.0" ?>
<!--
//
//  Copyright (c) by Hugo Leisink <hugo@leisink.net>
//  This file is part of the Banshee PHP framework
//  https://www.banshee-php.org/
//
//  Licensed under The MIT License
//
//-->
<xsl:stylesheet version="1.1" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="main.xslt" />

<!--
//
//  Login template
//
//-->
<xsl:template match="login">
<xsl:call-template name="show_messages" />
<div class="row">
<div class="col-sm-6">
<form id="login" action="{url}" method="post" autocomplete="off">
<div class="form-group">
<label for="username"><xsl:value-of select="/output/language/module/username" />:</label>
<input type="text" autocapitalize="off" autocorrect="off" id="username" name="username" value="{username}" class="form-control" />
</div>
<div class="form-group">
<label for="password"><xsl:value-of select="/output/language/module/password" />:</label>
<input type="password" id="password" name="password" class="form-control" />
</div>
<xsl:if test="@authenticator='yes'">
<div class="form-group">
<label for="code"><xsl:value-of select="/output/language/module/authenticator_code" />:</label> (<xsl:value-of select="/output/language/module/authenticator_explain" />)
<input type="text" id="code" name="code" class="form-control" />
</div>
</xsl:if>
<div class="form-group">
<xsl:value-of select="/output/language/module/bind_ip_address" /> (<xsl:value-of select="remote_addr" />): <input type="checkbox" name="bind_ip">
<xsl:if test="@bind_ip='yes'">
<xsl:attribute name="checked">checked</xsl:attribute>
</xsl:if>
</input>
</div>
<xsl:if test="postdata">
<div class="alert alert-danger">
<p><xsl:value-of select="/output/language/module/resend_1" /></p>
<p><xsl:value-of select="/output/language/module/resend_1" />: <input type="checkbox" name="repost" /></p>
</div>
<input type="hidden" name="postdata" value="{postdata}" />
</xsl:if>

<div class="btn-group">
<input type="submit" name="submit_button" value="{/output/language/module/btn_login}" class="btn btn-default" />
</div>
</form>

<xsl:if test="@password='yes'"><p><xsl:value-of select="/output/language/module/forgot_password" />&#160;<a href="/password"><xsl:value-of select="/output/language/module/here" /></a>.</p></xsl:if>
</div>
<div class="col-sm-6">
<xsl:if test="@register='yes' and /output/layout/@name='register'">
<h2><xsl:value-of select="/output/language/module/create_account" /></h2>
<p><xsl:value-of select="/output/language/module/click" />&#160;<a href="/register"><xsl:value-of select="/output/language/module/here" /></a>&#160;<xsl:value-of select="/output/language/module/new_account" /></p>
</xsl:if>

</div>
</div>

</xsl:template>

<!--
//
//  Content template
//
//-->
<xsl:template match="content">
<h1><xsl:value-of select="/output/language/module/login" /></h1>
<xsl:apply-templates select="login" />
<xsl:apply-templates select="result" />
</xsl:template>

</xsl:stylesheet>
