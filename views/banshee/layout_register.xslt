<?xml version="1.0" ?>
<!--
//
//  Copyright (c) by Hugo Leisink <hugo@leisink.net>
//  This file is part of the Banshee PHP framework
//  https://www.banshee-php.org/
//
//  Licensed under The MIT License
//
//-->
<xsl:stylesheet version="1.1" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:template match="layout[@name='register']">
<html lang="{language}">

<head>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
<meta name="author" content="AUTHOR" />
<meta name="publisher" content="PUBLISHER" />
<meta name="copyright" content="COPYRIGHT" />
<meta name="description" content="{description}" />
<meta name="keywords" content="{keywords}" />
<meta name="generator" content="Banshee PHP framework v{/output/banshee/version} (https://gitlab.com/hsleisink/banshee)" />
<meta property="og:site_name" content="{title}" />
<meta property="og:title" content="{title/@page}" />
<meta property="og:description" content="{description}" />
<meta property="og:image" content="{/output/page/@base}/images/risk_register.png" />
<meta property="og:url" content="{/output/page/@base}{/output/page/@url}" />
<xsl:if test="/output/banshee/session_timeout">
<meta http-equiv="refresh" content="{/output/banshee/session_timeout}; url=/logout" />
</xsl:if>
<link rel="apple-touch-icon" sizes="256x256" href="{/output/page/@base}/images/risk_register.png" />
<link rel="icon" href="/favicon.ico" />
<link rel="shortcut icon" href="/favicon.ico" />
<title><xsl:if test="title/@page!='' and title/@page!=title"><xsl:value-of select="title/@page" /> - </xsl:if><xsl:value-of select="title" /></title>
<xsl:for-each select="alternates/alternate">
<link rel="alternate" title="{.}" type="{@type}" href="{@url}" />
</xsl:for-each>
<xsl:for-each select="styles/style">
<link rel="stylesheet" type="text/css" href="{.}" />
</xsl:for-each>
<xsl:if test="inline_css">
<style type="text/css">
<xsl:value-of select="inline_css" />
</style>
</xsl:if>
<xsl:for-each select="javascripts/javascript">
<script type="text/javascript" src="{.}"></script><xsl:text>
</xsl:text></xsl:for-each>
</head>

<body>
<xsl:if test="javascripts/@onload">
	<xsl:attribute name="onLoad">javascript:<xsl:value-of select="javascripts/@onload" /></xsl:attribute>
</xsl:if>
<div class="wrapper">
	<div class="header">
		<div class="container">
			<div class="title"><xsl:value-of select="title" /></div>
		</div>
	</div>

	<nav class="navbar navbar-inverse">
		<div class="container">
			<div class="navbar-header">
				<xsl:if test="count(/output/menu/item)>0">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				</xsl:if>
			</div>

			<div id="navbar" class="collapse navbar-collapse">
				<ul class="nav navbar-nav">
				<xsl:for-each select="/output/menu/item">
					<xsl:if test="not(menu/item)">
						<li><a href="{link}" class="{class}"><xsl:value-of select="text" /></a></li>
					</xsl:if>
					<xsl:if test="menu/item">
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><xsl:value-of select="text" /> <span class="caret"></span></a>
						<ul class="dropdown-menu">
						<xsl:for-each select="menu/item">
						<li><a href="{link}" class="{class}"><xsl:value-of select="text" /></a></li>
						</xsl:for-each>
						</ul>
					</li>
					</xsl:if>
				</xsl:for-each>
				</ul>
			</div>
		</div>
	</nav>

	<div class="content">
		<div class="container">
			<xsl:apply-templates select="/output/system_warnings" />
			<xsl:apply-templates select="/output/system_messages" />
			<xsl:apply-templates select="/output/content" />
		</div>
	</div>

	<div class="footer">
		<div class="container">
			<span><xsl:value-of select="/output/layout/title" /> v<xsl:value-of select="/output/register/version" /></span>
			<span><a href="https://gitlab.com/hsleisink/register" target="_blank"><xsl:value-of select="/output/language/global/footer_source_code" /></a></span>
			<xsl:if test="/output/user">
			<span><xsl:value-of select="/output/language/global/footer_logged_in" />&#160;<a href="/account"><xsl:value-of select="/output/user" /></a></span>
			<span><a href="/session"><xsl:value-of select="/output/language/global/footer_session_manager" /></a></span>
			<span><a href="/cms">CMS</a></span>
			</xsl:if>
			<xsl:if test="count(/output/language/languages/language)>1">
			<span><form action="" method="post" class="language-selector" onChange="javascript:submit()"><select name="language">
			<xsl:for-each select="/output/language/languages/language">
			<xsl:sort select="." />
			<option value="{@code}"><xsl:if test="@code=../../@code"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if><xsl:value-of select="." /></option>
			</xsl:for-each>
			</select><input type="hidden" name="submit_button" value="select language" /></form></span>
			</xsl:if>
		</div>
	</div>

	<xsl:apply-templates select="/output/internal_errors" />
</div>
<xsl:apply-templates select="/output/language" />
</body>

</html>
</xsl:template>

</xsl:stylesheet>
