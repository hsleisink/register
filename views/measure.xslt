<?xml version="1.0" ?>
<xsl:stylesheet version="1.1" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="banshee/main.xslt" />
<xsl:import href="banshee/pagination.xslt" />

<!--
//
//  Overview template
//
//-->
<xsl:template match="overview">
<table class="table table-hover table-xs measures">
<thead class="table-xs">
<tr>
<th></th>
<th><xsl:value-of select="/output/language/module/measure" /></th>
<th><xsl:value-of select="/output/language/module/type" /></th>
<th><xsl:value-of select="/output/language/module/owner" /></th>
<th><xsl:value-of select="/output/language/module/deadline" /></th>
<th><xsl:value-of select="/output/language/module/status" /></th>
<th><xsl:value-of select="/output/language/module/risk" /></th>
</tr>
</thead>
<tbody>
<xsl:for-each select="measures/measure">
<xsl:variable name="urgency" select="urgency" />
<tr onClick="javascript:document.location='/{/output/page}/{type}/{@id}'">
<td><div class="levelbox" style="background-color:{../../levels/level[position()=$urgency]/@color}" title="Original risk" /></td>
<td><span class="table-xs"><xsl:value-of select="/output/language/module/measure" /></span><xsl:value-of select="title" /></td>
<td><span class="table-xs"><xsl:value-of select="/output/language/module/type" /></span><xsl:value-of select="measure_type" /></td>
<td><span class="table-xs"><xsl:value-of select="/output/language/module/owner" /></span><xsl:value-of select="owner" /></td>
<td><span class="table-xs"><xsl:value-of select="/output/language/module/deadline" /></span><xsl:value-of select="deadline" /></td>
<td><span class="table-xs"><xsl:value-of select="/output/language/module/status" /></span><xsl:value-of select="status" /></td>
<td class="identifier"><span class="table-xs"><xsl:value-of select="/output/language/module/risk" /></span>
<xsl:choose><xsl:when test="risk_viewable='yes'">
<a href="/risk/{risk_id}"><xsl:value-of select="identifier" /></a>
</xsl:when><xsl:otherwise>
<xsl:value-of select="identifier" />
</xsl:otherwise></xsl:choose>
</td>
</tr>
</xsl:for-each>
</tbody>
<xsl:if test="@subpage='yes'">
<tfoot>
<tr><td></td><td></td><td></td><td></td><td></td><td></td>
<td><xsl:value-of select="/output/language/global/total" />: <xsl:value-of select="count(measures/measure)" /></td></tr>
</tfoot>
</xsl:if>
</table>

<div class="right">
<xsl:apply-templates select="pagination" />
</div>

<div class="btn-group left">
<xsl:if test="@subpage='no'">
<a href="/{/output/page}/attention" class="btn btn-default"><xsl:value-of select="/output/language/module/btn_attention" /></a>
<a href="/{/output/page}/filter" class="btn btn-default"><xsl:value-of select="/output/language/module/btn_filter" /></a>
<xsl:if test="@calendar='yes'">
<a href="/{/output/page}/calendar" class="btn btn-default"><xsl:value-of select="/output/language/module/btn_view_calender" /></a>
</xsl:if>
</xsl:if>
<xsl:if test="@subpage='yes'">
<a href="/{/output/page}" class="btn btn-default"><xsl:value-of select="/output/language/global/btn_back" /></a>
</xsl:if>
</div>

<xsl:if test="../search='yes'">
<div class="btn-group search">
<form action="/{/output/page}" method="post">
<div class="input-group">
<input type="text" name="query" placeholder="{/output/language/module/search_query}" class="form-control" />
<span class="input-group-btn">
<input type="submit" name="submit_button" value="{/output/language/module/btn_search}" class="btn btn-default" />
</span>
</div>
</form>
</div>
</xsl:if>

<div id="help">
<xsl:value-of select="/output/language/module/help_overview_1" disable-output-escaping="yes" />
<xsl:value-of select="/output/language/global/help_measure_status" disable-output-escaping="yes" />
<p><xsl:value-of select="/output/language/module/help_overview_2" /></p>
</div>
</xsl:template>

<!--
//
//  Measure template
//
//-->
<xsl:template match="measure">
<div class="panel panel-primary">
<div class="panel-heading"><div class="type"><xsl:value-of select="measure_type" /></div><xsl:value-of select="title" /></div>
<div class="panel-body">
<xsl:if test="description!=''">
<div class="description"><xsl:value-of select="description" /></div>
<hr />
</xsl:if>
<xsl:if test="cause">
<p><span class="field"><xsl:value-of select="/output/language/module/cause" />:</span><span class="category"><xsl:value-of select="cause" /></span><xsl:value-of select="risk_cause" /></p>
</xsl:if>
<xsl:if test="effect">
<p><span class="field"><xsl:value-of select="/output/language/module/effect" />:</span><span class="category"><xsl:value-of select="effect" /></span><xsl:value-of select="risk_effect" /></p>
</xsl:if>
<div class="row">
<div class="col-sm-12 col-md-6">
<p><span class="field"><xsl:value-of select="/output/language/module/owner" />:</span><xsl:value-of select="owner" /></p>
<p><span class="field"><xsl:value-of select="/output/language/module/status" />:</span><xsl:value-of select="status" /></p>
<p><span class="field"><xsl:value-of select="reason" />&#160;<xsl:value-of select="/output/language/module/x_deadline" />:</span><xsl:value-of select="deadline" /><xsl:if test="deadline_passed='yes'"><img src="/images/warning.png" class="attention" /></xsl:if></p>
</div>
<div class="col-sm-12 col-md-6">
<hr class="mobile" />
<p><span class="field"><xsl:value-of select="/output/language/module/risk_identifier" />:</span><xsl:choose><xsl:when test="risk_viewable='yes'">
<a href="/risk/{risk_id}"><xsl:value-of select="identifier" /></a>
</xsl:when><xsl:otherwise>
<xsl:value-of select="identifier" />
</xsl:otherwise></xsl:choose></p>
<p><span class="field"><xsl:value-of select="/output/language/module/risk" />:</span><xsl:value-of select="risk" /></p>
<p><span class="field"><xsl:value-of select="/output/language/module/risk_owner" />:</span><xsl:value-of select="risk_owner" /></p>
</div>
</div>
<xsl:if test="../controls/standard">
<hr />
</xsl:if>
<xsl:for-each select="../controls/standard">
<h2><xsl:value-of select="/output/language/module/controls_from" />&#160;<xsl:value-of select="@name" /></h2>
<ul class="controls">
<xsl:for-each select="control">
<li><xsl:value-of select="number" /> - <xsl:value-of select="title" /></li>
</xsl:for-each>
</ul>
</xsl:for-each>
<xsl:if test="../issues">
<hr />
<h2><xsl:value-of select="/output/language/module/issues" /></h2>
<table class="table table-condensed table-hover issues">
<thead>
<tr>
<th><xsl:value-of select="/output/language/module/issue" /></th>
<th><xsl:value-of select="/output/language/module/created" /></th>
<th><xsl:value-of select="/output/language/module/status" /></th>
</tr>
</thead>
<tbody>
<xsl:for-each select="../issues/issue">
<xsl:variable name="status"><xsl:value-of select="status" /></xsl:variable>
<tr onClick="javascript:window.location='/issue/{type}/{@id}'">
<td><xsl:value-of select="title" /></td>
<td><xsl:value-of select="created" /></td>
<td><xsl:value-of select="../statuses/status[(position()-1)=$status]" /><xsl:if test="status!=(count(../statuses/status)-1)"><img src="/images/warning.png" class="attention" /></xsl:if></td>
</tr>
</xsl:for-each>
</tbody>
</table>
</xsl:if>
</div>
</div>

<div class="btn-group right">
<xsl:if test="editable='yes'">
<a href="/risk/measure/{type}/{@id}" class="btn btn-default"><xsl:value-of select="/output/language/module/btn_measure_edit" /></a>
</xsl:if>
<xsl:if test="issue='yes'">
<a href="/issue/{@id}/{type}" class="btn btn-default"><xsl:value-of select="/output/language/module/btn_create_issue" /></a>
</xsl:if>
</div>
<div class="btn-group left">
<a href="/measure" class="btn btn-default"><xsl:value-of select="/output/language/global/btn_back" /></a>
</div>

<xsl:if test="issue='yes'">
<div id="help">
<xsl:value-of select="/output/language/module/help_view_1" disable-output-escaping="yes" />
<ul>
<xsl:for-each select="../statuses/status">
<li><b><xsl:value-of select="." /></b>: <xsl:value-of select="@help" /></li>
</xsl:for-each>
</ul>
<xsl:value-of select="/output/language/module/help_view_2" disable-output-escaping="yes" />
</div>
</xsl:if>
</xsl:template>

<!--
//
//  Filter template
//
//-->
<xsl:template match="filter">
<form action="/{/output/page}" method="post">
<h2><xsl:value-of select="/output/language/module/type" /></h2>
<xsl:for-each select="types/type">
<div><input type="checkbox" name="type[]" value="{.}"><xsl:if test="@checked='yes'"><xsl:attribute name="checked">checked</xsl:attribute></xsl:if></input><xsl:value-of select="@label" /></div>
</xsl:for-each>

<h2><xsl:value-of select="/output/language/module/status" /></h2>
<xsl:for-each select="statuses/status">
<div><input type="checkbox" name="status[]" value="{position()-1}"><xsl:if test="@checked='yes'"><xsl:attribute name="checked">checked</xsl:attribute></xsl:if></input><xsl:value-of select="." /></div>
</xsl:for-each>

<xsl:if test="owners">
<h2><xsl:value-of select="/output/language/module/owner" /></h2>
<select name="owner" class="form-control owner">
<option value="0">-</option>
<xsl:for-each select="owners/owner">
<option value="{@id}"><xsl:if test="@id=../../owner"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if><xsl:value-of select="." /></option>
</xsl:for-each>
</select>
</xsl:if>

<div class="btn-group">
<input type="submit" name="submit_button" value="{/output/language/module/btn_set_filter}" class="btn btn-default" />
<button class="btn btn-default"><xsl:value-of select="/output/language/module/reset_filter" /></button>
<a href="/{/output/page}" class="btn btn-default"><xsl:value-of select="/output/language/global/btn_cancel" /></a>
</div>
</form>
</xsl:template>


<!--
//
//  Content template
//
//-->
<xsl:template match="content">
<h1><xsl:value-of select="/output/language/module/measures" /></h1>
<xsl:apply-templates select="overview" />
<xsl:apply-templates select="measure" />
<xsl:apply-templates select="filter" />
<xsl:apply-templates select="result" />
</xsl:template>

</xsl:stylesheet>
