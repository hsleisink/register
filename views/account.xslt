<?xml version="1.0" ?>
<!--
//
//  Copyright (c) by Hugo Leisink <hugo@leisink.net>
//  This file is part of the Banshee PHP framework
//  https://www.banshee-php.org/
//
//  Licensed under The MIT License
//
//-->
<xsl:stylesheet version="1.1" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="banshee/main.xslt" />

<!--
//
//  View template
//
//-->
<xsl:template match="view">
<div class="row">
<div class="col-lg-3 col-md-4 col-sm-5">
</div>
<div class="col-lg-9 col-md-8 col-sm-7">
<table class="table account">
<tbody>
<tr><td><xsl:value-of select="/output/language/module/name" />:</td><td><xsl:value-of select="user/fullname" /></td></tr>
<tr><td><xsl:value-of select="/output/language/module/email_address" />:</td><td><a href="mailto:{user/email}"><xsl:value-of select="user/email" /></a></td></tr>
</tbody>
</table>
</div>
</div>

<div class="btn-group">
<a href="/{previous}" class="btn btn-default"><xsl:value-of select="/output/language/global/btn_back" /></a>
</div>
</xsl:template>

<!--
//
//  Edit template
//
//-->
<xsl:template match="edit">
<xsl:call-template name="show_messages" />
<form action="/{/output/page}" method="post">
<div class="form-group">
<label for="fullname"><xsl:value-of select="/output/language/module/name" />:</label>
<input type="text" id="fullname" name="fullname" value="{fullname}" class="form-control" />
</div>
<div class="form-group">
<label for="email"><xsl:value-of select="/output/language/module/email_address" />:</label>
<input type="text" id="email" name="email" value="{email}" class="form-control" />
</div>
<div class="form-group">
<label><xsl:value-of select="/output/language/module/organisation" />:</label>
<input type="text" disabled="disabled" value="{organisation}" class="form-control" />
</div>
<div class="form-group">
<label for="current"><xsl:value-of select="/output/language/module/current_password" />:</label>
<input type="password" id="current" name="current" class="form-control" />
</div>
<div class="form-group">
<label for="password"><xsl:value-of select="/output/language/module/new_password" />:</label> <span class="blank" style="font-size:10px">(<xsl:value-of select="/output/language/module/when_left_blank" />)</span>
<input type="password" id="password" name="password" class="form-control" />
</div>
<div class="form-group">
<label for="repeat"><xsl:value-of select="/output/language/module/repeat_password" />:</label>
<input type="password" id="repeat" name="repeat" class="form-control" />
</div>
<xsl:if test="@authenticator='yes'">
<div class="form-group">
<div class="row">
<div class="col-xs-7">
<label for="secret"><xsl:value-of select="/output/language/module/authenticator" />:</label>
<div>
<div class="secret_set"><xsl:if test="authenticator_secret!=''"><xsl:value-of select="/output/language/module/authenticator_set" /></xsl:if></div>
<input type="hidden" id="secret" name="authenticator_secret" value="{authenticator_secret}" />
<div class="btn-group">
<input type="button" value="{/output/language/module/btn_generate}" class="btn btn-default" onClick="javascript:set_authenticator_code()" />
<input type="button" value="{/output/language/module/btn_remove}" class="btn btn-default" onClick="javascript:clear_authenticator_code()" />
</div>
</div>
</div>
<div class="col-xs-5 qrcode">
</div>
</div>
</div>
</xsl:if>

<div class="btn-group">
<input type="submit" name="submit_button" value="{/output/language/module/btn_update_account}" class="btn btn-default" />
<a href="/risk" class="btn btn-default"><xsl:value-of select="/output/language/global/btn_back" /></a>
</div>
<xsl:if test="/output/user/@admin='no'">
<div class="btn-group">
<input type="submit" name="submit_button" value="{/output/language/module/btn_delete_account}" class="btn btn-danger" onClick="javascript:return confirm('{/output/language/global/are_you_sure}')" />
</div>
</xsl:if>
</form>

<h2><xsl:value-of select="/output/language/module/recent_account_activity" /></h2>
<table class="table table-striped table-xs">
<thead>
<tr>
<th><xsl:value-of select="/output/language/module/ip_address" /></th>
<th><xsl:value-of select="/output/language/module/timestamp" /></th>
<th><xsl:value-of select="/output/language/module/path" /></th>
<th><xsl:value-of select="/output/language/module/activity" /></th>
</tr>
</thead>
<tbody>
<xsl:for-each select="actionlog/log">
<tr>
<td><xsl:value-of select="ip" /></td>
<td><xsl:value-of select="timestamp" /></td>
<td><xsl:value-of select="path" /></td>
<td><xsl:value-of select="message" /></td>
</tr>
</xsl:for-each>
</tbody>
</table>

<xsl:if test="@authenticator='yes'">
<div id="help">
<p><b><xsl:value-of select="/output/language/module/authenticator" />:</b>&#160;<xsl:value-of select="/output/language/module/authenticator_help" /></p>
</div>
</xsl:if>
</xsl:template>

<!--
//
//  Content template
//
//-->
<xsl:template match="content">
<img src="/images/icons/account.png" class="title_icon" />
<h1><xsl:value-of select="/output/layout/title/@page" /></h1>
<xsl:apply-templates select="view" />
<xsl:apply-templates select="edit" />
<xsl:apply-templates select="result" />
</xsl:template>

</xsl:stylesheet>
