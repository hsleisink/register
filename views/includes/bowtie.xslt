<?xml version="1.0" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="../banshee/main.xslt" />

<!--
//
//  Overview template
//
//-->
<xsl:template match="overview">
<table class="table table-condensed table-striped table-hover">
<thead>
<tr>
<th><xsl:value-of select="../item" /></th>
</tr>
</thead>
<tbody>
<xsl:for-each select="items/item">
<tr class="click" onClick="javascript:document.location='/{/output/page}/{@id}'">
<td><xsl:value-of select="*[name()=../../../../column]" /></td>
</tr>
</xsl:for-each>
</tbody>
</table>

<div class="btn-group left">
<a href="/{/output/page}/new" class="btn btn-default">New <xsl:value-of select="../item_lc" /></a>
<a href="/cms" class="btn btn-default">Back</a>
</div>
</xsl:template>

<!--
//
//  Edit template
//
//-->
<xsl:template match="edit">
<xsl:call-template name="show_messages" />
<form action="/{/output/page}" method="post">
<xsl:if test="item/@id">
<input type="hidden" name="id" value="{item/@id}" />
</xsl:if>

<label for="{column}"><xsl:value-of select="../item" />:</label>
<input type="text" id="{../column}" name="{../column}" value="{item/*[name()=../../../column]}" maxlength="{@maxlength}" class="form-control" />

<div class="btn-group">
<input type="submit" name="submit_button" value="Save {../item_lc}" class="btn btn-default" />
<a href="/{/output/page}" class="btn btn-default">Cancel</a>
<xsl:if test="item/@id">
<input type="submit" name="submit_button" value="Delete {../item_lc}" class="btn btn-default" onClick="javascript:return confirm('{/output/language/global/are_you_sure}')" />
</xsl:if>
</div>
</form>
</xsl:template>

<!--
//
//  Content template
//
//-->
<xsl:template match="content">
<img src="/images/icons/bowtie.png" class="title_icon" />
<h1><xsl:value-of select="item" /> administration</h1>
<xsl:apply-templates select="overview" />
<xsl:apply-templates select="edit" />
<xsl:apply-templates select="result" />
<xsl:apply-templates select="help" />
</xsl:template>

</xsl:stylesheet>
