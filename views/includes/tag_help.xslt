<?xml version="1.0" ?>
<!--
//
//  Copyright (c) by Hugo Leisink <hugo@leisink.net>
//  This file is part of the Banshee PHP framework
//  https://www.banshee-php.org/
//
//  Licensed under The MIT License
//
//-->
<xsl:stylesheet version="1.1" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<!--
//
//  Tag help template
//
//-->
<xsl:template name="tag_help">
<div id="help">
<p>Tags can be used to categorize and label risks. Tags can be made for anything, but consider at least the following categories and tags for your organisation:</p>
<ul>
<li>Intended objectives<ul>
	<li>(your organisation's intented objectives)</li></ul></li>
<li>Imposed objectives<ul>
	<li>(your organisation's imposed objectives)</li></ul></li>
<li>Actor<ul>
	<li>Activist / hacktivist</li>
	<li>Insider</li>
	<li>State actor</li>
	<li>Non-organized criminal</li>
	<li>Organized criminal organisation</li>
	<li>Script kiddie</li>
	<li>...</li></ul></li>
<li>Context<ul>
	<li>Financial</li>
	<li>Information</li>
	<li>Personnel</li>
	<li>Production</li>
	<li>...</li></ul></li>
<li>Information security<ul>
	<li>Availability</li>
	<li>Integrity</li>
	<li>Confidentiality</li></ul></li>
<li>Financial statement assertions<ul>
	<li>Accuracy and Valuation</li>
	<li>Existence</li>
	<li>Completeness</li>
	<li>Rights and Obligations</li>
	<li>Presentation and Disclosure</li></ul></li>
</ul>
<p>Of course, add those tags and tag categories in your own language.</p>
</div>
</xsl:template>

</xsl:stylesheet>
