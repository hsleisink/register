<?xml version="1.0" ?>
<!--
//
//  Copyright (c) by Hugo Leisink <hugo@leisink.net>
//  This file is part of the Banshee PHP framework
//  https://www.banshee-php.org/
//
//  Licensed under The MIT License
//
//-->
<xsl:stylesheet version="1.1" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="banshee/main.xslt" />

<!--
//
//  PHP extensions template
//
//-->
<xsl:template match="php_extensions">
<p>The following required PHP extensions are missing:</p>
<ul>
<xsl:for-each select="extension"><li><xsl:value-of select="." /></li></xsl:for-each>
</ul>
<p>Install and/or enable them and refresh this page.</p>

<div class="btn-group">
<a href="/{/output/page}" class="btn btn-default">Refresh</a>
</div>
</xsl:template>

<!--
//
//  Database settings template
//
//-->
<xsl:template match="db_settings">
<p>Enter your database settings in the file settings/banshee.conf and refresh this page.</p>
<p>If the specified database and database user do not exist, this setup will create them for you.</p>

<div class="btn-group">
<a href="/{/output/page}" class="btn btn-default">Refresh</a>
</div>
</xsl:template>

<!--
//
//  Create database template
//
//-->
<xsl:template match="create_db">
<xsl:call-template name="show_messages" />

<p>Enter the MySQL root credentials to create a database and a database user for your website as specified in settings/banshee.conf.</p>
<form action="/{/output/page}" method="post">
<label for="username">Username:</label>
<input type="text" id="username" name="username" value="{username}" class="form-control" autofocus="autofocus" />
<label for="password">Password:</label>
<input type="password" id="password" name="password" class="form-control" />

<div class="btn-group">
<input type="submit" name="submit_button" value="Create database" class="btn btn-default" />
<a href="/{/output/page}" class="btn btn-default">Proceed if you created them manually.</a>
</div>
</form>
</xsl:template>

<!--
//
//  Import database template
//
//-->
<xsl:template match="import_database">
<xsl:call-template name="show_messages" />

<p>The next step is to import the file database/mysql.sql into your database.</p>
<form action="/{/output/page}" method="post">
<input type="submit" name="submit_button" value="Import SQL" class="btn btn-default" />
</form>
</xsl:template>

<!--
//
//  Update database template
//
//-->
<xsl:template match="update_db">
<xsl:call-template name="show_messages" />

<p>Your database is outdated. Update your database to continue.</p>
<form action="/{/output/page}" method="post">
<input type="submit" name="submit_button" value="Update database" class="btn btn-default" />
</form>
</xsl:template>

<!--
//
//  Matrix template
//
//-->
<xsl:template match="matrix">
<xsl:call-template name="show_messages" />

<form action="/{/output/page}" method="post">
<label for="vertical">Vertical label risk matrix:</label>
<input type="text" id="vertical" name="matrix_vertical" value="{matrix_vertical}" class="form-control" />
<label for="horizontal">Horizontal label risk matrix:</label>
<input type="text" id="horizontal" name="matrix_horizontal" value="{matrix_horizontal}" class="form-control" />
<label for="email">Risk matrix size:</label>
<select name="matrix_size" class="form-control">
<xsl:for-each select="sizes/size">
<option value="{.}"><xsl:if test=".=../../matrix_size"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if><xsl:value-of select="." /> &#215; <xsl:value-of select="." /></option>
</xsl:for-each>
</select>
<label for="email">Amount of risk levels within the risk matrix:</label>
<select name="risk_levels" class="form-control">
<xsl:for-each select="sizes/size">
<option value="{.}"><xsl:if test=".=../../risk_levels"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if><xsl:value-of select="." /></option>
</xsl:for-each>
</select>

<div class="btn-group">
<input type="submit" name="submit_button" value="Define matrix" class="btn btn-default" />
</div>
</form>
</xsl:template>

<!--
//
//  Credentials template
//
//-->
<xsl:template match="credentials">
<xsl:call-template name="show_messages" />

<form action="/{/output/page}" method="post">
<label for="username">Enter the username for the administrator:</label>
<input type="username" id="username" name="username" value="{username}" class="form-control" />
<label for="password">Enter new password for this user:</label>
<input type="password" id="password" name="password" class="form-control" autofocus="autofocus" />
<label for="repeat">Repeat the password:</label>
<input type="password" id="repeat" name="repeat" class="form-control" />
<xsl:if test="encrypt_data='yes'">
<p style="margin-top:15px">Setting the password can take a few seconds, due to the crypto key generation that will also be done.</p>
</xsl:if>

<div class="btn-group">
<input type="submit" name="submit_button" value="Set password" class="btn btn-default" />
</div>
</form>
</xsl:template>

<!--
//
//  Done template
//
//-->
<xsl:template match="done">
<xsl:call-template name="show_messages" />

<p>Done! You can now login with your username and password.</p>
<p>The first thing you have to do is furter setup this risk register. To do so, login and click on the CMS link button in the bottom bar. Use all the available options in the Configuration section. Click on the Help buttons there for more information.</p>
<p>Don't forget to disable this setup module by removing it from settings/public_modules.conf.</p>

<div class="btn-group">
<a href="/" class="btn btn-default">Continue</a>
</div>
</xsl:template>

<!--
//
//  Denied template
//
//-->
<xsl:template match="denied">
<p>You are not allowed to use this module. Follow the instructions in the INSTALL file to gain access.</p>
</xsl:template>

<!--
//
//  Content template
//
//-->
<xsl:template match="content">
<h1>Risk Register setup</h1>
<xsl:apply-templates />
</xsl:template>

</xsl:stylesheet>
