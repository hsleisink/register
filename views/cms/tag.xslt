<?xml version="1.0" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="../banshee/main.xslt" />
<xsl:import href="../includes/tag_help.xslt" />

<!--
//
//  Overview template
//
//-->
<xsl:template match="overview">
<xsl:if test="count(categories/category)>0">
<form action="/{/output/page}" method="post" class="category">
<select id="category" name="category" class="form-control" onChange="javascript:submit()">
<xsl:for-each select="categories/category">
<option value="{@id}"><xsl:if test="@id=../@current"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if><xsl:value-of select="name" /></option>
</xsl:for-each>
</select>
<input type="hidden" name="submit_button" value="category" />
</form>
</xsl:if>

<table class="table table-condensed table-striped table-hover">
<thead>
<tr><th>Name</th></tr>
</thead>
<tbody>
<xsl:for-each select="tags/tag">
<tr class="click" onClick="javascript:document.location='/{/output/page}/{@id}'">
<td><xsl:value-of select="name" /></td>
</tr>
</xsl:for-each>
</tbody>
</table>

<div class="btn-group left">
<a href="/{/output/page}/new" class="btn btn-default">New tag</a>
<a href="/cms" class="btn btn-default">Back</a>
</div>
<div class="btn-group left">
<a href="/{/output/page}/category" class="btn btn-default">Tag categories</a>
</div>
<xsl:call-template name="tag_help" />
</xsl:template>

<!--
//
//  Edit template
//
//-->
<xsl:template match="edit">
<xsl:call-template name="show_messages" />
<form action="/{/output/page}" method="post">
<xsl:if test="tag/@id">
<input type="hidden" name="id" value="{tag/@id}" />
</xsl:if>

<div class="form-group">
<label for="name">Name:</label>
<input type="text" id="name" name="name" value="{tag/name}" class="form-control" />
</div>

<div class="btn-group">
<input type="submit" name="submit_button" value="Save tag" class="btn btn-default" />
<a href="/{/output/page}" class="btn btn-default">Cancel</a>
<xsl:if test="tag/@id">
<input type="submit" name="submit_button" value="Delete tag" class="btn btn-default" onClick="javascript:return confirm('DELETE: Are you sure?')" />
</xsl:if>
</div>
</form>
<xsl:call-template name="tag_help" />
</xsl:template>

<!--
//
//  Content template
//
//-->
<xsl:template match="content">
<img src="/images/icons/tag.png" class="title_icon" />
<h1>Tag administration</h1>
<xsl:apply-templates select="overview" />
<xsl:apply-templates select="edit" />
<xsl:apply-templates select="result" />
</xsl:template>

</xsl:stylesheet>
