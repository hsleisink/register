<?xml version="1.0" ?>
<xsl:stylesheet version="1.1" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="../banshee/main.xslt" />

<!--
//
//  Overview template
//
//-->
<xsl:template match="overview">
<xsl:call-template name="show_messages" />

<table class="table table-condensed advisors">
<thead>
<tr><th>Advisor</th><th>Organisation</th><th>Status</th><th></th></tr>
</thead>
<tbody>
<xsl:for-each select="advisor">
<tr>
<td><xsl:value-of select="fullname" /></td>
<td><xsl:value-of select="organisation" /></td>
<td><xsl:if test="@ready='no'"><form action="/{/output/page}" method="post"><input type="hidden" name="id" value="{@id}" /><input type="submit" name="submit_button" value="Accept request" class="btn btn-primary btn-xs" onClick="javascript:return confirm('ACCEPT: Are you sure?')" /></form></xsl:if><xsl:if test="@ready='yes'">Granted</xsl:if></td>
<td><form action="/{/output/page}" method="post"><input type="hidden" name="id" value="{@id}" /><input type="submit" name="submit_button" value="Delete" class="btn btn-danger btn-xs" onClick="javascript:return confirm('DELETE: Are you sure?')" /></form></td>
</tr>
</xsl:for-each>
</tbody>
</table>

<h2>Add advisor</h2>
<p>To add an advisor for your organization, enter the advisor's username or email address as used within this register.</p>
<form action="/{/output/page}" method="post" class="request">
<div class="input-group">
<input type="text" id="user" name="user" placeholder="Username or e-mail address" class="form-control datepicker" />
<span class="input-group-btn"><input type="submit" name="submit_button" value="Add advisor" class="btn btn-default" /></span>
</div>
</form>

<div class="btn-group">
<a href="/cms" class="btn btn-default">Back</a>
</div>

<div id="help">
<p>This register allows someone from another organization to act as an information security advisor. They do so by requesting access to your risk register. In this page you approve or reject requests and withdraw given permissions.</p>
</div>
</xsl:template>

<!--
//
//  Content template
//
//-->
<xsl:template match="content">
<img src="/images/icons/advisor.png" class="title_icon" />
<h1>Advisors</h1>
<xsl:apply-templates select="overview" />
<xsl:apply-templates select="result" />
</xsl:template>

</xsl:stylesheet>
