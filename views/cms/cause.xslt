<?xml version="1.0" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="../banshee/main.xslt" />
<xsl:import href="../includes/bowtie.xslt" />

<!--
//
//  Overview template
//
//-->
<xsl:template match="overview">
<table class="table table-condensed table-striped table-hover">
<thead>
<tr>
<th>Risk cause</th>
</tr>
</thead>
<tbody>
<xsl:for-each select="causes/cause">
<tr class="click" onClick="javascript:document.location='/{/output/page}/{@id}'">
<td><xsl:value-of select="cause" /></td>
</tr>
</xsl:for-each>
</tbody>
</table>

<div class="btn-group left">
<a href="/{/output/page}/new" class="btn btn-default">New risk cause</a>
<a href="/cms" class="btn btn-default">Back</a>
</div>

<div id="help">
<p>Here you define risk cause categories. Consider at least the following categories.</p>
<ul style="padding-left:15px">
<li>Human</li>
<li>Nature</li>
<li>Physical</li>
<li>Technology</li>
</ul>
</div>
</xsl:template>

<!--
//
//  Edit template
//
//-->
<xsl:template match="edit">
<xsl:call-template name="show_messages" />
<form action="/{/output/page}" method="post">
<xsl:if test="cause/@id">
<input type="hidden" name="id" value="{cause/@id}" />
</xsl:if>

<div class="form-group">
<label for="cause">Risk cause:</label>
<input type="text" id="cause" name="cause" value="{cause/cause}" maxlength="{@maxlength}" class="form-control" />
</div>

<div class="btn-group">
<input type="submit" name="submit_button" value="Save risk cause" class="btn btn-default" />
<a href="/{/output/page}" class="btn btn-default">Cancel</a>
<xsl:if test="cause/@id">
<input type="submit" name="submit_button" value="Delete risk cause" class="btn btn-default" onClick="javascript:return confirm('DELETE: Are you sure?')" />
</xsl:if>
</div>
</form>
</xsl:template>

<!--
//
//  Content template
//
//-->
<xsl:template match="content">
<img src="/images/icons/causes.png" class="title_icon" />
<h1>Risk cause administration</h1>
<xsl:apply-templates select="overview" />
<xsl:apply-templates select="edit" />
<xsl:apply-templates select="result" />
</xsl:template>

</xsl:stylesheet>
