<?xml version="1.0" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="../banshee/main.xslt" />
<xsl:import href="../banshee/pagination.xslt" />

<!--
//
//  Overview template
//
//-->
<xsl:template match="overview">
<div class="module">
<form action="/{/output/page}" method="post">
<input type="hidden" name="submit_button" value="module" />
<select name="module" class="form-control" onChange="javascript:submit()">
<xsl:for-each select="modules/module">
<option><xsl:if test=".=../@current"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if><xsl:value-of select="." /></option>
</xsl:for-each>
</select>
</form>
</div>

<table class="table table-condensed table-striped table-hover">
<thead>
<tr><th>Name</th><th>Text</th><th>Javascript</th></tr>
</thead>
<tbody>
<xsl:for-each select="translations/translation">
<tr class="click" onClick="javascript:document.location='/{/output/page}/{@id}'">
<td><xsl:value-of select="name" /></td>
<td><xsl:value-of select="text" /></td>
<td><xsl:value-of select="javascript" /></td>
</tr>
</xsl:for-each>
</tbody>
</table>

<div class="btn-group left">
<a href="/{/output/page}/new" class="btn btn-default">New translation</a>
<a href="/cms" class="btn btn-default">Back</a>
</div>
</xsl:template>

<!--
//
//  Edit template
//
//-->
<xsl:template match="edit">
<xsl:call-template name="show_messages" />
<form action="/{/output/page}" method="post">
<xsl:if test="translation/@id">
<input type="hidden" name="id" value="{translation/@id}" />
</xsl:if>

<div class="form-group">
<label for="module">Module:</label>
<select id="module" name="module" class="form-control">
<xsl:for-each select="modules/module">
<option><xsl:if test=".=../../translation/module"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if><xsl:value-of select="." /></option>
</xsl:for-each>
</select>
</div>

<div class="form-group">
<label for="name">Name:</label>
<input type="text" id="name" name="name" value="{translation/name}" class="form-control" />
</div>

<div class="form-group">
<b>Available for Javascripts:</b>
<input type="checkbox" name="javascript"><xsl:if test="translation/javascript='yes'"><xsl:attribute name="checked">checked</xsl:attribute></xsl:if></input>
</div>

<xsl:for-each select="languages/language">
<xsl:variable name="code" select="@code" />
<div class="form-group">
<label for="{@code}"><xsl:value-of select="." />:</label>
<textarea name="{@code}" class="form-control"><xsl:value-of select="../../translation/*[name()=$code]" /></textarea>
</div>
</xsl:for-each>

<div class="btn-group">
<input type="submit" name="submit_button" value="Save translation" class="btn btn-default" />
<a href="/{/output/page}" class="btn btn-default">Cancel</a>
<xsl:if test="translation/@id">
<input type="submit" name="submit_button" value="Delete translation" class="btn btn-default" onClick="javascript:return confirm('DELETE: Are you sure?')" />
</xsl:if>
</div>
</form>
</xsl:template>

<!--
//
//  Content template
//
//-->
<xsl:template match="content">
<img src="/images/icons/translations.png" class="title_icon" />
<h1>Translations</h1>
<xsl:apply-templates select="overview" />
<xsl:apply-templates select="edit" />
<xsl:apply-templates select="result" />
</xsl:template>

</xsl:stylesheet>
