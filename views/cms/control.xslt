<?xml version="1.0" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="../banshee/main.xslt" />

<!--
//
//  Overview template
//
//-->
<xsl:template match="overview">
<xsl:if test="count(standards/standard)>0">
<form action="/{/output/page}" method="post" class="standard">
<select id="standard" name="standard" class="form-control" onChange="javascript:submit()">
<xsl:for-each select="standards/standard">
<option value="{@id}"><xsl:if test="@id=../@current"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if><xsl:value-of select="name" /></option>
</xsl:for-each>
</select>
<input type="hidden" name="submit_button" value="standard" />
</form>
</xsl:if>

<table class="table table-condensed table-striped table-hover">
<thead>
<tr><th>Number</th><th>Title</th></tr>
</thead>
<tbody>
<xsl:for-each select="controls/control">
<tr class="click" onClick="javascript:document.location='/{/output/page}/{@id}'">
<td><xsl:value-of select="number" /></td>
<td><xsl:value-of select="title" /></td>
</tr>
</xsl:for-each>
</tbody>
</table>

<div class="btn-group left">
<a href="/{/output/page}/new" class="btn btn-default">New control</a>
<a href="/cms" class="btn btn-default">Back</a>
</div>
<div class="btn-group left">
<a href="/{/output/page}/standard" class="btn btn-default">Control standards</a>
</div>

<div id="help">
<p>Making changes to existing controls can cause issues when importing a risk register export. Only change existing controls when you know what you are doing!</p>
</div>
</xsl:template>

<!--
//
//  Edit template
//
//-->
<xsl:template match="edit">
<xsl:call-template name="show_messages" />
<form action="/{/output/page}" method="post">
<xsl:if test="control/@id">
<input type="hidden" name="id" value="{control/@id}" />
</xsl:if>

<label for="number">Number:</label>
<input type="text" id="number" name="number" value="{control/number}" maxlength="25" autofocus="autofocus" class="form-control" />
<label for="title">Title:</label>
<input type="text" id="title" name="title" value="{control/title}" class="form-control" />

<div class="btn-group">
<input type="submit" name="submit_button" value="Save control" class="btn btn-default" />
<xsl:if test="not(control/@id)">
<input type="submit" name="submit_button" value="Save control and new" class="btn btn-default" />
</xsl:if>
<a href="/{/output/page}" class="btn btn-default">Cancel</a>
<xsl:if test="control/@id">
<input type="submit" name="submit_button" value="Delete control" class="btn btn-default" onClick="javascript:return confirm('DELETE: Are you sure?')" />
</xsl:if>
</div>
</form>
</xsl:template>

<!--
//
//  Content template
//
//-->
<xsl:template match="content">
<img src="/images/icons/controls.png" class="title_icon" />
<h1>Control administration</h1>
<xsl:apply-templates select="overview" />
<xsl:apply-templates select="edit" />
<xsl:apply-templates select="result" />
</xsl:template>

</xsl:stylesheet>
