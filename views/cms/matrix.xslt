<?xml version="1.0" ?>
<xsl:stylesheet version="1.1" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="../banshee/main.xslt" />

<!--
//
//  Form template
//
//-->
<xsl:template match="form">
<xsl:call-template name="show_messages" />
<form action="/{/output/page}" method="post">
<table class="matrix">
<tbody>
<tr><td colspan="2"></td><td colspan="{matrix/@size}" class="horizontal"><xsl:value-of select="labels/horizontal/@label" /></td></tr>
<tr><td rowspan="{matrix/@size + 1}" class="vertical"><xsl:value-of select="labels/vertical/@label" /></td><td></td>
<xsl:for-each select="labels/horizontal/label">
<td class="mlabel"><xsl:value-of select="." /></td>
</xsl:for-each>
</tr>
<xsl:for-each select="matrix/row">
<xsl:variable name="vertical" select="position()" />
<tr><td class="mlabel"><xsl:value-of select="../../labels/vertical/label[position()=$vertical]" /></td>
<xsl:for-each select="cell"><td><select name="matrix[{$vertical}][column{position()}]" class="form-control">
<xsl:variable name="level" select="." />
<xsl:for-each select="../../../labels/levels/level">
<option value="{position()}" color="{@color}"><xsl:if test="position()=$level"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if><xsl:value-of select="." /></option>
</xsl:for-each>
</select></td></xsl:for-each>
</tr>
</xsl:for-each>
</tbody>
</table>

<div class="btn-group">
<input type="submit" name="submit_button" value="Save" class="btn btn-default" />
<a href="/cms" class="btn btn-default">Back</a>
</div>
</form>

<div id="help">
<p>Here you define the risk level for each <span class="lowercase"><xsl:value-of select="labels/vertical/@label" /></span> &#215; <span class="lowercase"><xsl:value-of select="labels/horizontal/@label" /></span> combination. The left lower corner must contain the lowest value and the right upper corner must contain the highest value. Between two neighbouring cells, there can be at most one level difference.</p>
<p>Once set, be careful about changing this matrix, as it will change the evaluation of the registered risks!</p>
</div>
</xsl:template>

<!--
//
//  Content template
//
//-->
<xsl:template match="content">
<img src="/images/icons/matrix.png" class="title_icon" />
<h1>Risk level administration</h1>
<xsl:apply-templates select="form" />
<xsl:apply-templates select="result" />
</xsl:template>

</xsl:stylesheet>
