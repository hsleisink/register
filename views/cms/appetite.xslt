<?xml version="1.0" ?>
<xsl:stylesheet version="1.1" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="../banshee/main.xslt" />

<!--
//
//  Form template
//
//-->
<xsl:template match="form">
<form action="/{/output/page}" method="post">
<div class="form-group">
<label for="appetite">Risk appetite:</label>
<select name="risk_appetite" class="form-control">
<xsl:for-each select="levels/level">
<option value="{position()}" color="{@color}"><xsl:if test="position()=../../appetite"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if><xsl:value-of select="." /></option>
</xsl:for-each>
</select>
</div>

<div class="btn-group">
<input type="submit" name="submit_button" value="Save" class="btn btn-default" />
<a href="/cms" class="btn btn-default">Back</a>
</div>
</form>

<div id="help">
<p>The risk appetite defines the risk level people are allowed to accept. Risks with a higher level can only be accepted with approval of someone with the appropriate authority. To give a user that authority, set the risk approve level in his/her account in the User administration section in the CMS.</p>
<p>Be careful about changing this setting, as it will change the evaluation of the registered risks!</p>
</div>
</xsl:template>

<!--
//
//  Content template
//
//-->
<xsl:template match="content">
<h1>Risk appetite administration</h1>
<xsl:apply-templates select="form" />
<xsl:apply-templates select="result" />
</xsl:template>

</xsl:stylesheet>
