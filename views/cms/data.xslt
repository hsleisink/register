<?xml version="1.0" ?>
<!--
//
//  Copyright (c) by Hugo Leisink <hugo@leisink.net>
//  Licensed under the RAFIS license.
//
//-->
<xsl:stylesheet version="1.1" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="../banshee/main.xslt" />

<!--
//
//  Overview template
//
//-->
<xsl:template match="overview">
<xsl:call-template name="show_messages" />
<div class="row">
<div class="col-sm-6">

<div class="panel panel-default">
<div class="panel-heading">Export data</div>
<div class="panel-body">
<form action="/{/output/page}" method="post">
<input type="submit" name="submit_button" value="Export" class="btn btn-default" />
</form>
</div>
</div>

</div>
<div class="col-sm-6">

<div class="panel panel-default">
<div class="panel-heading">Delete data</div>
<div class="panel-body">
<form action="/{/output/page}" method="post" enctype="multipart/form-data">
<div class="input-group">
<input type="text" name="confirmation" placeholder="To confirm, type 'Delete all data'." class="form-control" />
<span class="input-group-btn"><input type="submit" name="submit_button" value="Delete" class="btn btn-default" onClick="javascript:return confirm('DELETE: Are you sure?')" /></span>
</div>
</form>
</div>
</div>

</div>
<div class="col-sm-6">

<div class="panel panel-default">
<div class="panel-heading">Test data import</div>
<div class="panel-body">
<form action="/{/output/page}" method="post" enctype="multipart/form-data">
<div class="input-group">
<span class="input-group-btn"><label class="btn btn-default">
<input type="file" name="file" style="display:none" class="form-control" onChange="$('#test-file-info').val(this.files[0].name)" />Browse</label></span>
<input type="text" id="test-file-info" readonly="readonly" class="form-control" />
<span class="input-group-btn"><input type="submit" name="submit_button" value="Test" class="btn btn-default" /></span>
</div>
</form>
</div>
</div>
</div>
<div class="col-sm-6">

<div class="panel panel-default">
<div class="panel-heading">Import data</div>
<div class="panel-body">
<form action="/{/output/page}" method="post" enctype="multipart/form-data">
<div class="input-group">
<span class="input-group-btn"><label class="btn btn-default">
<input type="file" name="file" style="display:none" class="form-control" onChange="$('#upload-file-info').val(this.files[0].name)" />Browse</label></span>
<input type="text" id="upload-file-info" readonly="readonly" class="form-control" />
<span class="input-group-btn"><input type="submit" name="submit_button" value="Import" class="btn btn-default" onClick="javascript:return confirm('This action replaces all current data with the import. Continue?')" /></span>
</div>
</form>
</div>
</div>

</div>
<div class="col-sm-6">

</div>
</div>

<div class="btn-group">
<a href="/cms" class="btn btn-default">Back</a>
</div>

<div id="help">
<p>The export is a GZIP'd JSON file. The data inside that file is not encrypted, so keep it safe.</p>
<p>Before importing data, test it first to see if all needed user accounts are present. When you import data and a user account that is needed to assign a risk or a measure to is not present, your account will be used instead.</p>
</div>
</xsl:template>

<!--
//
//  Content template
//
//-->
<xsl:template match="content">
<img src="/images/icons/data.png" class="title_icon" />
<h1>Data management</h1>
<xsl:apply-templates select="overview" />
<xsl:apply-templates select="result" />
</xsl:template>

</xsl:stylesheet>
