<?xml version="1.0" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="../banshee/main.xslt" />

<!--
//
//  Overview template
//
//-->
<xsl:template match="overview">
<table class="table table-condensed table-striped table-hover">
<thead>
<tr>
<th>Risk effect</th>
</tr>
</thead>
<tbody>
<xsl:for-each select="effects/effect">
<tr class="click" onClick="javascript:document.location='/{/output/page}/{@id}'">
<td><xsl:value-of select="effect" /></td>
</tr>
</xsl:for-each>
</tbody>
</table>

<div class="btn-group left">
<a href="/{/output/page}/new" class="btn btn-default">New risk effect</a>
<a href="/cms" class="btn btn-default">Back</a>
</div>

<div id="help">
<p>Here you define risk effect categories. Consider at least the following categories.</p>
<ul style="padding-left:15px">
<li>Environmental</li>
<li>Financial</li>
<li>Legal</li>
<li>Physical safety</li>
<li>Politics</li>
<li>Production / service</li>
<li>Reputation</li>
<li>Society</li>
</ul>
<p>The impact descriptions of all effect categories will form the impact reference table.</p>
</div>
</xsl:template>

<!--
//
//  Edit template
//
//-->
<xsl:template match="edit">
<xsl:call-template name="show_messages" />
<form action="/{/output/page}" method="post">
<xsl:if test="effect/@id">
<input type="hidden" name="id" value="{effect/@id}" />
</xsl:if>

<div class="form-group">
<label for="effect">Risk effect:</label>
<input type="text" id="effect" name="effect" value="{effect/effect}" maxlength="{@maxlength}" class="form-control" />
</div>
<h2>Impact</h2>
<xsl:for-each select="effect/*[substring(name(), 1, 6)='impact']">
<xsl:variable name="level" select="position()" />
<div class="form-group">
<label for="{name()}">Impact <xsl:value-of select="../../impact/level[position()=$level]" />:</label>
<input type="text" id="{name()}" name="{name()}" value="{.}" class="form-control" />
</div>
</xsl:for-each>

<div class="btn-group">
<input type="submit" name="submit_button" value="Save risk effect" class="btn btn-default" />
<a href="/{/output/page}" class="btn btn-default">Cancel</a>
<xsl:if test="effect/@id">
<input type="submit" name="submit_button" value="Delete risk effect" class="btn btn-default" onClick="javascript:return confirm('DELETE: Are you sure?')" />
</xsl:if>
</div>
</form>

<div id="help">
<p><b>Impact &lt;level&gt;:</b> Specify for each impact level what an incident of that kind means in reality.</p>
</div>
</xsl:template>

<!--
//
//  Content template
//
//-->
<xsl:template match="content">
<img src="/images/icons/effects.png" class="title_icon" />
<h1>Risk effect administration</h1>
<xsl:apply-templates select="overview" />
<xsl:apply-templates select="edit" />
<xsl:apply-templates select="result" />
</xsl:template>

</xsl:stylesheet>
