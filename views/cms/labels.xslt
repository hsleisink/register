<?xml version="1.0" ?>
<xsl:stylesheet version="1.1" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="../banshee/main.xslt" />

<!--
//
//  Form template
//
//-->
<xsl:template match="form">
<xsl:call-template name="show_messages" />
<form action="/{/output/page}" method="post">

<div class="form-group">
<label for="impact_axis">Impact axis:</label>
<select id="impact_axis" name="impact_axis" class="form-control">
<option value="horizontal"><xsl:value-of select="horizontal/@label" /></option>
<option value="vertical"><xsl:if test="impact_axis='vertical'"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if><xsl:value-of select="vertical/@label" /></option>
</select>
</div>

<div class="row"><div class="col-xs-12 col-sm-6">

<div class="panel panel-default">
<div class="panel-heading">Labels for '<span class="lowercase"><xsl:value-of select="vertical/@label" /></span>'</div>
<div class="panel-body">
<xsl:for-each select="vertical/*">
<div class="form-group">
<label for="{name()}">Label <xsl:value-of select="position()" />:</label>
<input type="text" id="{name()}" name="vertical[{position()-1}]" value="{.}" maxlength="15" class="form-control" />
</div>
</xsl:for-each>
</div>
</div>

</div><div class="col-xs-12 col-sm-6">

<div class="panel panel-default">
<div class="panel-heading">Labels for '<span class="lowercase"><xsl:value-of select="horizontal/@label" /></span>'</div>
<div class="panel-body">
<xsl:for-each select="horizontal/*">
<div class="form-group">
<label for="{name()}">Label <xsl:value-of select="position()" />:</label>
<input type="text" id="{name()}" name="horizontal[{position()-1}]" value="{.}" maxlength="15" class="form-control" />
</div>
</xsl:for-each>
</div>
</div>

</div><div class="col-xs-12 col-sm-6">

<div class="panel panel-default">
<div class="panel-heading">Risk level labels</div>
<div class="panel-body">
<xsl:for-each select="level/*[substring(name(), 1, 5) = 'label']">
<div class="form-group">
<label for="impact1">Label <xsl:value-of select="position()" />:</label>
<input type="text" id="{name(.)}" name="level[label][{position()-1}]" value="{.}" maxlength="15" class="form-control" />
</div>
</xsl:for-each>
</div>
</div>

</div><div class="col-xs-12 col-sm-6">

<div class="panel panel-default colors">
<div class="panel-heading">Risk level colors</div>
<div class="panel-body">
<xsl:for-each select="level/*[substring(name(), 1, 5) = 'color']">
<div class="form-group">
<label for="impact1">Color <xsl:value-of select="position()" />:</label>
<input type="text" id="{name(.)}" name="level[color][{position()-1}]" value="{.}" maxlength="15" class="form-control color" />
</div>
</xsl:for-each>
</div>
</div>

</div></div>

<div class="btn-group">
<input type="submit" name="submit_button" value="Save" class="btn btn-default" />
<a href="/cms" class="btn btn-default">Back</a>
</div>
</form>

<div id="help">
<p>This is where you define your risk matrix. The labels for <span class="lowercase"><xsl:value-of select="vertical/@label" /></span> and <span class="lowercase"><xsl:value-of select="horizontal/@label" /></span> are the labels for the matrix axis. The risk level labels and colors are for the <span class="lowercase"><xsl:value-of select="vertical/@label" /></span> &#215; <span class="lowercase"><xsl:value-of select="horizontal/@label" /></span> combination.</p>
<p>Use the label with the lowest number for the lowest value and the label with the highest number for the highest value.</p>
<p><b>Impact axis:</b> The risk matrix can be defined completely freely, including what's on what axis and the language you use for it. However, for certain features this tool needs to know which axis is used for the impact.</p>
</div>
</xsl:template>

<!--
//
//  Content template
//
//-->
<xsl:template match="content">
<img src="/images/icons/labels.png" class="title_icon" />
<h1>Label administration</h1>
<xsl:apply-templates select="form" />
<xsl:apply-templates select="result" />
</xsl:template>

</xsl:stylesheet>
