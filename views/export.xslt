<?xml version="1.0" ?>
<xsl:stylesheet version="1.1" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="banshee/main.xslt" />

<!--
//
//  Form template
//
//-->
<xsl:template match="form">
<xsl:call-template name="show_messages" />
<form action="/{/output/page}" method="post">
<div class="form-group">
<label for="first"><xsl:value-of select="/output/language/module/from_date" />:</label>
<div class="input-group">
<input if="first" type="text" name="first" value="{first}" first="{@first}" class="form-control datepicker" />
<span class="input-group-btn"><button class="btn btn-default first" type="button"><xsl:value-of select="/output/language/module/from_first_risk" /></button></span>
</div>
</div>
<div class="form-group">
<label for="last"><xsl:value-of select="/output/language/module/to_date" />:</label>
<input id="last" type="text" name="last" value="{last}" class="form-control datepicker" />
</div>
<div class="form-group">
<input type="checkbox" name="description" />&#160;<xsl:value-of select="/output/language/module/include_descriptions" />
</div>

<div class="btn-group">
<input type="submit" name="submit_button" value="{/output/language/module/export_risks}" class="btn btn-default" />
<input type="submit" name="submit_button" value="{/output/language/module/export_tags}" class="btn btn-default" />
<input type="submit" name="submit_button" value="{/output/language/module/export_measures}" class="btn btn-default" />
<input type="submit" name="submit_button" value="{/output/language/module/export_issues}" class="btn btn-default" />
<input type="submit" name="submit_button" value="{/output/language/module/export_logs}" class="btn btn-default" />
</div>
</form>
</xsl:template>

<!--
//
//  Content template
//
//-->
<xsl:template match="content">
<h1>Export</h1>
<xsl:apply-templates select="form" />
<xsl:apply-templates select="result" />
</xsl:template>

</xsl:stylesheet>
