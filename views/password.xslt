<?xml version="1.0" ?>
<!--
//
//  Copyright (c) by Hugo Leisink <hugo@leisink.net>
//  This file is part of the Banshee PHP framework
//  https://www.banshee-php.org/
//
//  Licensed under The MIT License
//
//-->
<xsl:stylesheet version="1.1" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="banshee/main.xslt" />

<!--
//
//  request form template
//
//-->
<xsl:template match="request">
<xsl:value-of select="/output/language/module/enter_username_and_email" />
<form action="/{/output/page}" method="post">
<label for="username"><xsl:value-of select="/output/language/module/username" />:</label>
<input type="text" id="username" name="username" class="form-control" />
<label for="email"><xsl:value-of select="/output/language/module/email_address" />:</label>
<input type="text" id="email" name="email" class="form-control" />

<div class="btn-group">
<input type="submit" name="submit_button" value="{/output/language/module/btn_reset_password}" class="btn btn-default" />
<a href="/{@previous}" class="btn btn-default"><xsl:value-of select="/output/language/global/btn_cancel" /></a>
</div>
</form>
</xsl:template>

<!--
//
//  Link sent template
//
//-->
<xsl:template match="link_sent">
<xsl:value-of select="/output/language/module/link_sent" disable-output-escaping="yes" />
</xsl:template>

<!--
//
//  Reset form template
//
//-->
<xsl:template match="reset">
<p><xsl:value-of select="/output/language/module/enter_new_password" /></p>
<xsl:call-template name="show_messages" />
<form action="/{/output/page}" method="post">
<input type="hidden" name="key" value="{key}" />
<input type="hidden" id="username" value="{username}" />
<input type="hidden" id="password_hashed" name="password_hashed" value="no" />
<label for="password"><xsl:value-of select="/output/language/module/password" />:</label>
<input type="password" id="password" name="password" class="form-control" />
<label for="repeat"><xsl:value-of select="/output/language/module/repeat" />:</label>
<input type="password" id="repeat" name="repeat" class="form-control" />

<div class="btn-group">
<input type="submit" name="submit_button" value="{/output/language/module/btn_save_password}" class="btn btn-default" />
</div>
</form>
</xsl:template>

<!--
//
//  Crypto template
//
//-->
<xsl:template match="crypto">
<p><xsl:value-of select="/output/language/module/encryption_enabled" /></p>
<div class="btn-group">
<a href="/" class="btn btn-default"><xsl:value-of select="/output/language/global/btn_back" /></a>
</div>
</xsl:template>

<!--
//
//  Content template
//
//-->
<xsl:template match="content">
<h1><xsl:value-of select="/output/language/module/forgot_password" /></h1>
<xsl:apply-templates select="request" />
<xsl:apply-templates select="link_sent" />
<xsl:apply-templates select="reset" />
<xsl:apply-templates select="crypto" />
<xsl:apply-templates select="result" />
</xsl:template>

</xsl:stylesheet>
