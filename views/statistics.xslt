<?xml version="1.0" ?>
<xsl:stylesheet version="1.1" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="banshee/main.xslt" />

<!--
//
//  Statistics template
//
//-->
<xsl:template match="statistics">
<div class="right filter"><button class="btn btn-default btn-sm"><xsl:value-of select="/output/language/module/btn_show_filter" /></button></div>
<xsl:call-template name="show_messages" />

<div class="well">
<form action="/{/output/page}" method="post">
<div class="row">
<div class="col-sm-6">
<div class="form-group">
<label for="first"><xsl:value-of select="/output/language/module/from_this_date" />:</label>
<input type="text" name="first" value="{@first}" class="form-control datepicker" />
</div>
</div>
<div class="col-sm-6">
<div class="form-group">
<label for="last"><xsl:value-of select="/output/language/module/to_this_date" />:</label>
<input type="text" name="last" value="{@last}" class="form-control datepicker" />
</div>
</div>
</div>

<div class="btn-group">
<input type="submit" name="submit_button" value="{/output/language/module/btn_update}" class="btn btn-default" />
</div>
</form>
</div>

<div class="row">
<div class="col-xs-12 col-sm-6">
<p><b><xsl:value-of select="/output/language/module/risks" /></b>: <xsl:value-of select="risks" /></p>
</div>
<div class="col-xs-12 col-sm-6">
<table class="table table-condensed list">
<thead>
<tr><th><xsl:value-of select="/output/language/module/measures" /></th><th><xsl:value-of select="sum(measures/measure)" /></th></tr>
</thead>
<tbody>
<xsl:for-each select="measures/measure">
<tr>
<td><xsl:value-of select="@type" /></td>
<td><xsl:value-of select="." /></td>
</tr>
</xsl:for-each>
</tbody>
</table>
</div>
</div>

<h2><xsl:value-of select="/output/language/module/causes_and_effects" /></h2>
<div class="row">
<div class="col-xs-12 col-md-6">
<table class="table table-condensed table-hover list">
<thead>
<tr><th colspan="1"><xsl:value-of select="/output/language/module/causes" /></th><th><xsl:value-of select="sum(causes/cause/count)" /></th></tr>
</thead>
<tbody>
<xsl:for-each select="causes/cause">
<tr onClick="javascript:location='/{/output/page}/cause/{@id}'">
<td><xsl:value-of select="cause" /></td>
<td><xsl:value-of select="count" /></td>
</tr>
</xsl:for-each>
</tbody>
</table>
</div>
<div class="col-xs-12 col-md-6">
<table class="table table-condensed table-hover list">
<thead>
<tr><th colspan="1"><xsl:value-of select="/output/language/module/effects" /></th><th><xsl:value-of select="sum(effects/effect/count)" /></th></tr>
</thead>
<tbody>
<xsl:for-each select="effects/effect">
<tr onClick="javascript:location='/{/output/page}/effect/{@id}'">
<td><xsl:value-of select="effect" /></td>
<td><xsl:value-of select="count" /></td>
</tr>
</xsl:for-each>
</tbody>
</table>
</div>
</div>

<h2><xsl:value-of select="/output/language/module/tags" /></h2>
<div class="row">
<xsl:for-each select="categories/category">
<div class="col-xs-12 col-md-6 col-lg-4">
<div class="tags">
<table class="table table-condensed list">
<thead>
<tr><th colspan="2"><xsl:value-of select="@title" /></th><th><xsl:value-of select="sum(tag/count)" /></th></tr>
</thead>
<tbody>
<xsl:for-each select="tag">
<tr>
<td><xsl:value-of select="name" /></td>
<td><xsl:value-of select="percentage" />%</td>
<td><xsl:value-of select="count" /></td>
</tr>
</xsl:for-each>
</tbody>
</table>
</div>
</div>
</xsl:for-each>
</div>
</xsl:template>

<!--
//
//  Effects template
//
//-->
<xsl:template match="list">
<ul class="list-group">
<li class="list-group-item active"><span><xsl:value-of select="@type" /></span><xsl:value-of select="@title" /></li>
<xsl:for-each select="item">
<li class="list-group-item"><xsl:value-of select="." /></li>
</xsl:for-each>
</ul>

<div class="btn-group">
<a href="/{/output/page}" class="btn btn-default">Back</a>
</div>
</xsl:template>

<!--
//
//  Content template
//
//-->
<xsl:template match="content">
<h1><xsl:value-of select="/output/language/module/statistics" /></h1>
<xsl:apply-templates select="statistics" />
<xsl:apply-templates select="list" />
<xsl:apply-templates select="result" />
</xsl:template>

</xsl:stylesheet>
