<?xml version="1.0" ?>
<!--
//
//  Copyright (c) by Hugo Leisink <hugo@leisink.net>
//  This file is part of the Banshee PHP framework
//  https://www.banshee-php.org/
//
//  Licensed under The MIT License
//
//-->
<xsl:stylesheet version="1.1" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="banshee/main.xslt" />
<xsl:import href="banshee/splitform.xslt" />


<!--
//
//  Layout templates
//
<xsl:value-of select="/output/language/module/" />
//-->
<xsl:template name="splitform_header">
<h1><xsl:value-of select="/output/language/module/register" /></h1>
</xsl:template>

<xsl:template name="splitform_footer">
</xsl:template>

<!--
//
//  Invitation form template
//
//-->
<xsl:template match="splitform/invitation">
<p><xsl:value-of select="/output/language/module/received_invitation_code" /></p>
<label for="code"><xsl:value-of select="/output/language/module/invitation_code" />:</label>
<input type="text" name="invitation" value="{invitation}" class="form-control" />
</xsl:template>

<!--
//
//  E-mail form template
//
//-->
<xsl:template match="splitform/matrix">
<p><xsl:value-of select="/output/language/module/several_settings" /></p>
<div class="form-group">
<label for="horizontal"><xsl:value-of select="/output/language/module/horizontal_axis" />:</label>
<input type="text" id="horizontal" name="matrix_horizontal" value="{matrix_horizontal}" class="form-control" />
</div>
<div class="form-group">
<label for="vertical"><xsl:value-of select="/output/language/module/vertical_axis" />:</label>
<input type="text" id="vertical" name="matrix_vertical" value="{matrix_vertical}" class="form-control" />
</div>
<div class="form-group">
<label for="email"><xsl:value-of select="/output/language/module/risk_matrix_size" />:</label>
<select name="matrix_size" class="form-control">
<xsl:for-each select="sizes/size">
<option value="{.}"><xsl:if test=".=../../matrix_size"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if><xsl:value-of select="." /> &#215; <xsl:value-of select="." /></option>
</xsl:for-each>
</select>
</div>
<div class="form-group">
<label for="email"><xsl:value-of select="/output/language/module/risk_levels" />:</label>
<select name="risk_levels" class="form-control">
<xsl:for-each select="sizes/size">
<option value="{.}"><xsl:if test=".=../../risk_levels"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if><xsl:value-of select="." /></option>
</xsl:for-each>
</select>
</div>
<p><xsl:value-of select="/output/language/module/choose_correctly" /></p>
</xsl:template>

<!--
//
//  E-mail form template
//
//-->
<xsl:template match="splitform/email">
<label for="email"><xsl:value-of select="/output/language/module/email_address" />:</label>
<input type="input" id="email" name="email" value="{email}" class="form-control" />
</xsl:template>

<!--
//
//  Code form template
//
//-->
<xsl:template match="splitform/code">
<p><xsl:value-of select="/output/language/module/email_sent" /></p>
<label for="code"><xsl:value-of select="/output/language/module/verification_code" />:</label>
<input type="text" name="code" value="{code}" class="form-control" />
</xsl:template>

<!--
//
//  Account form template
//
//-->
<xsl:template match="splitform/account">
<label for="fullname"><xsl:value-of select="/output/language/module/fullname" />:</label>
<input type="text" id="fullname" name="fullname" value="{fullname}" maxlength="50" class="form-control" />
<label for="username"><xsl:value-of select="/output/language/module/username" />:</label>
<input type="text" id="username" name="username" value="{username}" maxlength="50" class="form-control" style="text-transform:lowercase" />
<label for="password"><xsl:value-of select="/output/language/module/password" />:</label>
<input type="password" id="password" name="password" class="form-control" />
<xsl:if test="ask_organisation='yes'">
<label for="organisation"><xsl:value-of select="/output/language/module/organisation" />:</label>
<input type="text" id="organisation" name="organisation" value="{organisation}" maxlength="50" class="form-control" />
</xsl:if>
</xsl:template>

<!--
//
//  Authenticator form template
//
//-->
<xsl:template match="splitform/authenticator">
<div class="form-group">
<div class="row">
<div class="col-xs-7">
<label for="secret"><xsl:value-of select="/output/language/module/authenticator" />:</label>
<div>
<div class="secret_set"><xsl:if test="authenticator_secret!=''"><xsl:value-of select="/output/language/module/authenticator_set" /></xsl:if></div>
<input type="hidden" id="secret" name="authenticator_secret" value="{authenticator_secret}" />
<div class="btn-group">
<input type="button" value="{/output/language/module/btn_generate}" class="btn btn-default" onClick="javascript:set_authenticator_code()" />
<input type="button" value="{/output/language/module/btn_remove}" class="btn btn-default" onClick="javascript:clear_authenticator_code()" />
</div>
</div>
</div>
<div class="col-xs-5 qrcode">
</div>
</div>
</div>

<div id="help">
<xsl:value-of select="/output/language/module/rfc_6238" disable-output-escaping="yes" />
</div>
</xsl:template>

<!--
//
//  Process template
//
//-->
<xsl:template match="submit">
<xsl:call-template name="splitform_header" />
<xsl:call-template name="progressbar" />
<p><xsl:value-of select="/output/language/module/account_created" /></p>
<xsl:call-template name="redirect"><xsl:with-param name="url" /></xsl:call-template>
</xsl:template>

</xsl:stylesheet>
