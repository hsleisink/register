<?php
	class register_email extends \Banshee\Protocol\email {
		protected $content_type = "text/html";
		private $footers = array();

		/* Constructor
		 *
		 * INPUT:  string subject[, string e-mail][, string name]
		 * OUTPUT: -
		 * ERROR:  -
		 */
		public function __construct($subject, $from_address = null, $from_name = null) {
			$this->add_footer('<a href="https://register.ravib.nl/">Risk Register website</a>');
			$this->add_footer("This e-mail is automatically generated by the Risk Register and is therefore unsigned.");

			if ($from_name == null) {
				$from_name = "Risk Register";
			}

			parent::__construct($subject, $from_address, $from_name);
		}

		/* Add e-mail footer
		 *
		 * INPUT:  string footer
		 * OUTPUT: -
		 * ERROR:  -
		 */
		public function add_footer($str) {
			array_push($this->footers, $str);
		}

		/* Set newsletter content
		 *
		 * INPUT:  string content
		 * OUTPUT: -
		 * ERROR:  -
		 */
		public function message($content) {
			$message = file_get_contents("../extra/register_email.txt");

			$footer = implode("<span style=\"margin:0 10px\">|</span>", $this->footers);
			$cid = $this->add_image("images/risk_register.png");

			$data = array(
				"TITLE"   => "Risk Register notification",
				"MESSAGE" => $content,
				"FOOTER"  => $footer,
				"LOGO"    => $cid);
			foreach ($data as $key => $value) {
				$message = str_replace("[".$key."]", $value, $message);
			}

			parent::message($message);
		}
	}
?>
