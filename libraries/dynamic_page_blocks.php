<?php
	/* Function arguments must always have a default value.
	 */

	class dynamic_page_blocks extends \Banshee\dynamic_blocks {
		protected function approaches() {
			foreach (RISK_APPROACHES as $approach => $description) {
				$this->view->add_tag("approach", $description, array("label" => $approach));
			}
		}

		protected function measure_statuses() {
			foreach (MEASURE_STATUSES as $status => $description) {
				$this->view->add_tag("status", $description, array("label" => $status));
			}
		}

		protected function issue_statuses() {
			foreach (ISSUE_STATUSES as $status => $description) {
				$this->view->add_tag("status", $description, array("label" => $status));
			}
		}

		private function matrix_label($axis) {
			static $labels = null;

			if ($this->user->logged_in == false) {
				return "?";
			}

			if ($labels == null) {
				$query = "select matrix_horizontal, matrix_vertical from organisations where id=%d";
				if (($result = $this->db->execute($query, $this->user->organisation_id)) == false) {
					return false;
				}

				$labels = $result[0];
			}

			return $labels["matrix_".$axis];
		}

		protected function matrix_label_horizontal() {
			return $this->matrix_label("horizontal");
		}

		protected function matrix_label_vertical() {
			return $this->matrix_label("vertical");
		}

		protected function measure_status_effective() {
			$measure_statuses = array_keys(MEASURE_STATUSES);

			return $measure_statuses[MEASURE_STATUS_EFFECTIVE];
		}

		protected function issue_status_closed() {
			$issue_statuses = array_keys(ISSUE_STATUSES);

			return $issue_statuses[ISSUE_STATUS_CLOSED];
		}
	}
?>
