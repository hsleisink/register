<?php
	abstract class register_controller extends Banshee\controller {
		protected function show_matrix_labels($axis, $axis_label, $labels) {
			$this->view->open_tag($axis, array("label" => $axis_label));

			foreach ($labels as $label) {
				$this->view->add_tag("label", $label);
			}

			$this->view->close_tag();
		}

		protected function show_risk_levels($levels) {
			$this->view->open_tag("levels");

			foreach ($risk_levels as $level) {
				$this->view->add_tag("level", $level["label"], array("color" => $level["color"]));
			}

			$this->view->close_tag();
		}

		protected function show_level_labels($labels, $add_level_none = false) {
			$this->view->open_tag("levels");

			if ($add_level_none) {
				$this->view->add_tag("level", "-", array("color" => "#ffffff"));
			}

			for ($l = 0; $l < $this->model->risk_levels; $l++) {
				$this->view->add_tag("level", $labels["label"][$l], array("color" => $labels["color"][$l]));
			}

			$this->view->close_tag();
		}

		protected function show_measure_statuses($check = false) {
			$this->view->open_tag("statuses");

			$measure_statuses = array_keys(MEASURE_STATUSES);

			foreach ($measure_statuses as $i => $status) {
				$attr = array("help" => MEASURE_STATUSES[$status]);

				if ($check) {
					$checked = in_array($i, $_SESSION["measure_filter"]["status"] ?? array());
					$attr["checked"] = show_boolean($checked);
				}

				$this->view->add_tag("status", $status, $attr);
			}

			$this->view->close_tag();
		}

		public function __construct() {
			$arguments = func_get_args();
			call_user_func_array(array(parent::class, "__construct"), $arguments);

			if (isset($_SESSION["advisor_organisation_id"])) {
				if (($organisation = $this->model->get_organisation($_SESSION["advisor_organisation_id"])) != false) {
					$this->view->add_system_warning("You now act as an advisor for %s.", $organisation);
				}
			}

			$this->view->add_help_button();
		}
	}
?>
