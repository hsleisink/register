<?php
	/* Only change these settings when you know what you are doing!
	 */
	define("REGISTER_VERSION", "1.0");

	define("UPDATE_TABLES", array("languages", "menu", "pages"));

	/* Crypto settings
	 */
	define("RSA_KEY_SIZE", 4096);
	define("CRYPTO_KEY_SIZE", 32);

	/* Matrix settings
	 */
	define("MATRIX_MIN_SIZE", 3);
	define("MATRIX_MAX_SIZE", 7);
	define("MATRIX_DEFAULT_HORIZONTAL", "Impact");
	define("MATRIX_DEFAULT_VERTICAL", "Probability");

	/* Maximum input lengths
	 */
	define("MAXLENGTH_BOWTIE_TITLE", 25);
	define("MAXLENGTH_RISK_IDENTIFIER", 15);
	define("MAXLENGTH_RISK_TITLE", 100);
	define("MAXLENGTH_CAUSE_EFFECT_TITLE", 50);
	define("MAXLENGTH_MEASURE_TITLE", 50);

	if (($_SESSION["language"] ?? null) == "nl") {
		/* Risk approaches
		 */
		define("RISK_APPROACHES", array(
			"bepaal later" => "Beslis later over de juiste risicoaanpak.",
			"accepteren"   => "Accepteer het risico. Dit kan goedkeuring vereisen van iemand met het juiste mandaat.",
			"ontwijken"    => "Verlaag de probability dat een risico leidt tot een incident.",
			"verweren"     => "Verlaag de impact van een incident dat het gevolg is van een risico.",
			"behandelen"   => "Verlaag zowel kans als impact van een incident als gevolg van een risico.",
			"overdragen"   => "Draag de (financiële) gevolgen van een incident over aan een andere entiteit.",
			"wegnemen"     => "Stop de activiteiten die leiden tot dit risico om dit risico volledig weg te nemen."));

		/* Measure statuses
		 */
		define("MEASURE_STATUSES", array(
			"ontwerp"         => "De maatregel zit in de ontwerpfase.",
			"geïmplementeerd" => "De maatregel is geïmplementeerd, maar de effectiviteit ervan is nog niet vastgesteld",
			"effectief"       => "De maatregel is geïmplementeerd en bewezen effectief.",
			"ineffectief"     => "De maatregel is geïmplementeerd, maar heeft (iets van) haar effectiviteit verloren."));

		/* Issue statuses
		 */
		define("ISSUE_STATUSES", array(
			"geïdentificeerd" => "Het issue is geïdentificeerd, maar moet nog geanalyseerd worden.",
			"geanalyseerd"    => "Het issue is geanalyseerd en taken om het af te handelen zijn gedefinieerd.",
			"goedgekeurd"     => "De taken om het issue af te handelen zijn goedgekeurd.",
			"uitgevoerd"      => "De taken om het issue af te handelen zijn uitgevoerd.",
			"afgehandeld"     => "Het isssue is afgehandeld."));

		define("DAYS_OF_WEEK", array("Maandag", "Dinsdag", "Woensdag", "Donderdag", "Vrijdag", "Zaturdag", "Zondag"));
		define("MONTHS_OF_YEAR", array("Januari", "Februari", "Maart", "April", "Mei", "Juni", "Juli", "Augustus", "September", "Oktober", "November", "December"));
	} else if (($_SESSION["language"] ?? null) == "de") {
		/* Risk approaches
		 */
		define("RISK_APPROACHES", array(
			"später entscheiden" => "Später über den richtigen Risikoansatz entscheiden.",
			"akzeptieren"        => "Akzeptieren Sie das Risiko. Dies erfordert möglicherweise die Genehmigung einer Person mit dem entsprechenden Mandat.",
			"vermeiden"          => "Reduzieren Sie die Wahrscheinlichkeit, dass ein Risiko zu einem Vorfall führt.",
			"widerstehen"        => "Reduzieren Sie die Auswirkungen eines Vorfalls, der aus einem Risiko resultiert.",
			"behandeln"          => "Reduzieren Sie sowohl die Wahrscheinlichkeit als auch die Auswirkungen eines Vorfalls als Folge eines Risikos.",
			"überweisen"         => "Die (finanziellen) Folgen eines Vorfalls auf eine andere Entität übertragen.",
			"entfernen"          => "Stoppen Sie die Aktivitäten, die zu diesem Risiko führen, um dieses Risiko vollständig zu beseitigen."));

		/* Measure statuses
		 */
		define("MEASURE_STATUSES", array(
			"entwurf"       => "Die Maßnahme befindet sich in der Entwurfsphase.",
			"implementiert" => "Die Maßnahme wurde umgesetzt, ihre Wirksamkeit ist jedoch noch nicht nachgewiesen",
			"wirksam"       => "Die Maßnahme wurde umgesetzt und hat sich als wirksam erwiesen.",
			"unwirksam"     => "Die Maßnahme wurde umgesetzt, hat jedoch (einen Teil) ihrer Wirksamkeit verloren."));

		/* Issue statuses
		 */
		define("ISSUE_STATUSES", array(
			"identifiziert" => "Das Problem wurde identifiziert, muss aber noch analysiert werden.",
			"analysiert"    => "Das Problem wurde analysiert und Aufgaben zu seiner Bearbeitung wurden definiert.",
			"genehmigt"     => "Die Aufgaben zur Bearbeitung des Problems wurden genehmigt.",
			"ausgeführt"    => "Die Aufgaben zur Behebung des Problems wurden abgeschlossen.",
			"ibehandelt"    => "Das Problem wurde behoben."));

		define("DAYS_OF_WEEK", array("Montag", "Dienstag", "Mittwoch", "Donnerstag", "Freitag", "Samstag", "Sonntag"));
		define("MONTHS_OF_YEAR", array("Januar", "Februar", "März", "April", "Mariy", "Junrie", "Julriy", "August", "September", "Oktober", "November", "Dezember"));
	} else if (($_SESSION["language"] ?? null) == "fr") {
		/* Risk approaches
		 */
		define("RISK_APPROACHES", array(
			"décider plus tard" => "Décider plus tard de l'approche de risque appropriée.",
			"accepter"          => "Accepter le risque. Cela peut nécessiter l'approbation d'une personne disposant de l'autorité appropriée.",
			"éviter"            => "Réduire la probabilité qu'un risque conduise à un incident.",
			"résister"          => "Réduire l'impact d'un incident en raison d'un risque.",
			"traiter"           => "Réduire à la fois la probabilité et l'impact d'un incident en raison d'un risque.",
			"transfert"         => "Transférer les conséquences (financières) d'un incident à une autre entité.",
			"retirer"           => "Arrêter les activités qui conduisent à ce risque pour éliminer complètement le risque."));

		/* Measure statuses
		 */
		define("MEASURE_STATUSES", array(
			"conception"   => "La mesure est en phase de conception.",
			"mis en œuvre" => "La mesure a été mise en œuvre, mais son efficacité reste à déterminer.",
			"efficace"     => "La mesure est mise en œuvre et s'est avérée efficace.",
			"inefficace"   => "La mesure est mise en œuvre, mais a perdu (une partie de) son efficacité."));

		/* Issue statuses
		 */
		define("ISSUE_STATUSES", array(
			"identifié" => "Le problème est identifié, mais doit encore être analysé.",
			"analysé"   => "Le problème a été analysé et les tâches pour traiter ce problème ont été définies.",
			"approuvé"  => "Les tâches pour traiter ce problème ont toutes été approuvées.",
			"exécuté"   => "Les tâches pour traiter ce problème ont toutes été exécutées.",
			"fermé"     => "Le traitement de ce problème est terminé."));

		define("DAYS_OF_WEEK", array("Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi", "Dimanche"));
		define("MONTHS_OF_YEAR", array("Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre"));
	} else {
		/* Risk approaches
		 */
		define("RISK_APPROACHES", array(
			"decide later" => "Decide later about the appropriate risk approach.",
			"accept"       => "Accept the risk. This may require approval from someone with the right authority.",
			"avoid"        => "Reduce the probability that a risk will lead to an incident.",
			"resist"       => "Reduce the impact of an incident as a result of a risk.",
			"treat"        => "Reduce both probability and impact of an incident as a result of a risk.",
			"transfer"     => "Transfer the (financial) consequences of an incident to another entity.",
			"remove"       => "Stop the activities that lead to this risk to remove the risk completely."));

		/* Measure statuses
		 */
		define("MEASURE_STATUSES", array(
			"design"       => "The measure is in the design phase.",
			"implemented"  => "The measure has been implemented, but its effectiveness is yet to be determined.",
			"effective"    => "The measure is implemented and is proven effective.",
			"ineffective"  => "The measure is implemented, but has lost (some of) its effectiveness."));

		/* Issue statuses
		 */
		define("ISSUE_STATUSES", array(
			"identified"   => "The issue is identified, but yet to be analyzed.",
			"analysed"     => "The issue has been analysed and tasks to deal with this issue have been defined.",
			"approved"     => "The tasks to deal with this issue have all been approved.",
			"executed"     => "The tasks to deal with this issue have all been executed.",
			"closed"       => "The handling of this issue is finished."));

		define("DAYS_OF_WEEK", array("Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"));
		define("MONTHS_OF_YEAR", array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"));
	}

	define("RISK_APPROACH_DECIDE_LATER", 0);
	define("RISK_APPROACH_ACCEPT", 1);

	define("MEASURE_STATUS_EFFECTIVE", 2);
	define("MEASURE_STATUS_INEFFECTIVE", 3);

	define("ISSUE_STATUS_CLOSED", 4);
?>
