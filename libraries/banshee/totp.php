<?php
	/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
	 * This file is part of the Banshee PHP framework
	 * https://gitlab.com/hsleisink/banshee/
	 *
	 * Licensed under The MIT License
	 */

	namespace Banshee;

	class TOTP {
		private $view = null;
		private $settings = null;

		/* Constructor
		 *
		 * INPUT:  object view, object settings
		 * OUTPUT: -
		 * ERROR:  -
		 */
		public function __construct($view, $settings) {
			$this->view = $view;
			$this->settings = $settings;
		}

		/* Graph to output
		 *
		 * INPUT:  string username, string secret
		 * OUTPUT: -
		 * ERROR:  -
		 */
		public function add_to_view($username, $secret) {
			$this->view->disable();

			$format = "otpauth://totp/%s?secret=%s&issuer=%s";
			$issuer = rawurlencode($this->settings->head_title);
			$data = sprintf($format, $username, $secret, $issuer);

			$options = array(
				"s"  => "qrl",
				"sf" => 3,
				"p"  => -10
			);

			$qr = new \QRCode($data, $options);
			$qr->output_image();
		}
	}
?>
