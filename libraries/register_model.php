<?php
	abstract class register_model extends Banshee\model {
		protected $matrix_horizontal = null;
		protected $matrix_vertical = null;
		protected $impact_axis = null;
		protected $label_probability = null;
		protected $label_impact = null;
		protected $matrix_size = null;
		protected $risk_levels = null;
		protected $risk_appetite = null;
        protected $organisation_id = null;

		public function __construct() {
			$arguments = func_get_args();
			call_user_func_array(array(parent::class, "__construct"), $arguments);

			$cookie = new \Banshee\secure_cookie($this->settings);
			if (isset($_SESSION["advisor_organisation_id"])) {
				if ($this->valid_advisor_id($_SESSION["advisor_organisation_id"])) {
					if (is_true(ENCRYPT_DATA)) {
						$this->aes = new \Banshee\Protocol\AES256($cookie->advisor_crypto_key);
					}
					$this->organisation_id = $_SESSION["advisor_organisation_id"];
				} else {
					$this->borrow("advisor")->deactivate_role();
				}
			}

			if (($this->organisation_id == null) && ($this->user != null)) {
				if (is_true(ENCRYPT_DATA)) {
					$this->aes = new \Banshee\Protocol\AES256($cookie->crypto_key);
				}
				$this->organisation_id = $this->user->organisation_id;
			}

			if (($organisation = $this->db->entry("organisations", $this->organisation_id)) == false) {
				return;
			}

			$this->matrix_horizontal = $organisation["matrix_horizontal"];
			$this->matrix_vertical = $organisation["matrix_vertical"];
			$this->impact_axis = $organisation["impact_axis"];
			$this->matrix_size = (int)$organisation["matrix_size"];
			$this->risk_levels = (int)$organisation["risk_levels"];
			$this->risk_appetite = (int)$organisation["risk_appetite"];

			$this->label_probability = ($this->impact_axis == "vertical") ? $this->matrix_horizontal : $this->matrix_vertical;
			$this->label_impact = ($this->impact_axis == "horizontal") ? $this->matrix_horizontal : $this->matrix_vertical;
		}

		public function __get($key) {
			switch ($key) {
				case "matrix_horizontal": return $this->matrix_horizontal;
				case "matrix_vertical": return $this->matrix_vertical;
				case "impact_axis": return $this->impact_axis;
				case "label_probability": return $this->label_probability;
				case "label_impact": return $this->label_impact;
				case "matrix_size": return $this->matrix_size;
				case "risk_levels": return $this->risk_levels;
				case "risk_appetite": return $this->risk_appetite;
				case "organisation_id": return $this->organisation_id;
			}

			return null;
		}

		private function valid_advisor_id($organisation_id) {
			$query = "select count(*) as count from advisors where user_id=%d and organisation_id=%d";
			if (($result = $this->db->execute($query, $this->user->id, $organisation_id)) == false) {
				return false;
			}

			return $result[0]["count"] > 0;
		}

		public function get_organisation($id) {
			if (($organisation = $this->db->entry("organisations", $id)) == false) {
				return false;
			}

			return $organisation["name"];
		}

		/* Label functions 
		 */
		private function get_matrix_labels($axis) {
			$query = "select * from %S where organisation_id=%d order by id";

			if (($result = $this->db->execute($query, "labels_".$axis, $this->organisation_id)) == false) {
				return false;
			}

			$labels = array();

			for ($l = 1; $l <= $this->matrix_size; $l++) {
				array_push($labels, $result[0]["label".$l]);
			}

			return $labels;
		}

		public function get_horizontal_labels() {
			return $this->get_matrix_labels("horizontal");
		}

		public function get_vertical_labels() {
			return $this->get_matrix_labels("vertical");
		}

		public function get_level_labels() {
			$query = "select * from labels_risk where organisation_id=%d order by id";

			if (($result = $this->db->execute($query, $this->organisation_id)) == false) {
				return false;
			}

			$levels = array(
				"label" => array(),
				"color" => array());

			for ($l = 1; $l <= $this->risk_levels; $l++) {
				array_push($levels["label"], $result[0]["label".$l]);
				array_push($levels["color"], $result[0]["color".$l]);
			}

			return $levels;
		}

		/* Matrix functions
		 */
		public function get_matrix() {
			static $matrix = null;

			if ($matrix === null) {
				$query = "select * from matrix where organisation_id=%d order by id desc";

				if (($result = $this->db->execute($query, $this->organisation_id)) == false) {
					return false;
				}

				$matrix = array();

				foreach ($result as $entry) {
					$impact = array();
					for ($c = 1; $c <= $this->matrix_size; $c++) {
						array_push($impact, $entry["column".$c]);
					}

					array_push($matrix, $impact);
				}
			}

			return $matrix;
		}

		public function level_from_matrix($matrix, $horizontal, $vertical) {
			return $matrix[count($matrix) - $vertical][$horizontal - 1];
		}

		/* Database query filters
		 */
		protected function query_level_filter($level, &$where, &$args, $and_up = false) {
			if (($matrix = $this->get_matrix()) == false) {
				return false;
			}

			$filter_where = array();
			$filter_args = array();

			for ($v = 1; $v <= $this->matrix_size; $v++) {
				for ($h = 1; $h <= $this->matrix_size; $h++) {
					$risk_level = $this->level_from_matrix($matrix, $h, $v);

					if (($risk_level == $level) and ($and_up == false)) {
						$add_filter = true;
					} else if (($risk_level >= $level) and $and_up) {
						$add_filter = true;
					} else {
						$add_filter = false;
					}

					if ($add_filter) {
						array_push($filter_where, "(horizontal=%d and vertical=%d)");
						array_push($filter_args, $h, $v);
					}
				}
			}

			if (count($filter_where) > 0) {
				array_push($where, "(".implode(" or ", $filter_where).")");
				array_push($args, $filter_args);
			}

			return true;
		}

		protected function query_risk_access(&$where, &$args) {
			if (in_array($this->settings->role_id_risk_manager, $this->user->role_ids)) {
				return;
			}

			if (in_array($this->settings->role_id_risk_viewer, $this->user->role_ids)) {
				return;
			}

			array_push($where, "((r.owner_id=%d) or (r.creator_id=%d) or (r.acceptor_id=%d))");
			array_push($args, $this->user->id, $this->user->id, $this->user->id);
		}

		public function may_edit_risk($risk_id) {
			$query = "select owner_id, creator_id from risks where id=%d and organisation_id=%d";
			if (($result = $this->db->execute($query, $risk_id, $this->organisation_id)) == false) {
				return false;
			}
			$risk = $result[0];

			$risk_owner = ($risk["owner_id"] == $this->user->id);
			$risk_creator = ($risk["owner_id"] == null) && ($risk["creator_id"] == $this->user->id);

			if (is_true(RISK_MANAGER_MAY_EDIT)) {
				$risk_manager = in_array($this->settings->role_id_risk_manager, $this->user->role_ids);
			} else {
				$risk_manager = false;
			}

			return $risk_owner || $risk_creator || $risk_manager;
		}

		public function query_measure_access(&$where, &$args) {
			if (in_array($this->settings->role_id_risk_manager, $this->user->role_ids)) {
				return;
			}

			if (in_array($this->settings->role_id_risk_viewer, $this->user->role_ids)) {
				return;
			}

			array_push($where, "(m.owner_id=%d or r.owner_id=%d or (r.owner_id is null and r.creator_id=%d))");
			array_push($args, $this->user->id, $this->user->id, $this->user->id);
		}

		public function may_edit_measure($measure_id, $type) {
			$query = "select risk_id, owner_id from measures where id=%d and type=%s";

			if (($result = $this->db->execute($query, $measure_id, $type)) == false) {
				return false;
			}
			$measure = $result[0];

			if (is_true(EDIT_OWN_MEASURE) && ($measure["owner_id"] == $this->user->id)) {
				return true;
			}

			if (is_true(RISK_MANAGER_MAY_EDIT)) {
				if (in_array($this->settings->role_id_risk_manager, $this->user->role_ids)) {
					return true;
				}
			}

			return $this->may_edit_risk($measure["risk_id"]);
		}

		/* User functions 
		 */
		public function get_user_fullname($user_id) {
			$query = "select fullname from users where id=%d and organisation_id=%d";

			if (($result = $this->db->execute($query, $user_id, $this->organisation_id)) == false) {
				return false;
			}

			return $result[0]["fullname"];
		}

		/* Log functions
		 */
		public function log_risk_event($risk_id, $event) {
			if (func_num_args() > 2) {
				$args = func_get_args();
				array_shift($args);
				array_shift($args);

				$args = array_flatten($args);

				$event = vsprintf($event, $args);
			}

			if (is_true(ENCRYPT_DATA)) {
				$event = $this->encrypt($event);
			}

			$data = array(
				"id"        => NULL,
				"user_id"   => $this->user->id,
				"risk_id"   => $risk_id,
				"timestamp" => date("Y-m-d H:i:s"),
				"event"     => $event);

			return $this->db->insert("risk_log", $data);
		}

		/* E-mail functions
		 */
		protected function cc_risk_managers($email, $to_email = null) {
			$query = "select email, fullname from users u, user_role r ".
			         "where u.id=r.user_id and r.role_id=%d and u.organisation_id=%d";

			if (($users = $this->db->execute($query, $this->settings->role_id_risk_manager, $this->organisation_id)) == false) {
				return;
			}

			foreach ($users as $user) {
				if ($user["email"] == $to_email) {
					continue;
				}

				$email->cc($user["email"], $user["fullname"]);
			}
		}
	}
?>
